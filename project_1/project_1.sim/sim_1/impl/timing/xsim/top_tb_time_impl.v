// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.1 (win64) Build 3247384 Thu Jun 10 19:36:33 MDT 2021
// Date        : Mon Dec 20 22:08:19 2021
// Host        : DESKTOP-02QC6R6 running 64-bit major release  (build 9200)
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               C:/Users/marti/Documents/4to/SED/TRABAJO/project_1/project_1.sim/sim_1/impl/timing/xsim/top_tb_time_impl.v
// Design      : TOP_Implementation
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module BCDCNTR
   (\count_reg[3]_0 ,
    Q,
    E,
    \count_reg[2]_0 ,
    \count_reg[1]_0 ,
    \count_reg[3]_1 ,
    \count_reg[1]_1 ,
    \count_reg[0]_0 ,
    \display_bus_OBUF[6]_inst_i_2 ,
    \display_bus_OBUF[2]_inst_i_4 ,
    \display_bus_OBUF[6]_inst_i_2_0 ,
    \display_bus_OBUF[2]_inst_i_4_0 ,
    \display_bus_OBUF[6]_inst_i_2_1 ,
    CLK,
    AR);
  output \count_reg[3]_0 ;
  output [1:0]Q;
  output [0:0]E;
  output \count_reg[2]_0 ;
  output \count_reg[1]_0 ;
  output \count_reg[3]_1 ;
  output \count_reg[1]_1 ;
  input [0:0]\count_reg[0]_0 ;
  input [2:0]\display_bus_OBUF[6]_inst_i_2 ;
  input \display_bus_OBUF[2]_inst_i_4 ;
  input [2:0]\display_bus_OBUF[6]_inst_i_2_0 ;
  input \display_bus_OBUF[2]_inst_i_4_0 ;
  input [2:0]\display_bus_OBUF[6]_inst_i_2_1 ;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire [1:0]Q;
  wire \count[0]_i_1_n_0 ;
  wire \count[1]_i_1_n_0 ;
  wire \count[2]_i_1_n_0 ;
  wire \count[3]_i_2_n_0 ;
  wire [0:0]\count_reg[0]_0 ;
  wire \count_reg[1]_0 ;
  wire \count_reg[1]_1 ;
  wire \count_reg[2]_0 ;
  wire \count_reg[3]_0 ;
  wire \count_reg[3]_1 ;
  wire \display_bus_OBUF[2]_inst_i_4 ;
  wire \display_bus_OBUF[2]_inst_i_4_0 ;
  wire [2:0]\display_bus_OBUF[6]_inst_i_2 ;
  wire [2:0]\display_bus_OBUF[6]_inst_i_2_0 ;
  wire [2:0]\display_bus_OBUF[6]_inst_i_2_1 ;
  wire [3:2]\tiempo[0]_0 ;

  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_current_state[1]_i_5 
       (.I0(\tiempo[0]_0 [3]),
        .I1(\tiempo[0]_0 [2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\count_reg[3]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(Q[0]),
        .O(\count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h29C2)) 
    \count[1]_i_1 
       (.I0(\tiempo[0]_0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\tiempo[0]_0 [3]),
        .O(\count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hD402)) 
    \count[2]_i_1 
       (.I0(\tiempo[0]_0 [3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\tiempo[0]_0 [2]),
        .O(\count[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000002)) 
    \count[3]_i_1__1 
       (.I0(\count_reg[0]_0 ),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\tiempo[0]_0 [2]),
        .I4(\tiempo[0]_0 [3]),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0601)) 
    \count[3]_i_2 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\tiempo[0]_0 [2]),
        .I3(\tiempo[0]_0 [3]),
        .O(\count[3]_i_2_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .D(\count[0]_i_1_n_0 ),
        .PRE(AR),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[1]_i_1_n_0 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[2]_i_1_n_0 ),
        .Q(\tiempo[0]_0 [2]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .D(\count[3]_i_2_n_0 ),
        .PRE(AR),
        .Q(\tiempo[0]_0 [3]));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \display_bus_OBUF[6]_inst_i_11 
       (.I0(\tiempo[0]_0 [2]),
        .I1(\display_bus_OBUF[6]_inst_i_2 [1]),
        .I2(\display_bus_OBUF[2]_inst_i_4 ),
        .I3(\display_bus_OBUF[6]_inst_i_2_0 [1]),
        .I4(\display_bus_OBUF[2]_inst_i_4_0 ),
        .I5(\display_bus_OBUF[6]_inst_i_2_1 [1]),
        .O(\count_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_bus_OBUF[6]_inst_i_13 
       (.I0(Q[1]),
        .I1(\display_bus_OBUF[6]_inst_i_2 [0]),
        .I2(\display_bus_OBUF[2]_inst_i_4 ),
        .I3(\display_bus_OBUF[6]_inst_i_2_0 [0]),
        .I4(\display_bus_OBUF[2]_inst_i_4_0 ),
        .I5(\display_bus_OBUF[6]_inst_i_2_1 [0]),
        .O(\count_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \display_bus_OBUF[6]_inst_i_15 
       (.I0(Q[1]),
        .I1(\display_bus_OBUF[6]_inst_i_2 [0]),
        .I2(\display_bus_OBUF[2]_inst_i_4 ),
        .I3(\display_bus_OBUF[6]_inst_i_2_0 [0]),
        .I4(\display_bus_OBUF[2]_inst_i_4_0 ),
        .I5(\display_bus_OBUF[6]_inst_i_2_1 [0]),
        .O(\count_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_bus_OBUF[6]_inst_i_9 
       (.I0(\tiempo[0]_0 [3]),
        .I1(\display_bus_OBUF[6]_inst_i_2 [2]),
        .I2(\display_bus_OBUF[2]_inst_i_4 ),
        .I3(\display_bus_OBUF[6]_inst_i_2_0 [2]),
        .I4(\display_bus_OBUF[2]_inst_i_4_0 ),
        .I5(\display_bus_OBUF[6]_inst_i_2_1 [2]),
        .O(\count_reg[3]_1 ));
endmodule

(* ORIG_REF_NAME = "BCDCNTR" *) 
module BCDCNTR_6
   (E,
    Q,
    \count_reg[2]_0 ,
    \count_reg[3]_0 ,
    CLK,
    AR);
  output [0:0]E;
  output [3:0]Q;
  output \count_reg[2]_0 ;
  input [0:0]\count_reg[3]_0 ;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire [3:0]Q;
  wire \count[0]_i_1__0_n_0 ;
  wire \count[1]_i_1__1_n_0 ;
  wire \count[2]_i_1__1_n_0 ;
  wire \count[3]_i_2__1_n_0 ;
  wire \count_reg[2]_0 ;
  wire [0:0]\count_reg[3]_0 ;

  (* \PinAttr:I0:HOLD_DETOUR  = "192" *) 
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1__0 
       (.I0(Q[0]),
        .O(\count[0]_i_1__0_n_0 ));
  (* \PinAttr:I3:HOLD_DETOUR  = "192" *) 
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h5886)) 
    \count[1]_i_1__1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\count[1]_i_1__1_n_0 ));
  (* \PinAttr:I2:HOLD_DETOUR  = "192" *) 
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h81A8)) 
    \count[2]_i_1__1 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[3]),
        .O(\count[2]_i_1__1_n_0 ));
  LUT5 #(
    .INIT(32'h00010000)) 
    \count[3]_i_1__0 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(\count_reg[3]_0 ),
        .O(E));
  (* \PinAttr:I2:HOLD_DETOUR  = "192" *) 
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0029)) 
    \count[3]_i_2__1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .O(\count[3]_i_2__1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \count[3]_i_3 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[3]),
        .O(\count_reg[2]_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(\count_reg[3]_0 ),
        .D(\count[0]_i_1__0_n_0 ),
        .PRE(AR),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(\count_reg[3]_0 ),
        .CLR(AR),
        .D(\count[1]_i_1__1_n_0 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(\count_reg[3]_0 ),
        .CLR(AR),
        .D(\count[2]_i_1__1_n_0 ),
        .Q(Q[2]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(\count_reg[3]_0 ),
        .D(\count[3]_i_2__1_n_0 ),
        .PRE(AR),
        .Q(Q[3]));
endmodule

(* ORIG_REF_NAME = "BCDCNTR" *) 
module BCDCNTR_7
   (\count_reg[3]_0 ,
    Q,
    \FSM_sequential_current_state_reg[0] ,
    \FSM_sequential_current_state_reg[0]_0 ,
    \FSM_sequential_current_state_reg[0]_1 ,
    done,
    E,
    CLK,
    AR);
  output \count_reg[3]_0 ;
  output [3:0]Q;
  input \FSM_sequential_current_state_reg[0] ;
  input \FSM_sequential_current_state_reg[0]_0 ;
  input \FSM_sequential_current_state_reg[0]_1 ;
  input done;
  input [0:0]E;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire \FSM_sequential_current_state[1]_i_4_n_0 ;
  wire \FSM_sequential_current_state_reg[0] ;
  wire \FSM_sequential_current_state_reg[0]_0 ;
  wire \FSM_sequential_current_state_reg[0]_1 ;
  wire [3:0]Q;
  wire \count[0]_i_1__2_n_0 ;
  wire \count[1]_i_1__0_n_0 ;
  wire \count[2]_i_1__0_n_0 ;
  wire \count[3]_i_2__0_n_0 ;
  wire \count_reg[3]_0 ;
  wire done;

  LUT6 #(
    .INIT(64'h00000000FFFBFFFF)) 
    \FSM_sequential_current_state[1]_i_3 
       (.I0(Q[3]),
        .I1(\FSM_sequential_current_state[1]_i_4_n_0 ),
        .I2(\FSM_sequential_current_state_reg[0] ),
        .I3(\FSM_sequential_current_state_reg[0]_0 ),
        .I4(\FSM_sequential_current_state_reg[0]_1 ),
        .I5(done),
        .O(\count_reg[3]_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \FSM_sequential_current_state[1]_i_4 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(\FSM_sequential_current_state[1]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1__2 
       (.I0(Q[0]),
        .O(\count[0]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h29C2)) 
    \count[1]_i_1__0 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[3]),
        .O(\count[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hD402)) 
    \count[2]_i_1__0 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .O(\count[2]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h0601)) 
    \count[3]_i_2__0 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(\count[3]_i_2__0_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(\count[0]_i_1__2_n_0 ),
        .PRE(AR),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(\count[1]_i_1__0_n_0 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(\count[2]_i_1__0_n_0 ),
        .Q(Q[2]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(\count[3]_i_2__0_n_0 ),
        .PRE(AR),
        .Q(Q[3]));
endmodule

(* ORIG_REF_NAME = "BCDCNTR" *) 
module BCDCNTR_8
   (Q,
    E,
    \count_reg[2]_0 ,
    \count_reg[3]_0 ,
    \count_reg[3]_1 ,
    \count_reg[3]_2 ,
    CLK,
    AR);
  output [3:0]Q;
  output [0:0]E;
  output \count_reg[2]_0 ;
  input [0:0]\count_reg[3]_0 ;
  input \count_reg[3]_1 ;
  input [0:0]\count_reg[3]_2 ;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire [3:0]Q;
  wire \count[0]_i_1__1_n_0 ;
  wire \count[1]_i_1__2_n_0 ;
  wire \count[2]_i_1__2_n_0 ;
  wire \count[3]_i_2__2_n_0 ;
  wire \count_reg[2]_0 ;
  wire [0:0]\count_reg[3]_0 ;
  wire \count_reg[3]_1 ;
  wire [0:0]\count_reg[3]_2 ;

  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_current_state[1]_i_6 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\count_reg[2]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1__1 
       (.I0(Q[0]),
        .O(\count[0]_i_1__1_n_0 ));
  (* \PinAttr:I3:HOLD_DETOUR  = "192" *) 
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h29C2)) 
    \count[1]_i_1__2 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[3]),
        .O(\count[1]_i_1__2_n_0 ));
  (* \PinAttr:I0:HOLD_DETOUR  = "192" *) 
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hD402)) 
    \count[2]_i_1__2 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(\count[2]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \count[3]_i_1 
       (.I0(\count_reg[3]_0 ),
        .I1(\count_reg[3]_1 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(E));
  LUT4 #(
    .INIT(16'h0061)) 
    \count[3]_i_2__2 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .O(\count[3]_i_2__2_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(\count_reg[3]_2 ),
        .D(\count[0]_i_1__1_n_0 ),
        .PRE(AR),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(\count_reg[3]_2 ),
        .CLR(AR),
        .D(\count[1]_i_1__2_n_0 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(\count_reg[3]_2 ),
        .CLR(AR),
        .D(\count[2]_i_1__2_n_0 ),
        .Q(Q[2]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(\count_reg[3]_2 ),
        .D(\count[3]_i_2__2_n_0 ),
        .PRE(AR),
        .Q(Q[3]));
endmodule

module EDGEDTCTR
   (\sreg_reg[3]_0 ,
    \sreg_reg[2]_0 ,
    E,
    CLK,
    D);
  output \sreg_reg[3]_0 ;
  output \sreg_reg[2]_0 ;
  input [0:0]E;
  input CLK;
  input [0:0]D;

  wire CLK;
  wire [0:0]D;
  wire [0:0]E;
  wire [4:0]sreg;
  wire \sreg_reg[2]_0 ;
  wire \sreg_reg[3]_0 ;

  LUT5 #(
    .INIT(32'h00000100)) 
    \FSM_sequential_current_state[0]_i_2 
       (.I0(sreg[2]),
        .I1(sreg[1]),
        .I2(sreg[0]),
        .I3(sreg[4]),
        .I4(sreg[3]),
        .O(\sreg_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    done_i_2
       (.I0(E),
        .I1(sreg[3]),
        .I2(sreg[4]),
        .I3(sreg[0]),
        .I4(sreg[1]),
        .I5(sreg[2]),
        .O(\sreg_reg[3]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(sreg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(sreg[0]),
        .Q(sreg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(sreg[1]),
        .Q(sreg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(sreg[2]),
        .Q(sreg[3]),
        .R(1'b0));
  (* \PinAttr:D:HOLD_DETOUR  = "192" *) 
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(sreg[3]),
        .Q(sreg[4]),
        .R(1'b0));
endmodule

module Flash_Visualizer
   (\aux_reg[7] ,
    CLK,
    \count_reg[24] );
  output [7:0]\aux_reg[7] ;
  input CLK;
  input \count_reg[24] ;

  wire CLK;
  wire Tim2OnOff;
  wire [7:0]\aux_reg[7] ;
  wire \count_reg[24] ;

  OnOff_4 OnOff_unit
       (.CLK(CLK),
        .E(Tim2OnOff),
        .\aux_reg[7]_0 (\aux_reg[7] ));
  TIMER_5 timer_unit
       (.CLK(CLK),
        .E(Tim2OnOff),
        .\count_reg[24]_0 (\count_reg[24] ));
endmodule

(* ORIG_REF_NAME = "Flash_Visualizer" *) 
module Flash_Visualizer_0
   (\aux_reg[7] ,
    CLK,
    \count_reg[24] );
  output [7:0]\aux_reg[7] ;
  input CLK;
  input \count_reg[24] ;

  wire CLK;
  wire Tim2OnOff;
  wire [7:0]\aux_reg[7] ;
  wire \count_reg[24] ;

  OnOff_2 OnOff_unit
       (.CLK(CLK),
        .E(Tim2OnOff),
        .\aux_reg[7]_0 (\aux_reg[7] ));
  TIMER_3 timer_unit
       (.CLK(CLK),
        .E(Tim2OnOff),
        .\count_reg[24]_0 (\count_reg[24] ));
endmodule

(* ORIG_REF_NAME = "Flash_Visualizer" *) 
module Flash_Visualizer_1
   (\aux_reg[7] ,
    CLK,
    \count_reg[24] );
  output [7:0]\aux_reg[7] ;
  input CLK;
  input \count_reg[24] ;

  wire CLK;
  wire Tim2OnOff;
  wire [7:0]\aux_reg[7] ;
  wire \count_reg[24] ;

  OnOff OnOff_unit
       (.CLK(CLK),
        .E(Tim2OnOff),
        .\aux_reg[7]_0 (\aux_reg[7] ));
  TIMER timer_unit
       (.CLK(CLK),
        .E(Tim2OnOff),
        .\count_reg[24]_0 (\count_reg[24] ));
endmodule

module NormalVisualizer
   (display_bus_OBUF,
    \FSM_onehot_current_state_reg[4] ,
    \FSM_onehot_current_state_reg[4]_0 ,
    Q,
    \sel_i_reg[4] ,
    \sel_i_reg[7] ,
    \sel_i_reg[4]_0 ,
    reset,
    \display_bus[4] ,
    \display_bus[2] ,
    \display_bus[1] ,
    \display_bus[2]_0 ,
    \display_bus[3] ,
    \display_bus[3]_0 ,
    \display_bus_OBUF[3]_inst_i_1 ,
    \display_bus_OBUF[0]_inst_i_1 ,
    \display_bus_OBUF[0]_inst_i_1_0 ,
    \display_bus_OBUF[0]_inst_i_1_1 ,
    \display_bus_OBUF[1]_inst_i_1 ,
    \display_bus_OBUF[4]_inst_i_2 ,
    \display_bus_OBUF[4]_inst_i_2_0 ,
    \display_bus_OBUF[4]_inst_i_2_1 ,
    \display_bus_OBUF[4]_inst_i_2_2 ,
    \display_bus_OBUF[5]_inst_i_4 ,
    \display_bus_OBUF[5]_inst_i_4_0 ,
    \display_bus_OBUF[6]_inst_i_1 ,
    \display_bus_OBUF[3]_inst_i_3 ,
    \display_bus_OBUF[3]_inst_i_3_0 ,
    reset_IBUF,
    CLK);
  output [4:0]display_bus_OBUF;
  output \FSM_onehot_current_state_reg[4] ;
  output \FSM_onehot_current_state_reg[4]_0 ;
  output [7:0]Q;
  output \sel_i_reg[4] ;
  output \sel_i_reg[7] ;
  output \sel_i_reg[4]_0 ;
  output reset;
  input \display_bus[4] ;
  input \display_bus[2] ;
  input \display_bus[1] ;
  input \display_bus[2]_0 ;
  input \display_bus[3] ;
  input \display_bus[3]_0 ;
  input \display_bus_OBUF[3]_inst_i_1 ;
  input \display_bus_OBUF[0]_inst_i_1 ;
  input \display_bus_OBUF[0]_inst_i_1_0 ;
  input \display_bus_OBUF[0]_inst_i_1_1 ;
  input \display_bus_OBUF[1]_inst_i_1 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  input [0:0]\display_bus_OBUF[5]_inst_i_4 ;
  input \display_bus_OBUF[5]_inst_i_4_0 ;
  input \display_bus_OBUF[6]_inst_i_1 ;
  input \display_bus_OBUF[3]_inst_i_3 ;
  input \display_bus_OBUF[3]_inst_i_3_0 ;
  input reset_IBUF;
  input CLK;

  wire CLK;
  wire \FSM_onehot_current_state_reg[4] ;
  wire \FSM_onehot_current_state_reg[4]_0 ;
  wire [7:0]Q;
  wire STROBE;
  wire \display_bus[1] ;
  wire \display_bus[2] ;
  wire \display_bus[2]_0 ;
  wire \display_bus[3] ;
  wire \display_bus[3]_0 ;
  wire \display_bus[4] ;
  wire [4:0]display_bus_OBUF;
  wire \display_bus_OBUF[0]_inst_i_1 ;
  wire \display_bus_OBUF[0]_inst_i_1_0 ;
  wire \display_bus_OBUF[0]_inst_i_1_1 ;
  wire \display_bus_OBUF[1]_inst_i_1 ;
  wire \display_bus_OBUF[3]_inst_i_1 ;
  wire \display_bus_OBUF[3]_inst_i_3 ;
  wire \display_bus_OBUF[3]_inst_i_3_0 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  wire [0:0]\display_bus_OBUF[5]_inst_i_4 ;
  wire \display_bus_OBUF[5]_inst_i_4_0 ;
  wire \display_bus_OBUF[6]_inst_i_1 ;
  wire reset;
  wire reset_IBUF;
  wire \sel_i_reg[4] ;
  wire \sel_i_reg[4]_0 ;
  wire \sel_i_reg[7] ;

  SCANNER scanner_unit
       (.CLK(CLK),
        .E(STROBE),
        .\FSM_onehot_current_state_reg[4] (\FSM_onehot_current_state_reg[4] ),
        .\FSM_onehot_current_state_reg[4]_0 (\FSM_onehot_current_state_reg[4]_0 ),
        .Q(Q),
        .\display_bus[1] (\display_bus[1] ),
        .\display_bus[2] (\display_bus[2] ),
        .\display_bus[2]_0 (\display_bus[2]_0 ),
        .\display_bus[3] (\display_bus[3] ),
        .\display_bus[3]_0 (\display_bus[3]_0 ),
        .\display_bus[4] (\display_bus[4] ),
        .display_bus_OBUF(display_bus_OBUF),
        .\display_bus_OBUF[0]_inst_i_1_0 (\display_bus_OBUF[0]_inst_i_1 ),
        .\display_bus_OBUF[0]_inst_i_1_1 (\display_bus_OBUF[0]_inst_i_1_0 ),
        .\display_bus_OBUF[0]_inst_i_1_2 (\display_bus_OBUF[0]_inst_i_1_1 ),
        .\display_bus_OBUF[1]_inst_i_1_0 (\display_bus_OBUF[1]_inst_i_1 ),
        .\display_bus_OBUF[3]_inst_i_1_0 (\display_bus_OBUF[3]_inst_i_1 ),
        .\display_bus_OBUF[3]_inst_i_3_0 (\display_bus_OBUF[3]_inst_i_3 ),
        .\display_bus_OBUF[3]_inst_i_3_1 (\display_bus_OBUF[3]_inst_i_3_0 ),
        .\display_bus_OBUF[4]_inst_i_2_0 (\display_bus_OBUF[4]_inst_i_2 ),
        .\display_bus_OBUF[4]_inst_i_2_1 (\display_bus_OBUF[4]_inst_i_2_0 ),
        .\display_bus_OBUF[4]_inst_i_2_2 (\display_bus_OBUF[4]_inst_i_2_1 ),
        .\display_bus_OBUF[4]_inst_i_2_3 (\display_bus_OBUF[4]_inst_i_2_2 ),
        .\display_bus_OBUF[5]_inst_i_4_0 (\display_bus_OBUF[5]_inst_i_4 ),
        .\display_bus_OBUF[5]_inst_i_4_1 (\display_bus_OBUF[5]_inst_i_4_0 ),
        .\display_bus_OBUF[6]_inst_i_1 (\display_bus_OBUF[6]_inst_i_1 ),
        .\sel_i_reg[0]_0 (reset),
        .\sel_i_reg[4]_0 (\sel_i_reg[4] ),
        .\sel_i_reg[4]_1 (\sel_i_reg[4]_0 ),
        .\sel_i_reg[7]_0 (\sel_i_reg[7] ));
  TIMER__parameterized1 timer_unit
       (.CLK(CLK),
        .E(STROBE),
        .reset(reset),
        .reset_IBUF(reset_IBUF));
endmodule

module OnOff
   (\aux_reg[7]_0 ,
    E,
    CLK);
  output [7:0]\aux_reg[7]_0 ;
  input [0:0]E;
  input CLK;

  wire CLK;
  wire [0:0]E;
  wire [7:0]\aux_reg[7]_0 ;
  wire [7:0]p_0_in;

  LUT1 #(
    .INIT(2'h1)) 
    \aux[0]_i_1 
       (.I0(\aux_reg[7]_0 [0]),
        .O(p_0_in[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[1]_i_1 
       (.I0(\aux_reg[7]_0 [1]),
        .O(p_0_in[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[2]_i_1 
       (.I0(\aux_reg[7]_0 [2]),
        .O(p_0_in[2]));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[3]_i_1 
       (.I0(\aux_reg[7]_0 [3]),
        .O(p_0_in[3]));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[4]_i_1 
       (.I0(\aux_reg[7]_0 [4]),
        .O(p_0_in[4]));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[5]_i_1 
       (.I0(\aux_reg[7]_0 [5]),
        .O(p_0_in[5]));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[6]_i_1 
       (.I0(\aux_reg[7]_0 [6]),
        .O(p_0_in[6]));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[7]_i_2 
       (.I0(\aux_reg[7]_0 [7]),
        .O(p_0_in[7]));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[0]),
        .Q(\aux_reg[7]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[1]),
        .Q(\aux_reg[7]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[2]),
        .Q(\aux_reg[7]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[3]),
        .Q(\aux_reg[7]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[4]),
        .Q(\aux_reg[7]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[5]),
        .Q(\aux_reg[7]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[6]),
        .Q(\aux_reg[7]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(p_0_in[7]),
        .Q(\aux_reg[7]_0 [7]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "OnOff" *) 
module OnOff_2
   (\aux_reg[7]_0 ,
    E,
    CLK);
  output [7:0]\aux_reg[7]_0 ;
  input [0:0]E;
  input CLK;

  wire CLK;
  wire [0:0]E;
  wire \aux[0]_i_1__0_n_0 ;
  wire \aux[1]_i_1__0_n_0 ;
  wire \aux[2]_i_1__0_n_0 ;
  wire \aux[3]_i_1__0_n_0 ;
  wire \aux[4]_i_1__0_n_0 ;
  wire \aux[5]_i_1__0_n_0 ;
  wire \aux[6]_i_1__0_n_0 ;
  wire \aux[7]_i_2__0_n_0 ;
  wire [7:0]\aux_reg[7]_0 ;

  LUT1 #(
    .INIT(2'h1)) 
    \aux[0]_i_1__0 
       (.I0(\aux_reg[7]_0 [0]),
        .O(\aux[0]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[1]_i_1__0 
       (.I0(\aux_reg[7]_0 [1]),
        .O(\aux[1]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[2]_i_1__0 
       (.I0(\aux_reg[7]_0 [2]),
        .O(\aux[2]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[3]_i_1__0 
       (.I0(\aux_reg[7]_0 [3]),
        .O(\aux[3]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[4]_i_1__0 
       (.I0(\aux_reg[7]_0 [4]),
        .O(\aux[4]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[5]_i_1__0 
       (.I0(\aux_reg[7]_0 [5]),
        .O(\aux[5]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[6]_i_1__0 
       (.I0(\aux_reg[7]_0 [6]),
        .O(\aux[6]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[7]_i_2__0 
       (.I0(\aux_reg[7]_0 [7]),
        .O(\aux[7]_i_2__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(\aux[0]_i_1__0_n_0 ),
        .Q(\aux_reg[7]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(\aux[1]_i_1__0_n_0 ),
        .Q(\aux_reg[7]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(\aux[2]_i_1__0_n_0 ),
        .Q(\aux_reg[7]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(\aux[3]_i_1__0_n_0 ),
        .Q(\aux_reg[7]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(\aux[4]_i_1__0_n_0 ),
        .Q(\aux_reg[7]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(\aux[5]_i_1__0_n_0 ),
        .Q(\aux_reg[7]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(\aux[6]_i_1__0_n_0 ),
        .Q(\aux_reg[7]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(\aux[7]_i_2__0_n_0 ),
        .Q(\aux_reg[7]_0 [7]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "OnOff" *) 
module OnOff_4
   (\aux_reg[7]_0 ,
    E,
    CLK);
  output [7:0]\aux_reg[7]_0 ;
  input [0:0]E;
  input CLK;

  wire CLK;
  wire [0:0]E;
  wire \aux[0]_i_1__1_n_0 ;
  wire \aux[1]_i_1__1_n_0 ;
  wire \aux[2]_i_1__1_n_0 ;
  wire \aux[3]_i_1__1_n_0 ;
  wire \aux[4]_i_1__1_n_0 ;
  wire \aux[5]_i_1__1_n_0 ;
  wire \aux[6]_i_1__1_n_0 ;
  wire \aux[7]_i_2__1_n_0 ;
  wire [7:0]\aux_reg[7]_0 ;

  LUT1 #(
    .INIT(2'h1)) 
    \aux[0]_i_1__1 
       (.I0(\aux_reg[7]_0 [0]),
        .O(\aux[0]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[1]_i_1__1 
       (.I0(\aux_reg[7]_0 [1]),
        .O(\aux[1]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[2]_i_1__1 
       (.I0(\aux_reg[7]_0 [2]),
        .O(\aux[2]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[3]_i_1__1 
       (.I0(\aux_reg[7]_0 [3]),
        .O(\aux[3]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[4]_i_1__1 
       (.I0(\aux_reg[7]_0 [4]),
        .O(\aux[4]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[5]_i_1__1 
       (.I0(\aux_reg[7]_0 [5]),
        .O(\aux[5]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[6]_i_1__1 
       (.I0(\aux_reg[7]_0 [6]),
        .O(\aux[6]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \aux[7]_i_2__1 
       (.I0(\aux_reg[7]_0 [7]),
        .O(\aux[7]_i_2__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(\aux[0]_i_1__1_n_0 ),
        .Q(\aux_reg[7]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(\aux[1]_i_1__1_n_0 ),
        .Q(\aux_reg[7]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(\aux[2]_i_1__1_n_0 ),
        .Q(\aux_reg[7]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(\aux[3]_i_1__1_n_0 ),
        .Q(\aux_reg[7]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(\aux[4]_i_1__1_n_0 ),
        .Q(\aux_reg[7]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(\aux[5]_i_1__1_n_0 ),
        .Q(\aux_reg[7]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(\aux[6]_i_1__1_n_0 ),
        .Q(\aux_reg[7]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \aux_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(\aux[7]_i_2__1_n_0 ),
        .Q(\aux_reg[7]_0 [7]),
        .R(1'b0));
endmodule

module OutputTreatmentFSMVisualizer
   (en_point,
    reset,
    display_bus_OBUF,
    Q,
    \sel_i_reg[7] ,
    \sel_i_reg[4] ,
    \aux_reg[7] ,
    \aux_reg[7]_0 ,
    \aux_reg[7]_1 ,
    en_point_reg,
    CLK,
    \display_bus[4] ,
    \display_bus_OBUF[2]_inst_i_1 ,
    \display_bus_OBUF[1]_inst_i_1 ,
    \FSM_onehot_current_state_reg[0] ,
    \FSM_onehot_current_state_reg[0]_0 ,
    \display_bus[2] ,
    \display_bus_OBUF[1]_inst_i_1_0 ,
    \display_bus[3] ,
    \display_bus[6] ,
    \display_bus_OBUF[4]_inst_i_2 ,
    \display_bus_OBUF[4]_inst_i_2_0 ,
    \display_bus_OBUF[4]_inst_i_2_1 ,
    \display_bus_OBUF[4]_inst_i_2_2 ,
    \display_bus_OBUF[6]_inst_i_1 ,
    reset_IBUF);
  output en_point;
  output reset;
  output [6:0]display_bus_OBUF;
  output [7:0]Q;
  output \sel_i_reg[7] ;
  output \sel_i_reg[4] ;
  output [7:0]\aux_reg[7] ;
  output [7:0]\aux_reg[7]_0 ;
  output [7:0]\aux_reg[7]_1 ;
  input en_point_reg;
  input CLK;
  input \display_bus[4] ;
  input \display_bus_OBUF[2]_inst_i_1 ;
  input \display_bus_OBUF[1]_inst_i_1 ;
  input \FSM_onehot_current_state_reg[0] ;
  input [0:0]\FSM_onehot_current_state_reg[0]_0 ;
  input \display_bus[2] ;
  input \display_bus_OBUF[1]_inst_i_1_0 ;
  input \display_bus[3] ;
  input \display_bus[6] ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  input \display_bus_OBUF[6]_inst_i_1 ;
  input reset_IBUF;

  wire CLK;
  wire \FSM_onehot_current_state_reg[0] ;
  wire [0:0]\FSM_onehot_current_state_reg[0]_0 ;
  wire [7:0]Q;
  wire [7:0]\aux_reg[7] ;
  wire [7:0]\aux_reg[7]_0 ;
  wire [7:0]\aux_reg[7]_1 ;
  wire \display_bus[2] ;
  wire \display_bus[3] ;
  wire \display_bus[4] ;
  wire \display_bus[6] ;
  wire [6:0]display_bus_OBUF;
  wire \display_bus_OBUF[1]_inst_i_1 ;
  wire \display_bus_OBUF[1]_inst_i_1_0 ;
  wire \display_bus_OBUF[2]_inst_i_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  wire \display_bus_OBUF[6]_inst_i_1 ;
  wire en_point;
  wire en_point_reg;
  wire reset;
  wire reset_IBUF;
  wire \sel_i_reg[4] ;
  wire \sel_i_reg[7] ;

  Flash_Visualizer BadEnd_unit
       (.CLK(CLK),
        .\aux_reg[7] (\aux_reg[7]_1 ),
        .\count_reg[24] (reset));
  Flash_Visualizer_0 GoodEnd_unit
       (.CLK(CLK),
        .\aux_reg[7] (\aux_reg[7]_0 ),
        .\count_reg[24] (reset));
  TimePassVisualizer TimPassVis_unit
       (.CLK(CLK),
        .\FSM_onehot_current_state_reg[0] (\FSM_onehot_current_state_reg[0] ),
        .\FSM_onehot_current_state_reg[0]_0 (\FSM_onehot_current_state_reg[0]_0 ),
        .Q(Q),
        .\display_bus[2] (\display_bus[2] ),
        .\display_bus[3] (\display_bus[3] ),
        .\display_bus[4] (\display_bus[4] ),
        .\display_bus[6] (\display_bus[6] ),
        .display_bus_OBUF(display_bus_OBUF),
        .\display_bus_OBUF[1]_inst_i_1 (\display_bus_OBUF[1]_inst_i_1 ),
        .\display_bus_OBUF[1]_inst_i_1_0 (\display_bus_OBUF[1]_inst_i_1_0 ),
        .\display_bus_OBUF[2]_inst_i_1 (\display_bus_OBUF[2]_inst_i_1 ),
        .\display_bus_OBUF[4]_inst_i_2 (\display_bus_OBUF[4]_inst_i_2 ),
        .\display_bus_OBUF[4]_inst_i_2_0 (\display_bus_OBUF[4]_inst_i_2_0 ),
        .\display_bus_OBUF[4]_inst_i_2_1 (\display_bus_OBUF[4]_inst_i_2_1 ),
        .\display_bus_OBUF[4]_inst_i_2_2 (\display_bus_OBUF[4]_inst_i_2_2 ),
        .\display_bus_OBUF[6]_inst_i_1 (\display_bus_OBUF[6]_inst_i_1 ),
        .en_point(en_point),
        .en_point_reg_0(en_point_reg),
        .reset(reset),
        .reset_IBUF(reset_IBUF),
        .\sel_i_reg[4] (\sel_i_reg[4] ),
        .\sel_i_reg[7] (\sel_i_reg[7] ));
  Flash_Visualizer_1 flasher_unit
       (.CLK(CLK),
        .\aux_reg[7] (\aux_reg[7] ),
        .\count_reg[24] (reset));
endmodule

module SCANNER
   (display_bus_OBUF,
    \FSM_onehot_current_state_reg[4] ,
    \FSM_onehot_current_state_reg[4]_0 ,
    Q,
    \sel_i_reg[4]_0 ,
    \sel_i_reg[7]_0 ,
    \sel_i_reg[4]_1 ,
    \display_bus[4] ,
    \display_bus[2] ,
    \display_bus[1] ,
    \display_bus[2]_0 ,
    \display_bus[3] ,
    \display_bus[3]_0 ,
    \display_bus_OBUF[3]_inst_i_1_0 ,
    \display_bus_OBUF[0]_inst_i_1_0 ,
    \display_bus_OBUF[0]_inst_i_1_1 ,
    \display_bus_OBUF[0]_inst_i_1_2 ,
    \display_bus_OBUF[1]_inst_i_1_0 ,
    \display_bus_OBUF[4]_inst_i_2_0 ,
    \display_bus_OBUF[4]_inst_i_2_1 ,
    \display_bus_OBUF[4]_inst_i_2_2 ,
    \display_bus_OBUF[4]_inst_i_2_3 ,
    \display_bus_OBUF[5]_inst_i_4_0 ,
    \display_bus_OBUF[5]_inst_i_4_1 ,
    \display_bus_OBUF[6]_inst_i_1 ,
    \display_bus_OBUF[3]_inst_i_3_0 ,
    \display_bus_OBUF[3]_inst_i_3_1 ,
    E,
    CLK,
    \sel_i_reg[0]_0 );
  output [4:0]display_bus_OBUF;
  output \FSM_onehot_current_state_reg[4] ;
  output \FSM_onehot_current_state_reg[4]_0 ;
  output [7:0]Q;
  output \sel_i_reg[4]_0 ;
  output \sel_i_reg[7]_0 ;
  output \sel_i_reg[4]_1 ;
  input \display_bus[4] ;
  input \display_bus[2] ;
  input \display_bus[1] ;
  input \display_bus[2]_0 ;
  input \display_bus[3] ;
  input \display_bus[3]_0 ;
  input \display_bus_OBUF[3]_inst_i_1_0 ;
  input \display_bus_OBUF[0]_inst_i_1_0 ;
  input \display_bus_OBUF[0]_inst_i_1_1 ;
  input \display_bus_OBUF[0]_inst_i_1_2 ;
  input \display_bus_OBUF[1]_inst_i_1_0 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_3 ;
  input [0:0]\display_bus_OBUF[5]_inst_i_4_0 ;
  input \display_bus_OBUF[5]_inst_i_4_1 ;
  input \display_bus_OBUF[6]_inst_i_1 ;
  input \display_bus_OBUF[3]_inst_i_3_0 ;
  input \display_bus_OBUF[3]_inst_i_3_1 ;
  input [0:0]E;
  input CLK;
  input \sel_i_reg[0]_0 ;

  wire CLK;
  wire [0:0]E;
  wire \FSM_onehot_current_state_reg[4] ;
  wire \FSM_onehot_current_state_reg[4]_0 ;
  wire [7:0]Q;
  wire \display_bus[1] ;
  wire \display_bus[2] ;
  wire \display_bus[2]_0 ;
  wire \display_bus[3] ;
  wire \display_bus[3]_0 ;
  wire \display_bus[4] ;
  wire [4:0]display_bus_OBUF;
  wire \display_bus_OBUF[0]_inst_i_1_0 ;
  wire \display_bus_OBUF[0]_inst_i_1_1 ;
  wire \display_bus_OBUF[0]_inst_i_1_2 ;
  wire \display_bus_OBUF[1]_inst_i_1_0 ;
  wire \display_bus_OBUF[1]_inst_i_3_n_0 ;
  wire \display_bus_OBUF[2]_inst_i_3_n_0 ;
  wire \display_bus_OBUF[3]_inst_i_1_0 ;
  wire \display_bus_OBUF[3]_inst_i_2_n_0 ;
  wire \display_bus_OBUF[3]_inst_i_3_0 ;
  wire \display_bus_OBUF[3]_inst_i_3_1 ;
  wire \display_bus_OBUF[3]_inst_i_3_n_0 ;
  wire \display_bus_OBUF[3]_inst_i_4_n_0 ;
  wire \display_bus_OBUF[3]_inst_i_5_n_0 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_3 ;
  wire \display_bus_OBUF[4]_inst_i_2_n_0 ;
  wire \display_bus_OBUF[4]_inst_i_3_n_0 ;
  wire \display_bus_OBUF[4]_inst_i_4_n_0 ;
  wire \display_bus_OBUF[4]_inst_i_5_n_0 ;
  wire \display_bus_OBUF[4]_inst_i_6_n_0 ;
  wire [0:0]\display_bus_OBUF[5]_inst_i_4_0 ;
  wire \display_bus_OBUF[5]_inst_i_4_1 ;
  wire \display_bus_OBUF[5]_inst_i_5_n_0 ;
  wire \display_bus_OBUF[5]_inst_i_6_n_0 ;
  wire \display_bus_OBUF[5]_inst_i_7_n_0 ;
  wire \display_bus_OBUF[5]_inst_i_8_n_0 ;
  wire \display_bus_OBUF[6]_inst_i_1 ;
  wire \display_bus_OBUF[6]_inst_i_16_n_0 ;
  wire \display_bus_OBUF[6]_inst_i_17_n_0 ;
  wire \sel_i_reg[0]_0 ;
  wire \sel_i_reg[4]_0 ;
  wire \sel_i_reg[4]_1 ;
  wire \sel_i_reg[7]_0 ;

  LUT6 #(
    .INIT(64'h0000882280A28080)) 
    \display_bus_OBUF[0]_inst_i_1 
       (.I0(\display_bus[4] ),
        .I1(\display_bus_OBUF[4]_inst_i_3_n_0 ),
        .I2(\display_bus_OBUF[2]_inst_i_3_n_0 ),
        .I3(\display_bus_OBUF[4]_inst_i_2_n_0 ),
        .I4(\FSM_onehot_current_state_reg[4] ),
        .I5(\FSM_onehot_current_state_reg[4]_0 ),
        .O(display_bus_OBUF[0]));
  LUT6 #(
    .INIT(64'hBABABABFAAAAAAAA)) 
    \display_bus_OBUF[1]_inst_i_1 
       (.I0(\display_bus[2] ),
        .I1(\display_bus[1] ),
        .I2(\FSM_onehot_current_state_reg[4]_0 ),
        .I3(\display_bus_OBUF[4]_inst_i_3_n_0 ),
        .I4(\display_bus_OBUF[1]_inst_i_3_n_0 ),
        .I5(\display_bus[4] ),
        .O(display_bus_OBUF[1]));
  LUT4 #(
    .INIT(16'h56A6)) 
    \display_bus_OBUF[1]_inst_i_3 
       (.I0(\FSM_onehot_current_state_reg[4] ),
        .I1(\display_bus_OBUF[1]_inst_i_1_0 ),
        .I2(\sel_i_reg[4]_0 ),
        .I3(\display_bus_OBUF[0]_inst_i_1_2 ),
        .O(\display_bus_OBUF[1]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hABAAABFFAAAAAAAA)) 
    \display_bus_OBUF[2]_inst_i_1 
       (.I0(\display_bus[2] ),
        .I1(\display_bus_OBUF[4]_inst_i_3_n_0 ),
        .I2(\display_bus_OBUF[2]_inst_i_3_n_0 ),
        .I3(\FSM_onehot_current_state_reg[4]_0 ),
        .I4(\display_bus[2]_0 ),
        .I5(\display_bus[4] ),
        .O(display_bus_OBUF[2]));
  LUT4 #(
    .INIT(16'h002E)) 
    \display_bus_OBUF[2]_inst_i_3 
       (.I0(\display_bus_OBUF[0]_inst_i_1_1 ),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[0]_inst_i_1_2 ),
        .I3(\FSM_onehot_current_state_reg[4] ),
        .O(\display_bus_OBUF[2]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hCEEECECC)) 
    \display_bus_OBUF[3]_inst_i_1 
       (.I0(\display_bus[4] ),
        .I1(\display_bus[3] ),
        .I2(\display_bus[3]_0 ),
        .I3(\FSM_onehot_current_state_reg[4]_0 ),
        .I4(\display_bus_OBUF[3]_inst_i_2_n_0 ),
        .O(display_bus_OBUF[3]));
  LUT6 #(
    .INIT(64'h0000BABF303F8A80)) 
    \display_bus_OBUF[3]_inst_i_2 
       (.I0(\display_bus_OBUF[3]_inst_i_3_n_0 ),
        .I1(\display_bus_OBUF[3]_inst_i_1_0 ),
        .I2(\sel_i_reg[4]_0 ),
        .I3(\display_bus_OBUF[0]_inst_i_1_0 ),
        .I4(\FSM_onehot_current_state_reg[4] ),
        .I5(\display_bus_OBUF[4]_inst_i_2_n_0 ),
        .O(\display_bus_OBUF[3]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \display_bus_OBUF[3]_inst_i_3 
       (.I0(\display_bus_OBUF[4]_inst_i_4_n_0 ),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[3]_inst_i_4_n_0 ),
        .I3(\sel_i_reg[7]_0 ),
        .I4(\display_bus_OBUF[3]_inst_i_5_n_0 ),
        .O(\display_bus_OBUF[3]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000051FFFFFF5D)) 
    \display_bus_OBUF[3]_inst_i_4 
       (.I0(\display_bus_OBUF[4]_inst_i_2_0 [1]),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I5(\display_bus_OBUF[4]_inst_i_2_1 [1]),
        .O(\display_bus_OBUF[3]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00000051FFFFFF5D)) 
    \display_bus_OBUF[3]_inst_i_5 
       (.I0(\display_bus_OBUF[4]_inst_i_2_2 [1]),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I5(\display_bus_OBUF[4]_inst_i_2_3 [1]),
        .O(\display_bus_OBUF[3]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h0444040C)) 
    \display_bus_OBUF[4]_inst_i_1 
       (.I0(\FSM_onehot_current_state_reg[4] ),
        .I1(\display_bus[4] ),
        .I2(\FSM_onehot_current_state_reg[4]_0 ),
        .I3(\display_bus_OBUF[4]_inst_i_2_n_0 ),
        .I4(\display_bus_OBUF[4]_inst_i_3_n_0 ),
        .O(display_bus_OBUF[4]));
  LUT5 #(
    .INIT(32'h30773044)) 
    \display_bus_OBUF[4]_inst_i_2 
       (.I0(\display_bus_OBUF[4]_inst_i_4_n_0 ),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[4]_inst_i_5_n_0 ),
        .I3(\sel_i_reg[7]_0 ),
        .I4(\display_bus_OBUF[4]_inst_i_6_n_0 ),
        .O(\display_bus_OBUF[4]_inst_i_2_n_0 ));
  MUXF7 \display_bus_OBUF[4]_inst_i_3 
       (.I0(\display_bus_OBUF[0]_inst_i_1_0 ),
        .I1(\display_bus_OBUF[3]_inst_i_1_0 ),
        .O(\display_bus_OBUF[4]_inst_i_3_n_0 ),
        .S(\sel_i_reg[4]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFAE000000A2)) 
    \display_bus_OBUF[4]_inst_i_4 
       (.I0(\display_bus_OBUF[3]_inst_i_3_0 ),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I5(\display_bus_OBUF[3]_inst_i_3_1 ),
        .O(\display_bus_OBUF[4]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFAE000000A2)) 
    \display_bus_OBUF[4]_inst_i_5 
       (.I0(\display_bus_OBUF[4]_inst_i_2_0 [1]),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I5(\display_bus_OBUF[4]_inst_i_2_1 [1]),
        .O(\display_bus_OBUF[4]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFAE000000A2)) 
    \display_bus_OBUF[4]_inst_i_6 
       (.I0(\display_bus_OBUF[4]_inst_i_2_2 [1]),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I5(\display_bus_OBUF[4]_inst_i_2_3 [1]),
        .O(\display_bus_OBUF[4]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB8CC)) 
    \display_bus_OBUF[5]_inst_i_4 
       (.I0(\display_bus_OBUF[5]_inst_i_5_n_0 ),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[5]_inst_i_6_n_0 ),
        .I3(\sel_i_reg[7]_0 ),
        .I4(\display_bus_OBUF[5]_inst_i_7_n_0 ),
        .O(\FSM_onehot_current_state_reg[4] ));
  LUT6 #(
    .INIT(64'hFFFFFFF2FFFF0000)) 
    \display_bus_OBUF[5]_inst_i_5 
       (.I0(\sel_i_reg[4]_0 ),
        .I1(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I2(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I3(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I4(\display_bus_OBUF[5]_inst_i_4_0 ),
        .I5(\display_bus_OBUF[5]_inst_i_4_1 ),
        .O(\display_bus_OBUF[5]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000051FFFFFF5D)) 
    \display_bus_OBUF[5]_inst_i_6 
       (.I0(\display_bus_OBUF[4]_inst_i_2_0 [0]),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I5(\display_bus_OBUF[4]_inst_i_2_1 [0]),
        .O(\display_bus_OBUF[5]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000051FFFFFF5D)) 
    \display_bus_OBUF[5]_inst_i_7 
       (.I0(\display_bus_OBUF[4]_inst_i_2_2 [0]),
        .I1(\sel_i_reg[4]_0 ),
        .I2(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_8_n_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .I5(\display_bus_OBUF[4]_inst_i_2_3 [0]),
        .O(\display_bus_OBUF[5]_inst_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \display_bus_OBUF[5]_inst_i_8 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[7]),
        .O(\display_bus_OBUF[5]_inst_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \display_bus_OBUF[6]_inst_i_10 
       (.I0(Q[4]),
        .I1(Q[5]),
        .I2(Q[7]),
        .I3(Q[6]),
        .O(\sel_i_reg[4]_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \display_bus_OBUF[6]_inst_i_16 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\display_bus_OBUF[6]_inst_i_16_n_0 ));
  LUT5 #(
    .INIT(32'h5D000000)) 
    \display_bus_OBUF[6]_inst_i_17 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[4]),
        .I4(Q[6]),
        .O(\display_bus_OBUF[6]_inst_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h00100010FFFF0000)) 
    \display_bus_OBUF[6]_inst_i_2 
       (.I0(\sel_i_reg[4]_1 ),
        .I1(\display_bus_OBUF[5]_inst_i_4_0 ),
        .I2(\sel_i_reg[7]_0 ),
        .I3(\display_bus_OBUF[5]_inst_i_4_1 ),
        .I4(\display_bus_OBUF[6]_inst_i_1 ),
        .I5(\sel_i_reg[4]_0 ),
        .O(\FSM_onehot_current_state_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h00000000D0FF0000)) 
    \display_bus_OBUF[6]_inst_i_6 
       (.I0(Q[4]),
        .I1(\display_bus_OBUF[6]_inst_i_16_n_0 ),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(Q[7]),
        .I5(\display_bus_OBUF[6]_inst_i_17_n_0 ),
        .O(\sel_i_reg[4]_1 ));
  LUT6 #(
    .INIT(64'h8000888888888888)) 
    \display_bus_OBUF[6]_inst_i_7 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[5]),
        .I5(Q[4]),
        .O(\sel_i_reg[7]_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \sel_i_reg[0] 
       (.C(CLK),
        .CE(E),
        .CLR(\sel_i_reg[0]_0 ),
        .D(Q[7]),
        .Q(Q[0]));
  FDPE #(
    .INIT(1'b1)) 
    \sel_i_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(Q[0]),
        .PRE(\sel_i_reg[0]_0 ),
        .Q(Q[1]));
  FDPE #(
    .INIT(1'b1)) 
    \sel_i_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(Q[1]),
        .PRE(\sel_i_reg[0]_0 ),
        .Q(Q[2]));
  FDPE #(
    .INIT(1'b1)) 
    \sel_i_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(Q[2]),
        .PRE(\sel_i_reg[0]_0 ),
        .Q(Q[3]));
  FDPE #(
    .INIT(1'b1)) 
    \sel_i_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(Q[3]),
        .PRE(\sel_i_reg[0]_0 ),
        .Q(Q[4]));
  FDPE #(
    .INIT(1'b1)) 
    \sel_i_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(Q[4]),
        .PRE(\sel_i_reg[0]_0 ),
        .Q(Q[5]));
  FDPE #(
    .INIT(1'b1)) 
    \sel_i_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(Q[5]),
        .PRE(\sel_i_reg[0]_0 ),
        .Q(Q[6]));
  FDPE #(
    .INIT(1'b1)) 
    \sel_i_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(Q[6]),
        .PRE(\sel_i_reg[0]_0 ),
        .Q(Q[7]));
endmodule

module STROBE
   (E,
    \count_reg[0]_0 ,
    CLK,
    AR);
  output [0:0]E;
  input [0:0]\count_reg[0]_0 ;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire count0__39_carry__0_i_1_n_0;
  wire count0__39_carry__0_i_2_n_0;
  wire count0__39_carry__0_i_3_n_0;
  wire count0__39_carry__0_i_4_n_0;
  wire count0__39_carry__0_n_0;
  wire count0__39_carry__0_n_4;
  wire count0__39_carry__0_n_5;
  wire count0__39_carry__0_n_6;
  wire count0__39_carry__0_n_7;
  wire count0__39_carry__1_i_1_n_0;
  wire count0__39_carry__1_n_0;
  wire count0__39_carry__1_n_4;
  wire count0__39_carry__1_n_5;
  wire count0__39_carry__1_n_6;
  wire count0__39_carry__1_n_7;
  wire count0__39_carry__2_i_1_n_3;
  wire count0__39_carry__2_n_1;
  wire count0__39_carry__2_n_6;
  wire count0__39_carry__2_n_7;
  wire count0__39_carry_i_1_n_0;
  wire count0__39_carry_i_2_n_0;
  wire count0__39_carry_n_0;
  wire count0__39_carry_n_4;
  wire count0__39_carry_n_5;
  wire count0__39_carry_n_6;
  wire count0__39_carry_n_7;
  wire count0_carry__0_i_1__2_n_0;
  wire count0_carry__0_i_2__2_n_0;
  wire count0_carry__0_i_3__2_n_0;
  wire count0_carry__0_i_4__2_n_0;
  wire count0_carry__0_n_0;
  wire count0_carry__0_n_4;
  wire count0_carry__0_n_5;
  wire count0_carry__0_n_6;
  wire count0_carry__0_n_7;
  wire count0_carry__1_i_1__2_n_0;
  wire count0_carry__1_i_2__2_n_0;
  wire count0_carry__1_i_3__2_n_0;
  wire count0_carry__1_i_4__2_n_0;
  wire count0_carry__1_n_0;
  wire count0_carry__1_n_4;
  wire count0_carry__1_n_5;
  wire count0_carry__1_n_6;
  wire count0_carry__1_n_7;
  wire count0_carry__2_i_1__2_n_0;
  wire count0_carry__2_i_2__2_n_0;
  wire count0_carry__2_i_3__2_n_0;
  wire count0_carry__2_i_4__2_n_0;
  wire count0_carry__2_n_0;
  wire count0_carry__2_n_4;
  wire count0_carry__2_n_5;
  wire count0_carry__2_n_6;
  wire count0_carry__2_n_7;
  wire count0_carry__3_i_1__2_n_0;
  wire count0_carry__3_i_2__2_n_0;
  wire count0_carry__3_i_3__2_n_0;
  wire count0_carry__3_i_4__2_n_0;
  wire count0_carry__3_n_0;
  wire count0_carry__3_n_4;
  wire count0_carry__3_n_5;
  wire count0_carry__3_n_6;
  wire count0_carry__3_n_7;
  wire count0_carry_i_2__2_n_0;
  wire count0_carry_i_3__2_n_0;
  wire count0_carry_i_4__2_n_0;
  wire count0_carry_n_0;
  wire count0_carry_n_4;
  wire count0_carry_n_5;
  wire count0_carry_n_6;
  wire count0_carry_n_7;
  wire [19:0]count1;
  wire count1_carry__0_n_0;
  wire count1_carry__1_n_0;
  wire count1_carry__2_n_0;
  wire count1_carry_n_0;
  wire \count[10]_i_1__2_n_0 ;
  wire \count[11]_i_1__2_n_0 ;
  wire \count[12]_i_1__2_n_0 ;
  wire \count[13]_i_1__2_n_0 ;
  wire \count[14]_i_1__2_n_0 ;
  wire \count[15]_i_1__2_n_0 ;
  wire \count[16]_i_1__2_n_0 ;
  wire \count[17]_i_1__2_n_0 ;
  wire \count[18]_i_1__2_n_0 ;
  wire \count[19]_i_2_n_0 ;
  wire \count[3]_i_3__0_n_0 ;
  wire \count[3]_i_4_n_0 ;
  wire \count[3]_i_5_n_0 ;
  wire \count[3]_i_6_n_0 ;
  wire \count[6]_i_1_n_0 ;
  wire \count[7]_i_1_n_0 ;
  wire \count[8]_i_1__2_n_0 ;
  wire \count[9]_i_1__2_n_0 ;
  wire [0:0]\count_reg[0]_0 ;
  wire \count_reg_n_0_[0] ;
  wire \count_reg_n_0_[10] ;
  wire \count_reg_n_0_[11] ;
  wire \count_reg_n_0_[12] ;
  wire \count_reg_n_0_[13] ;
  wire \count_reg_n_0_[14] ;
  wire \count_reg_n_0_[15] ;
  wire \count_reg_n_0_[16] ;
  wire \count_reg_n_0_[17] ;
  wire \count_reg_n_0_[18] ;
  wire \count_reg_n_0_[19] ;
  wire \count_reg_n_0_[1] ;
  wire \count_reg_n_0_[2] ;
  wire \count_reg_n_0_[3] ;
  wire \count_reg_n_0_[4] ;
  wire \count_reg_n_0_[5] ;
  wire \count_reg_n_0_[6] ;
  wire \count_reg_n_0_[7] ;
  wire \count_reg_n_0_[8] ;
  wire \count_reg_n_0_[9] ;
  wire [2:0]NLW_count0__39_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0__39_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0__39_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_count0__39_carry__2_CO_UNCONNECTED;
  wire [3:2]NLW_count0__39_carry__2_O_UNCONNECTED;
  wire [3:1]NLW_count0__39_carry__2_i_1_CO_UNCONNECTED;
  wire [3:0]NLW_count0__39_carry__2_i_1_O_UNCONNECTED;
  wire [2:0]NLW_count0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_count1_carry__3_CO_UNCONNECTED;
  wire [3:3]NLW_count1_carry__3_O_UNCONNECTED;

  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__39_carry
       (.CI(1'b0),
        .CO({count0__39_carry_n_0,NLW_count0__39_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b1,1'b1,1'b0}),
        .O({count0__39_carry_n_4,count0__39_carry_n_5,count0__39_carry_n_6,count0__39_carry_n_7}),
        .S({count0_carry__1_n_6,count0__39_carry_i_1_n_0,count0__39_carry_i_2_n_0,count0_carry__0_n_5}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__39_carry__0
       (.CI(count0__39_carry_n_0),
        .CO({count0__39_carry__0_n_0,NLW_count0__39_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O({count0__39_carry__0_n_4,count0__39_carry__0_n_5,count0__39_carry__0_n_6,count0__39_carry__0_n_7}),
        .S({count0__39_carry__0_i_1_n_0,count0__39_carry__0_i_2_n_0,count0__39_carry__0_i_3_n_0,count0__39_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__39_carry__0_i_1
       (.I0(count0_carry__2_n_6),
        .O(count0__39_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__39_carry__0_i_2
       (.I0(count0_carry__2_n_7),
        .O(count0__39_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__39_carry__0_i_3
       (.I0(count0_carry__1_n_4),
        .O(count0__39_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__39_carry__0_i_4
       (.I0(count0_carry__1_n_5),
        .O(count0__39_carry__0_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__39_carry__1
       (.CI(count0__39_carry__0_n_0),
        .CO({count0__39_carry__1_n_0,NLW_count0__39_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({count0__39_carry__1_n_4,count0__39_carry__1_n_5,count0__39_carry__1_n_6,count0__39_carry__1_n_7}),
        .S({count0_carry__3_n_6,count0_carry__3_n_7,count0__39_carry__1_i_1_n_0,count0_carry__2_n_5}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__39_carry__1_i_1
       (.I0(count0_carry__2_n_4),
        .O(count0__39_carry__1_i_1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__39_carry__2
       (.CI(count0__39_carry__1_n_0),
        .CO({NLW_count0__39_carry__2_CO_UNCONNECTED[3],count0__39_carry__2_n_1,NLW_count0__39_carry__2_CO_UNCONNECTED[1:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b1,1'b0,1'b0}),
        .O({NLW_count0__39_carry__2_O_UNCONNECTED[3:2],count0__39_carry__2_n_6,count0__39_carry__2_n_7}),
        .S({1'b0,count0__39_carry__2_i_1_n_3,count0_carry__3_n_4,count0_carry__3_n_5}));
  CARRY4 count0__39_carry__2_i_1
       (.CI(count0_carry__3_n_0),
        .CO({NLW_count0__39_carry__2_i_1_CO_UNCONNECTED[3:1],count0__39_carry__2_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count0__39_carry__2_i_1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__39_carry_i_1
       (.I0(count0_carry__1_n_7),
        .O(count0__39_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__39_carry_i_2
       (.I0(count0_carry__0_n_4),
        .O(count0__39_carry_i_2_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry
       (.CI(1'b0),
        .CO({count0_carry_n_0,NLW_count0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(count1[3:0]),
        .O({count0_carry_n_4,count0_carry_n_5,count0_carry_n_6,count0_carry_n_7}),
        .S({count0_carry_i_2__2_n_0,count0_carry_i_3__2_n_0,count0_carry_i_4__2_n_0,\count_reg_n_0_[0] }));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__0
       (.CI(count0_carry_n_0),
        .CO({count0_carry__0_n_0,NLW_count0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[7:4]),
        .O({count0_carry__0_n_4,count0_carry__0_n_5,count0_carry__0_n_6,count0_carry__0_n_7}),
        .S({count0_carry__0_i_1__2_n_0,count0_carry__0_i_2__2_n_0,count0_carry__0_i_3__2_n_0,count0_carry__0_i_4__2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_1__2
       (.I0(count1[7]),
        .O(count0_carry__0_i_1__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_2__2
       (.I0(count1[6]),
        .O(count0_carry__0_i_2__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_3__2
       (.I0(count1[5]),
        .O(count0_carry__0_i_3__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_4__2
       (.I0(count1[4]),
        .O(count0_carry__0_i_4__2_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__1
       (.CI(count0_carry__0_n_0),
        .CO({count0_carry__1_n_0,NLW_count0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[11:8]),
        .O({count0_carry__1_n_4,count0_carry__1_n_5,count0_carry__1_n_6,count0_carry__1_n_7}),
        .S({count0_carry__1_i_1__2_n_0,count0_carry__1_i_2__2_n_0,count0_carry__1_i_3__2_n_0,count0_carry__1_i_4__2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_1__2
       (.I0(count1[11]),
        .O(count0_carry__1_i_1__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_2__2
       (.I0(count1[10]),
        .O(count0_carry__1_i_2__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_3__2
       (.I0(count1[9]),
        .O(count0_carry__1_i_3__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_4__2
       (.I0(count1[8]),
        .O(count0_carry__1_i_4__2_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__2
       (.CI(count0_carry__1_n_0),
        .CO({count0_carry__2_n_0,NLW_count0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[15:12]),
        .O({count0_carry__2_n_4,count0_carry__2_n_5,count0_carry__2_n_6,count0_carry__2_n_7}),
        .S({count0_carry__2_i_1__2_n_0,count0_carry__2_i_2__2_n_0,count0_carry__2_i_3__2_n_0,count0_carry__2_i_4__2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_1__2
       (.I0(count1[15]),
        .O(count0_carry__2_i_1__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_2__2
       (.I0(count1[14]),
        .O(count0_carry__2_i_2__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_3__2
       (.I0(count1[13]),
        .O(count0_carry__2_i_3__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_4__2
       (.I0(count1[12]),
        .O(count0_carry__2_i_4__2_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__3
       (.CI(count0_carry__2_n_0),
        .CO({count0_carry__3_n_0,NLW_count0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[19:16]),
        .O({count0_carry__3_n_4,count0_carry__3_n_5,count0_carry__3_n_6,count0_carry__3_n_7}),
        .S({count0_carry__3_i_1__2_n_0,count0_carry__3_i_2__2_n_0,count0_carry__3_i_3__2_n_0,count0_carry__3_i_4__2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_1__2
       (.I0(count1[19]),
        .O(count0_carry__3_i_1__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_2__2
       (.I0(count1[18]),
        .O(count0_carry__3_i_2__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_3__2
       (.I0(count1[17]),
        .O(count0_carry__3_i_3__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_4__2
       (.I0(count1[16]),
        .O(count0_carry__3_i_4__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_1__2
       (.I0(\count_reg_n_0_[0] ),
        .O(count1[0]));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_2__2
       (.I0(count1[3]),
        .O(count0_carry_i_2__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_3__2
       (.I0(count1[2]),
        .O(count0_carry_i_3__2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_4__2
       (.I0(count1[1]),
        .O(count0_carry_i_4__2_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry
       (.CI(1'b0),
        .CO({count1_carry_n_0,NLW_count1_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(\count_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[4:1]),
        .S({\count_reg_n_0_[4] ,\count_reg_n_0_[3] ,\count_reg_n_0_[2] ,\count_reg_n_0_[1] }));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__0
       (.CI(count1_carry_n_0),
        .CO({count1_carry__0_n_0,NLW_count1_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[8:5]),
        .S({\count_reg_n_0_[8] ,\count_reg_n_0_[7] ,\count_reg_n_0_[6] ,\count_reg_n_0_[5] }));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__1
       (.CI(count1_carry__0_n_0),
        .CO({count1_carry__1_n_0,NLW_count1_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[12:9]),
        .S({\count_reg_n_0_[12] ,\count_reg_n_0_[11] ,\count_reg_n_0_[10] ,\count_reg_n_0_[9] }));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__2
       (.CI(count1_carry__1_n_0),
        .CO({count1_carry__2_n_0,NLW_count1_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[16:13]),
        .S({\count_reg_n_0_[16] ,\count_reg_n_0_[15] ,\count_reg_n_0_[14] ,\count_reg_n_0_[13] }));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__3
       (.CI(count1_carry__2_n_0),
        .CO(NLW_count1_carry__3_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_count1_carry__3_O_UNCONNECTED[3],count1[19:17]}),
        .S({1'b0,\count_reg_n_0_[19] ,\count_reg_n_0_[18] ,\count_reg_n_0_[17] }));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[10]_i_1__2 
       (.I0(count0__39_carry__0_n_7),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__1_n_5),
        .O(\count[10]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[11]_i_1__2 
       (.I0(count0__39_carry__0_n_6),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__1_n_4),
        .O(\count[11]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[12]_i_1__2 
       (.I0(count0__39_carry__0_n_5),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__2_n_7),
        .O(\count[12]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[13]_i_1__2 
       (.I0(count0__39_carry__0_n_4),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__2_n_6),
        .O(\count[13]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[14]_i_1__2 
       (.I0(count0__39_carry__1_n_7),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__2_n_5),
        .O(\count[14]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[15]_i_1__2 
       (.I0(count0__39_carry__1_n_6),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__2_n_4),
        .O(\count[15]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[16]_i_1__2 
       (.I0(count0__39_carry__1_n_5),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__3_n_7),
        .O(\count[16]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[17]_i_1__2 
       (.I0(count0__39_carry__1_n_4),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__3_n_6),
        .O(\count[17]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[18]_i_1__2 
       (.I0(count0__39_carry__2_n_7),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__3_n_5),
        .O(\count[18]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[19]_i_2 
       (.I0(count0__39_carry__2_n_6),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__3_n_4),
        .O(\count[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \count[3]_i_1__2 
       (.I0(\count[3]_i_3__0_n_0 ),
        .I1(\count_reg_n_0_[9] ),
        .I2(\count_reg_n_0_[4] ),
        .I3(\count_reg_n_0_[14] ),
        .I4(\count_reg_n_0_[0] ),
        .I5(\count[3]_i_4_n_0 ),
        .O(E));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \count[3]_i_3__0 
       (.I0(\count_reg_n_0_[5] ),
        .I1(\count_reg_n_0_[2] ),
        .I2(\count_reg_n_0_[19] ),
        .I3(\count_reg_n_0_[17] ),
        .O(\count[3]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \count[3]_i_4 
       (.I0(\count_reg_n_0_[7] ),
        .I1(\count_reg_n_0_[10] ),
        .I2(\count_reg_n_0_[11] ),
        .I3(\count_reg_n_0_[15] ),
        .I4(\count[3]_i_5_n_0 ),
        .I5(\count[3]_i_6_n_0 ),
        .O(\count[3]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \count[3]_i_5 
       (.I0(\count_reg_n_0_[16] ),
        .I1(\count_reg_n_0_[13] ),
        .I2(\count_reg_n_0_[3] ),
        .I3(\count_reg_n_0_[6] ),
        .O(\count[3]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFF7)) 
    \count[3]_i_6 
       (.I0(\count_reg_n_0_[18] ),
        .I1(\count_reg_n_0_[1] ),
        .I2(\count_reg_n_0_[12] ),
        .I3(\count_reg_n_0_[8] ),
        .O(\count[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[6]_i_1 
       (.I0(count0__39_carry_n_7),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__0_n_5),
        .O(\count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[7]_i_1 
       (.I0(count0__39_carry_n_6),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__0_n_4),
        .O(\count[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[8]_i_1__2 
       (.I0(count0__39_carry_n_5),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__1_n_7),
        .O(\count[8]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[9]_i_1__2 
       (.I0(count0__39_carry_n_4),
        .I1(count0__39_carry__2_n_1),
        .I2(count0_carry__1_n_6),
        .O(\count[9]_i_1__2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(count0_carry_n_7),
        .Q(\count_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[10]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[10] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[11]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[11] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[12]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[12] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[13]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[13] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[14]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[14] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[15]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[15] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[16]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[16] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[17] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[17]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[17] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[18] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[18]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[18] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[19] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[19]_i_2_n_0 ),
        .Q(\count_reg_n_0_[19] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(count0_carry_n_6),
        .Q(\count_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(count0_carry_n_5),
        .Q(\count_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(count0_carry_n_4),
        .Q(\count_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(count0_carry__0_n_7),
        .Q(\count_reg_n_0_[4] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(count0_carry__0_n_6),
        .Q(\count_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[6]_i_1_n_0 ),
        .Q(\count_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[7]_i_1_n_0 ),
        .Q(\count_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[8]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[8] ));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(CLK),
        .CE(\count_reg[0]_0 ),
        .CLR(AR),
        .D(\count[9]_i_1__2_n_0 ),
        .Q(\count_reg_n_0_[9] ));
endmodule

module SYNCHRNZR
   (\sreg_reg[0]_0 ,
    CLK,
    button_IBUF);
  output [0:0]\sreg_reg[0]_0 ;
  input CLK;
  input button_IBUF;

  wire CLK;
  wire button_IBUF;
  wire [0:0]\sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  (* srl_name = "\unit/trat_boton/inst_sincro/SYNC_OUT_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    SYNC_OUT_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(button_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
endmodule

module TIMER
   (E,
    CLK,
    \count_reg[24]_0 );
  output [0:0]E;
  input CLK;
  input \count_reg[24]_0 ;

  wire CLK;
  wire [0:0]E;
  wire \aux[7]_i_3_n_0 ;
  wire \aux[7]_i_4_n_0 ;
  wire \aux[7]_i_5_n_0 ;
  wire \aux[7]_i_6_n_0 ;
  wire [24:0]count;
  wire count0__49_carry__0_i_1_n_0;
  wire count0__49_carry__0_i_2_n_0;
  wire count0__49_carry__0_i_3_n_0;
  wire count0__49_carry__0_n_0;
  wire count0__49_carry__0_n_4;
  wire count0__49_carry__0_n_5;
  wire count0__49_carry__0_n_6;
  wire count0__49_carry__0_n_7;
  wire count0__49_carry__1_i_1_n_0;
  wire count0__49_carry__1_i_2_n_0;
  wire count0__49_carry__1_i_3_n_0;
  wire count0__49_carry__1_n_0;
  wire count0__49_carry__1_n_4;
  wire count0__49_carry__1_n_5;
  wire count0__49_carry__1_n_6;
  wire count0__49_carry__1_n_7;
  wire count0__49_carry__2_i_1_n_0;
  wire count0__49_carry__2_i_2_n_0;
  wire count0__49_carry__2_n_0;
  wire count0__49_carry__2_n_4;
  wire count0__49_carry__2_n_5;
  wire count0__49_carry__2_n_6;
  wire count0__49_carry__2_n_7;
  wire count0__49_carry__3_n_2;
  wire count0__49_carry__3_n_7;
  wire count0__49_carry_i_1_n_0;
  wire count0__49_carry_n_0;
  wire count0__49_carry_n_4;
  wire count0__49_carry_n_5;
  wire count0__49_carry_n_6;
  wire count0__49_carry_n_7;
  wire count0_carry__0_i_1_n_0;
  wire count0_carry__0_i_2_n_0;
  wire count0_carry__0_i_3_n_0;
  wire count0_carry__0_i_4_n_0;
  wire count0_carry__0_n_0;
  wire count0_carry__1_i_1_n_0;
  wire count0_carry__1_i_2_n_0;
  wire count0_carry__1_i_3_n_0;
  wire count0_carry__1_i_4_n_0;
  wire count0_carry__1_n_0;
  wire count0_carry__1_n_4;
  wire count0_carry__1_n_5;
  wire count0_carry__1_n_6;
  wire count0_carry__1_n_7;
  wire count0_carry__2_i_1_n_0;
  wire count0_carry__2_i_2_n_0;
  wire count0_carry__2_i_3_n_0;
  wire count0_carry__2_i_4_n_0;
  wire count0_carry__2_n_0;
  wire count0_carry__2_n_4;
  wire count0_carry__2_n_5;
  wire count0_carry__2_n_6;
  wire count0_carry__2_n_7;
  wire count0_carry__3_i_1_n_0;
  wire count0_carry__3_i_2_n_0;
  wire count0_carry__3_i_3_n_0;
  wire count0_carry__3_i_4_n_0;
  wire count0_carry__3_n_0;
  wire count0_carry__3_n_4;
  wire count0_carry__3_n_5;
  wire count0_carry__3_n_6;
  wire count0_carry__3_n_7;
  wire count0_carry__4_i_1_n_0;
  wire count0_carry__4_i_2_n_0;
  wire count0_carry__4_i_3_n_0;
  wire count0_carry__4_i_4_n_0;
  wire count0_carry__4_n_0;
  wire count0_carry__4_n_4;
  wire count0_carry__4_n_5;
  wire count0_carry__4_n_6;
  wire count0_carry__4_n_7;
  wire count0_carry__5_i_1_n_0;
  wire count0_carry__5_n_2;
  wire count0_carry__5_n_7;
  wire count0_carry_i_2_n_0;
  wire count0_carry_i_3_n_0;
  wire count0_carry_i_4_n_0;
  wire count0_carry_n_0;
  wire [24:0]count1;
  wire count1_carry__0_n_0;
  wire count1_carry__1_n_0;
  wire count1_carry__2_n_0;
  wire count1_carry__3_n_0;
  wire count1_carry_n_0;
  wire \count_reg[24]_0 ;
  wire [24:0]p_0_in;
  wire [2:0]NLW_count0__49_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_count0__49_carry__3_CO_UNCONNECTED;
  wire [3:1]NLW_count0__49_carry__3_O_UNCONNECTED;
  wire [2:0]NLW_count0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__4_CO_UNCONNECTED;
  wire [3:0]NLW_count0_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_count0_carry__5_O_UNCONNECTED;
  wire [2:0]NLW_count1_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_count1_carry__4_CO_UNCONNECTED;

  LUT5 #(
    .INIT(32'h80000000)) 
    \aux[7]_i_1 
       (.I0(\aux[7]_i_3_n_0 ),
        .I1(\aux[7]_i_4_n_0 ),
        .I2(\aux[7]_i_5_n_0 ),
        .I3(count[0]),
        .I4(\aux[7]_i_6_n_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \aux[7]_i_3 
       (.I0(count[3]),
        .I1(count[4]),
        .I2(count[1]),
        .I3(count[2]),
        .I4(count[6]),
        .I5(count[5]),
        .O(\aux[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \aux[7]_i_4 
       (.I0(count[21]),
        .I1(count[22]),
        .I2(count[20]),
        .I3(count[19]),
        .I4(count[23]),
        .I5(count[24]),
        .O(\aux[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \aux[7]_i_5 
       (.I0(count[16]),
        .I1(count[15]),
        .I2(count[13]),
        .I3(count[14]),
        .I4(count[18]),
        .I5(count[17]),
        .O(\aux[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \aux[7]_i_6 
       (.I0(count[10]),
        .I1(count[9]),
        .I2(count[7]),
        .I3(count[8]),
        .I4(count[12]),
        .I5(count[11]),
        .O(\aux[7]_i_6_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry
       (.CI(1'b0),
        .CO({count0__49_carry_n_0,NLW_count0__49_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({count0__49_carry_n_4,count0__49_carry_n_5,count0__49_carry_n_6,count0__49_carry_n_7}),
        .S({count0_carry__1_n_4,count0_carry__1_n_5,count0__49_carry_i_1_n_0,count0_carry__1_n_7}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__0
       (.CI(count0__49_carry_n_0),
        .CO({count0__49_carry__0_n_0,NLW_count0__49_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b0,1'b1}),
        .O({count0__49_carry__0_n_4,count0__49_carry__0_n_5,count0__49_carry__0_n_6,count0__49_carry__0_n_7}),
        .S({count0__49_carry__0_i_1_n_0,count0__49_carry__0_i_2_n_0,count0_carry__2_n_6,count0__49_carry__0_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_1
       (.I0(count0_carry__2_n_4),
        .O(count0__49_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_2
       (.I0(count0_carry__2_n_5),
        .O(count0__49_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_3
       (.I0(count0_carry__2_n_7),
        .O(count0__49_carry__0_i_3_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__1
       (.CI(count0__49_carry__0_n_0),
        .CO({count0__49_carry__1_n_0,NLW_count0__49_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b0}),
        .O({count0__49_carry__1_n_4,count0__49_carry__1_n_5,count0__49_carry__1_n_6,count0__49_carry__1_n_7}),
        .S({count0__49_carry__1_i_1_n_0,count0__49_carry__1_i_2_n_0,count0__49_carry__1_i_3_n_0,count0_carry__3_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_1
       (.I0(count0_carry__3_n_4),
        .O(count0__49_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_2
       (.I0(count0_carry__3_n_5),
        .O(count0__49_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_3
       (.I0(count0_carry__3_n_6),
        .O(count0__49_carry__1_i_3_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__2
       (.CI(count0__49_carry__1_n_0),
        .CO({count0__49_carry__2_n_0,NLW_count0__49_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b0,1'b0}),
        .O({count0__49_carry__2_n_4,count0__49_carry__2_n_5,count0__49_carry__2_n_6,count0__49_carry__2_n_7}),
        .S({count0__49_carry__2_i_1_n_0,count0__49_carry__2_i_2_n_0,count0_carry__4_n_6,count0_carry__4_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__2_i_1
       (.I0(count0_carry__4_n_4),
        .O(count0__49_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__2_i_2
       (.I0(count0_carry__4_n_5),
        .O(count0__49_carry__2_i_2_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__3
       (.CI(count0__49_carry__2_n_0),
        .CO({NLW_count0__49_carry__3_CO_UNCONNECTED[3:2],count0__49_carry__3_n_2,NLW_count0__49_carry__3_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({NLW_count0__49_carry__3_O_UNCONNECTED[3:1],count0__49_carry__3_n_7}),
        .S({1'b0,1'b0,count0_carry__5_n_2,count0_carry__5_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry_i_1
       (.I0(count0_carry__1_n_6),
        .O(count0__49_carry_i_1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry
       (.CI(1'b0),
        .CO({count0_carry_n_0,NLW_count0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(count1[3:0]),
        .O(p_0_in[3:0]),
        .S({count0_carry_i_2_n_0,count0_carry_i_3_n_0,count0_carry_i_4_n_0,count[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__0
       (.CI(count0_carry_n_0),
        .CO({count0_carry__0_n_0,NLW_count0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[7:4]),
        .O(p_0_in[7:4]),
        .S({count0_carry__0_i_1_n_0,count0_carry__0_i_2_n_0,count0_carry__0_i_3_n_0,count0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_1
       (.I0(count1[7]),
        .O(count0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_2
       (.I0(count1[6]),
        .O(count0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_3
       (.I0(count1[5]),
        .O(count0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_4
       (.I0(count1[4]),
        .O(count0_carry__0_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__1
       (.CI(count0_carry__0_n_0),
        .CO({count0_carry__1_n_0,NLW_count0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[11:8]),
        .O({count0_carry__1_n_4,count0_carry__1_n_5,count0_carry__1_n_6,count0_carry__1_n_7}),
        .S({count0_carry__1_i_1_n_0,count0_carry__1_i_2_n_0,count0_carry__1_i_3_n_0,count0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_1
       (.I0(count1[11]),
        .O(count0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_2
       (.I0(count1[10]),
        .O(count0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_3
       (.I0(count1[9]),
        .O(count0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_4
       (.I0(count1[8]),
        .O(count0_carry__1_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__2
       (.CI(count0_carry__1_n_0),
        .CO({count0_carry__2_n_0,NLW_count0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[15:12]),
        .O({count0_carry__2_n_4,count0_carry__2_n_5,count0_carry__2_n_6,count0_carry__2_n_7}),
        .S({count0_carry__2_i_1_n_0,count0_carry__2_i_2_n_0,count0_carry__2_i_3_n_0,count0_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_1
       (.I0(count1[15]),
        .O(count0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_2
       (.I0(count1[14]),
        .O(count0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_3
       (.I0(count1[13]),
        .O(count0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_4
       (.I0(count1[12]),
        .O(count0_carry__2_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__3
       (.CI(count0_carry__2_n_0),
        .CO({count0_carry__3_n_0,NLW_count0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[19:16]),
        .O({count0_carry__3_n_4,count0_carry__3_n_5,count0_carry__3_n_6,count0_carry__3_n_7}),
        .S({count0_carry__3_i_1_n_0,count0_carry__3_i_2_n_0,count0_carry__3_i_3_n_0,count0_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_1
       (.I0(count1[19]),
        .O(count0_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_2
       (.I0(count1[18]),
        .O(count0_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_3
       (.I0(count1[17]),
        .O(count0_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_4
       (.I0(count1[16]),
        .O(count0_carry__3_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__4
       (.CI(count0_carry__3_n_0),
        .CO({count0_carry__4_n_0,NLW_count0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[23:20]),
        .O({count0_carry__4_n_4,count0_carry__4_n_5,count0_carry__4_n_6,count0_carry__4_n_7}),
        .S({count0_carry__4_i_1_n_0,count0_carry__4_i_2_n_0,count0_carry__4_i_3_n_0,count0_carry__4_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_1
       (.I0(count1[23]),
        .O(count0_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_2
       (.I0(count1[22]),
        .O(count0_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_3
       (.I0(count1[21]),
        .O(count0_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_4
       (.I0(count1[20]),
        .O(count0_carry__4_i_4_n_0));
  CARRY4 count0_carry__5
       (.CI(count0_carry__4_n_0),
        .CO({NLW_count0_carry__5_CO_UNCONNECTED[3:2],count0_carry__5_n_2,NLW_count0_carry__5_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,count1[24]}),
        .O({NLW_count0_carry__5_O_UNCONNECTED[3:1],count0_carry__5_n_7}),
        .S({1'b0,1'b0,1'b1,count0_carry__5_i_1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__5_i_1
       (.I0(count1[24]),
        .O(count0_carry__5_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_1
       (.I0(count[0]),
        .O(count1[0]));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_2
       (.I0(count1[3]),
        .O(count0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_3
       (.I0(count1[2]),
        .O(count0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_4
       (.I0(count1[1]),
        .O(count0_carry_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry
       (.CI(1'b0),
        .CO({count1_carry_n_0,NLW_count1_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(count[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[4:1]),
        .S(count[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__0
       (.CI(count1_carry_n_0),
        .CO({count1_carry__0_n_0,NLW_count1_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[8:5]),
        .S(count[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__1
       (.CI(count1_carry__0_n_0),
        .CO({count1_carry__1_n_0,NLW_count1_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[12:9]),
        .S(count[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__2
       (.CI(count1_carry__1_n_0),
        .CO({count1_carry__2_n_0,NLW_count1_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[16:13]),
        .S(count[16:13]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__3
       (.CI(count1_carry__2_n_0),
        .CO({count1_carry__3_n_0,NLW_count1_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[20:17]),
        .S(count[20:17]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__4
       (.CI(count1_carry__3_n_0),
        .CO(NLW_count1_carry__4_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[24:21]),
        .S(count[24:21]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[10]_i_1 
       (.I0(count0__49_carry_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_5),
        .O(p_0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[11]_i_1 
       (.I0(count0__49_carry_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_4),
        .O(p_0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[12]_i_1 
       (.I0(count0__49_carry__0_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_7),
        .O(p_0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[13]_i_1 
       (.I0(count0__49_carry__0_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_6),
        .O(p_0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[14]_i_1 
       (.I0(count0__49_carry__0_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_5),
        .O(p_0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[15]_i_1 
       (.I0(count0__49_carry__0_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_4),
        .O(p_0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[16]_i_1 
       (.I0(count0__49_carry__1_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_7),
        .O(p_0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[17]_i_1 
       (.I0(count0__49_carry__1_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_6),
        .O(p_0_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[18]_i_1 
       (.I0(count0__49_carry__1_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_5),
        .O(p_0_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[19]_i_1 
       (.I0(count0__49_carry__1_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_4),
        .O(p_0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[20]_i_1 
       (.I0(count0__49_carry__2_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_7),
        .O(p_0_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[21]_i_1 
       (.I0(count0__49_carry__2_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_6),
        .O(p_0_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[22]_i_1 
       (.I0(count0__49_carry__2_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_5),
        .O(p_0_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[23]_i_1 
       (.I0(count0__49_carry__2_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_4),
        .O(p_0_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[24]_i_1 
       (.I0(count0__49_carry__3_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__5_n_7),
        .O(p_0_in[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \count[8]_i_1 
       (.I0(count0__49_carry_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_7),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[9]_i_1 
       (.I0(count0__49_carry_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_6),
        .O(p_0_in[9]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[0]),
        .Q(count[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[10]),
        .Q(count[10]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[11]),
        .Q(count[11]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[12]),
        .Q(count[12]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[13]),
        .Q(count[13]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[14]),
        .Q(count[14]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[15]),
        .Q(count[15]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[16]),
        .Q(count[16]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[17]),
        .Q(count[17]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[18]),
        .Q(count[18]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[19]),
        .Q(count[19]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[1]),
        .Q(count[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[20]),
        .Q(count[20]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[21]),
        .Q(count[21]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[22]),
        .Q(count[22]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[23]),
        .Q(count[23]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[24]),
        .Q(count[24]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[2]),
        .Q(count[2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[3]),
        .Q(count[3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[4]),
        .Q(count[4]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[5]),
        .Q(count[5]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[6]),
        .Q(count[6]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[7]),
        .Q(count[7]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[8]),
        .Q(count[8]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[9]),
        .Q(count[9]));
endmodule

(* ORIG_REF_NAME = "TIMER" *) 
module TIMER_3
   (E,
    CLK,
    \count_reg[24]_0 );
  output [0:0]E;
  input CLK;
  input \count_reg[24]_0 ;

  wire CLK;
  wire [0:0]E;
  wire \aux[7]_i_3__0_n_0 ;
  wire \aux[7]_i_4__0_n_0 ;
  wire \aux[7]_i_5__0_n_0 ;
  wire \aux[7]_i_6__0_n_0 ;
  wire [24:0]count;
  wire count0__49_carry__0_i_1__0_n_0;
  wire count0__49_carry__0_i_2__0_n_0;
  wire count0__49_carry__0_i_3__0_n_0;
  wire count0__49_carry__0_n_0;
  wire count0__49_carry__0_n_4;
  wire count0__49_carry__0_n_5;
  wire count0__49_carry__0_n_6;
  wire count0__49_carry__0_n_7;
  wire count0__49_carry__1_i_1__0_n_0;
  wire count0__49_carry__1_i_2__0_n_0;
  wire count0__49_carry__1_i_3__0_n_0;
  wire count0__49_carry__1_n_0;
  wire count0__49_carry__1_n_4;
  wire count0__49_carry__1_n_5;
  wire count0__49_carry__1_n_6;
  wire count0__49_carry__1_n_7;
  wire count0__49_carry__2_i_1__0_n_0;
  wire count0__49_carry__2_i_2__0_n_0;
  wire count0__49_carry__2_n_0;
  wire count0__49_carry__2_n_4;
  wire count0__49_carry__2_n_5;
  wire count0__49_carry__2_n_6;
  wire count0__49_carry__2_n_7;
  wire count0__49_carry__3_n_2;
  wire count0__49_carry__3_n_7;
  wire count0__49_carry_i_1__0_n_0;
  wire count0__49_carry_n_0;
  wire count0__49_carry_n_4;
  wire count0__49_carry_n_5;
  wire count0__49_carry_n_6;
  wire count0__49_carry_n_7;
  wire count0_carry__0_i_1__0_n_0;
  wire count0_carry__0_i_2__0_n_0;
  wire count0_carry__0_i_3__0_n_0;
  wire count0_carry__0_i_4__0_n_0;
  wire count0_carry__0_n_0;
  wire count0_carry__1_i_1__0_n_0;
  wire count0_carry__1_i_2__0_n_0;
  wire count0_carry__1_i_3__0_n_0;
  wire count0_carry__1_i_4__0_n_0;
  wire count0_carry__1_n_0;
  wire count0_carry__1_n_4;
  wire count0_carry__1_n_5;
  wire count0_carry__1_n_6;
  wire count0_carry__1_n_7;
  wire count0_carry__2_i_1__0_n_0;
  wire count0_carry__2_i_2__0_n_0;
  wire count0_carry__2_i_3__0_n_0;
  wire count0_carry__2_i_4__0_n_0;
  wire count0_carry__2_n_0;
  wire count0_carry__2_n_4;
  wire count0_carry__2_n_5;
  wire count0_carry__2_n_6;
  wire count0_carry__2_n_7;
  wire count0_carry__3_i_1__0_n_0;
  wire count0_carry__3_i_2__0_n_0;
  wire count0_carry__3_i_3__0_n_0;
  wire count0_carry__3_i_4__0_n_0;
  wire count0_carry__3_n_0;
  wire count0_carry__3_n_4;
  wire count0_carry__3_n_5;
  wire count0_carry__3_n_6;
  wire count0_carry__3_n_7;
  wire count0_carry__4_i_1__0_n_0;
  wire count0_carry__4_i_2__0_n_0;
  wire count0_carry__4_i_3__0_n_0;
  wire count0_carry__4_i_4__0_n_0;
  wire count0_carry__4_n_0;
  wire count0_carry__4_n_4;
  wire count0_carry__4_n_5;
  wire count0_carry__4_n_6;
  wire count0_carry__4_n_7;
  wire count0_carry__5_i_1__0_n_0;
  wire count0_carry__5_n_2;
  wire count0_carry__5_n_7;
  wire count0_carry_i_2__0_n_0;
  wire count0_carry_i_3__0_n_0;
  wire count0_carry_i_4__0_n_0;
  wire count0_carry_n_0;
  wire [24:0]count1;
  wire count1_carry__0_n_0;
  wire count1_carry__1_n_0;
  wire count1_carry__2_n_0;
  wire count1_carry__3_n_0;
  wire count1_carry_n_0;
  wire \count_reg[24]_0 ;
  wire [24:0]p_0_in;
  wire [2:0]NLW_count0__49_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_count0__49_carry__3_CO_UNCONNECTED;
  wire [3:1]NLW_count0__49_carry__3_O_UNCONNECTED;
  wire [2:0]NLW_count0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__4_CO_UNCONNECTED;
  wire [3:0]NLW_count0_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_count0_carry__5_O_UNCONNECTED;
  wire [2:0]NLW_count1_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_count1_carry__4_CO_UNCONNECTED;

  LUT5 #(
    .INIT(32'h80000000)) 
    \aux[7]_i_1__0 
       (.I0(\aux[7]_i_3__0_n_0 ),
        .I1(\aux[7]_i_4__0_n_0 ),
        .I2(\aux[7]_i_5__0_n_0 ),
        .I3(count[0]),
        .I4(\aux[7]_i_6__0_n_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \aux[7]_i_3__0 
       (.I0(count[3]),
        .I1(count[4]),
        .I2(count[1]),
        .I3(count[2]),
        .I4(count[6]),
        .I5(count[5]),
        .O(\aux[7]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \aux[7]_i_4__0 
       (.I0(count[21]),
        .I1(count[22]),
        .I2(count[20]),
        .I3(count[19]),
        .I4(count[23]),
        .I5(count[24]),
        .O(\aux[7]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \aux[7]_i_5__0 
       (.I0(count[16]),
        .I1(count[15]),
        .I2(count[13]),
        .I3(count[14]),
        .I4(count[18]),
        .I5(count[17]),
        .O(\aux[7]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \aux[7]_i_6__0 
       (.I0(count[10]),
        .I1(count[9]),
        .I2(count[7]),
        .I3(count[8]),
        .I4(count[12]),
        .I5(count[11]),
        .O(\aux[7]_i_6__0_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry
       (.CI(1'b0),
        .CO({count0__49_carry_n_0,NLW_count0__49_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({count0__49_carry_n_4,count0__49_carry_n_5,count0__49_carry_n_6,count0__49_carry_n_7}),
        .S({count0_carry__1_n_4,count0_carry__1_n_5,count0__49_carry_i_1__0_n_0,count0_carry__1_n_7}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__0
       (.CI(count0__49_carry_n_0),
        .CO({count0__49_carry__0_n_0,NLW_count0__49_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b0,1'b1}),
        .O({count0__49_carry__0_n_4,count0__49_carry__0_n_5,count0__49_carry__0_n_6,count0__49_carry__0_n_7}),
        .S({count0__49_carry__0_i_1__0_n_0,count0__49_carry__0_i_2__0_n_0,count0_carry__2_n_6,count0__49_carry__0_i_3__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_1__0
       (.I0(count0_carry__2_n_4),
        .O(count0__49_carry__0_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_2__0
       (.I0(count0_carry__2_n_5),
        .O(count0__49_carry__0_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_3__0
       (.I0(count0_carry__2_n_7),
        .O(count0__49_carry__0_i_3__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__1
       (.CI(count0__49_carry__0_n_0),
        .CO({count0__49_carry__1_n_0,NLW_count0__49_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b0}),
        .O({count0__49_carry__1_n_4,count0__49_carry__1_n_5,count0__49_carry__1_n_6,count0__49_carry__1_n_7}),
        .S({count0__49_carry__1_i_1__0_n_0,count0__49_carry__1_i_2__0_n_0,count0__49_carry__1_i_3__0_n_0,count0_carry__3_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_1__0
       (.I0(count0_carry__3_n_4),
        .O(count0__49_carry__1_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_2__0
       (.I0(count0_carry__3_n_5),
        .O(count0__49_carry__1_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_3__0
       (.I0(count0_carry__3_n_6),
        .O(count0__49_carry__1_i_3__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__2
       (.CI(count0__49_carry__1_n_0),
        .CO({count0__49_carry__2_n_0,NLW_count0__49_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b0,1'b0}),
        .O({count0__49_carry__2_n_4,count0__49_carry__2_n_5,count0__49_carry__2_n_6,count0__49_carry__2_n_7}),
        .S({count0__49_carry__2_i_1__0_n_0,count0__49_carry__2_i_2__0_n_0,count0_carry__4_n_6,count0_carry__4_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__2_i_1__0
       (.I0(count0_carry__4_n_4),
        .O(count0__49_carry__2_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__2_i_2__0
       (.I0(count0_carry__4_n_5),
        .O(count0__49_carry__2_i_2__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__3
       (.CI(count0__49_carry__2_n_0),
        .CO({NLW_count0__49_carry__3_CO_UNCONNECTED[3:2],count0__49_carry__3_n_2,NLW_count0__49_carry__3_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({NLW_count0__49_carry__3_O_UNCONNECTED[3:1],count0__49_carry__3_n_7}),
        .S({1'b0,1'b0,count0_carry__5_n_2,count0_carry__5_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry_i_1__0
       (.I0(count0_carry__1_n_6),
        .O(count0__49_carry_i_1__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry
       (.CI(1'b0),
        .CO({count0_carry_n_0,NLW_count0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(count1[3:0]),
        .O(p_0_in[3:0]),
        .S({count0_carry_i_2__0_n_0,count0_carry_i_3__0_n_0,count0_carry_i_4__0_n_0,count[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__0
       (.CI(count0_carry_n_0),
        .CO({count0_carry__0_n_0,NLW_count0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[7:4]),
        .O(p_0_in[7:4]),
        .S({count0_carry__0_i_1__0_n_0,count0_carry__0_i_2__0_n_0,count0_carry__0_i_3__0_n_0,count0_carry__0_i_4__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_1__0
       (.I0(count1[7]),
        .O(count0_carry__0_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_2__0
       (.I0(count1[6]),
        .O(count0_carry__0_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_3__0
       (.I0(count1[5]),
        .O(count0_carry__0_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_4__0
       (.I0(count1[4]),
        .O(count0_carry__0_i_4__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__1
       (.CI(count0_carry__0_n_0),
        .CO({count0_carry__1_n_0,NLW_count0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[11:8]),
        .O({count0_carry__1_n_4,count0_carry__1_n_5,count0_carry__1_n_6,count0_carry__1_n_7}),
        .S({count0_carry__1_i_1__0_n_0,count0_carry__1_i_2__0_n_0,count0_carry__1_i_3__0_n_0,count0_carry__1_i_4__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_1__0
       (.I0(count1[11]),
        .O(count0_carry__1_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_2__0
       (.I0(count1[10]),
        .O(count0_carry__1_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_3__0
       (.I0(count1[9]),
        .O(count0_carry__1_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_4__0
       (.I0(count1[8]),
        .O(count0_carry__1_i_4__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__2
       (.CI(count0_carry__1_n_0),
        .CO({count0_carry__2_n_0,NLW_count0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[15:12]),
        .O({count0_carry__2_n_4,count0_carry__2_n_5,count0_carry__2_n_6,count0_carry__2_n_7}),
        .S({count0_carry__2_i_1__0_n_0,count0_carry__2_i_2__0_n_0,count0_carry__2_i_3__0_n_0,count0_carry__2_i_4__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_1__0
       (.I0(count1[15]),
        .O(count0_carry__2_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_2__0
       (.I0(count1[14]),
        .O(count0_carry__2_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_3__0
       (.I0(count1[13]),
        .O(count0_carry__2_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_4__0
       (.I0(count1[12]),
        .O(count0_carry__2_i_4__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__3
       (.CI(count0_carry__2_n_0),
        .CO({count0_carry__3_n_0,NLW_count0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[19:16]),
        .O({count0_carry__3_n_4,count0_carry__3_n_5,count0_carry__3_n_6,count0_carry__3_n_7}),
        .S({count0_carry__3_i_1__0_n_0,count0_carry__3_i_2__0_n_0,count0_carry__3_i_3__0_n_0,count0_carry__3_i_4__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_1__0
       (.I0(count1[19]),
        .O(count0_carry__3_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_2__0
       (.I0(count1[18]),
        .O(count0_carry__3_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_3__0
       (.I0(count1[17]),
        .O(count0_carry__3_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_4__0
       (.I0(count1[16]),
        .O(count0_carry__3_i_4__0_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__4
       (.CI(count0_carry__3_n_0),
        .CO({count0_carry__4_n_0,NLW_count0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[23:20]),
        .O({count0_carry__4_n_4,count0_carry__4_n_5,count0_carry__4_n_6,count0_carry__4_n_7}),
        .S({count0_carry__4_i_1__0_n_0,count0_carry__4_i_2__0_n_0,count0_carry__4_i_3__0_n_0,count0_carry__4_i_4__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_1__0
       (.I0(count1[23]),
        .O(count0_carry__4_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_2__0
       (.I0(count1[22]),
        .O(count0_carry__4_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_3__0
       (.I0(count1[21]),
        .O(count0_carry__4_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_4__0
       (.I0(count1[20]),
        .O(count0_carry__4_i_4__0_n_0));
  CARRY4 count0_carry__5
       (.CI(count0_carry__4_n_0),
        .CO({NLW_count0_carry__5_CO_UNCONNECTED[3:2],count0_carry__5_n_2,NLW_count0_carry__5_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,count1[24]}),
        .O({NLW_count0_carry__5_O_UNCONNECTED[3:1],count0_carry__5_n_7}),
        .S({1'b0,1'b0,1'b1,count0_carry__5_i_1__0_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__5_i_1__0
       (.I0(count1[24]),
        .O(count0_carry__5_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_1__0
       (.I0(count[0]),
        .O(count1[0]));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_2__0
       (.I0(count1[3]),
        .O(count0_carry_i_2__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_3__0
       (.I0(count1[2]),
        .O(count0_carry_i_3__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_4__0
       (.I0(count1[1]),
        .O(count0_carry_i_4__0_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry
       (.CI(1'b0),
        .CO({count1_carry_n_0,NLW_count1_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(count[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[4:1]),
        .S(count[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__0
       (.CI(count1_carry_n_0),
        .CO({count1_carry__0_n_0,NLW_count1_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[8:5]),
        .S(count[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__1
       (.CI(count1_carry__0_n_0),
        .CO({count1_carry__1_n_0,NLW_count1_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[12:9]),
        .S(count[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__2
       (.CI(count1_carry__1_n_0),
        .CO({count1_carry__2_n_0,NLW_count1_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[16:13]),
        .S(count[16:13]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__3
       (.CI(count1_carry__2_n_0),
        .CO({count1_carry__3_n_0,NLW_count1_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[20:17]),
        .S(count[20:17]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__4
       (.CI(count1_carry__3_n_0),
        .CO(NLW_count1_carry__4_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[24:21]),
        .S(count[24:21]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[10]_i_1__0 
       (.I0(count0__49_carry_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_5),
        .O(p_0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[11]_i_1__0 
       (.I0(count0__49_carry_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_4),
        .O(p_0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[12]_i_1__0 
       (.I0(count0__49_carry__0_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_7),
        .O(p_0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[13]_i_1__0 
       (.I0(count0__49_carry__0_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_6),
        .O(p_0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[14]_i_1__0 
       (.I0(count0__49_carry__0_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_5),
        .O(p_0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[15]_i_1__0 
       (.I0(count0__49_carry__0_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_4),
        .O(p_0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[16]_i_1__0 
       (.I0(count0__49_carry__1_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_7),
        .O(p_0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[17]_i_1__0 
       (.I0(count0__49_carry__1_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_6),
        .O(p_0_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[18]_i_1__0 
       (.I0(count0__49_carry__1_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_5),
        .O(p_0_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[19]_i_1__0 
       (.I0(count0__49_carry__1_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_4),
        .O(p_0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[20]_i_1__0 
       (.I0(count0__49_carry__2_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_7),
        .O(p_0_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[21]_i_1__0 
       (.I0(count0__49_carry__2_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_6),
        .O(p_0_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[22]_i_1__0 
       (.I0(count0__49_carry__2_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_5),
        .O(p_0_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[23]_i_1__0 
       (.I0(count0__49_carry__2_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_4),
        .O(p_0_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[24]_i_1__0 
       (.I0(count0__49_carry__3_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__5_n_7),
        .O(p_0_in[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \count[8]_i_1__0 
       (.I0(count0__49_carry_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_7),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[9]_i_1__0 
       (.I0(count0__49_carry_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_6),
        .O(p_0_in[9]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[0]),
        .Q(count[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[10]),
        .Q(count[10]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[11]),
        .Q(count[11]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[12]),
        .Q(count[12]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[13]),
        .Q(count[13]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[14]),
        .Q(count[14]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[15]),
        .Q(count[15]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[16]),
        .Q(count[16]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[17]),
        .Q(count[17]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[18]),
        .Q(count[18]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[19]),
        .Q(count[19]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[1]),
        .Q(count[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[20]),
        .Q(count[20]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[21]),
        .Q(count[21]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[22]),
        .Q(count[22]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[23]),
        .Q(count[23]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[24]),
        .Q(count[24]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[2]),
        .Q(count[2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[3]),
        .Q(count[3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[4]),
        .Q(count[4]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[5]),
        .Q(count[5]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[6]),
        .Q(count[6]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[7]),
        .Q(count[7]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[8]),
        .Q(count[8]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[9]),
        .Q(count[9]));
endmodule

(* ORIG_REF_NAME = "TIMER" *) 
module TIMER_5
   (E,
    CLK,
    \count_reg[24]_0 );
  output [0:0]E;
  input CLK;
  input \count_reg[24]_0 ;

  wire CLK;
  wire [0:0]E;
  wire \aux[7]_i_3__1_n_0 ;
  wire \aux[7]_i_4__1_n_0 ;
  wire \aux[7]_i_5__1_n_0 ;
  wire \aux[7]_i_6__1_n_0 ;
  wire [24:0]count;
  wire count0__49_carry__0_i_1__1_n_0;
  wire count0__49_carry__0_i_2__1_n_0;
  wire count0__49_carry__0_i_3__1_n_0;
  wire count0__49_carry__0_n_0;
  wire count0__49_carry__0_n_4;
  wire count0__49_carry__0_n_5;
  wire count0__49_carry__0_n_6;
  wire count0__49_carry__0_n_7;
  wire count0__49_carry__1_i_1__1_n_0;
  wire count0__49_carry__1_i_2__1_n_0;
  wire count0__49_carry__1_i_3__1_n_0;
  wire count0__49_carry__1_n_0;
  wire count0__49_carry__1_n_4;
  wire count0__49_carry__1_n_5;
  wire count0__49_carry__1_n_6;
  wire count0__49_carry__1_n_7;
  wire count0__49_carry__2_i_1__1_n_0;
  wire count0__49_carry__2_i_2__1_n_0;
  wire count0__49_carry__2_n_0;
  wire count0__49_carry__2_n_4;
  wire count0__49_carry__2_n_5;
  wire count0__49_carry__2_n_6;
  wire count0__49_carry__2_n_7;
  wire count0__49_carry__3_n_2;
  wire count0__49_carry__3_n_7;
  wire count0__49_carry_i_1__1_n_0;
  wire count0__49_carry_n_0;
  wire count0__49_carry_n_4;
  wire count0__49_carry_n_5;
  wire count0__49_carry_n_6;
  wire count0__49_carry_n_7;
  wire count0_carry__0_i_1__1_n_0;
  wire count0_carry__0_i_2__1_n_0;
  wire count0_carry__0_i_3__1_n_0;
  wire count0_carry__0_i_4__1_n_0;
  wire count0_carry__0_n_0;
  wire count0_carry__1_i_1__1_n_0;
  wire count0_carry__1_i_2__1_n_0;
  wire count0_carry__1_i_3__1_n_0;
  wire count0_carry__1_i_4__1_n_0;
  wire count0_carry__1_n_0;
  wire count0_carry__1_n_4;
  wire count0_carry__1_n_5;
  wire count0_carry__1_n_6;
  wire count0_carry__1_n_7;
  wire count0_carry__2_i_1__1_n_0;
  wire count0_carry__2_i_2__1_n_0;
  wire count0_carry__2_i_3__1_n_0;
  wire count0_carry__2_i_4__1_n_0;
  wire count0_carry__2_n_0;
  wire count0_carry__2_n_4;
  wire count0_carry__2_n_5;
  wire count0_carry__2_n_6;
  wire count0_carry__2_n_7;
  wire count0_carry__3_i_1__1_n_0;
  wire count0_carry__3_i_2__1_n_0;
  wire count0_carry__3_i_3__1_n_0;
  wire count0_carry__3_i_4__1_n_0;
  wire count0_carry__3_n_0;
  wire count0_carry__3_n_4;
  wire count0_carry__3_n_5;
  wire count0_carry__3_n_6;
  wire count0_carry__3_n_7;
  wire count0_carry__4_i_1__1_n_0;
  wire count0_carry__4_i_2__1_n_0;
  wire count0_carry__4_i_3__1_n_0;
  wire count0_carry__4_i_4__1_n_0;
  wire count0_carry__4_n_0;
  wire count0_carry__4_n_4;
  wire count0_carry__4_n_5;
  wire count0_carry__4_n_6;
  wire count0_carry__4_n_7;
  wire count0_carry__5_i_1__1_n_0;
  wire count0_carry__5_n_2;
  wire count0_carry__5_n_7;
  wire count0_carry_i_2__1_n_0;
  wire count0_carry_i_3__1_n_0;
  wire count0_carry_i_4__1_n_0;
  wire count0_carry_n_0;
  wire [24:0]count1;
  wire count1_carry__0_n_0;
  wire count1_carry__1_n_0;
  wire count1_carry__2_n_0;
  wire count1_carry__3_n_0;
  wire count1_carry_n_0;
  wire \count_reg[24]_0 ;
  wire [24:0]p_0_in;
  wire [2:0]NLW_count0__49_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0__49_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_count0__49_carry__3_CO_UNCONNECTED;
  wire [3:1]NLW_count0__49_carry__3_O_UNCONNECTED;
  wire [2:0]NLW_count0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__4_CO_UNCONNECTED;
  wire [3:0]NLW_count0_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_count0_carry__5_O_UNCONNECTED;
  wire [2:0]NLW_count1_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_count1_carry__4_CO_UNCONNECTED;

  LUT5 #(
    .INIT(32'h80000000)) 
    \aux[7]_i_1__1 
       (.I0(\aux[7]_i_3__1_n_0 ),
        .I1(\aux[7]_i_4__1_n_0 ),
        .I2(\aux[7]_i_5__1_n_0 ),
        .I3(count[0]),
        .I4(\aux[7]_i_6__1_n_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \aux[7]_i_3__1 
       (.I0(count[3]),
        .I1(count[4]),
        .I2(count[1]),
        .I3(count[2]),
        .I4(count[6]),
        .I5(count[5]),
        .O(\aux[7]_i_3__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \aux[7]_i_4__1 
       (.I0(count[21]),
        .I1(count[22]),
        .I2(count[20]),
        .I3(count[19]),
        .I4(count[23]),
        .I5(count[24]),
        .O(\aux[7]_i_4__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \aux[7]_i_5__1 
       (.I0(count[16]),
        .I1(count[15]),
        .I2(count[13]),
        .I3(count[14]),
        .I4(count[18]),
        .I5(count[17]),
        .O(\aux[7]_i_5__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \aux[7]_i_6__1 
       (.I0(count[10]),
        .I1(count[9]),
        .I2(count[7]),
        .I3(count[8]),
        .I4(count[12]),
        .I5(count[11]),
        .O(\aux[7]_i_6__1_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry
       (.CI(1'b0),
        .CO({count0__49_carry_n_0,NLW_count0__49_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({count0__49_carry_n_4,count0__49_carry_n_5,count0__49_carry_n_6,count0__49_carry_n_7}),
        .S({count0_carry__1_n_4,count0_carry__1_n_5,count0__49_carry_i_1__1_n_0,count0_carry__1_n_7}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__0
       (.CI(count0__49_carry_n_0),
        .CO({count0__49_carry__0_n_0,NLW_count0__49_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b0,1'b1}),
        .O({count0__49_carry__0_n_4,count0__49_carry__0_n_5,count0__49_carry__0_n_6,count0__49_carry__0_n_7}),
        .S({count0__49_carry__0_i_1__1_n_0,count0__49_carry__0_i_2__1_n_0,count0_carry__2_n_6,count0__49_carry__0_i_3__1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_1__1
       (.I0(count0_carry__2_n_4),
        .O(count0__49_carry__0_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_2__1
       (.I0(count0_carry__2_n_5),
        .O(count0__49_carry__0_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__0_i_3__1
       (.I0(count0_carry__2_n_7),
        .O(count0__49_carry__0_i_3__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__1
       (.CI(count0__49_carry__0_n_0),
        .CO({count0__49_carry__1_n_0,NLW_count0__49_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b0}),
        .O({count0__49_carry__1_n_4,count0__49_carry__1_n_5,count0__49_carry__1_n_6,count0__49_carry__1_n_7}),
        .S({count0__49_carry__1_i_1__1_n_0,count0__49_carry__1_i_2__1_n_0,count0__49_carry__1_i_3__1_n_0,count0_carry__3_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_1__1
       (.I0(count0_carry__3_n_4),
        .O(count0__49_carry__1_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_2__1
       (.I0(count0_carry__3_n_5),
        .O(count0__49_carry__1_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__1_i_3__1
       (.I0(count0_carry__3_n_6),
        .O(count0__49_carry__1_i_3__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__2
       (.CI(count0__49_carry__1_n_0),
        .CO({count0__49_carry__2_n_0,NLW_count0__49_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b0,1'b0}),
        .O({count0__49_carry__2_n_4,count0__49_carry__2_n_5,count0__49_carry__2_n_6,count0__49_carry__2_n_7}),
        .S({count0__49_carry__2_i_1__1_n_0,count0__49_carry__2_i_2__1_n_0,count0_carry__4_n_6,count0_carry__4_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__2_i_1__1
       (.I0(count0_carry__4_n_4),
        .O(count0__49_carry__2_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry__2_i_2__1
       (.I0(count0_carry__4_n_5),
        .O(count0__49_carry__2_i_2__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__49_carry__3
       (.CI(count0__49_carry__2_n_0),
        .CO({NLW_count0__49_carry__3_CO_UNCONNECTED[3:2],count0__49_carry__3_n_2,NLW_count0__49_carry__3_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({NLW_count0__49_carry__3_O_UNCONNECTED[3:1],count0__49_carry__3_n_7}),
        .S({1'b0,1'b0,count0_carry__5_n_2,count0_carry__5_n_7}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__49_carry_i_1__1
       (.I0(count0_carry__1_n_6),
        .O(count0__49_carry_i_1__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry
       (.CI(1'b0),
        .CO({count0_carry_n_0,NLW_count0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(count1[3:0]),
        .O(p_0_in[3:0]),
        .S({count0_carry_i_2__1_n_0,count0_carry_i_3__1_n_0,count0_carry_i_4__1_n_0,count[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__0
       (.CI(count0_carry_n_0),
        .CO({count0_carry__0_n_0,NLW_count0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[7:4]),
        .O(p_0_in[7:4]),
        .S({count0_carry__0_i_1__1_n_0,count0_carry__0_i_2__1_n_0,count0_carry__0_i_3__1_n_0,count0_carry__0_i_4__1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_1__1
       (.I0(count1[7]),
        .O(count0_carry__0_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_2__1
       (.I0(count1[6]),
        .O(count0_carry__0_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_3__1
       (.I0(count1[5]),
        .O(count0_carry__0_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_4__1
       (.I0(count1[4]),
        .O(count0_carry__0_i_4__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__1
       (.CI(count0_carry__0_n_0),
        .CO({count0_carry__1_n_0,NLW_count0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[11:8]),
        .O({count0_carry__1_n_4,count0_carry__1_n_5,count0_carry__1_n_6,count0_carry__1_n_7}),
        .S({count0_carry__1_i_1__1_n_0,count0_carry__1_i_2__1_n_0,count0_carry__1_i_3__1_n_0,count0_carry__1_i_4__1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_1__1
       (.I0(count1[11]),
        .O(count0_carry__1_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_2__1
       (.I0(count1[10]),
        .O(count0_carry__1_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_3__1
       (.I0(count1[9]),
        .O(count0_carry__1_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_4__1
       (.I0(count1[8]),
        .O(count0_carry__1_i_4__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__2
       (.CI(count0_carry__1_n_0),
        .CO({count0_carry__2_n_0,NLW_count0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[15:12]),
        .O({count0_carry__2_n_4,count0_carry__2_n_5,count0_carry__2_n_6,count0_carry__2_n_7}),
        .S({count0_carry__2_i_1__1_n_0,count0_carry__2_i_2__1_n_0,count0_carry__2_i_3__1_n_0,count0_carry__2_i_4__1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_1__1
       (.I0(count1[15]),
        .O(count0_carry__2_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_2__1
       (.I0(count1[14]),
        .O(count0_carry__2_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_3__1
       (.I0(count1[13]),
        .O(count0_carry__2_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_4__1
       (.I0(count1[12]),
        .O(count0_carry__2_i_4__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__3
       (.CI(count0_carry__2_n_0),
        .CO({count0_carry__3_n_0,NLW_count0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[19:16]),
        .O({count0_carry__3_n_4,count0_carry__3_n_5,count0_carry__3_n_6,count0_carry__3_n_7}),
        .S({count0_carry__3_i_1__1_n_0,count0_carry__3_i_2__1_n_0,count0_carry__3_i_3__1_n_0,count0_carry__3_i_4__1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_1__1
       (.I0(count1[19]),
        .O(count0_carry__3_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_2__1
       (.I0(count1[18]),
        .O(count0_carry__3_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_3__1
       (.I0(count1[17]),
        .O(count0_carry__3_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_4__1
       (.I0(count1[16]),
        .O(count0_carry__3_i_4__1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__4
       (.CI(count0_carry__3_n_0),
        .CO({count0_carry__4_n_0,NLW_count0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[23:20]),
        .O({count0_carry__4_n_4,count0_carry__4_n_5,count0_carry__4_n_6,count0_carry__4_n_7}),
        .S({count0_carry__4_i_1__1_n_0,count0_carry__4_i_2__1_n_0,count0_carry__4_i_3__1_n_0,count0_carry__4_i_4__1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_1__1
       (.I0(count1[23]),
        .O(count0_carry__4_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_2__1
       (.I0(count1[22]),
        .O(count0_carry__4_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_3__1
       (.I0(count1[21]),
        .O(count0_carry__4_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__4_i_4__1
       (.I0(count1[20]),
        .O(count0_carry__4_i_4__1_n_0));
  CARRY4 count0_carry__5
       (.CI(count0_carry__4_n_0),
        .CO({NLW_count0_carry__5_CO_UNCONNECTED[3:2],count0_carry__5_n_2,NLW_count0_carry__5_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,count1[24]}),
        .O({NLW_count0_carry__5_O_UNCONNECTED[3:1],count0_carry__5_n_7}),
        .S({1'b0,1'b0,1'b1,count0_carry__5_i_1__1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__5_i_1__1
       (.I0(count1[24]),
        .O(count0_carry__5_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_1__1
       (.I0(count[0]),
        .O(count1[0]));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_2__1
       (.I0(count1[3]),
        .O(count0_carry_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_3__1
       (.I0(count1[2]),
        .O(count0_carry_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_4__1
       (.I0(count1[1]),
        .O(count0_carry_i_4__1_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry
       (.CI(1'b0),
        .CO({count1_carry_n_0,NLW_count1_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(count[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[4:1]),
        .S(count[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__0
       (.CI(count1_carry_n_0),
        .CO({count1_carry__0_n_0,NLW_count1_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[8:5]),
        .S(count[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__1
       (.CI(count1_carry__0_n_0),
        .CO({count1_carry__1_n_0,NLW_count1_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[12:9]),
        .S(count[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__2
       (.CI(count1_carry__1_n_0),
        .CO({count1_carry__2_n_0,NLW_count1_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[16:13]),
        .S(count[16:13]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__3
       (.CI(count1_carry__2_n_0),
        .CO({count1_carry__3_n_0,NLW_count1_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[20:17]),
        .S(count[20:17]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__4
       (.CI(count1_carry__3_n_0),
        .CO(NLW_count1_carry__4_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[24:21]),
        .S(count[24:21]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[10]_i_1__1 
       (.I0(count0__49_carry_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_5),
        .O(p_0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[11]_i_1__1 
       (.I0(count0__49_carry_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_4),
        .O(p_0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[12]_i_1__1 
       (.I0(count0__49_carry__0_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_7),
        .O(p_0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[13]_i_1__1 
       (.I0(count0__49_carry__0_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_6),
        .O(p_0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[14]_i_1__1 
       (.I0(count0__49_carry__0_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_5),
        .O(p_0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[15]_i_1__1 
       (.I0(count0__49_carry__0_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__2_n_4),
        .O(p_0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[16]_i_1__1 
       (.I0(count0__49_carry__1_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_7),
        .O(p_0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[17]_i_1__1 
       (.I0(count0__49_carry__1_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_6),
        .O(p_0_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[18]_i_1__1 
       (.I0(count0__49_carry__1_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_5),
        .O(p_0_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[19]_i_1__1 
       (.I0(count0__49_carry__1_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__3_n_4),
        .O(p_0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[20]_i_1__1 
       (.I0(count0__49_carry__2_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_7),
        .O(p_0_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[21]_i_1__1 
       (.I0(count0__49_carry__2_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_6),
        .O(p_0_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[22]_i_1__1 
       (.I0(count0__49_carry__2_n_5),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_5),
        .O(p_0_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[23]_i_1__1 
       (.I0(count0__49_carry__2_n_4),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__4_n_4),
        .O(p_0_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[24]_i_1__1 
       (.I0(count0__49_carry__3_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__5_n_7),
        .O(p_0_in[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \count[8]_i_1__1 
       (.I0(count0__49_carry_n_7),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_7),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[9]_i_1__1 
       (.I0(count0__49_carry_n_6),
        .I1(count0__49_carry__3_n_2),
        .I2(count0_carry__1_n_6),
        .O(p_0_in[9]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[0]),
        .Q(count[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[10]),
        .Q(count[10]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[11]),
        .Q(count[11]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[12]),
        .Q(count[12]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[13]),
        .Q(count[13]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[14]),
        .Q(count[14]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[15]),
        .Q(count[15]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[16]),
        .Q(count[16]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[17]),
        .Q(count[17]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[18]),
        .Q(count[18]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[19]),
        .Q(count[19]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[1]),
        .Q(count[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[20]),
        .Q(count[20]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[21]),
        .Q(count[21]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[22]),
        .Q(count[22]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[23]),
        .Q(count[23]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[24]),
        .Q(count[24]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[2]),
        .Q(count[2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[3]),
        .Q(count[3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[4]),
        .Q(count[4]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[5]),
        .Q(count[5]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[6]),
        .Q(count[6]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[7]),
        .Q(count[7]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[8]),
        .Q(count[8]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\count_reg[24]_0 ),
        .D(p_0_in[9]),
        .Q(count[9]));
endmodule

(* ORIG_REF_NAME = "TIMER" *) 
module TIMER__parameterized1
   (reset,
    E,
    reset_IBUF,
    CLK);
  output reset;
  output [0:0]E;
  input reset_IBUF;
  input CLK;

  wire CLK;
  wire [0:0]E;
  wire [16:0]count;
  wire count0__33_carry__0_i_1_n_0;
  wire count0__33_carry__0_i_2_n_0;
  wire count0__33_carry__0_i_3_n_0;
  wire count0__33_carry__0_i_4_n_0;
  wire count0__33_carry__0_n_0;
  wire count0__33_carry__0_n_4;
  wire count0__33_carry__0_n_5;
  wire count0__33_carry__0_n_6;
  wire count0__33_carry__0_n_7;
  wire count0__33_carry__1_i_1_n_0;
  wire count0__33_carry__1_n_0;
  wire count0__33_carry__1_n_4;
  wire count0__33_carry__1_n_5;
  wire count0__33_carry__1_n_6;
  wire count0__33_carry__1_n_7;
  wire count0__33_carry__2_n_1;
  wire count0__33_carry__2_n_6;
  wire count0__33_carry__2_n_7;
  wire count0__33_carry_i_1_n_0;
  wire count0__33_carry_i_2_n_0;
  wire count0__33_carry_n_0;
  wire count0__33_carry_n_4;
  wire count0__33_carry_n_5;
  wire count0__33_carry_n_6;
  wire count0__33_carry_n_7;
  wire count0_carry__0_i_1__3_n_0;
  wire count0_carry__0_i_2__3_n_0;
  wire count0_carry__0_i_3__3_n_0;
  wire count0_carry__0_i_4__3_n_0;
  wire count0_carry__0_n_0;
  wire count0_carry__0_n_4;
  wire count0_carry__0_n_5;
  wire count0_carry__0_n_6;
  wire count0_carry__0_n_7;
  wire count0_carry__1_i_1__3_n_0;
  wire count0_carry__1_i_2__3_n_0;
  wire count0_carry__1_i_3__3_n_0;
  wire count0_carry__1_i_4__3_n_0;
  wire count0_carry__1_n_0;
  wire count0_carry__1_n_4;
  wire count0_carry__1_n_5;
  wire count0_carry__1_n_6;
  wire count0_carry__1_n_7;
  wire count0_carry__2_i_1__3_n_0;
  wire count0_carry__2_i_2__3_n_0;
  wire count0_carry__2_i_3__3_n_0;
  wire count0_carry__2_i_4__3_n_0;
  wire count0_carry__2_n_0;
  wire count0_carry__2_n_4;
  wire count0_carry__2_n_5;
  wire count0_carry__2_n_6;
  wire count0_carry__2_n_7;
  wire count0_carry__3_i_1__3_n_0;
  wire count0_carry__3_n_2;
  wire count0_carry__3_n_7;
  wire count0_carry_i_2__3_n_0;
  wire count0_carry_i_3__3_n_0;
  wire count0_carry_i_4__3_n_0;
  wire count0_carry_n_0;
  wire count0_carry_n_4;
  wire [16:0]count1;
  wire count1_carry__0_n_0;
  wire count1_carry__1_n_0;
  wire count1_carry_n_0;
  wire [16:0]p_0_in;
  wire reset;
  wire reset_IBUF;
  wire \sel_i[7]_i_2_n_0 ;
  wire \sel_i[7]_i_3_n_0 ;
  wire \sel_i[7]_i_4_n_0 ;
  wire [2:0]NLW_count0__33_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0__33_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0__33_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_count0__33_carry__2_CO_UNCONNECTED;
  wire [3:2]NLW_count0__33_carry__2_O_UNCONNECTED;
  wire [2:0]NLW_count0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_count0_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_count0_carry__3_CO_UNCONNECTED;
  wire [3:1]NLW_count0_carry__3_O_UNCONNECTED;
  wire [2:0]NLW_count1_carry_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_count1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_count1_carry__2_CO_UNCONNECTED;

  LUT1 #(
    .INIT(2'h1)) 
    \FSM_sequential_current_state[1]_i_2 
       (.I0(reset_IBUF),
        .O(reset));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__33_carry
       (.CI(1'b0),
        .CO({count0__33_carry_n_0,NLW_count0__33_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b1,1'b1,1'b0}),
        .O({count0__33_carry_n_4,count0__33_carry_n_5,count0__33_carry_n_6,count0__33_carry_n_7}),
        .S({count0_carry__0_n_5,count0__33_carry_i_1_n_0,count0__33_carry_i_2_n_0,count0_carry_n_4}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__33_carry__0
       (.CI(count0__33_carry_n_0),
        .CO({count0__33_carry__0_n_0,NLW_count0__33_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O({count0__33_carry__0_n_4,count0__33_carry__0_n_5,count0__33_carry__0_n_6,count0__33_carry__0_n_7}),
        .S({count0__33_carry__0_i_1_n_0,count0__33_carry__0_i_2_n_0,count0__33_carry__0_i_3_n_0,count0__33_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__33_carry__0_i_1
       (.I0(count0_carry__1_n_5),
        .O(count0__33_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__33_carry__0_i_2
       (.I0(count0_carry__1_n_6),
        .O(count0__33_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__33_carry__0_i_3
       (.I0(count0_carry__1_n_7),
        .O(count0__33_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__33_carry__0_i_4
       (.I0(count0_carry__0_n_4),
        .O(count0__33_carry__0_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__33_carry__1
       (.CI(count0__33_carry__0_n_0),
        .CO({count0__33_carry__1_n_0,NLW_count0__33_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({count0__33_carry__1_n_4,count0__33_carry__1_n_5,count0__33_carry__1_n_6,count0__33_carry__1_n_7}),
        .S({count0_carry__2_n_5,count0_carry__2_n_6,count0__33_carry__1_i_1_n_0,count0_carry__1_n_4}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__33_carry__1_i_1
       (.I0(count0_carry__2_n_7),
        .O(count0__33_carry__1_i_1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0__33_carry__2
       (.CI(count0__33_carry__1_n_0),
        .CO({NLW_count0__33_carry__2_CO_UNCONNECTED[3],count0__33_carry__2_n_1,NLW_count0__33_carry__2_CO_UNCONNECTED[1:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b1,1'b0,1'b0}),
        .O({NLW_count0__33_carry__2_O_UNCONNECTED[3:2],count0__33_carry__2_n_6,count0__33_carry__2_n_7}),
        .S({1'b0,count0_carry__3_n_2,count0_carry__3_n_7,count0_carry__2_n_4}));
  LUT1 #(
    .INIT(2'h1)) 
    count0__33_carry_i_1
       (.I0(count0_carry__0_n_6),
        .O(count0__33_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0__33_carry_i_2
       (.I0(count0_carry__0_n_7),
        .O(count0__33_carry_i_2_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry
       (.CI(1'b0),
        .CO({count0_carry_n_0,NLW_count0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(count1[3:0]),
        .O({count0_carry_n_4,p_0_in[2:0]}),
        .S({count0_carry_i_2__3_n_0,count0_carry_i_3__3_n_0,count0_carry_i_4__3_n_0,count[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__0
       (.CI(count0_carry_n_0),
        .CO({count0_carry__0_n_0,NLW_count0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[7:4]),
        .O({count0_carry__0_n_4,count0_carry__0_n_5,count0_carry__0_n_6,count0_carry__0_n_7}),
        .S({count0_carry__0_i_1__3_n_0,count0_carry__0_i_2__3_n_0,count0_carry__0_i_3__3_n_0,count0_carry__0_i_4__3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_1__3
       (.I0(count1[7]),
        .O(count0_carry__0_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_2__3
       (.I0(count1[6]),
        .O(count0_carry__0_i_2__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_3__3
       (.I0(count1[5]),
        .O(count0_carry__0_i_3__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__0_i_4__3
       (.I0(count1[4]),
        .O(count0_carry__0_i_4__3_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__1
       (.CI(count0_carry__0_n_0),
        .CO({count0_carry__1_n_0,NLW_count0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[11:8]),
        .O({count0_carry__1_n_4,count0_carry__1_n_5,count0_carry__1_n_6,count0_carry__1_n_7}),
        .S({count0_carry__1_i_1__3_n_0,count0_carry__1_i_2__3_n_0,count0_carry__1_i_3__3_n_0,count0_carry__1_i_4__3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_1__3
       (.I0(count1[11]),
        .O(count0_carry__1_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_2__3
       (.I0(count1[10]),
        .O(count0_carry__1_i_2__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_3__3
       (.I0(count1[9]),
        .O(count0_carry__1_i_3__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__1_i_4__3
       (.I0(count1[8]),
        .O(count0_carry__1_i_4__3_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count0_carry__2
       (.CI(count0_carry__1_n_0),
        .CO({count0_carry__2_n_0,NLW_count0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(count1[15:12]),
        .O({count0_carry__2_n_4,count0_carry__2_n_5,count0_carry__2_n_6,count0_carry__2_n_7}),
        .S({count0_carry__2_i_1__3_n_0,count0_carry__2_i_2__3_n_0,count0_carry__2_i_3__3_n_0,count0_carry__2_i_4__3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_1__3
       (.I0(count1[15]),
        .O(count0_carry__2_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_2__3
       (.I0(count1[14]),
        .O(count0_carry__2_i_2__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_3__3
       (.I0(count1[13]),
        .O(count0_carry__2_i_3__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__2_i_4__3
       (.I0(count1[12]),
        .O(count0_carry__2_i_4__3_n_0));
  CARRY4 count0_carry__3
       (.CI(count0_carry__2_n_0),
        .CO({NLW_count0_carry__3_CO_UNCONNECTED[3:2],count0_carry__3_n_2,NLW_count0_carry__3_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,count1[16]}),
        .O({NLW_count0_carry__3_O_UNCONNECTED[3:1],count0_carry__3_n_7}),
        .S({1'b0,1'b0,1'b1,count0_carry__3_i_1__3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry__3_i_1__3
       (.I0(count1[16]),
        .O(count0_carry__3_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_1__3
       (.I0(count[0]),
        .O(count1[0]));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_2__3
       (.I0(count1[3]),
        .O(count0_carry_i_2__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_3__3
       (.I0(count1[2]),
        .O(count0_carry_i_3__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    count0_carry_i_4__3
       (.I0(count1[1]),
        .O(count0_carry_i_4__3_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry
       (.CI(1'b0),
        .CO({count1_carry_n_0,NLW_count1_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(count[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[4:1]),
        .S(count[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__0
       (.CI(count1_carry_n_0),
        .CO({count1_carry__0_n_0,NLW_count1_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[8:5]),
        .S(count[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__1
       (.CI(count1_carry__0_n_0),
        .CO({count1_carry__1_n_0,NLW_count1_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[12:9]),
        .S(count[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 count1_carry__2
       (.CI(count1_carry__1_n_0),
        .CO(NLW_count1_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count1[16:13]),
        .S(count[16:13]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[10]_i_1__3 
       (.I0(count0__33_carry__0_n_4),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__1_n_5),
        .O(p_0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[11]_i_1__3 
       (.I0(count0__33_carry__1_n_7),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__1_n_4),
        .O(p_0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[12]_i_1__3 
       (.I0(count0__33_carry__1_n_6),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__2_n_7),
        .O(p_0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[13]_i_1__3 
       (.I0(count0__33_carry__1_n_5),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__2_n_6),
        .O(p_0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[14]_i_1__3 
       (.I0(count0__33_carry__1_n_4),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__2_n_5),
        .O(p_0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[15]_i_1__3 
       (.I0(count0__33_carry__2_n_7),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__2_n_4),
        .O(p_0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[16]_i_1__3 
       (.I0(count0__33_carry__2_n_6),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__3_n_7),
        .O(p_0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[3]_i_1__3 
       (.I0(count0__33_carry_n_7),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry_n_4),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[4]_i_1 
       (.I0(count0__33_carry_n_6),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__0_n_7),
        .O(p_0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[5]_i_1 
       (.I0(count0__33_carry_n_5),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__0_n_6),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[6]_i_1__0 
       (.I0(count0__33_carry_n_4),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__0_n_5),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[7]_i_1__0 
       (.I0(count0__33_carry__0_n_7),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__0_n_4),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[8]_i_1__3 
       (.I0(count0__33_carry__0_n_6),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__1_n_7),
        .O(p_0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \count[9]_i_1__3 
       (.I0(count0__33_carry__0_n_5),
        .I1(count0__33_carry__2_n_1),
        .I2(count0_carry__1_n_6),
        .O(p_0_in[9]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[0]),
        .Q(count[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[10]),
        .Q(count[10]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[11]),
        .Q(count[11]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[12]),
        .Q(count[12]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[13]),
        .Q(count[13]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[14]),
        .Q(count[14]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[15]),
        .Q(count[15]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[16]),
        .Q(count[16]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[1]),
        .Q(count[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[2]),
        .Q(count[2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[3]),
        .Q(count[3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[4]),
        .Q(count[4]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[5]),
        .Q(count[5]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[6]),
        .Q(count[6]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[7]),
        .Q(count[7]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[8]),
        .Q(count[8]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(p_0_in[9]),
        .Q(count[9]));
  LUT3 #(
    .INIT(8'h80)) 
    \sel_i[7]_i_1 
       (.I0(\sel_i[7]_i_2_n_0 ),
        .I1(\sel_i[7]_i_3_n_0 ),
        .I2(\sel_i[7]_i_4_n_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \sel_i[7]_i_2 
       (.I0(count[7]),
        .I1(count[8]),
        .I2(count[6]),
        .I3(count[5]),
        .I4(count[10]),
        .I5(count[9]),
        .O(\sel_i[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \sel_i[7]_i_3 
       (.I0(count[0]),
        .I1(count[1]),
        .I2(count[2]),
        .I3(count[4]),
        .I4(count[3]),
        .O(\sel_i[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \sel_i[7]_i_4 
       (.I0(count[13]),
        .I1(count[14]),
        .I2(count[11]),
        .I3(count[12]),
        .I4(count[16]),
        .I5(count[15]),
        .O(\sel_i[7]_i_4_n_0 ));
endmodule

(* ECO_CHECKSUM = "5a12bc45" *) 
(* NotValidForBitStream *)
module TOP_Implementation
   (reset,
    clk,
    button,
    switches,
    ctrl_bus,
    display_bus,
    en_point);
  input reset;
  input clk;
  input button;
  input [3:0]switches;
  output [7:0]ctrl_bus;
  output [6:0]display_bus;
  output en_point;

  wire button;
  wire button_IBUF;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire [7:0]ctrl_bus;
  wire [7:0]ctrl_bus_OBUF;
  wire [6:0]display_bus;
  wire [6:0]display_bus_OBUF;
  wire en_point;
  wire en_point_OBUF;
  wire reset;
  wire reset_IBUF;
  wire [3:0]switches;
  wire [3:0]switches_IBUF;

initial begin
 $sdf_annotate("top_tb_time_impl.sdf",,,,"tool_control");
end
  IBUF button_IBUF_inst
       (.I(button),
        .O(button_IBUF));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  OBUF \ctrl_bus_OBUF[0]_inst 
       (.I(ctrl_bus_OBUF[0]),
        .O(ctrl_bus[0]));
  OBUF \ctrl_bus_OBUF[1]_inst 
       (.I(ctrl_bus_OBUF[1]),
        .O(ctrl_bus[1]));
  OBUF \ctrl_bus_OBUF[2]_inst 
       (.I(ctrl_bus_OBUF[2]),
        .O(ctrl_bus[2]));
  OBUF \ctrl_bus_OBUF[3]_inst 
       (.I(ctrl_bus_OBUF[3]),
        .O(ctrl_bus[3]));
  OBUF \ctrl_bus_OBUF[4]_inst 
       (.I(ctrl_bus_OBUF[4]),
        .O(ctrl_bus[4]));
  OBUF \ctrl_bus_OBUF[5]_inst 
       (.I(ctrl_bus_OBUF[5]),
        .O(ctrl_bus[5]));
  OBUF \ctrl_bus_OBUF[6]_inst 
       (.I(ctrl_bus_OBUF[6]),
        .O(ctrl_bus[6]));
  OBUF \ctrl_bus_OBUF[7]_inst 
       (.I(ctrl_bus_OBUF[7]),
        .O(ctrl_bus[7]));
  OBUF \display_bus_OBUF[0]_inst 
       (.I(display_bus_OBUF[0]),
        .O(display_bus[0]));
  OBUF \display_bus_OBUF[1]_inst 
       (.I(display_bus_OBUF[1]),
        .O(display_bus[1]));
  OBUF \display_bus_OBUF[2]_inst 
       (.I(display_bus_OBUF[2]),
        .O(display_bus[2]));
  OBUF \display_bus_OBUF[3]_inst 
       (.I(display_bus_OBUF[3]),
        .O(display_bus[3]));
  OBUF \display_bus_OBUF[4]_inst 
       (.I(display_bus_OBUF[4]),
        .O(display_bus[4]));
  OBUF \display_bus_OBUF[5]_inst 
       (.I(display_bus_OBUF[5]),
        .O(display_bus[5]));
  OBUF \display_bus_OBUF[6]_inst 
       (.I(display_bus_OBUF[6]),
        .O(display_bus[6]));
  OBUF en_point_OBUF_inst
       (.I(en_point_OBUF),
        .O(en_point));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  IBUF \switches_IBUF[0]_inst 
       (.I(switches[0]),
        .O(switches_IBUF[0]));
  IBUF \switches_IBUF[1]_inst 
       (.I(switches[1]),
        .O(switches_IBUF[1]));
  IBUF \switches_IBUF[2]_inst 
       (.I(switches[2]),
        .O(switches_IBUF[2]));
  IBUF \switches_IBUF[3]_inst 
       (.I(switches[3]),
        .O(switches_IBUF[3]));
  top unit
       (.CLK(clk_IBUF_BUFG),
        .D(switches_IBUF),
        .button_IBUF(button_IBUF),
        .ctrl_bus_OBUF(ctrl_bus_OBUF),
        .display_bus_OBUF(display_bus_OBUF),
        .en_point(en_point_OBUF),
        .reset_IBUF(reset_IBUF));
endmodule

module TimePassVisualizer
   (en_point,
    reset,
    display_bus_OBUF,
    Q,
    \sel_i_reg[7] ,
    \sel_i_reg[4] ,
    en_point_reg_0,
    CLK,
    \display_bus[4] ,
    \display_bus_OBUF[2]_inst_i_1 ,
    \display_bus_OBUF[1]_inst_i_1 ,
    \FSM_onehot_current_state_reg[0] ,
    \FSM_onehot_current_state_reg[0]_0 ,
    \display_bus[2] ,
    \display_bus_OBUF[1]_inst_i_1_0 ,
    \display_bus[3] ,
    \display_bus[6] ,
    \display_bus_OBUF[4]_inst_i_2 ,
    \display_bus_OBUF[4]_inst_i_2_0 ,
    \display_bus_OBUF[4]_inst_i_2_1 ,
    \display_bus_OBUF[4]_inst_i_2_2 ,
    \display_bus_OBUF[6]_inst_i_1 ,
    reset_IBUF);
  output en_point;
  output reset;
  output [6:0]display_bus_OBUF;
  output [7:0]Q;
  output \sel_i_reg[7] ;
  output \sel_i_reg[4] ;
  input en_point_reg_0;
  input CLK;
  input \display_bus[4] ;
  input \display_bus_OBUF[2]_inst_i_1 ;
  input \display_bus_OBUF[1]_inst_i_1 ;
  input \FSM_onehot_current_state_reg[0] ;
  input [0:0]\FSM_onehot_current_state_reg[0]_0 ;
  input \display_bus[2] ;
  input \display_bus_OBUF[1]_inst_i_1_0 ;
  input \display_bus[3] ;
  input \display_bus[6] ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  input \display_bus_OBUF[6]_inst_i_1 ;
  input reset_IBUF;

  wire CLK;
  wire \FSM_onehot_current_state_reg[0] ;
  wire [0:0]\FSM_onehot_current_state_reg[0]_0 ;
  wire FSM_unit_n_0;
  wire FSM_unit_n_1;
  wire FSM_unit_n_10;
  wire FSM_unit_n_2;
  wire FSM_unit_n_3;
  wire FSM_unit_n_6;
  wire FSM_unit_n_7;
  wire FSM_unit_n_8;
  wire FSM_unit_n_9;
  wire [7:0]Q;
  wire Vis_unit_n_15;
  wire Vis_unit_n_5;
  wire Vis_unit_n_6;
  wire \display_bus[2] ;
  wire \display_bus[3] ;
  wire \display_bus[4] ;
  wire \display_bus[6] ;
  wire [6:0]display_bus_OBUF;
  wire \display_bus_OBUF[1]_inst_i_1 ;
  wire \display_bus_OBUF[1]_inst_i_1_0 ;
  wire \display_bus_OBUF[2]_inst_i_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  wire \display_bus_OBUF[6]_inst_i_1 ;
  wire en_point;
  wire en_point_reg_0;
  wire reset;
  wire reset_IBUF;
  wire \sel_i_reg[4] ;
  wire \sel_i_reg[7] ;

  selected FSM_unit
       (.CLK(CLK),
        .\FSM_onehot_current_state_reg[0]_0 (\FSM_onehot_current_state_reg[0] ),
        .\FSM_onehot_current_state_reg[0]_1 (\FSM_onehot_current_state_reg[0]_0 ),
        .\FSM_onehot_current_state_reg[2]_0 (FSM_unit_n_10),
        .\FSM_onehot_current_state_reg[4]_0 (FSM_unit_n_0),
        .\FSM_onehot_current_state_reg[4]_1 (FSM_unit_n_1),
        .\FSM_onehot_current_state_reg[4]_2 (FSM_unit_n_2),
        .\FSM_onehot_current_state_reg[4]_3 (FSM_unit_n_3),
        .\FSM_onehot_current_state_reg[4]_4 (FSM_unit_n_7),
        .\FSM_onehot_current_state_reg[4]_5 (FSM_unit_n_9),
        .\FSM_onehot_current_state_reg[4]_6 (reset),
        .Q(FSM_unit_n_8),
        .\count_reg[2] (FSM_unit_n_6),
        .\display_bus[5] (Vis_unit_n_5),
        .\display_bus[5]_0 (\display_bus[4] ),
        .\display_bus[5]_1 (Vis_unit_n_6),
        .\display_bus[6] (\display_bus[6] ),
        .display_bus_OBUF(display_bus_OBUF[6:5]),
        .\display_bus_OBUF[1]_inst_i_1 (\display_bus_OBUF[1]_inst_i_1 ),
        .\display_bus_OBUF[1]_inst_i_1_0 (\display_bus_OBUF[1]_inst_i_1_0 ),
        .\display_bus_OBUF[2]_inst_i_1 (\display_bus_OBUF[2]_inst_i_1 ),
        .\display_bus_OBUF[3]_inst_i_2 (\sel_i_reg[4] ),
        .\display_bus_OBUF[3]_inst_i_2_0 (\sel_i_reg[7] ),
        .\display_bus_OBUF[6]_inst_i_1_0 (Vis_unit_n_15));
  NormalVisualizer Vis_unit
       (.CLK(CLK),
        .\FSM_onehot_current_state_reg[4] (Vis_unit_n_5),
        .\FSM_onehot_current_state_reg[4]_0 (Vis_unit_n_6),
        .Q(Q),
        .\display_bus[1] (FSM_unit_n_3),
        .\display_bus[2] (\display_bus[2] ),
        .\display_bus[2]_0 (FSM_unit_n_0),
        .\display_bus[3] (\display_bus[3] ),
        .\display_bus[3]_0 (FSM_unit_n_6),
        .\display_bus[4] (\display_bus[4] ),
        .display_bus_OBUF(display_bus_OBUF[4:0]),
        .\display_bus_OBUF[0]_inst_i_1 (\display_bus_OBUF[1]_inst_i_1 ),
        .\display_bus_OBUF[0]_inst_i_1_0 (\display_bus_OBUF[1]_inst_i_1_0 ),
        .\display_bus_OBUF[0]_inst_i_1_1 (FSM_unit_n_1),
        .\display_bus_OBUF[1]_inst_i_1 (\display_bus_OBUF[2]_inst_i_1 ),
        .\display_bus_OBUF[3]_inst_i_1 (FSM_unit_n_2),
        .\display_bus_OBUF[3]_inst_i_3 (FSM_unit_n_9),
        .\display_bus_OBUF[3]_inst_i_3_0 (FSM_unit_n_10),
        .\display_bus_OBUF[4]_inst_i_2 (\display_bus_OBUF[4]_inst_i_2 ),
        .\display_bus_OBUF[4]_inst_i_2_0 (\display_bus_OBUF[4]_inst_i_2_0 ),
        .\display_bus_OBUF[4]_inst_i_2_1 (\display_bus_OBUF[4]_inst_i_2_1 ),
        .\display_bus_OBUF[4]_inst_i_2_2 (\display_bus_OBUF[4]_inst_i_2_2 ),
        .\display_bus_OBUF[5]_inst_i_4 (FSM_unit_n_8),
        .\display_bus_OBUF[5]_inst_i_4_0 (FSM_unit_n_7),
        .\display_bus_OBUF[6]_inst_i_1 (\display_bus_OBUF[6]_inst_i_1 ),
        .reset(reset),
        .reset_IBUF(reset_IBUF),
        .\sel_i_reg[4] (Vis_unit_n_15),
        .\sel_i_reg[4]_0 (\sel_i_reg[4] ),
        .\sel_i_reg[7] (\sel_i_reg[7] ));
  FDPE #(
    .INIT(1'b1)) 
    en_point_reg
       (.C(CLK),
        .CE(1'b1),
        .D(en_point_reg_0),
        .PRE(reset),
        .Q(en_point));
endmodule

module Visualizer
   (en_point,
    AR,
    display_bus_OBUF,
    Q,
    \sel_i_reg[7] ,
    \sel_i_reg[4] ,
    ctrl_bus_OBUF,
    CLK,
    \display_bus_OBUF[2]_inst_i_1 ,
    \display_bus_OBUF[1]_inst_i_1 ,
    current_state_reg,
    \FSM_onehot_current_state_reg[0] ,
    \display_bus_OBUF[1]_inst_i_1_0 ,
    \display_bus_OBUF[4]_inst_i_2 ,
    \display_bus_OBUF[4]_inst_i_2_0 ,
    \display_bus_OBUF[4]_inst_i_2_1 ,
    \display_bus_OBUF[4]_inst_i_2_2 ,
    \display_bus_OBUF[6]_inst_i_1 ,
    reset_IBUF,
    D);
  output en_point;
  output [0:0]AR;
  output [6:0]display_bus_OBUF;
  output [0:0]Q;
  output \sel_i_reg[7] ;
  output \sel_i_reg[4] ;
  output [7:0]ctrl_bus_OBUF;
  input CLK;
  input \display_bus_OBUF[2]_inst_i_1 ;
  input \display_bus_OBUF[1]_inst_i_1 ;
  input [1:0]current_state_reg;
  input \FSM_onehot_current_state_reg[0] ;
  input \display_bus_OBUF[1]_inst_i_1_0 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  input [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  input \display_bus_OBUF[6]_inst_i_1 ;
  input reset_IBUF;
  input [1:0]D;

  wire [0:0]AR;
  wire CLK;
  wire [1:0]D;
  wire \FSM_onehot_current_state_reg[0] ;
  wire FSM_unit_n_1;
  wire FSM_unit_n_10;
  wire FSM_unit_n_11;
  wire FSM_unit_n_12;
  wire FSM_unit_n_13;
  wire Out_unit_n_27;
  wire Out_unit_n_28;
  wire Out_unit_n_29;
  wire Out_unit_n_30;
  wire Out_unit_n_31;
  wire Out_unit_n_32;
  wire Out_unit_n_33;
  wire Out_unit_n_34;
  wire Out_unit_n_35;
  wire Out_unit_n_36;
  wire Out_unit_n_37;
  wire Out_unit_n_38;
  wire Out_unit_n_39;
  wire Out_unit_n_40;
  wire Out_unit_n_41;
  wire Out_unit_n_42;
  wire [0:0]Q;
  wire [7:0]aux;
  wire [7:0]bus_ctrl;
  wire [7:0]ctrl_bus_OBUF;
  wire [1:0]current_state_reg;
  wire [6:0]display_bus_OBUF;
  wire \display_bus_OBUF[1]_inst_i_1 ;
  wire \display_bus_OBUF[1]_inst_i_1_0 ;
  wire \display_bus_OBUF[2]_inst_i_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_0 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_1 ;
  wire [1:0]\display_bus_OBUF[4]_inst_i_2_2 ;
  wire \display_bus_OBUF[6]_inst_i_1 ;
  wire en_point;
  wire reset_IBUF;
  wire \sel_i_reg[4] ;
  wire \sel_i_reg[7] ;

  fsm_visualizer FSM_unit
       (.CLK(CLK),
        .D(D),
        .\FSM_onehot_current_state_reg[0]_0 (AR),
        .\FSM_onehot_current_state_reg[1]_0 (FSM_unit_n_1),
        .\FSM_onehot_current_state_reg[1]_1 (FSM_unit_n_13),
        .\FSM_onehot_current_state_reg[3]_0 (FSM_unit_n_10),
        .\FSM_onehot_current_state_reg[3]_1 (FSM_unit_n_11),
        .\FSM_onehot_current_state_reg[3]_2 (FSM_unit_n_12),
        .Q(Q),
        .\ctrl_bus[7] ({Out_unit_n_35,Out_unit_n_36,Out_unit_n_37,Out_unit_n_38,Out_unit_n_39,Out_unit_n_40,Out_unit_n_41,Out_unit_n_42}),
        .\ctrl_bus[7]_0 ({Out_unit_n_27,Out_unit_n_28,Out_unit_n_29,Out_unit_n_30,Out_unit_n_31,Out_unit_n_32,Out_unit_n_33,Out_unit_n_34}),
        .ctrl_bus_OBUF(ctrl_bus_OBUF),
        .\ctrl_bus_OBUF[7]_inst_i_1_0 (aux),
        .\ctrl_bus_OBUF[7]_inst_i_1_1 (bus_ctrl),
        .current_state_reg(current_state_reg));
  OutputTreatmentFSMVisualizer Out_unit
       (.CLK(CLK),
        .\FSM_onehot_current_state_reg[0] (\FSM_onehot_current_state_reg[0] ),
        .\FSM_onehot_current_state_reg[0]_0 (Q),
        .Q(bus_ctrl),
        .\aux_reg[7] (aux),
        .\aux_reg[7]_0 ({Out_unit_n_27,Out_unit_n_28,Out_unit_n_29,Out_unit_n_30,Out_unit_n_31,Out_unit_n_32,Out_unit_n_33,Out_unit_n_34}),
        .\aux_reg[7]_1 ({Out_unit_n_35,Out_unit_n_36,Out_unit_n_37,Out_unit_n_38,Out_unit_n_39,Out_unit_n_40,Out_unit_n_41,Out_unit_n_42}),
        .\display_bus[2] (FSM_unit_n_13),
        .\display_bus[3] (FSM_unit_n_10),
        .\display_bus[4] (FSM_unit_n_11),
        .\display_bus[6] (FSM_unit_n_12),
        .display_bus_OBUF(display_bus_OBUF),
        .\display_bus_OBUF[1]_inst_i_1 (\display_bus_OBUF[1]_inst_i_1 ),
        .\display_bus_OBUF[1]_inst_i_1_0 (\display_bus_OBUF[1]_inst_i_1_0 ),
        .\display_bus_OBUF[2]_inst_i_1 (\display_bus_OBUF[2]_inst_i_1 ),
        .\display_bus_OBUF[4]_inst_i_2 (\display_bus_OBUF[4]_inst_i_2 ),
        .\display_bus_OBUF[4]_inst_i_2_0 (\display_bus_OBUF[4]_inst_i_2_0 ),
        .\display_bus_OBUF[4]_inst_i_2_1 (\display_bus_OBUF[4]_inst_i_2_1 ),
        .\display_bus_OBUF[4]_inst_i_2_2 (\display_bus_OBUF[4]_inst_i_2_2 ),
        .\display_bus_OBUF[6]_inst_i_1 (\display_bus_OBUF[6]_inst_i_1 ),
        .en_point(en_point),
        .en_point_reg(FSM_unit_n_1),
        .reset(AR),
        .reset_IBUF(reset_IBUF),
        .\sel_i_reg[4] (\sel_i_reg[4] ),
        .\sel_i_reg[7] (\sel_i_reg[7] ));
endmodule

module button_treatment
   (\sreg_reg[3] ,
    \sreg_reg[2] ,
    CLK,
    button_IBUF,
    AR,
    E,
    reset_IBUF);
  output \sreg_reg[3] ;
  output \sreg_reg[2] ;
  input CLK;
  input button_IBUF;
  input [0:0]AR;
  input [0:0]E;
  input reset_IBUF;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire button_IBUF;
  wire [0:0]p_1_in;
  wire reset_IBUF;
  wire result;
  wire \sreg_reg[2] ;
  wire \sreg_reg[3] ;

  debounce inst_debouncer
       (.AR(AR),
        .CLK(CLK),
        .D(result),
        .\flipflops_reg[0]_0 (p_1_in),
        .reset_IBUF(reset_IBUF));
  EDGEDTCTR inst_edgedtctr
       (.CLK(CLK),
        .D(result),
        .E(E),
        .\sreg_reg[2]_0 (\sreg_reg[2] ),
        .\sreg_reg[3]_0 (\sreg_reg[3] ));
  SYNCHRNZR inst_sincro
       (.CLK(CLK),
        .button_IBUF(button_IBUF),
        .\sreg_reg[0]_0 (p_1_in));
endmodule

module comparador_completo
   (coinciden,
    \FSM_sequential_current_state_reg[0] ,
    done,
    AR);
  output coinciden;
  input \FSM_sequential_current_state_reg[0] ;
  input done;
  input [0:0]AR;

  wire [0:0]AR;
  wire \FSM_sequential_current_state_reg[0] ;
  wire coinciden;
  wire done;

  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    inter_comparacion_reg
       (.CLR(AR),
        .D(\FSM_sequential_current_state_reg[0] ),
        .G(done),
        .GE(1'b1),
        .Q(coinciden));
endmodule

module debounce
   (D,
    CLK,
    AR,
    \flipflops_reg[0]_0 ,
    reset_IBUF);
  output [0:0]D;
  input CLK;
  input [0:0]AR;
  input [0:0]\flipflops_reg[0]_0 ;
  input reset_IBUF;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]D;
  wire count0;
  wire \count[0]_i_1__3_n_0 ;
  wire \count[0]_i_4_n_0 ;
  wire \count[0]_i_5_n_0 ;
  wire \count[0]_i_6_n_0 ;
  wire \count[0]_i_7_n_0 ;
  wire [19:6]count_reg;
  wire \count_reg[0]_i_3_n_0 ;
  wire \count_reg[0]_i_3_n_4 ;
  wire \count_reg[0]_i_3_n_5 ;
  wire \count_reg[0]_i_3_n_6 ;
  wire \count_reg[0]_i_3_n_7 ;
  wire \count_reg[12]_i_1_n_0 ;
  wire \count_reg[12]_i_1_n_4 ;
  wire \count_reg[12]_i_1_n_5 ;
  wire \count_reg[12]_i_1_n_6 ;
  wire \count_reg[12]_i_1_n_7 ;
  wire \count_reg[16]_i_1_n_4 ;
  wire \count_reg[16]_i_1_n_5 ;
  wire \count_reg[16]_i_1_n_6 ;
  wire \count_reg[16]_i_1_n_7 ;
  wire \count_reg[4]_i_1_n_0 ;
  wire \count_reg[4]_i_1_n_4 ;
  wire \count_reg[4]_i_1_n_5 ;
  wire \count_reg[4]_i_1_n_6 ;
  wire \count_reg[4]_i_1_n_7 ;
  wire \count_reg[8]_i_1_n_0 ;
  wire \count_reg[8]_i_1_n_4 ;
  wire \count_reg[8]_i_1_n_5 ;
  wire \count_reg[8]_i_1_n_6 ;
  wire \count_reg[8]_i_1_n_7 ;
  wire \count_reg_n_0_[0] ;
  wire \count_reg_n_0_[1] ;
  wire \count_reg_n_0_[2] ;
  wire \count_reg_n_0_[3] ;
  wire \count_reg_n_0_[4] ;
  wire \count_reg_n_0_[5] ;
  wire [0:0]\flipflops_reg[0]_0 ;
  wire \flipflops_reg_n_0_[0] ;
  wire p_0_in;
  wire reset_IBUF;
  wire result_i_1_n_0;
  wire [2:0]\NLW_count_reg[0]_i_3_CO_UNCONNECTED ;
  wire [2:0]\NLW_count_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_count_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_count_reg[8]_i_1_CO_UNCONNECTED ;

  LUT3 #(
    .INIT(8'h28)) 
    \count[0]_i_1__3 
       (.I0(reset_IBUF),
        .I1(\flipflops_reg_n_0_[0] ),
        .I2(p_0_in),
        .O(\count[0]_i_1__3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \count[0]_i_2 
       (.I0(reset_IBUF),
        .I1(\count[0]_i_4_n_0 ),
        .O(count0));
  LUT6 #(
    .INIT(64'h4FFFFFFFFFFFFFFF)) 
    \count[0]_i_4 
       (.I0(count_reg[15]),
        .I1(\count[0]_i_6_n_0 ),
        .I2(count_reg[18]),
        .I3(count_reg[19]),
        .I4(count_reg[16]),
        .I5(count_reg[17]),
        .O(\count[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_5 
       (.I0(\count_reg_n_0_[0] ),
        .O(\count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00015555FFFFFFFF)) 
    \count[0]_i_6 
       (.I0(\count[0]_i_7_n_0 ),
        .I1(count_reg[6]),
        .I2(count_reg[7]),
        .I3(count_reg[8]),
        .I4(count_reg[9]),
        .I5(count_reg[14]),
        .O(\count[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count[0]_i_7 
       (.I0(count_reg[11]),
        .I1(count_reg[10]),
        .I2(count_reg[13]),
        .I3(count_reg[12]),
        .O(\count[0]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[0]_i_3_n_7 ),
        .Q(\count_reg_n_0_[0] ),
        .R(\count[0]_i_1__3_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\count_reg[0]_i_3_n_0 ,\NLW_count_reg[0]_i_3_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\count_reg[0]_i_3_n_4 ,\count_reg[0]_i_3_n_5 ,\count_reg[0]_i_3_n_6 ,\count_reg[0]_i_3_n_7 }),
        .S({\count_reg_n_0_[3] ,\count_reg_n_0_[2] ,\count_reg_n_0_[1] ,\count[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[8]_i_1_n_5 ),
        .Q(count_reg[10]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[8]_i_1_n_4 ),
        .Q(count_reg[11]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[12]_i_1_n_7 ),
        .Q(count_reg[12]),
        .R(\count[0]_i_1__3_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \count_reg[12]_i_1 
       (.CI(\count_reg[8]_i_1_n_0 ),
        .CO({\count_reg[12]_i_1_n_0 ,\NLW_count_reg[12]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[12]_i_1_n_4 ,\count_reg[12]_i_1_n_5 ,\count_reg[12]_i_1_n_6 ,\count_reg[12]_i_1_n_7 }),
        .S(count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[12]_i_1_n_6 ),
        .Q(count_reg[13]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[12]_i_1_n_5 ),
        .Q(count_reg[14]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[12]_i_1_n_4 ),
        .Q(count_reg[15]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[16]_i_1_n_7 ),
        .Q(count_reg[16]),
        .R(\count[0]_i_1__3_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \count_reg[16]_i_1 
       (.CI(\count_reg[12]_i_1_n_0 ),
        .CO(\NLW_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[16]_i_1_n_4 ,\count_reg[16]_i_1_n_5 ,\count_reg[16]_i_1_n_6 ,\count_reg[16]_i_1_n_7 }),
        .S(count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[17] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[16]_i_1_n_6 ),
        .Q(count_reg[17]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[18] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[16]_i_1_n_5 ),
        .Q(count_reg[18]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[19] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[16]_i_1_n_4 ),
        .Q(count_reg[19]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[0]_i_3_n_6 ),
        .Q(\count_reg_n_0_[1] ),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[0]_i_3_n_5 ),
        .Q(\count_reg_n_0_[2] ),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[0]_i_3_n_4 ),
        .Q(\count_reg_n_0_[3] ),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[4]_i_1_n_7 ),
        .Q(\count_reg_n_0_[4] ),
        .R(\count[0]_i_1__3_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \count_reg[4]_i_1 
       (.CI(\count_reg[0]_i_3_n_0 ),
        .CO({\count_reg[4]_i_1_n_0 ,\NLW_count_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[4]_i_1_n_4 ,\count_reg[4]_i_1_n_5 ,\count_reg[4]_i_1_n_6 ,\count_reg[4]_i_1_n_7 }),
        .S({count_reg[7:6],\count_reg_n_0_[5] ,\count_reg_n_0_[4] }));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[4]_i_1_n_6 ),
        .Q(\count_reg_n_0_[5] ),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[4]_i_1_n_5 ),
        .Q(count_reg[6]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[4]_i_1_n_4 ),
        .Q(count_reg[7]),
        .R(\count[0]_i_1__3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[8]_i_1_n_7 ),
        .Q(count_reg[8]),
        .R(\count[0]_i_1__3_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \count_reg[8]_i_1 
       (.CI(\count_reg[4]_i_1_n_0 ),
        .CO({\count_reg[8]_i_1_n_0 ,\NLW_count_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[8]_i_1_n_4 ,\count_reg[8]_i_1_n_5 ,\count_reg[8]_i_1_n_6 ,\count_reg[8]_i_1_n_7 }),
        .S(count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(CLK),
        .CE(count0),
        .D(\count_reg[8]_i_1_n_6 ),
        .Q(count_reg[9]),
        .R(\count[0]_i_1__3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \flipflops_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(\flipflops_reg[0]_0 ),
        .Q(\flipflops_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \flipflops_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(\flipflops_reg_n_0_[0] ),
        .Q(p_0_in));
  LUT4 #(
    .INIT(16'hFE08)) 
    result_i_1
       (.I0(\flipflops_reg_n_0_[0] ),
        .I1(p_0_in),
        .I2(\count[0]_i_4_n_0 ),
        .I3(D),
        .O(result_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    result_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(result_i_1_n_0),
        .Q(D));
endmodule

module fsm
   (\FSM_sequential_current_state_reg[0]_0 ,
    current_state_reg,
    E,
    D,
    \FSM_sequential_current_state_reg[0]_1 ,
    i,
    \FSM_sequential_current_state_reg[0]_2 ,
    Q,
    coinciden,
    CLK,
    AR);
  output \FSM_sequential_current_state_reg[0]_0 ;
  output [1:0]current_state_reg;
  output [0:0]E;
  output [1:0]D;
  input \FSM_sequential_current_state_reg[0]_1 ;
  input [1:0]i;
  input \FSM_sequential_current_state_reg[0]_2 ;
  input [0:0]Q;
  input coinciden;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_sequential_current_state[0]_i_1_n_0 ;
  wire \FSM_sequential_current_state[1]_i_1_n_0 ;
  wire \FSM_sequential_current_state_reg[0]_0 ;
  wire \FSM_sequential_current_state_reg[0]_1 ;
  wire \FSM_sequential_current_state_reg[0]_2 ;
  wire [0:0]Q;
  wire coinciden;
  wire [1:0]current_state_reg;
  wire [1:0]i;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_onehot_current_state[2]_i_1 
       (.I0(current_state_reg[1]),
        .I1(Q),
        .I2(current_state_reg[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \FSM_onehot_current_state[3]_i_2 
       (.I0(Q),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hF0DFF0D0)) 
    \FSM_sequential_current_state[0]_i_1 
       (.I0(coinciden),
        .I1(\FSM_sequential_current_state_reg[0]_2 ),
        .I2(current_state_reg[0]),
        .I3(current_state_reg[1]),
        .I4(\FSM_sequential_current_state_reg[0]_1 ),
        .O(\FSM_sequential_current_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \FSM_sequential_current_state[1]_i_1 
       (.I0(current_state_reg[1]),
        .I1(current_state_reg[0]),
        .I2(\FSM_sequential_current_state_reg[0]_2 ),
        .O(\FSM_sequential_current_state[1]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "s1:01,iSTATE:10,s0:00,iSTATE0:11" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_current_state_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(\FSM_sequential_current_state[0]_i_1_n_0 ),
        .Q(current_state_reg[0]));
  (* FSM_ENCODED_STATES = "s1:01,iSTATE:10,s0:00,iSTATE0:11" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_current_state_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .Q(current_state_reg[1]));
  LUT2 #(
    .INIT(4'h2)) 
    \count[19]_i_1__2 
       (.I0(current_state_reg[0]),
        .I1(current_state_reg[1]),
        .O(E));
  LUT5 #(
    .INIT(32'hFFF70008)) 
    \i[0]_i_1 
       (.I0(\FSM_sequential_current_state_reg[0]_1 ),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(i[1]),
        .I4(i[0]),
        .O(\FSM_sequential_current_state_reg[0]_0 ));
endmodule

module fsm_visualizer
   (Q,
    \FSM_onehot_current_state_reg[1]_0 ,
    ctrl_bus_OBUF,
    \FSM_onehot_current_state_reg[3]_0 ,
    \FSM_onehot_current_state_reg[3]_1 ,
    \FSM_onehot_current_state_reg[3]_2 ,
    \FSM_onehot_current_state_reg[1]_1 ,
    current_state_reg,
    \ctrl_bus[7] ,
    \ctrl_bus[7]_0 ,
    \ctrl_bus_OBUF[7]_inst_i_1_0 ,
    \ctrl_bus_OBUF[7]_inst_i_1_1 ,
    D,
    CLK,
    \FSM_onehot_current_state_reg[0]_0 );
  output [0:0]Q;
  output \FSM_onehot_current_state_reg[1]_0 ;
  output [7:0]ctrl_bus_OBUF;
  output \FSM_onehot_current_state_reg[3]_0 ;
  output \FSM_onehot_current_state_reg[3]_1 ;
  output \FSM_onehot_current_state_reg[3]_2 ;
  output \FSM_onehot_current_state_reg[1]_1 ;
  input [1:0]current_state_reg;
  input [7:0]\ctrl_bus[7] ;
  input [7:0]\ctrl_bus[7]_0 ;
  input [7:0]\ctrl_bus_OBUF[7]_inst_i_1_0 ;
  input [7:0]\ctrl_bus_OBUF[7]_inst_i_1_1 ;
  input [1:0]D;
  input CLK;
  input \FSM_onehot_current_state_reg[0]_0 ;

  wire CLK;
  wire [1:0]D;
  wire \FSM_onehot_current_state[3]_i_1_n_0 ;
  wire \FSM_onehot_current_state_reg[0]_0 ;
  wire \FSM_onehot_current_state_reg[1]_0 ;
  wire \FSM_onehot_current_state_reg[1]_1 ;
  wire \FSM_onehot_current_state_reg[3]_0 ;
  wire \FSM_onehot_current_state_reg[3]_1 ;
  wire \FSM_onehot_current_state_reg[3]_2 ;
  wire [0:0]Q;
  wire [7:0]\ctrl_bus[7] ;
  wire [7:0]\ctrl_bus[7]_0 ;
  wire [7:0]ctrl_bus_OBUF;
  wire \ctrl_bus_OBUF[0]_inst_i_2_n_0 ;
  wire \ctrl_bus_OBUF[1]_inst_i_2_n_0 ;
  wire \ctrl_bus_OBUF[2]_inst_i_2_n_0 ;
  wire \ctrl_bus_OBUF[3]_inst_i_2_n_0 ;
  wire \ctrl_bus_OBUF[4]_inst_i_2_n_0 ;
  wire \ctrl_bus_OBUF[5]_inst_i_2_n_0 ;
  wire \ctrl_bus_OBUF[6]_inst_i_2_n_0 ;
  wire [7:0]\ctrl_bus_OBUF[7]_inst_i_1_0 ;
  wire [7:0]\ctrl_bus_OBUF[7]_inst_i_1_1 ;
  wire \ctrl_bus_OBUF[7]_inst_i_2_n_0 ;
  wire \ctrl_bus_OBUF[7]_inst_i_3_n_0 ;
  wire [1:0]current_state_reg;
  wire [3:0]fsm2Output;

  LUT4 #(
    .INIT(16'hACA0)) 
    \FSM_onehot_current_state[3]_i_1 
       (.I0(Q),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(fsm2Output[3]),
        .O(\FSM_onehot_current_state[3]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_current_state_reg[0] 
       (.C(CLK),
        .CE(\FSM_onehot_current_state[3]_i_1_n_0 ),
        .D(1'b0),
        .PRE(\FSM_onehot_current_state_reg[0]_0 ),
        .Q(fsm2Output[3]));
  (* FSM_ENCODED_STATES = "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[1] 
       (.C(CLK),
        .CE(\FSM_onehot_current_state[3]_i_1_n_0 ),
        .CLR(\FSM_onehot_current_state_reg[0]_0 ),
        .D(fsm2Output[3]),
        .Q(Q));
  (* FSM_ENCODED_STATES = "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[2] 
       (.C(CLK),
        .CE(\FSM_onehot_current_state[3]_i_1_n_0 ),
        .CLR(\FSM_onehot_current_state_reg[0]_0 ),
        .D(D[0]),
        .Q(fsm2Output[1]));
  (* FSM_ENCODED_STATES = "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[3] 
       (.C(CLK),
        .CE(\FSM_onehot_current_state[3]_i_1_n_0 ),
        .CLR(\FSM_onehot_current_state_reg[0]_0 ),
        .D(D[1]),
        .Q(fsm2Output[0]));
  LUT5 #(
    .INIT(32'hFFF8F8F8)) 
    \ctrl_bus_OBUF[0]_inst_i_1 
       (.I0(\ctrl_bus[7] [0]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[0]_inst_i_2_n_0 ),
        .I3(\ctrl_bus_OBUF[7]_inst_i_1_1 [0]),
        .I4(\FSM_onehot_current_state_reg[3]_1 ),
        .O(ctrl_bus_OBUF[0]));
  LUT6 #(
    .INIT(64'h00000000000C0A00)) 
    \ctrl_bus_OBUF[0]_inst_i_2 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_0 [0]),
        .I1(\ctrl_bus[7]_0 [0]),
        .I2(fsm2Output[0]),
        .I3(fsm2Output[3]),
        .I4(fsm2Output[1]),
        .I5(Q),
        .O(\ctrl_bus_OBUF[0]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFF8F8F8)) 
    \ctrl_bus_OBUF[1]_inst_i_1 
       (.I0(\ctrl_bus[7] [1]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[1]_inst_i_2_n_0 ),
        .I3(\FSM_onehot_current_state_reg[3]_0 ),
        .I4(\ctrl_bus[7]_0 [1]),
        .O(ctrl_bus_OBUF[1]));
  LUT6 #(
    .INIT(64'h0002030000020000)) 
    \ctrl_bus_OBUF[1]_inst_i_2 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_0 [1]),
        .I1(fsm2Output[0]),
        .I2(fsm2Output[1]),
        .I3(Q),
        .I4(fsm2Output[3]),
        .I5(\ctrl_bus_OBUF[7]_inst_i_1_1 [1]),
        .O(\ctrl_bus_OBUF[1]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hF8F8FFF8)) 
    \ctrl_bus_OBUF[2]_inst_i_1 
       (.I0(\ctrl_bus[7] [2]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[2]_inst_i_2_n_0 ),
        .I3(\ctrl_bus_OBUF[7]_inst_i_1_0 [2]),
        .I4(\FSM_onehot_current_state_reg[3]_2 ),
        .O(ctrl_bus_OBUF[2]));
  LUT6 #(
    .INIT(64'h0000000A000C0000)) 
    \ctrl_bus_OBUF[2]_inst_i_2 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_1 [2]),
        .I1(\ctrl_bus[7]_0 [2]),
        .I2(fsm2Output[0]),
        .I3(fsm2Output[3]),
        .I4(fsm2Output[1]),
        .I5(Q),
        .O(\ctrl_bus_OBUF[2]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFF8F8F8)) 
    \ctrl_bus_OBUF[3]_inst_i_1 
       (.I0(\ctrl_bus[7] [3]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[3]_inst_i_2_n_0 ),
        .I3(\FSM_onehot_current_state_reg[3]_0 ),
        .I4(\ctrl_bus[7]_0 [3]),
        .O(ctrl_bus_OBUF[3]));
  LUT6 #(
    .INIT(64'h0000000A000C0000)) 
    \ctrl_bus_OBUF[3]_inst_i_2 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_0 [3]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_1_1 [3]),
        .I2(fsm2Output[0]),
        .I3(fsm2Output[1]),
        .I4(Q),
        .I5(fsm2Output[3]),
        .O(\ctrl_bus_OBUF[3]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFF8F8F8)) 
    \ctrl_bus_OBUF[4]_inst_i_1 
       (.I0(\ctrl_bus[7] [4]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[4]_inst_i_2_n_0 ),
        .I3(\ctrl_bus_OBUF[7]_inst_i_1_1 [4]),
        .I4(\FSM_onehot_current_state_reg[3]_1 ),
        .O(ctrl_bus_OBUF[4]));
  LUT6 #(
    .INIT(64'h00000000000C0A00)) 
    \ctrl_bus_OBUF[4]_inst_i_2 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_0 [4]),
        .I1(\ctrl_bus[7]_0 [4]),
        .I2(fsm2Output[0]),
        .I3(fsm2Output[3]),
        .I4(fsm2Output[1]),
        .I5(Q),
        .O(\ctrl_bus_OBUF[4]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \ctrl_bus_OBUF[4]_inst_i_3 
       (.I0(fsm2Output[0]),
        .I1(fsm2Output[1]),
        .I2(Q),
        .I3(fsm2Output[3]),
        .O(\FSM_onehot_current_state_reg[3]_1 ));
  LUT5 #(
    .INIT(32'hFFF8F8F8)) 
    \ctrl_bus_OBUF[5]_inst_i_1 
       (.I0(\ctrl_bus[7] [5]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[5]_inst_i_2_n_0 ),
        .I3(\FSM_onehot_current_state_reg[3]_0 ),
        .I4(\ctrl_bus[7]_0 [5]),
        .O(ctrl_bus_OBUF[5]));
  LUT6 #(
    .INIT(64'h0002030000020000)) 
    \ctrl_bus_OBUF[5]_inst_i_2 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_0 [5]),
        .I1(fsm2Output[0]),
        .I2(fsm2Output[1]),
        .I3(Q),
        .I4(fsm2Output[3]),
        .I5(\ctrl_bus_OBUF[7]_inst_i_1_1 [5]),
        .O(\ctrl_bus_OBUF[5]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hF8F8FFF8)) 
    \ctrl_bus_OBUF[6]_inst_i_1 
       (.I0(\ctrl_bus[7] [6]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[6]_inst_i_2_n_0 ),
        .I3(\ctrl_bus_OBUF[7]_inst_i_1_0 [6]),
        .I4(\FSM_onehot_current_state_reg[3]_2 ),
        .O(ctrl_bus_OBUF[6]));
  LUT6 #(
    .INIT(64'h0000000A000C0000)) 
    \ctrl_bus_OBUF[6]_inst_i_2 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_1 [6]),
        .I1(\ctrl_bus[7]_0 [6]),
        .I2(fsm2Output[0]),
        .I3(fsm2Output[3]),
        .I4(fsm2Output[1]),
        .I5(Q),
        .O(\ctrl_bus_OBUF[6]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \ctrl_bus_OBUF[6]_inst_i_3 
       (.I0(fsm2Output[0]),
        .I1(fsm2Output[1]),
        .I2(Q),
        .I3(fsm2Output[3]),
        .O(\FSM_onehot_current_state_reg[3]_2 ));
  LUT5 #(
    .INIT(32'hFFF8F8F8)) 
    \ctrl_bus_OBUF[7]_inst_i_1 
       (.I0(\ctrl_bus[7] [7]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ),
        .I2(\ctrl_bus_OBUF[7]_inst_i_3_n_0 ),
        .I3(\FSM_onehot_current_state_reg[3]_0 ),
        .I4(\ctrl_bus[7]_0 [7]),
        .O(ctrl_bus_OBUF[7]));
  LUT4 #(
    .INIT(16'hFEEB)) 
    \ctrl_bus_OBUF[7]_inst_i_2 
       (.I0(fsm2Output[0]),
        .I1(fsm2Output[3]),
        .I2(fsm2Output[1]),
        .I3(Q),
        .O(\ctrl_bus_OBUF[7]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000A000C0000)) 
    \ctrl_bus_OBUF[7]_inst_i_3 
       (.I0(\ctrl_bus_OBUF[7]_inst_i_1_0 [7]),
        .I1(\ctrl_bus_OBUF[7]_inst_i_1_1 [7]),
        .I2(fsm2Output[0]),
        .I3(fsm2Output[1]),
        .I4(Q),
        .I5(fsm2Output[3]),
        .O(\ctrl_bus_OBUF[7]_inst_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \ctrl_bus_OBUF[7]_inst_i_4 
       (.I0(fsm2Output[0]),
        .I1(fsm2Output[3]),
        .I2(fsm2Output[1]),
        .I3(Q),
        .O(\FSM_onehot_current_state_reg[3]_0 ));
  LUT4 #(
    .INIT(16'hFFED)) 
    \display_bus_OBUF[2]_inst_i_2 
       (.I0(Q),
        .I1(fsm2Output[1]),
        .I2(fsm2Output[3]),
        .I3(fsm2Output[0]),
        .O(\FSM_onehot_current_state_reg[1]_1 ));
  LUT1 #(
    .INIT(2'h1)) 
    en_point_i_1
       (.I0(Q),
        .O(\FSM_onehot_current_state_reg[1]_0 ));
endmodule

module pass_storage
   (done,
    \i_reg[2]_0 ,
    \pass_completa_reg[3][1]_0 ,
    CLK,
    AR,
    \pass_completa_reg[0][0]_0 ,
    current_state_reg,
    done_reg_0,
    \i_reg[0]_0 ,
    D);
  output done;
  output [1:0]\i_reg[2]_0 ;
  output \pass_completa_reg[3][1]_0 ;
  input CLK;
  input [0:0]AR;
  input \pass_completa_reg[0][0]_0 ;
  input [1:0]current_state_reg;
  input done_reg_0;
  input \i_reg[0]_0 ;
  input [3:0]D;

  wire [0:0]AR;
  wire CLK;
  wire [3:0]D;
  wire [1:0]current_state_reg;
  wire done;
  wire done_i_1_n_0;
  wire done_reg_0;
  wire [1:1]i;
  wire \i[1]_i_1_n_0 ;
  wire \i[2]_i_1_n_0 ;
  wire \i_reg[0]_0 ;
  wire [1:0]\i_reg[2]_0 ;
  wire inter_comparacion_reg_i_2_n_0;
  wire inter_comparacion_reg_i_3_n_0;
  wire [3:0]\pass_completa_reg[0] ;
  wire \pass_completa_reg[0]0 ;
  wire \pass_completa_reg[0][0]_0 ;
  wire [3:0]\pass_completa_reg[1] ;
  wire \pass_completa_reg[1]0 ;
  wire [3:0]\pass_completa_reg[2] ;
  wire \pass_completa_reg[2]0 ;
  wire [3:0]\pass_completa_reg[3] ;
  wire \pass_completa_reg[3]0 ;
  wire \pass_completa_reg[3][1]_0 ;

  LUT5 #(
    .INIT(32'hAAFF8000)) 
    done_i_1
       (.I0(\i_reg[2]_0 [1]),
        .I1(\i_reg[2]_0 [0]),
        .I2(i),
        .I3(done_reg_0),
        .I4(done),
        .O(done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    done_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(done_i_1_n_0),
        .Q(done));
  LUT6 #(
    .INIT(64'hFFFFFFF700000008)) 
    \i[1]_i_1 
       (.I0(\pass_completa_reg[0][0]_0 ),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(\i_reg[2]_0 [1]),
        .I4(\i_reg[2]_0 [0]),
        .I5(i),
        .O(\i[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF00FF00FF00FF08)) 
    \i[2]_i_1 
       (.I0(\pass_completa_reg[0][0]_0 ),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(\i_reg[2]_0 [1]),
        .I4(i),
        .I5(\i_reg[2]_0 [0]),
        .O(\i[2]_i_1_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \i_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\i_reg[0]_0 ),
        .PRE(AR),
        .Q(\i_reg[2]_0 [0]));
  FDPE #(
    .INIT(1'b1)) 
    \i_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\i[1]_i_1_n_0 ),
        .PRE(AR),
        .Q(i));
  FDCE #(
    .INIT(1'b0)) 
    \i_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(\i[2]_i_1_n_0 ),
        .Q(\i_reg[2]_0 [1]));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    inter_comparacion_reg_i_1
       (.I0(inter_comparacion_reg_i_2_n_0),
        .I1(\pass_completa_reg[3] [1]),
        .I2(\pass_completa_reg[3] [0]),
        .I3(\pass_completa_reg[3] [3]),
        .I4(\pass_completa_reg[3] [2]),
        .I5(inter_comparacion_reg_i_3_n_0),
        .O(\pass_completa_reg[3][1]_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    inter_comparacion_reg_i_2
       (.I0(\pass_completa_reg[0] [1]),
        .I1(\pass_completa_reg[0] [0]),
        .I2(\pass_completa_reg[1] [2]),
        .I3(\pass_completa_reg[1] [3]),
        .I4(\pass_completa_reg[0] [3]),
        .I5(\pass_completa_reg[0] [2]),
        .O(inter_comparacion_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    inter_comparacion_reg_i_3
       (.I0(\pass_completa_reg[2] [3]),
        .I1(\pass_completa_reg[2] [2]),
        .I2(\pass_completa_reg[2] [0]),
        .I3(\pass_completa_reg[2] [1]),
        .I4(\pass_completa_reg[1] [0]),
        .I5(\pass_completa_reg[1] [1]),
        .O(inter_comparacion_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \pass_completa[0][3]_i_1 
       (.I0(\pass_completa_reg[0][0]_0 ),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(\i_reg[2]_0 [0]),
        .I4(i),
        .I5(\i_reg[2]_0 [1]),
        .O(\pass_completa_reg[0]0 ));
  LUT6 #(
    .INIT(64'h0000000000080000)) 
    \pass_completa[1][3]_i_1 
       (.I0(\pass_completa_reg[0][0]_0 ),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(\i_reg[2]_0 [1]),
        .I4(\i_reg[2]_0 [0]),
        .I5(i),
        .O(\pass_completa_reg[1]0 ));
  LUT6 #(
    .INIT(64'h0000000000080000)) 
    \pass_completa[2][3]_i_1 
       (.I0(\pass_completa_reg[0][0]_0 ),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(\i_reg[2]_0 [1]),
        .I4(i),
        .I5(\i_reg[2]_0 [0]),
        .O(\pass_completa_reg[2]0 ));
  LUT6 #(
    .INIT(64'h0008000000000000)) 
    \pass_completa[3][3]_i_1 
       (.I0(\pass_completa_reg[0][0]_0 ),
        .I1(current_state_reg[0]),
        .I2(current_state_reg[1]),
        .I3(\i_reg[2]_0 [1]),
        .I4(i),
        .I5(\i_reg[2]_0 [0]),
        .O(\pass_completa_reg[3]0 ));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[0][0] 
       (.C(CLK),
        .CE(\pass_completa_reg[0]0 ),
        .CLR(AR),
        .D(D[0]),
        .Q(\pass_completa_reg[0] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[0][1] 
       (.C(CLK),
        .CE(\pass_completa_reg[0]0 ),
        .CLR(AR),
        .D(D[1]),
        .Q(\pass_completa_reg[0] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[0][2] 
       (.C(CLK),
        .CE(\pass_completa_reg[0]0 ),
        .CLR(AR),
        .D(D[2]),
        .Q(\pass_completa_reg[0] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[0][3] 
       (.C(CLK),
        .CE(\pass_completa_reg[0]0 ),
        .CLR(AR),
        .D(D[3]),
        .Q(\pass_completa_reg[0] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[1][0] 
       (.C(CLK),
        .CE(\pass_completa_reg[1]0 ),
        .CLR(AR),
        .D(D[0]),
        .Q(\pass_completa_reg[1] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[1][1] 
       (.C(CLK),
        .CE(\pass_completa_reg[1]0 ),
        .CLR(AR),
        .D(D[1]),
        .Q(\pass_completa_reg[1] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[1][2] 
       (.C(CLK),
        .CE(\pass_completa_reg[1]0 ),
        .CLR(AR),
        .D(D[2]),
        .Q(\pass_completa_reg[1] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[1][3] 
       (.C(CLK),
        .CE(\pass_completa_reg[1]0 ),
        .CLR(AR),
        .D(D[3]),
        .Q(\pass_completa_reg[1] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[2][0] 
       (.C(CLK),
        .CE(\pass_completa_reg[2]0 ),
        .CLR(AR),
        .D(D[0]),
        .Q(\pass_completa_reg[2] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[2][1] 
       (.C(CLK),
        .CE(\pass_completa_reg[2]0 ),
        .CLR(AR),
        .D(D[1]),
        .Q(\pass_completa_reg[2] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[2][2] 
       (.C(CLK),
        .CE(\pass_completa_reg[2]0 ),
        .CLR(AR),
        .D(D[2]),
        .Q(\pass_completa_reg[2] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[2][3] 
       (.C(CLK),
        .CE(\pass_completa_reg[2]0 ),
        .CLR(AR),
        .D(D[3]),
        .Q(\pass_completa_reg[2] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[3][0] 
       (.C(CLK),
        .CE(\pass_completa_reg[3]0 ),
        .CLR(AR),
        .D(D[0]),
        .Q(\pass_completa_reg[3] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[3][1] 
       (.C(CLK),
        .CE(\pass_completa_reg[3]0 ),
        .CLR(AR),
        .D(D[1]),
        .Q(\pass_completa_reg[3] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[3][2] 
       (.C(CLK),
        .CE(\pass_completa_reg[3]0 ),
        .CLR(AR),
        .D(D[2]),
        .Q(\pass_completa_reg[3] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \pass_completa_reg[3][3] 
       (.C(CLK),
        .CE(\pass_completa_reg[3]0 ),
        .CLR(AR),
        .D(D[3]),
        .Q(\pass_completa_reg[3] [3]));
endmodule

module selected
   (\FSM_onehot_current_state_reg[4]_0 ,
    \FSM_onehot_current_state_reg[4]_1 ,
    \FSM_onehot_current_state_reg[4]_2 ,
    \FSM_onehot_current_state_reg[4]_3 ,
    display_bus_OBUF,
    \count_reg[2] ,
    \FSM_onehot_current_state_reg[4]_4 ,
    Q,
    \FSM_onehot_current_state_reg[4]_5 ,
    \FSM_onehot_current_state_reg[2]_0 ,
    \display_bus_OBUF[6]_inst_i_1_0 ,
    \display_bus_OBUF[2]_inst_i_1 ,
    \display_bus_OBUF[1]_inst_i_1 ,
    \display_bus[5] ,
    \FSM_onehot_current_state_reg[0]_0 ,
    \FSM_onehot_current_state_reg[0]_1 ,
    \display_bus_OBUF[1]_inst_i_1_0 ,
    \display_bus[5]_0 ,
    \display_bus[5]_1 ,
    \display_bus[6] ,
    \display_bus_OBUF[3]_inst_i_2 ,
    \display_bus_OBUF[3]_inst_i_2_0 ,
    CLK,
    \FSM_onehot_current_state_reg[4]_6 );
  output \FSM_onehot_current_state_reg[4]_0 ;
  output \FSM_onehot_current_state_reg[4]_1 ;
  output \FSM_onehot_current_state_reg[4]_2 ;
  output \FSM_onehot_current_state_reg[4]_3 ;
  output [1:0]display_bus_OBUF;
  output \count_reg[2] ;
  output \FSM_onehot_current_state_reg[4]_4 ;
  output [0:0]Q;
  output \FSM_onehot_current_state_reg[4]_5 ;
  output \FSM_onehot_current_state_reg[2]_0 ;
  input \display_bus_OBUF[6]_inst_i_1_0 ;
  input \display_bus_OBUF[2]_inst_i_1 ;
  input \display_bus_OBUF[1]_inst_i_1 ;
  input \display_bus[5] ;
  input \FSM_onehot_current_state_reg[0]_0 ;
  input [0:0]\FSM_onehot_current_state_reg[0]_1 ;
  input \display_bus_OBUF[1]_inst_i_1_0 ;
  input \display_bus[5]_0 ;
  input \display_bus[5]_1 ;
  input \display_bus[6] ;
  input \display_bus_OBUF[3]_inst_i_2 ;
  input \display_bus_OBUF[3]_inst_i_2_0 ;
  input CLK;
  input \FSM_onehot_current_state_reg[4]_6 ;

  wire CLK;
  wire \FSM_onehot_current_state_reg[0]_0 ;
  wire [0:0]\FSM_onehot_current_state_reg[0]_1 ;
  wire \FSM_onehot_current_state_reg[2]_0 ;
  wire \FSM_onehot_current_state_reg[4]_0 ;
  wire \FSM_onehot_current_state_reg[4]_1 ;
  wire \FSM_onehot_current_state_reg[4]_2 ;
  wire \FSM_onehot_current_state_reg[4]_3 ;
  wire \FSM_onehot_current_state_reg[4]_4 ;
  wire \FSM_onehot_current_state_reg[4]_5 ;
  wire \FSM_onehot_current_state_reg[4]_6 ;
  wire \FSM_onehot_current_state_reg_n_0_[0] ;
  wire \FSM_onehot_current_state_reg_n_0_[1] ;
  wire \FSM_onehot_current_state_reg_n_0_[2] ;
  wire \FSM_onehot_current_state_reg_n_0_[3] ;
  wire [0:0]Q;
  wire \count_reg[2] ;
  wire current_state;
  wire \display_bus[5] ;
  wire \display_bus[5]_0 ;
  wire \display_bus[5]_1 ;
  wire \display_bus[6] ;
  wire [1:0]display_bus_OBUF;
  wire \display_bus_OBUF[1]_inst_i_1 ;
  wire \display_bus_OBUF[1]_inst_i_1_0 ;
  wire \display_bus_OBUF[2]_inst_i_1 ;
  wire \display_bus_OBUF[3]_inst_i_2 ;
  wire \display_bus_OBUF[3]_inst_i_2_0 ;
  wire \display_bus_OBUF[5]_inst_i_2_n_0 ;
  wire \display_bus_OBUF[5]_inst_i_3_n_0 ;
  wire \display_bus_OBUF[6]_inst_i_1_0 ;
  wire \display_bus_OBUF[6]_inst_i_3_n_0 ;
  wire \display_bus_OBUF[6]_inst_i_5_n_0 ;

  LUT6 #(
    .INIT(64'h8888888888888880)) 
    \FSM_onehot_current_state[4]_i_1 
       (.I0(\FSM_onehot_current_state_reg[0]_0 ),
        .I1(\FSM_onehot_current_state_reg[0]_1 ),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_current_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_current_state_reg_n_0_[2] ),
        .I5(\FSM_onehot_current_state_reg_n_0_[3] ),
        .O(current_state));
  (* FSM_ENCODED_STATES = "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001," *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_current_state_reg[0] 
       (.C(CLK),
        .CE(current_state),
        .D(1'b0),
        .PRE(\FSM_onehot_current_state_reg[4]_6 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[0] ));
  (* FSM_ENCODED_STATES = "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001," *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[1] 
       (.C(CLK),
        .CE(current_state),
        .CLR(\FSM_onehot_current_state_reg[4]_6 ),
        .D(\FSM_onehot_current_state_reg_n_0_[0] ),
        .Q(\FSM_onehot_current_state_reg_n_0_[1] ));
  (* FSM_ENCODED_STATES = "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001," *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[2] 
       (.C(CLK),
        .CE(current_state),
        .CLR(\FSM_onehot_current_state_reg[4]_6 ),
        .D(\FSM_onehot_current_state_reg_n_0_[1] ),
        .Q(\FSM_onehot_current_state_reg_n_0_[2] ));
  (* FSM_ENCODED_STATES = "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001," *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[3] 
       (.C(CLK),
        .CE(current_state),
        .CLR(\FSM_onehot_current_state_reg[4]_6 ),
        .D(\FSM_onehot_current_state_reg_n_0_[2] ),
        .Q(\FSM_onehot_current_state_reg_n_0_[3] ));
  (* FSM_ENCODED_STATES = "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001," *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[4] 
       (.C(CLK),
        .CE(current_state),
        .CLR(\FSM_onehot_current_state_reg[4]_6 ),
        .D(\FSM_onehot_current_state_reg_n_0_[3] ),
        .Q(Q));
  LUT6 #(
    .INIT(64'hA0AFCFCFA0AFC0C0)) 
    \display_bus_OBUF[1]_inst_i_2 
       (.I0(\FSM_onehot_current_state_reg[4]_2 ),
        .I1(\display_bus_OBUF[1]_inst_i_1 ),
        .I2(\display_bus[5] ),
        .I3(\FSM_onehot_current_state_reg[4]_1 ),
        .I4(\display_bus_OBUF[6]_inst_i_1_0 ),
        .I5(\display_bus_OBUF[1]_inst_i_1_0 ),
        .O(\FSM_onehot_current_state_reg[4]_3 ));
  LUT6 #(
    .INIT(64'h47CF77FFFFFFFFFF)) 
    \display_bus_OBUF[2]_inst_i_4 
       (.I0(\FSM_onehot_current_state_reg[4]_1 ),
        .I1(\display_bus_OBUF[6]_inst_i_1_0 ),
        .I2(\display_bus_OBUF[2]_inst_i_1 ),
        .I3(\FSM_onehot_current_state_reg[4]_2 ),
        .I4(\display_bus_OBUF[1]_inst_i_1 ),
        .I5(\display_bus[5] ),
        .O(\FSM_onehot_current_state_reg[4]_0 ));
  LUT4 #(
    .INIT(16'hBBBA)) 
    \display_bus_OBUF[4]_inst_i_7 
       (.I0(Q),
        .I1(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[3] ),
        .I3(\FSM_onehot_current_state_reg_n_0_[2] ),
        .O(\FSM_onehot_current_state_reg[4]_5 ));
  LUT4 #(
    .INIT(16'hFF5E)) 
    \display_bus_OBUF[4]_inst_i_8 
       (.I0(\FSM_onehot_current_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[3] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(Q),
        .O(\FSM_onehot_current_state_reg[2]_0 ));
  LUT5 #(
    .INIT(32'h0A220888)) 
    \display_bus_OBUF[5]_inst_i_1 
       (.I0(\display_bus[5]_0 ),
        .I1(\display_bus_OBUF[5]_inst_i_2_n_0 ),
        .I2(\display_bus_OBUF[5]_inst_i_3_n_0 ),
        .I3(\display_bus[5] ),
        .I4(\display_bus[5]_1 ),
        .O(display_bus_OBUF[0]));
  LUT5 #(
    .INIT(32'hFFFACCFA)) 
    \display_bus_OBUF[5]_inst_i_2 
       (.I0(\display_bus_OBUF[1]_inst_i_1 ),
        .I1(\FSM_onehot_current_state_reg[4]_2 ),
        .I2(\display_bus_OBUF[2]_inst_i_1 ),
        .I3(\display_bus_OBUF[6]_inst_i_1_0 ),
        .I4(\FSM_onehot_current_state_reg[4]_1 ),
        .O(\display_bus_OBUF[5]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h335FFF5F)) 
    \display_bus_OBUF[5]_inst_i_3 
       (.I0(\display_bus_OBUF[1]_inst_i_1 ),
        .I1(\FSM_onehot_current_state_reg[4]_2 ),
        .I2(\display_bus_OBUF[2]_inst_i_1 ),
        .I3(\display_bus_OBUF[6]_inst_i_1_0 ),
        .I4(\FSM_onehot_current_state_reg[4]_1 ),
        .O(\display_bus_OBUF[5]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4500EF00FFFFFFFF)) 
    \display_bus_OBUF[6]_inst_i_1 
       (.I0(\display_bus[5]_1 ),
        .I1(\display_bus_OBUF[6]_inst_i_3_n_0 ),
        .I2(\count_reg[2] ),
        .I3(\display_bus[5]_0 ),
        .I4(\display_bus_OBUF[6]_inst_i_5_n_0 ),
        .I5(\display_bus[6] ),
        .O(display_bus_OBUF[1]));
  LUT6 #(
    .INIT(64'hDEDFDEDFDFDFDFDE)) 
    \display_bus_OBUF[6]_inst_i_12 
       (.I0(\display_bus_OBUF[3]_inst_i_2 ),
        .I1(Q),
        .I2(\display_bus_OBUF[3]_inst_i_2_0 ),
        .I3(\FSM_onehot_current_state_reg_n_0_[2] ),
        .I4(\FSM_onehot_current_state_reg_n_0_[3] ),
        .I5(\FSM_onehot_current_state_reg_n_0_[1] ),
        .O(\FSM_onehot_current_state_reg[4]_2 ));
  LUT6 #(
    .INIT(64'h1010101110100001)) 
    \display_bus_OBUF[6]_inst_i_14 
       (.I0(\display_bus_OBUF[3]_inst_i_2_0 ),
        .I1(Q),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_current_state_reg_n_0_[3] ),
        .I4(\FSM_onehot_current_state_reg_n_0_[2] ),
        .I5(\display_bus_OBUF[3]_inst_i_2 ),
        .O(\FSM_onehot_current_state_reg[4]_1 ));
  LUT5 #(
    .INIT(32'h000ACC0A)) 
    \display_bus_OBUF[6]_inst_i_3 
       (.I0(\display_bus_OBUF[1]_inst_i_1 ),
        .I1(\FSM_onehot_current_state_reg[4]_2 ),
        .I2(\display_bus_OBUF[2]_inst_i_1 ),
        .I3(\display_bus_OBUF[6]_inst_i_1_0 ),
        .I4(\FSM_onehot_current_state_reg[4]_1 ),
        .O(\display_bus_OBUF[6]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFCFFFCFAA)) 
    \display_bus_OBUF[6]_inst_i_4 
       (.I0(\display_bus_OBUF[1]_inst_i_1 ),
        .I1(\FSM_onehot_current_state_reg[4]_2 ),
        .I2(\FSM_onehot_current_state_reg[4]_1 ),
        .I3(\display_bus_OBUF[6]_inst_i_1_0 ),
        .I4(\display_bus_OBUF[1]_inst_i_1_0 ),
        .I5(\display_bus[5] ),
        .O(\count_reg[2] ));
  LUT6 #(
    .INIT(64'hFFBBFCB8FFFFFFFF)) 
    \display_bus_OBUF[6]_inst_i_5 
       (.I0(\FSM_onehot_current_state_reg[4]_1 ),
        .I1(\display_bus_OBUF[6]_inst_i_1_0 ),
        .I2(\display_bus_OBUF[2]_inst_i_1 ),
        .I3(\FSM_onehot_current_state_reg[4]_2 ),
        .I4(\display_bus_OBUF[1]_inst_i_1 ),
        .I5(\display_bus[5] ),
        .O(\display_bus_OBUF[6]_inst_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \display_bus_OBUF[6]_inst_i_8 
       (.I0(Q),
        .I1(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[3] ),
        .I3(\FSM_onehot_current_state_reg_n_0_[2] ),
        .O(\FSM_onehot_current_state_reg[4]_4 ));
endmodule

module top
   (display_bus_OBUF,
    en_point,
    ctrl_bus_OBUF,
    CLK,
    button_IBUF,
    D,
    reset_IBUF);
  output [6:0]display_bus_OBUF;
  output en_point;
  output [7:0]ctrl_bus_OBUF;
  input CLK;
  input button_IBUF;
  input [3:0]D;
  input reset_IBUF;

  wire CLK;
  wire [3:0]D;
  wire button_IBUF;
  wire coinciden;
  wire [7:0]ctrl_bus_OBUF;
  wire [1:0]current_state_reg;
  wire [6:0]display_bus_OBUF;
  wire done;
  wire en_point;
  wire fsm1_n_0;
  wire fsm1_n_4;
  wire fsm1_n_5;
  wire [2:2]fsm2Output;
  wire [2:0]i;
  wire reg_clave_n_3;
  wire reset_IBUF;
  wire start_cuenta;
  wire [1:0]\tiempo[0]_0 ;
  wire [1:0]\tiempo[1]_1 ;
  wire [1:0]\tiempo[2]_2 ;
  wire [1:0]\tiempo[3]_3 ;
  wire timer_n_10;
  wire timer_n_11;
  wire timer_n_12;
  wire timer_n_4;
  wire timer_n_9;
  wire trat_boton_n_0;
  wire trat_boton_n_1;
  wire visualizador_n_1;
  wire visualizador_n_10;
  wire visualizador_n_11;

  comparador_completo comparator
       (.AR(visualizador_n_1),
        .\FSM_sequential_current_state_reg[0] (reg_clave_n_3),
        .coinciden(coinciden),
        .done(done));
  fsm fsm1
       (.AR(visualizador_n_1),
        .CLK(CLK),
        .D({fsm1_n_4,fsm1_n_5}),
        .E(start_cuenta),
        .\FSM_sequential_current_state_reg[0]_0 (fsm1_n_0),
        .\FSM_sequential_current_state_reg[0]_1 (trat_boton_n_1),
        .\FSM_sequential_current_state_reg[0]_2 (timer_n_4),
        .Q(fsm2Output),
        .coinciden(coinciden),
        .current_state_reg(current_state_reg),
        .i({i[2],i[0]}));
  pass_storage reg_clave
       (.AR(visualizador_n_1),
        .CLK(CLK),
        .D(D),
        .current_state_reg(current_state_reg),
        .done(done),
        .done_reg_0(trat_boton_n_0),
        .\i_reg[0]_0 (fsm1_n_0),
        .\i_reg[2]_0 ({i[2],i[0]}),
        .\pass_completa_reg[0][0]_0 (trat_boton_n_1),
        .\pass_completa_reg[3][1]_0 (reg_clave_n_3));
  top_countdown_time timer
       (.AR(visualizador_n_1),
        .CLK(CLK),
        .E(start_cuenta),
        .Q(\tiempo[0]_0 ),
        .\count_reg[1] (\tiempo[2]_2 ),
        .\count_reg[1]_0 (\tiempo[3]_3 ),
        .\count_reg[1]_1 (\tiempo[1]_1 ),
        .\count_reg[1]_2 (timer_n_10),
        .\count_reg[1]_3 (timer_n_12),
        .\count_reg[2] (timer_n_9),
        .\count_reg[3] (timer_n_4),
        .\count_reg[3]_0 (timer_n_11),
        .\display_bus_OBUF[2]_inst_i_4 (visualizador_n_10),
        .\display_bus_OBUF[2]_inst_i_4_0 (visualizador_n_11),
        .done(done));
  button_treatment trat_boton
       (.AR(visualizador_n_1),
        .CLK(CLK),
        .E(start_cuenta),
        .button_IBUF(button_IBUF),
        .reset_IBUF(reset_IBUF),
        .\sreg_reg[2] (trat_boton_n_1),
        .\sreg_reg[3] (trat_boton_n_0));
  Visualizer visualizador
       (.AR(visualizador_n_1),
        .CLK(CLK),
        .D({fsm1_n_4,fsm1_n_5}),
        .\FSM_onehot_current_state_reg[0] (trat_boton_n_1),
        .Q(fsm2Output),
        .ctrl_bus_OBUF(ctrl_bus_OBUF),
        .current_state_reg(current_state_reg),
        .display_bus_OBUF(display_bus_OBUF),
        .\display_bus_OBUF[1]_inst_i_1 (timer_n_9),
        .\display_bus_OBUF[1]_inst_i_1_0 (timer_n_10),
        .\display_bus_OBUF[2]_inst_i_1 (timer_n_12),
        .\display_bus_OBUF[4]_inst_i_2 (\tiempo[0]_0 ),
        .\display_bus_OBUF[4]_inst_i_2_0 (\tiempo[1]_1 ),
        .\display_bus_OBUF[4]_inst_i_2_1 (\tiempo[2]_2 ),
        .\display_bus_OBUF[4]_inst_i_2_2 (\tiempo[3]_3 ),
        .\display_bus_OBUF[6]_inst_i_1 (timer_n_11),
        .en_point(en_point),
        .reset_IBUF(reset_IBUF),
        .\sel_i_reg[4] (visualizador_n_11),
        .\sel_i_reg[7] (visualizador_n_10));
endmodule

module top_countdown_time
   (Q,
    \count_reg[1] ,
    \count_reg[3] ,
    \count_reg[1]_0 ,
    \count_reg[1]_1 ,
    \count_reg[2] ,
    \count_reg[1]_2 ,
    \count_reg[3]_0 ,
    \count_reg[1]_3 ,
    done,
    \display_bus_OBUF[2]_inst_i_4 ,
    \display_bus_OBUF[2]_inst_i_4_0 ,
    E,
    CLK,
    AR);
  output [1:0]Q;
  output [1:0]\count_reg[1] ;
  output \count_reg[3] ;
  output [1:0]\count_reg[1]_0 ;
  output [1:0]\count_reg[1]_1 ;
  output \count_reg[2] ;
  output \count_reg[1]_2 ;
  output \count_reg[3]_0 ;
  output \count_reg[1]_3 ;
  input done;
  input \display_bus_OBUF[2]_inst_i_4 ;
  input \display_bus_OBUF[2]_inst_i_4_0 ;
  input [0:0]E;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire [1:0]Q;
  wire S_STROBE;
  wire [1:0]\count_reg[1] ;
  wire [1:0]\count_reg[1]_0 ;
  wire [1:0]\count_reg[1]_1 ;
  wire \count_reg[1]_2 ;
  wire \count_reg[1]_3 ;
  wire \count_reg[2] ;
  wire \count_reg[3] ;
  wire \count_reg[3]_0 ;
  wire \display_bus_OBUF[2]_inst_i_4 ;
  wire \display_bus_OBUF[2]_inst_i_4_0 ;
  wire done;

  top_timer counter
       (.AR(AR),
        .CLK(CLK),
        .E(S_STROBE),
        .Q(Q),
        .\count_reg[1] (\count_reg[1] ),
        .\count_reg[1]_0 (\count_reg[1]_1 ),
        .\count_reg[1]_1 (\count_reg[1]_0 ),
        .\count_reg[1]_2 (\count_reg[1]_2 ),
        .\count_reg[1]_3 (\count_reg[1]_3 ),
        .\count_reg[2] (\count_reg[2] ),
        .\count_reg[3] (\count_reg[3] ),
        .\count_reg[3]_0 (\count_reg[3]_0 ),
        .\display_bus_OBUF[2]_inst_i_4 (\display_bus_OBUF[2]_inst_i_4 ),
        .\display_bus_OBUF[2]_inst_i_4_0 (\display_bus_OBUF[2]_inst_i_4_0 ),
        .done(done));
  STROBE strobe_gen
       (.AR(AR),
        .CLK(CLK),
        .E(S_STROBE),
        .\count_reg[0]_0 (E));
endmodule

module top_timer
   (Q,
    \count_reg[1] ,
    \count_reg[3] ,
    \count_reg[1]_0 ,
    \count_reg[1]_1 ,
    \count_reg[2] ,
    \count_reg[1]_2 ,
    \count_reg[3]_0 ,
    \count_reg[1]_3 ,
    done,
    E,
    \display_bus_OBUF[2]_inst_i_4 ,
    \display_bus_OBUF[2]_inst_i_4_0 ,
    CLK,
    AR);
  output [1:0]Q;
  output [1:0]\count_reg[1] ;
  output \count_reg[3] ;
  output [1:0]\count_reg[1]_0 ;
  output [1:0]\count_reg[1]_1 ;
  output \count_reg[2] ;
  output \count_reg[1]_2 ;
  output \count_reg[3]_0 ;
  output \count_reg[1]_3 ;
  input done;
  input [0:0]E;
  input \display_bus_OBUF[2]_inst_i_4 ;
  input \display_bus_OBUF[2]_inst_i_4_0 ;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CENTESIMAS;
  wire CLK;
  wire DECIMAS;
  wire [0:0]E;
  wire [1:0]Q;
  wire SEC;
  wire [1:0]\count_reg[1] ;
  wire [1:0]\count_reg[1]_0 ;
  wire [1:0]\count_reg[1]_1 ;
  wire \count_reg[1]_2 ;
  wire \count_reg[1]_3 ;
  wire \count_reg[2] ;
  wire \count_reg[3] ;
  wire \count_reg[3]_0 ;
  wire \display_bus_OBUF[2]_inst_i_4 ;
  wire \display_bus_OBUF[2]_inst_i_4_0 ;
  wire done;
  wire t_cent_n_0;
  wire t_dec_n_5;
  wire t_sec_n_5;
  wire [3:2]\tiempo[1]_1 ;
  wire [3:2]\tiempo[2]_2 ;
  wire [3:2]\tiempo[3]_3 ;

  BCDCNTR t_cent
       (.AR(AR),
        .CLK(CLK),
        .E(CENTESIMAS),
        .Q(Q),
        .\count_reg[0]_0 (E),
        .\count_reg[1]_0 (\count_reg[1]_2 ),
        .\count_reg[1]_1 (\count_reg[1]_3 ),
        .\count_reg[2]_0 (\count_reg[2] ),
        .\count_reg[3]_0 (t_cent_n_0),
        .\count_reg[3]_1 (\count_reg[3]_0 ),
        .\display_bus_OBUF[2]_inst_i_4 (\display_bus_OBUF[2]_inst_i_4 ),
        .\display_bus_OBUF[2]_inst_i_4_0 (\display_bus_OBUF[2]_inst_i_4_0 ),
        .\display_bus_OBUF[6]_inst_i_2 ({\tiempo[1]_1 ,\count_reg[1]_0 [1]}),
        .\display_bus_OBUF[6]_inst_i_2_0 ({\tiempo[2]_2 ,\count_reg[1] [1]}),
        .\display_bus_OBUF[6]_inst_i_2_1 ({\tiempo[3]_3 ,\count_reg[1]_1 [1]}));
  BCDCNTR_6 t_dec
       (.AR(AR),
        .CLK(CLK),
        .E(DECIMAS),
        .Q({\tiempo[1]_1 ,\count_reg[1]_0 }),
        .\count_reg[2]_0 (t_dec_n_5),
        .\count_reg[3]_0 (CENTESIMAS));
  BCDCNTR_7 t_dsec
       (.AR(AR),
        .CLK(CLK),
        .E(SEC),
        .\FSM_sequential_current_state_reg[0] (t_cent_n_0),
        .\FSM_sequential_current_state_reg[0]_0 (t_sec_n_5),
        .\FSM_sequential_current_state_reg[0]_1 (t_dec_n_5),
        .Q({\tiempo[3]_3 ,\count_reg[1]_1 }),
        .\count_reg[3]_0 (\count_reg[3] ),
        .done(done));
  BCDCNTR_8 t_sec
       (.AR(AR),
        .CLK(CLK),
        .E(SEC),
        .Q({\tiempo[2]_2 ,\count_reg[1] }),
        .\count_reg[2]_0 (t_sec_n_5),
        .\count_reg[3]_0 (CENTESIMAS),
        .\count_reg[3]_1 (t_dec_n_5),
        .\count_reg[3]_2 (DECIMAS));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
