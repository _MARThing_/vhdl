-- Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2021.1 (win64) Build 3247384 Thu Jun 10 19:36:33 MDT 2021
-- Date        : Mon Dec 20 18:43:35 2021
-- Host        : DESKTOP-02QC6R6 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/marti/Documents/4to/SED/TRABAJO/project_1/project_1.sim/sim_1/impl/func/xsim/top_tb_func_impl.vhd
-- Design      : TOP_Implementation
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity BCDCNTR is
  port (
    \count_reg[3]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \count_reg[2]_0\ : out STD_LOGIC;
    \count_reg[0]_0\ : out STD_LOGIC;
    \count_reg[1]_0\ : out STD_LOGIC;
    \count_reg[0]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus_OBUF[2]_inst_i_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[2]_inst_i_3_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[2]_inst_i_3_1\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_3_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[2]_inst_i_3_3\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end BCDCNTR;

architecture STRUCTURE of BCDCNTR is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \count[0]_i_1_n_0\ : STD_LOGIC;
  signal \count[1]_i_1_n_0\ : STD_LOGIC;
  signal \count[2]_i_1_n_0\ : STD_LOGIC;
  signal \count[3]_i_2_n_0\ : STD_LOGIC;
  signal \tiempo[0]_4\ : STD_LOGIC_VECTOR ( 2 to 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_5\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \count[0]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \count[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \count[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \count[3]_i_1__1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \count[3]_i_2\ : label is "soft_lutpair5";
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
\FSM_sequential_current_state[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(2),
      I1 => \tiempo[0]_4\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \count_reg[3]_0\
    );
\count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \count[0]_i_1_n_0\
    );
\count[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"29C2"
    )
        port map (
      I0 => \tiempo[0]_4\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => \count[1]_i_1_n_0\
    );
\count[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D402"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \tiempo[0]_4\(2),
      O => \count[2]_i_1_n_0\
    );
\count[3]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \count_reg[0]_1\(0),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \tiempo[0]_4\(2),
      I4 => \^q\(2),
      O => E(0)
    );
\count[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0601"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \tiempo[0]_4\(2),
      I3 => \^q\(2),
      O => \count[3]_i_2_n_0\
    );
\count_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_1\(0),
      D => \count[0]_i_1_n_0\,
      PRE => AR(0),
      Q => \^q\(0)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_1\(0),
      CLR => AR(0),
      D => \count[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_1\(0),
      CLR => AR(0),
      D => \count[2]_i_1_n_0\,
      Q => \tiempo[0]_4\(2)
    );
\count_reg[3]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_1\(0),
      D => \count[3]_i_2_n_0\,
      PRE => AR(0),
      Q => \^q\(2)
    );
\display_bus_OBUF[1]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \^q\(0),
      I1 => \display_bus_OBUF[2]_inst_i_3\(0),
      I2 => \display_bus_OBUF[2]_inst_i_3_3\,
      I3 => \display_bus_OBUF[2]_inst_i_3_0\(0),
      I4 => \display_bus_OBUF[2]_inst_i_3_1\,
      I5 => \display_bus_OBUF[2]_inst_i_3_2\(0),
      O => \count_reg[0]_0\
    );
\display_bus_OBUF[2]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(1),
      I1 => \display_bus_OBUF[2]_inst_i_3\(1),
      I2 => \display_bus_OBUF[2]_inst_i_3_3\,
      I3 => \display_bus_OBUF[2]_inst_i_3_0\(1),
      I4 => \display_bus_OBUF[2]_inst_i_3_1\,
      I5 => \display_bus_OBUF[2]_inst_i_3_2\(1),
      O => \count_reg[1]_0\
    );
\display_bus_OBUF[6]_inst_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"553355330F000FFF"
    )
        port map (
      I0 => \tiempo[0]_4\(2),
      I1 => \display_bus_OBUF[2]_inst_i_3\(2),
      I2 => \display_bus_OBUF[2]_inst_i_3_0\(2),
      I3 => \display_bus_OBUF[2]_inst_i_3_1\,
      I4 => \display_bus_OBUF[2]_inst_i_3_2\(2),
      I5 => \display_bus_OBUF[2]_inst_i_3_3\,
      O => \count_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity BCDCNTR_6 is
  port (
    \count_reg[3]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \count_reg[1]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \count_reg[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \count_reg[3]_2\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of BCDCNTR_6 : entity is "BCDCNTR";
end BCDCNTR_6;

architecture STRUCTURE of BCDCNTR_6 is
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \count[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[3]_i_2__2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_6\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \count[0]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \count[1]_i_1__2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \count[2]_i_1__2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \count[3]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \count[3]_i_2__2\ : label is "soft_lutpair8";
begin
  Q(3 downto 0) <= \^q\(3 downto 0);
\FSM_sequential_current_state[1]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \count_reg[3]_0\
    );
\count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \count[0]_i_1__0_n_0\
    );
\count[1]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"29C2"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(3),
      O => \count[1]_i_1__2_n_0\
    );
\count[2]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D402"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => \count[2]_i_1__2_n_0\
    );
\count[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \count_reg[3]_1\(0),
      I5 => \count_reg[3]_2\,
      O => E(0)
    );
\count[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \count_reg[3]_1\(0),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^q\(3),
      O => \count_reg[1]_0\(0)
    );
\count[3]_i_2__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0601"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      O => \count[3]_i_2__2_n_0\
    );
\count_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => \count_reg[3]_1\(0),
      D => \count[0]_i_1__0_n_0\,
      PRE => AR(0),
      Q => \^q\(0)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[3]_1\(0),
      CLR => AR(0),
      D => \count[1]_i_1__2_n_0\,
      Q => \^q\(1)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[3]_1\(0),
      CLR => AR(0),
      D => \count[2]_i_1__2_n_0\,
      Q => \^q\(2)
    );
\count_reg[3]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => \count_reg[3]_1\(0),
      D => \count[3]_i_2__2_n_0\,
      PRE => AR(0),
      Q => \^q\(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity BCDCNTR_7 is
  port (
    \count_reg[3]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \FSM_sequential_current_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_1\ : in STD_LOGIC;
    done : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of BCDCNTR_7 : entity is "BCDCNTR";
end BCDCNTR_7;

architecture STRUCTURE of BCDCNTR_7 is
  signal \FSM_sequential_current_state[1]_i_4_n_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \count[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \count[3]_i_2__0_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_4\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \count[1]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \count[2]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \count[3]_i_2__0\ : label is "soft_lutpair10";
begin
  Q(3 downto 0) <= \^q\(3 downto 0);
\FSM_sequential_current_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFFFFB"
    )
        port map (
      I0 => \^q\(3),
      I1 => \FSM_sequential_current_state[1]_i_4_n_0\,
      I2 => \FSM_sequential_current_state_reg[0]\,
      I3 => \FSM_sequential_current_state_reg[0]_0\,
      I4 => \FSM_sequential_current_state_reg[0]_1\,
      I5 => done,
      O => \count_reg[3]_0\
    );
\FSM_sequential_current_state[1]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \FSM_sequential_current_state[1]_i_4_n_0\
    );
\count[0]_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \count[0]_i_1__2_n_0\
    );
\count[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"29C2"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(3),
      O => \count[1]_i_1__0_n_0\
    );
\count[2]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D402"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => \count[2]_i_1__0_n_0\
    );
\count[3]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0601"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      O => \count[3]_i_2__0_n_0\
    );
\count_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \count[0]_i_1__2_n_0\,
      PRE => AR(0),
      Q => \^q\(0)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \count[1]_i_1__0_n_0\,
      Q => \^q\(1)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \count[2]_i_1__0_n_0\,
      Q => \^q\(2)
    );
\count_reg[3]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \count[3]_i_2__0_n_0\,
      PRE => AR(0),
      Q => \^q\(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity BCDCNTR_8 is
  port (
    \count_reg[3]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of BCDCNTR_8 : entity is "BCDCNTR";
end BCDCNTR_8;

architecture STRUCTURE of BCDCNTR_8 is
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \count[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \count[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \count[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \count[3]_i_2__1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[0]_i_1__1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \count[2]_i_1__1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \count[3]_i_2__1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \count[3]_i_3__0\ : label is "soft_lutpair11";
begin
  Q(3 downto 0) <= \^q\(3 downto 0);
\count[0]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \count[0]_i_1__1_n_0\
    );
\count[1]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"29C2"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(3),
      O => \count[1]_i_1__1_n_0\
    );
\count[2]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D402"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => \count[2]_i_1__1_n_0\
    );
\count[3]_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0601"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      O => \count[3]_i_2__1_n_0\
    );
\count[3]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \count_reg[3]_0\
    );
\count_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \count[0]_i_1__1_n_0\,
      PRE => AR(0),
      Q => \^q\(0)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \count[1]_i_1__1_n_0\,
      Q => \^q\(1)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \count[2]_i_1__1_n_0\,
      Q => \^q\(2)
    );
\count_reg[3]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \count[3]_i_2__1_n_0\,
      PRE => AR(0),
      Q => \^q\(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity EDGEDTCTR is
  port (
    \sreg_reg[3]_0\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end EDGEDTCTR;

architecture STRUCTURE of EDGEDTCTR is
  signal sreg : STD_LOGIC_VECTOR ( 4 downto 0 );
begin
\FSM_sequential_current_state[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      I2 => sreg(0),
      I3 => sreg(4),
      I4 => sreg(3),
      O => \sreg_reg[2]_0\
    );
done_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => E(0),
      I1 => sreg(3),
      I2 => sreg(4),
      I3 => sreg(0),
      I4 => sreg(1),
      I5 => sreg(2),
      O => \sreg_reg[3]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
\sreg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => sreg(2),
      Q => sreg(3),
      R => '0'
    );
\sreg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => sreg(3),
      Q => sreg(4),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity OnOff is
  port (
    \aux_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC
  );
end OnOff;

architecture STRUCTURE of OnOff is
  signal \^aux_reg[7]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
  \aux_reg[7]_0\(7 downto 0) <= \^aux_reg[7]_0\(7 downto 0);
\aux[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(0),
      O => p_0_in(0)
    );
\aux[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(1),
      O => p_0_in(1)
    );
\aux[2]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(2),
      O => p_0_in(2)
    );
\aux[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(3),
      O => p_0_in(3)
    );
\aux[4]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(4),
      O => p_0_in(4)
    );
\aux[5]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(5),
      O => p_0_in(5)
    );
\aux[6]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(6),
      O => p_0_in(6)
    );
\aux[7]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(7),
      O => p_0_in(7)
    );
\aux_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(0),
      Q => \^aux_reg[7]_0\(0),
      R => '0'
    );
\aux_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(1),
      Q => \^aux_reg[7]_0\(1),
      R => '0'
    );
\aux_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(2),
      Q => \^aux_reg[7]_0\(2),
      R => '0'
    );
\aux_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(3),
      Q => \^aux_reg[7]_0\(3),
      R => '0'
    );
\aux_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(4),
      Q => \^aux_reg[7]_0\(4),
      R => '0'
    );
\aux_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(5),
      Q => \^aux_reg[7]_0\(5),
      R => '0'
    );
\aux_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(6),
      Q => \^aux_reg[7]_0\(6),
      R => '0'
    );
\aux_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => p_0_in(7),
      Q => \^aux_reg[7]_0\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity OnOff_2 is
  port (
    \aux_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of OnOff_2 : entity is "OnOff";
end OnOff_2;

architecture STRUCTURE of OnOff_2 is
  signal \aux[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \aux[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \aux[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \aux[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \aux[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \aux[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \aux[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \aux[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \^aux_reg[7]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
  \aux_reg[7]_0\(7 downto 0) <= \^aux_reg[7]_0\(7 downto 0);
\aux[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(0),
      O => \aux[0]_i_1__0_n_0\
    );
\aux[1]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(1),
      O => \aux[1]_i_1__0_n_0\
    );
\aux[2]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(2),
      O => \aux[2]_i_1__0_n_0\
    );
\aux[3]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(3),
      O => \aux[3]_i_1__0_n_0\
    );
\aux[4]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(4),
      O => \aux[4]_i_1__0_n_0\
    );
\aux[5]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(5),
      O => \aux[5]_i_1__0_n_0\
    );
\aux[6]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(6),
      O => \aux[6]_i_1__0_n_0\
    );
\aux[7]_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(7),
      O => \aux[7]_i_2__0_n_0\
    );
\aux_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[0]_i_1__0_n_0\,
      Q => \^aux_reg[7]_0\(0),
      R => '0'
    );
\aux_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[1]_i_1__0_n_0\,
      Q => \^aux_reg[7]_0\(1),
      R => '0'
    );
\aux_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[2]_i_1__0_n_0\,
      Q => \^aux_reg[7]_0\(2),
      R => '0'
    );
\aux_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[3]_i_1__0_n_0\,
      Q => \^aux_reg[7]_0\(3),
      R => '0'
    );
\aux_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[4]_i_1__0_n_0\,
      Q => \^aux_reg[7]_0\(4),
      R => '0'
    );
\aux_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[5]_i_1__0_n_0\,
      Q => \^aux_reg[7]_0\(5),
      R => '0'
    );
\aux_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[6]_i_1__0_n_0\,
      Q => \^aux_reg[7]_0\(6),
      R => '0'
    );
\aux_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[7]_i_2__0_n_0\,
      Q => \^aux_reg[7]_0\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity OnOff_4 is
  port (
    \aux_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of OnOff_4 : entity is "OnOff";
end OnOff_4;

architecture STRUCTURE of OnOff_4 is
  signal \aux[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \aux[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \aux[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \aux[3]_i_1__1_n_0\ : STD_LOGIC;
  signal \aux[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \aux[5]_i_1__1_n_0\ : STD_LOGIC;
  signal \aux[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \aux[7]_i_2__1_n_0\ : STD_LOGIC;
  signal \^aux_reg[7]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
  \aux_reg[7]_0\(7 downto 0) <= \^aux_reg[7]_0\(7 downto 0);
\aux[0]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(0),
      O => \aux[0]_i_1__1_n_0\
    );
\aux[1]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(1),
      O => \aux[1]_i_1__1_n_0\
    );
\aux[2]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(2),
      O => \aux[2]_i_1__1_n_0\
    );
\aux[3]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(3),
      O => \aux[3]_i_1__1_n_0\
    );
\aux[4]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(4),
      O => \aux[4]_i_1__1_n_0\
    );
\aux[5]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(5),
      O => \aux[5]_i_1__1_n_0\
    );
\aux[6]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(6),
      O => \aux[6]_i_1__1_n_0\
    );
\aux[7]_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^aux_reg[7]_0\(7),
      O => \aux[7]_i_2__1_n_0\
    );
\aux_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[0]_i_1__1_n_0\,
      Q => \^aux_reg[7]_0\(0),
      R => '0'
    );
\aux_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[1]_i_1__1_n_0\,
      Q => \^aux_reg[7]_0\(1),
      R => '0'
    );
\aux_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[2]_i_1__1_n_0\,
      Q => \^aux_reg[7]_0\(2),
      R => '0'
    );
\aux_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[3]_i_1__1_n_0\,
      Q => \^aux_reg[7]_0\(3),
      R => '0'
    );
\aux_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[4]_i_1__1_n_0\,
      Q => \^aux_reg[7]_0\(4),
      R => '0'
    );
\aux_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[5]_i_1__1_n_0\,
      Q => \^aux_reg[7]_0\(5),
      R => '0'
    );
\aux_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[6]_i_1__1_n_0\,
      Q => \^aux_reg[7]_0\(6),
      R => '0'
    );
\aux_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \aux[7]_i_2__1_n_0\,
      Q => \^aux_reg[7]_0\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SCANNER is
  port (
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    display_bus_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \sel_i_reg[4]_0\ : out STD_LOGIC;
    \sel_i_reg[4]_1\ : out STD_LOGIC;
    \sel_i_reg[4]_2\ : out STD_LOGIC;
    \sel_i_reg[7]_0\ : out STD_LOGIC;
    \display_bus[6]\ : in STD_LOGIC;
    \display_bus[2]\ : in STD_LOGIC;
    \display_bus[1]\ : in STD_LOGIC;
    \display_bus[1]_0\ : in STD_LOGIC;
    \display_bus[2]_0\ : in STD_LOGIC;
    \display_bus[3]\ : in STD_LOGIC;
    \display_bus[6]_0\ : in STD_LOGIC;
    \sel2vis[1]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus_OBUF[6]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \sel2vis[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sel2vis[0]_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus_OBUF[0]_inst_i_1_0\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1_0\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1_1\ : in STD_LOGIC;
    \sel2vis[2]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    \sel_i_reg[0]_0\ : in STD_LOGIC
  );
end SCANNER;

architecture STRUCTURE of SCANNER is
  signal \^q\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \display_bus_OBUF[2]_inst_i_4_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[4]_inst_i_2_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[4]_inst_i_3_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[4]_inst_i_4_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[5]_inst_i_2_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_10_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_11_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_12_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_13_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_14_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_15_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_16_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_17_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_18_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_19_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_20_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_21_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_22_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_2_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_3_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[6]_inst_i_5_n_0\ : STD_LOGIC;
  signal \^sel_i_reg[4]_0\ : STD_LOGIC;
  signal \^sel_i_reg[4]_1\ : STD_LOGIC;
  signal \^sel_i_reg[4]_2\ : STD_LOGIC;
  signal \^sel_i_reg[7]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \display_bus_OBUF[6]_inst_i_22\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \display_bus_OBUF[6]_inst_i_8\ : label is "soft_lutpair42";
begin
  Q(7 downto 0) <= \^q\(7 downto 0);
  \sel_i_reg[4]_0\ <= \^sel_i_reg[4]_0\;
  \sel_i_reg[4]_1\ <= \^sel_i_reg[4]_1\;
  \sel_i_reg[4]_2\ <= \^sel_i_reg[4]_2\;
  \sel_i_reg[7]_0\ <= \^sel_i_reg[7]_0\;
\display_bus_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20028020"
    )
        port map (
      I0 => \display_bus[6]\,
      I1 => \display_bus_OBUF[6]_inst_i_3_n_0\,
      I2 => \^sel_i_reg[4]_0\,
      I3 => \display_bus_OBUF[6]_inst_i_2_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_5_n_0\,
      O => display_bus_OBUF(0)
    );
\display_bus_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAAABFFAAAAAAAA"
    )
        port map (
      I0 => \display_bus[2]\,
      I1 => \display_bus_OBUF[6]_inst_i_2_n_0\,
      I2 => \display_bus[1]\,
      I3 => \display_bus_OBUF[6]_inst_i_5_n_0\,
      I4 => \display_bus[1]_0\,
      I5 => \display_bus[6]\,
      O => display_bus_OBUF(1)
    );
\display_bus_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BABABABFAAAAAAAA"
    )
        port map (
      I0 => \display_bus[2]\,
      I1 => \display_bus[2]_0\,
      I2 => \display_bus_OBUF[6]_inst_i_5_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_2_n_0\,
      I4 => \display_bus_OBUF[2]_inst_i_4_n_0\,
      I5 => \display_bus[6]\,
      O => display_bus_OBUF(2)
    );
\display_bus_OBUF[2]_inst_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \^sel_i_reg[4]_0\,
      I1 => \display_bus_OBUF[2]_inst_i_1_0\,
      I2 => \^sel_i_reg[4]_2\,
      I3 => \display_bus_OBUF[2]_inst_i_1_1\,
      O => \display_bus_OBUF[2]_inst_i_4_n_0\
    );
\display_bus_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CECCECCECECCCCCC"
    )
        port map (
      I0 => \display_bus[6]\,
      I1 => \display_bus[3]\,
      I2 => \display_bus_OBUF[6]_inst_i_2_n_0\,
      I3 => \^sel_i_reg[4]_0\,
      I4 => \display_bus_OBUF[6]_inst_i_3_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_n_0\,
      O => display_bus_OBUF(3)
    );
\display_bus_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"080C0888"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_n_0\,
      I1 => \display_bus[6]\,
      I2 => \display_bus_OBUF[4]_inst_i_2_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_3_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_2_n_0\,
      O => display_bus_OBUF(4)
    );
\display_bus_OBUF[4]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2F202F2F2F202020"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_15_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_14_n_0\,
      I2 => \^sel_i_reg[4]_2\,
      I3 => \display_bus_OBUF[4]_inst_i_3_n_0\,
      I4 => \^sel_i_reg[7]_0\,
      I5 => \display_bus_OBUF[4]_inst_i_4_n_0\,
      O => \display_bus_OBUF[4]_inst_i_2_n_0\
    );
\display_bus_OBUF[4]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000051FFFFFF5D"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_0\(0),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_1\(0),
      O => \display_bus_OBUF[4]_inst_i_3_n_0\
    );
\display_bus_OBUF[4]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000051FFFFFF5D"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_2\(0),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_3\(0),
      O => \display_bus_OBUF[4]_inst_i_4_n_0\
    );
\display_bus_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B010A000B01000A0"
    )
        port map (
      I0 => \^sel_i_reg[4]_0\,
      I1 => \display_bus_OBUF[5]_inst_i_2_n_0\,
      I2 => \display_bus[6]\,
      I3 => \display_bus_OBUF[6]_inst_i_5_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_3_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_2_n_0\,
      O => display_bus_OBUF(5)
    );
\display_bus_OBUF[5]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C00A0000C00AFFFF"
    )
        port map (
      I0 => \sel2vis[3]_0\(0),
      I1 => \sel2vis[0]_3\(0),
      I2 => \^sel_i_reg[7]_0\,
      I3 => \^sel_i_reg[4]_1\,
      I4 => \^sel_i_reg[4]_2\,
      I5 => \display_bus_OBUF[0]_inst_i_1_0\,
      O => \display_bus_OBUF[5]_inst_i_2_n_0\
    );
\display_bus_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"62010000FFFFFFFF"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_2_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_3_n_0\,
      I2 => \^sel_i_reg[4]_0\,
      I3 => \display_bus_OBUF[6]_inst_i_5_n_0\,
      I4 => \display_bus[6]\,
      I5 => \display_bus[6]_0\,
      O => display_bus_OBUF(6)
    );
\display_bus_OBUF[6]_inst_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011000000000000"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \^sel_i_reg[4]_2\,
      I4 => \^sel_i_reg[7]_0\,
      I5 => \sel2vis[0]_3\(0),
      O => \display_bus_OBUF[6]_inst_i_10_n_0\
    );
\display_bus_OBUF[6]_inst_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010110000"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \^sel_i_reg[4]_2\,
      I4 => \sel2vis[2]_1\(0),
      I5 => \^sel_i_reg[7]_0\,
      O => \display_bus_OBUF[6]_inst_i_11_n_0\
    );
\display_bus_OBUF[6]_inst_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFAE000000A2"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_0\(1),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_1\(1),
      O => \display_bus_OBUF[6]_inst_i_12_n_0\
    );
\display_bus_OBUF[6]_inst_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFAE000000A2"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_2\(1),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_3\(1),
      O => \display_bus_OBUF[6]_inst_i_13_n_0\
    );
\display_bus_OBUF[6]_inst_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EFEE0000"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \^sel_i_reg[4]_2\,
      I4 => \sel2vis[3]_0\(0),
      I5 => \^sel_i_reg[7]_0\,
      O => \display_bus_OBUF[6]_inst_i_14_n_0\
    );
\display_bus_OBUF[6]_inst_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011FFFFFFFFFFFF"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \^sel_i_reg[4]_2\,
      I4 => \^sel_i_reg[7]_0\,
      I5 => \sel2vis[1]_2\(0),
      O => \display_bus_OBUF[6]_inst_i_15_n_0\
    );
\display_bus_OBUF[6]_inst_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFAE000000A2"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_0\(0),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_1\(0),
      O => \display_bus_OBUF[6]_inst_i_16_n_0\
    );
\display_bus_OBUF[6]_inst_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFAE000000A2"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_2\(0),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_3\(0),
      O => \display_bus_OBUF[6]_inst_i_17_n_0\
    );
\display_bus_OBUF[6]_inst_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000051FFFFFF5D"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_0\(2),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_1\(2),
      O => \display_bus_OBUF[6]_inst_i_18_n_0\
    );
\display_bus_OBUF[6]_inst_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000051FFFFFF5D"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_5_2\(2),
      I1 => \^sel_i_reg[4]_2\,
      I2 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I3 => \display_bus_OBUF[6]_inst_i_22_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      I5 => \display_bus_OBUF[6]_inst_i_5_3\(2),
      O => \display_bus_OBUF[6]_inst_i_19_n_0\
    );
\display_bus_OBUF[6]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3FF5FFFF3FF50000"
    )
        port map (
      I0 => \sel2vis[3]_0\(0),
      I1 => \sel2vis[0]_3\(0),
      I2 => \^sel_i_reg[7]_0\,
      I3 => \^sel_i_reg[4]_1\,
      I4 => \^sel_i_reg[4]_2\,
      I5 => \display_bus_OBUF[0]_inst_i_1_0\,
      O => \display_bus_OBUF[6]_inst_i_2_n_0\
    );
\display_bus_OBUF[6]_inst_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \display_bus_OBUF[6]_inst_i_20_n_0\
    );
\display_bus_OBUF[6]_inst_i_21\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5D000000"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(4),
      I4 => \^q\(6),
      O => \display_bus_OBUF[6]_inst_i_21_n_0\
    );
\display_bus_OBUF[6]_inst_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(6),
      I2 => \^q\(7),
      O => \display_bus_OBUF[6]_inst_i_22_n_0\
    );
\display_bus_OBUF[6]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFE0EFEFEFE0E0E0"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_10_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_11_n_0\,
      I2 => \^sel_i_reg[4]_2\,
      I3 => \display_bus_OBUF[6]_inst_i_12_n_0\,
      I4 => \^sel_i_reg[7]_0\,
      I5 => \display_bus_OBUF[6]_inst_i_13_n_0\,
      O => \display_bus_OBUF[6]_inst_i_3_n_0\
    );
\display_bus_OBUF[6]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0BFBFBFB0B0B0"
    )
        port map (
      I0 => \display_bus_OBUF[6]_inst_i_14_n_0\,
      I1 => \display_bus_OBUF[6]_inst_i_15_n_0\,
      I2 => \^sel_i_reg[4]_2\,
      I3 => \display_bus_OBUF[6]_inst_i_16_n_0\,
      I4 => \^sel_i_reg[7]_0\,
      I5 => \display_bus_OBUF[6]_inst_i_17_n_0\,
      O => \^sel_i_reg[4]_0\
    );
\display_bus_OBUF[6]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0BFB0FFFFF0F0"
    )
        port map (
      I0 => \^sel_i_reg[4]_1\,
      I1 => \sel2vis[1]_2\(0),
      I2 => \^sel_i_reg[4]_2\,
      I3 => \display_bus_OBUF[6]_inst_i_18_n_0\,
      I4 => \display_bus_OBUF[6]_inst_i_19_n_0\,
      I5 => \^sel_i_reg[7]_0\,
      O => \display_bus_OBUF[6]_inst_i_5_n_0\
    );
\display_bus_OBUF[6]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000888888888888"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(6),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(5),
      I5 => \^q\(4),
      O => \^sel_i_reg[7]_0\
    );
\display_bus_OBUF[6]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000D0FF0000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \display_bus_OBUF[6]_inst_i_20_n_0\,
      I2 => \^q\(5),
      I3 => \^q\(6),
      I4 => \^q\(7),
      I5 => \display_bus_OBUF[6]_inst_i_21_n_0\,
      O => \^sel_i_reg[4]_1\
    );
\display_bus_OBUF[6]_inst_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(5),
      I2 => \^q\(7),
      I3 => \^q\(6),
      O => \^sel_i_reg[4]_2\
    );
\sel_i_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => \sel_i_reg[0]_0\,
      D => \^q\(7),
      Q => \^q\(0)
    );
\sel_i_reg[1]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \^q\(0),
      PRE => \sel_i_reg[0]_0\,
      Q => \^q\(1)
    );
\sel_i_reg[2]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \^q\(1),
      PRE => \sel_i_reg[0]_0\,
      Q => \^q\(2)
    );
\sel_i_reg[3]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \^q\(2),
      PRE => \sel_i_reg[0]_0\,
      Q => \^q\(3)
    );
\sel_i_reg[4]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \^q\(3),
      PRE => \sel_i_reg[0]_0\,
      Q => \^q\(4)
    );
\sel_i_reg[5]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \^q\(4),
      PRE => \sel_i_reg[0]_0\,
      Q => \^q\(5)
    );
\sel_i_reg[6]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \^q\(5),
      PRE => \sel_i_reg[0]_0\,
      Q => \^q\(6)
    );
\sel_i_reg[7]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \^q\(6),
      PRE => \sel_i_reg[0]_0\,
      Q => \^q\(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity STROBE is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \count_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end STROBE;

architecture STRUCTURE of STROBE is
  signal \count0__39_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \count0__39_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \count0__39_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \count0__39_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \count0__39_carry__0_n_0\ : STD_LOGIC;
  signal \count0__39_carry__0_n_4\ : STD_LOGIC;
  signal \count0__39_carry__0_n_5\ : STD_LOGIC;
  signal \count0__39_carry__0_n_6\ : STD_LOGIC;
  signal \count0__39_carry__0_n_7\ : STD_LOGIC;
  signal \count0__39_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \count0__39_carry__1_n_0\ : STD_LOGIC;
  signal \count0__39_carry__1_n_4\ : STD_LOGIC;
  signal \count0__39_carry__1_n_5\ : STD_LOGIC;
  signal \count0__39_carry__1_n_6\ : STD_LOGIC;
  signal \count0__39_carry__1_n_7\ : STD_LOGIC;
  signal \count0__39_carry__2_i_1_n_3\ : STD_LOGIC;
  signal \count0__39_carry__2_n_1\ : STD_LOGIC;
  signal \count0__39_carry__2_n_6\ : STD_LOGIC;
  signal \count0__39_carry__2_n_7\ : STD_LOGIC;
  signal \count0__39_carry_i_1_n_0\ : STD_LOGIC;
  signal \count0__39_carry_i_2_n_0\ : STD_LOGIC;
  signal \count0__39_carry_n_0\ : STD_LOGIC;
  signal \count0__39_carry_n_4\ : STD_LOGIC;
  signal \count0__39_carry_n_5\ : STD_LOGIC;
  signal \count0__39_carry_n_6\ : STD_LOGIC;
  signal \count0__39_carry_n_7\ : STD_LOGIC;
  signal \count0_carry__0_i_1__2_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_2__2_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_3__2_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_4__2_n_0\ : STD_LOGIC;
  signal \count0_carry__0_n_0\ : STD_LOGIC;
  signal \count0_carry__0_n_4\ : STD_LOGIC;
  signal \count0_carry__0_n_5\ : STD_LOGIC;
  signal \count0_carry__0_n_6\ : STD_LOGIC;
  signal \count0_carry__0_n_7\ : STD_LOGIC;
  signal \count0_carry__1_i_1__2_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_2__2_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_3__2_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_4__2_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_4\ : STD_LOGIC;
  signal \count0_carry__1_n_5\ : STD_LOGIC;
  signal \count0_carry__1_n_6\ : STD_LOGIC;
  signal \count0_carry__1_n_7\ : STD_LOGIC;
  signal \count0_carry__2_i_1__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_2__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_3__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_4__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_4\ : STD_LOGIC;
  signal \count0_carry__2_n_5\ : STD_LOGIC;
  signal \count0_carry__2_n_6\ : STD_LOGIC;
  signal \count0_carry__2_n_7\ : STD_LOGIC;
  signal \count0_carry__3_i_1__2_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_2__2_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_3__2_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_4__2_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_4\ : STD_LOGIC;
  signal \count0_carry__3_n_5\ : STD_LOGIC;
  signal \count0_carry__3_n_6\ : STD_LOGIC;
  signal \count0_carry__3_n_7\ : STD_LOGIC;
  signal \count0_carry_i_2__2_n_0\ : STD_LOGIC;
  signal \count0_carry_i_3__2_n_0\ : STD_LOGIC;
  signal \count0_carry_i_4__2_n_0\ : STD_LOGIC;
  signal count0_carry_n_0 : STD_LOGIC;
  signal count0_carry_n_4 : STD_LOGIC;
  signal count0_carry_n_5 : STD_LOGIC;
  signal count0_carry_n_6 : STD_LOGIC;
  signal count0_carry_n_7 : STD_LOGIC;
  signal count1 : STD_LOGIC_VECTOR ( 19 downto 0 );
  signal \count1_carry__0_n_0\ : STD_LOGIC;
  signal \count1_carry__1_n_0\ : STD_LOGIC;
  signal \count1_carry__2_n_0\ : STD_LOGIC;
  signal count1_carry_n_0 : STD_LOGIC;
  signal \count[10]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[11]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[12]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[13]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[14]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[15]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[16]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[17]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[18]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[19]_i_2_n_0\ : STD_LOGIC;
  signal \count[3]_i_3_n_0\ : STD_LOGIC;
  signal \count[3]_i_4_n_0\ : STD_LOGIC;
  signal \count[3]_i_5_n_0\ : STD_LOGIC;
  signal \count[3]_i_6_n_0\ : STD_LOGIC;
  signal \count[6]_i_1_n_0\ : STD_LOGIC;
  signal \count[7]_i_1_n_0\ : STD_LOGIC;
  signal \count[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \count[9]_i_1__2_n_0\ : STD_LOGIC;
  signal \count_reg_n_0_[0]\ : STD_LOGIC;
  signal \count_reg_n_0_[10]\ : STD_LOGIC;
  signal \count_reg_n_0_[11]\ : STD_LOGIC;
  signal \count_reg_n_0_[12]\ : STD_LOGIC;
  signal \count_reg_n_0_[13]\ : STD_LOGIC;
  signal \count_reg_n_0_[14]\ : STD_LOGIC;
  signal \count_reg_n_0_[15]\ : STD_LOGIC;
  signal \count_reg_n_0_[16]\ : STD_LOGIC;
  signal \count_reg_n_0_[17]\ : STD_LOGIC;
  signal \count_reg_n_0_[18]\ : STD_LOGIC;
  signal \count_reg_n_0_[19]\ : STD_LOGIC;
  signal \count_reg_n_0_[1]\ : STD_LOGIC;
  signal \count_reg_n_0_[2]\ : STD_LOGIC;
  signal \count_reg_n_0_[3]\ : STD_LOGIC;
  signal \count_reg_n_0_[4]\ : STD_LOGIC;
  signal \count_reg_n_0_[5]\ : STD_LOGIC;
  signal \count_reg_n_0_[6]\ : STD_LOGIC;
  signal \count_reg_n_0_[7]\ : STD_LOGIC;
  signal \count_reg_n_0_[8]\ : STD_LOGIC;
  signal \count_reg_n_0_[9]\ : STD_LOGIC;
  signal \NLW_count0__39_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__39_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__39_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__39_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0__39_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count0__39_carry__2_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_count0__39_carry__2_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_count0_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_count1_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count1_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \count0__39_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__39_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__39_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__39_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of count0_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__3\ : label is "SWEEP";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of count1_carry : label is 35;
  attribute OPT_MODIFIED of count1_carry : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__0\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__0\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__1\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__2\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__2\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__3\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__3\ : label is "SWEEP";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[10]_i_1__2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \count[11]_i_1__2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \count[12]_i_1__2\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \count[13]_i_1__2\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \count[14]_i_1__2\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \count[15]_i_1__2\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \count[16]_i_1__2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \count[17]_i_1__2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \count[18]_i_1__2\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \count[19]_i_2\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \count[6]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \count[7]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \count[8]_i_1__2\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \count[9]_i_1__2\ : label is "soft_lutpair18";
begin
\count0__39_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count0__39_carry_n_0\,
      CO(2 downto 0) => \NLW_count0__39_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => B"0110",
      O(3) => \count0__39_carry_n_4\,
      O(2) => \count0__39_carry_n_5\,
      O(1) => \count0__39_carry_n_6\,
      O(0) => \count0__39_carry_n_7\,
      S(3) => \count0_carry__1_n_6\,
      S(2) => \count0__39_carry_i_1_n_0\,
      S(1) => \count0__39_carry_i_2_n_0\,
      S(0) => \count0_carry__0_n_5\
    );
\count0__39_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__39_carry_n_0\,
      CO(3) => \count0__39_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0__39_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3) => \count0__39_carry__0_n_4\,
      O(2) => \count0__39_carry__0_n_5\,
      O(1) => \count0__39_carry__0_n_6\,
      O(0) => \count0__39_carry__0_n_7\,
      S(3) => \count0__39_carry__0_i_1_n_0\,
      S(2) => \count0__39_carry__0_i_2_n_0\,
      S(1) => \count0__39_carry__0_i_3_n_0\,
      S(0) => \count0__39_carry__0_i_4_n_0\
    );
\count0__39_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_6\,
      O => \count0__39_carry__0_i_1_n_0\
    );
\count0__39_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_7\,
      O => \count0__39_carry__0_i_2_n_0\
    );
\count0__39_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_4\,
      O => \count0__39_carry__0_i_3_n_0\
    );
\count0__39_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_5\,
      O => \count0__39_carry__0_i_4_n_0\
    );
\count0__39_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__39_carry__0_n_0\,
      CO(3) => \count0__39_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0__39_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3) => \count0__39_carry__1_n_4\,
      O(2) => \count0__39_carry__1_n_5\,
      O(1) => \count0__39_carry__1_n_6\,
      O(0) => \count0__39_carry__1_n_7\,
      S(3) => \count0_carry__3_n_6\,
      S(2) => \count0_carry__3_n_7\,
      S(1) => \count0__39_carry__1_i_1_n_0\,
      S(0) => \count0_carry__2_n_5\
    );
\count0__39_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_4\,
      O => \count0__39_carry__1_i_1_n_0\
    );
\count0__39_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__39_carry__1_n_0\,
      CO(3) => \NLW_count0__39_carry__2_CO_UNCONNECTED\(3),
      CO(2) => \count0__39_carry__2_n_1\,
      CO(1 downto 0) => \NLW_count0__39_carry__2_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0100",
      O(3 downto 2) => \NLW_count0__39_carry__2_O_UNCONNECTED\(3 downto 2),
      O(1) => \count0__39_carry__2_n_6\,
      O(0) => \count0__39_carry__2_n_7\,
      S(3) => '0',
      S(2) => \count0__39_carry__2_i_1_n_3\,
      S(1) => \count0_carry__3_n_4\,
      S(0) => \count0_carry__3_n_5\
    );
\count0__39_carry__2_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__3_n_0\,
      CO(3 downto 1) => \NLW_count0__39_carry__2_i_1_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \count0__39_carry__2_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_count0__39_carry__2_i_1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\count0__39_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_7\,
      O => \count0__39_carry_i_1_n_0\
    );
\count0__39_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__0_n_4\,
      O => \count0__39_carry_i_2_n_0\
    );
count0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count0_carry_n_0,
      CO(2 downto 0) => NLW_count0_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => count1(3 downto 0),
      O(3) => count0_carry_n_4,
      O(2) => count0_carry_n_5,
      O(1) => count0_carry_n_6,
      O(0) => count0_carry_n_7,
      S(3) => \count0_carry_i_2__2_n_0\,
      S(2) => \count0_carry_i_3__2_n_0\,
      S(1) => \count0_carry_i_4__2_n_0\,
      S(0) => \count_reg_n_0_[0]\
    );
\count0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count0_carry_n_0,
      CO(3) => \count0_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(7 downto 4),
      O(3) => \count0_carry__0_n_4\,
      O(2) => \count0_carry__0_n_5\,
      O(1) => \count0_carry__0_n_6\,
      O(0) => \count0_carry__0_n_7\,
      S(3) => \count0_carry__0_i_1__2_n_0\,
      S(2) => \count0_carry__0_i_2__2_n_0\,
      S(1) => \count0_carry__0_i_3__2_n_0\,
      S(0) => \count0_carry__0_i_4__2_n_0\
    );
\count0_carry__0_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(7),
      O => \count0_carry__0_i_1__2_n_0\
    );
\count0_carry__0_i_2__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(6),
      O => \count0_carry__0_i_2__2_n_0\
    );
\count0_carry__0_i_3__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(5),
      O => \count0_carry__0_i_3__2_n_0\
    );
\count0_carry__0_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(4),
      O => \count0_carry__0_i_4__2_n_0\
    );
\count0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__0_n_0\,
      CO(3) => \count0_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(11 downto 8),
      O(3) => \count0_carry__1_n_4\,
      O(2) => \count0_carry__1_n_5\,
      O(1) => \count0_carry__1_n_6\,
      O(0) => \count0_carry__1_n_7\,
      S(3) => \count0_carry__1_i_1__2_n_0\,
      S(2) => \count0_carry__1_i_2__2_n_0\,
      S(1) => \count0_carry__1_i_3__2_n_0\,
      S(0) => \count0_carry__1_i_4__2_n_0\
    );
\count0_carry__1_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(11),
      O => \count0_carry__1_i_1__2_n_0\
    );
\count0_carry__1_i_2__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(10),
      O => \count0_carry__1_i_2__2_n_0\
    );
\count0_carry__1_i_3__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(9),
      O => \count0_carry__1_i_3__2_n_0\
    );
\count0_carry__1_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(8),
      O => \count0_carry__1_i_4__2_n_0\
    );
\count0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__1_n_0\,
      CO(3) => \count0_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(15 downto 12),
      O(3) => \count0_carry__2_n_4\,
      O(2) => \count0_carry__2_n_5\,
      O(1) => \count0_carry__2_n_6\,
      O(0) => \count0_carry__2_n_7\,
      S(3) => \count0_carry__2_i_1__2_n_0\,
      S(2) => \count0_carry__2_i_2__2_n_0\,
      S(1) => \count0_carry__2_i_3__2_n_0\,
      S(0) => \count0_carry__2_i_4__2_n_0\
    );
\count0_carry__2_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(15),
      O => \count0_carry__2_i_1__2_n_0\
    );
\count0_carry__2_i_2__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(14),
      O => \count0_carry__2_i_2__2_n_0\
    );
\count0_carry__2_i_3__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(13),
      O => \count0_carry__2_i_3__2_n_0\
    );
\count0_carry__2_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(12),
      O => \count0_carry__2_i_4__2_n_0\
    );
\count0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__2_n_0\,
      CO(3) => \count0_carry__3_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(19 downto 16),
      O(3) => \count0_carry__3_n_4\,
      O(2) => \count0_carry__3_n_5\,
      O(1) => \count0_carry__3_n_6\,
      O(0) => \count0_carry__3_n_7\,
      S(3) => \count0_carry__3_i_1__2_n_0\,
      S(2) => \count0_carry__3_i_2__2_n_0\,
      S(1) => \count0_carry__3_i_3__2_n_0\,
      S(0) => \count0_carry__3_i_4__2_n_0\
    );
\count0_carry__3_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(19),
      O => \count0_carry__3_i_1__2_n_0\
    );
\count0_carry__3_i_2__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(18),
      O => \count0_carry__3_i_2__2_n_0\
    );
\count0_carry__3_i_3__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(17),
      O => \count0_carry__3_i_3__2_n_0\
    );
\count0_carry__3_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(16),
      O => \count0_carry__3_i_4__2_n_0\
    );
\count0_carry_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count_reg_n_0_[0]\,
      O => count1(0)
    );
\count0_carry_i_2__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(3),
      O => \count0_carry_i_2__2_n_0\
    );
\count0_carry_i_3__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(2),
      O => \count0_carry_i_3__2_n_0\
    );
\count0_carry_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(1),
      O => \count0_carry_i_4__2_n_0\
    );
count1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count1_carry_n_0,
      CO(2 downto 0) => NLW_count1_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => \count_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(4 downto 1),
      S(3) => \count_reg_n_0_[4]\,
      S(2) => \count_reg_n_0_[3]\,
      S(1) => \count_reg_n_0_[2]\,
      S(0) => \count_reg_n_0_[1]\
    );
\count1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count1_carry_n_0,
      CO(3) => \count1_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(8 downto 5),
      S(3) => \count_reg_n_0_[8]\,
      S(2) => \count_reg_n_0_[7]\,
      S(1) => \count_reg_n_0_[6]\,
      S(0) => \count_reg_n_0_[5]\
    );
\count1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__0_n_0\,
      CO(3) => \count1_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(12 downto 9),
      S(3) => \count_reg_n_0_[12]\,
      S(2) => \count_reg_n_0_[11]\,
      S(1) => \count_reg_n_0_[10]\,
      S(0) => \count_reg_n_0_[9]\
    );
\count1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__1_n_0\,
      CO(3) => \count1_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(16 downto 13),
      S(3) => \count_reg_n_0_[16]\,
      S(2) => \count_reg_n_0_[15]\,
      S(1) => \count_reg_n_0_[14]\,
      S(0) => \count_reg_n_0_[13]\
    );
\count1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__2_n_0\,
      CO(3 downto 0) => \NLW_count1_carry__3_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count1_carry__3_O_UNCONNECTED\(3),
      O(2 downto 0) => count1(19 downto 17),
      S(3) => '0',
      S(2) => \count_reg_n_0_[19]\,
      S(1) => \count_reg_n_0_[18]\,
      S(0) => \count_reg_n_0_[17]\
    );
\count[10]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__0_n_7\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__1_n_5\,
      O => \count[10]_i_1__2_n_0\
    );
\count[11]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__0_n_6\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__1_n_4\,
      O => \count[11]_i_1__2_n_0\
    );
\count[12]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__0_n_5\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__2_n_7\,
      O => \count[12]_i_1__2_n_0\
    );
\count[13]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__0_n_4\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__2_n_6\,
      O => \count[13]_i_1__2_n_0\
    );
\count[14]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__1_n_7\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__2_n_5\,
      O => \count[14]_i_1__2_n_0\
    );
\count[15]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__1_n_6\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__2_n_4\,
      O => \count[15]_i_1__2_n_0\
    );
\count[16]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__1_n_5\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__3_n_7\,
      O => \count[16]_i_1__2_n_0\
    );
\count[17]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__1_n_4\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__3_n_6\,
      O => \count[17]_i_1__2_n_0\
    );
\count[18]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__2_n_7\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__3_n_5\,
      O => \count[18]_i_1__2_n_0\
    );
\count[19]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry__2_n_6\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__3_n_4\,
      O => \count[19]_i_2_n_0\
    );
\count[3]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \count[3]_i_3_n_0\,
      I1 => \count_reg_n_0_[9]\,
      I2 => \count_reg_n_0_[4]\,
      I3 => \count_reg_n_0_[14]\,
      I4 => \count_reg_n_0_[0]\,
      I5 => \count[3]_i_4_n_0\,
      O => E(0)
    );
\count[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \count_reg_n_0_[5]\,
      I1 => \count_reg_n_0_[2]\,
      I2 => \count_reg_n_0_[19]\,
      I3 => \count_reg_n_0_[17]\,
      O => \count[3]_i_3_n_0\
    );
\count[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \count_reg_n_0_[7]\,
      I1 => \count_reg_n_0_[10]\,
      I2 => \count_reg_n_0_[11]\,
      I3 => \count_reg_n_0_[15]\,
      I4 => \count[3]_i_5_n_0\,
      I5 => \count[3]_i_6_n_0\,
      O => \count[3]_i_4_n_0\
    );
\count[3]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \count_reg_n_0_[16]\,
      I1 => \count_reg_n_0_[13]\,
      I2 => \count_reg_n_0_[3]\,
      I3 => \count_reg_n_0_[6]\,
      O => \count[3]_i_5_n_0\
    );
\count[3]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => \count_reg_n_0_[18]\,
      I1 => \count_reg_n_0_[1]\,
      I2 => \count_reg_n_0_[12]\,
      I3 => \count_reg_n_0_[8]\,
      O => \count[3]_i_6_n_0\
    );
\count[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry_n_7\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__0_n_5\,
      O => \count[6]_i_1_n_0\
    );
\count[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry_n_6\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__0_n_4\,
      O => \count[7]_i_1_n_0\
    );
\count[8]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry_n_5\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__1_n_7\,
      O => \count[8]_i_1__2_n_0\
    );
\count[9]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__39_carry_n_4\,
      I1 => \count0__39_carry__2_n_1\,
      I2 => \count0_carry__1_n_6\,
      O => \count[9]_i_1__2_n_0\
    );
\count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => count0_carry_n_7,
      Q => \count_reg_n_0_[0]\
    );
\count_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[10]_i_1__2_n_0\,
      Q => \count_reg_n_0_[10]\
    );
\count_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[11]_i_1__2_n_0\,
      Q => \count_reg_n_0_[11]\
    );
\count_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[12]_i_1__2_n_0\,
      Q => \count_reg_n_0_[12]\
    );
\count_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[13]_i_1__2_n_0\,
      Q => \count_reg_n_0_[13]\
    );
\count_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[14]_i_1__2_n_0\,
      Q => \count_reg_n_0_[14]\
    );
\count_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[15]_i_1__2_n_0\,
      Q => \count_reg_n_0_[15]\
    );
\count_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[16]_i_1__2_n_0\,
      Q => \count_reg_n_0_[16]\
    );
\count_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[17]_i_1__2_n_0\,
      Q => \count_reg_n_0_[17]\
    );
\count_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[18]_i_1__2_n_0\,
      Q => \count_reg_n_0_[18]\
    );
\count_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[19]_i_2_n_0\,
      Q => \count_reg_n_0_[19]\
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => count0_carry_n_6,
      Q => \count_reg_n_0_[1]\
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => count0_carry_n_5,
      Q => \count_reg_n_0_[2]\
    );
\count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => count0_carry_n_4,
      Q => \count_reg_n_0_[3]\
    );
\count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count0_carry__0_n_7\,
      Q => \count_reg_n_0_[4]\
    );
\count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count0_carry__0_n_6\,
      Q => \count_reg_n_0_[5]\
    );
\count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[6]_i_1_n_0\,
      Q => \count_reg_n_0_[6]\
    );
\count_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[7]_i_1_n_0\,
      Q => \count_reg_n_0_[7]\
    );
\count_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[8]_i_1__2_n_0\,
      Q => \count_reg_n_0_[8]\
    );
\count_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \count_reg[0]_0\(0),
      CLR => AR(0),
      D => \count[9]_i_1__2_n_0\,
      Q => \count_reg_n_0_[9]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SYNCHRNZR is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    button_IBUF : in STD_LOGIC
  );
end SYNCHRNZR;

architecture STRUCTURE of SYNCHRNZR is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of SYNC_OUT_reg_srl2 : label is "\unit/trat_boton/inst_sincro/SYNC_OUT_reg_srl2 ";
begin
SYNC_OUT_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => CLK,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\(0)
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => button_IBUF,
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TIMER is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    \count_reg[24]_0\ : in STD_LOGIC
  );
end TIMER;

architecture STRUCTURE of TIMER is
  signal \aux[7]_i_3_n_0\ : STD_LOGIC;
  signal \aux[7]_i_4_n_0\ : STD_LOGIC;
  signal \aux[7]_i_5_n_0\ : STD_LOGIC;
  signal \aux[7]_i_6_n_0\ : STD_LOGIC;
  signal count : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \count0__49_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_n_4\ : STD_LOGIC;
  signal \count0__49_carry__0_n_5\ : STD_LOGIC;
  signal \count0__49_carry__0_n_6\ : STD_LOGIC;
  signal \count0__49_carry__0_n_7\ : STD_LOGIC;
  signal \count0__49_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_n_4\ : STD_LOGIC;
  signal \count0__49_carry__1_n_5\ : STD_LOGIC;
  signal \count0__49_carry__1_n_6\ : STD_LOGIC;
  signal \count0__49_carry__1_n_7\ : STD_LOGIC;
  signal \count0__49_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_n_4\ : STD_LOGIC;
  signal \count0__49_carry__2_n_5\ : STD_LOGIC;
  signal \count0__49_carry__2_n_6\ : STD_LOGIC;
  signal \count0__49_carry__2_n_7\ : STD_LOGIC;
  signal \count0__49_carry__3_n_2\ : STD_LOGIC;
  signal \count0__49_carry__3_n_7\ : STD_LOGIC;
  signal \count0__49_carry_i_1_n_0\ : STD_LOGIC;
  signal \count0__49_carry_n_0\ : STD_LOGIC;
  signal \count0__49_carry_n_4\ : STD_LOGIC;
  signal \count0__49_carry_n_5\ : STD_LOGIC;
  signal \count0__49_carry_n_6\ : STD_LOGIC;
  signal \count0__49_carry_n_7\ : STD_LOGIC;
  signal \count0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \count0_carry__0_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_4\ : STD_LOGIC;
  signal \count0_carry__1_n_5\ : STD_LOGIC;
  signal \count0_carry__1_n_6\ : STD_LOGIC;
  signal \count0_carry__1_n_7\ : STD_LOGIC;
  signal \count0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_4\ : STD_LOGIC;
  signal \count0_carry__2_n_5\ : STD_LOGIC;
  signal \count0_carry__2_n_6\ : STD_LOGIC;
  signal \count0_carry__2_n_7\ : STD_LOGIC;
  signal \count0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_4\ : STD_LOGIC;
  signal \count0_carry__3_n_5\ : STD_LOGIC;
  signal \count0_carry__3_n_6\ : STD_LOGIC;
  signal \count0_carry__3_n_7\ : STD_LOGIC;
  signal \count0_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \count0_carry__4_n_0\ : STD_LOGIC;
  signal \count0_carry__4_n_4\ : STD_LOGIC;
  signal \count0_carry__4_n_5\ : STD_LOGIC;
  signal \count0_carry__4_n_6\ : STD_LOGIC;
  signal \count0_carry__4_n_7\ : STD_LOGIC;
  signal \count0_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \count0_carry__5_n_2\ : STD_LOGIC;
  signal \count0_carry__5_n_7\ : STD_LOGIC;
  signal count0_carry_i_2_n_0 : STD_LOGIC;
  signal count0_carry_i_3_n_0 : STD_LOGIC;
  signal count0_carry_i_4_n_0 : STD_LOGIC;
  signal count0_carry_n_0 : STD_LOGIC;
  signal count1 : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \count1_carry__0_n_0\ : STD_LOGIC;
  signal \count1_carry__1_n_0\ : STD_LOGIC;
  signal \count1_carry__2_n_0\ : STD_LOGIC;
  signal \count1_carry__3_n_0\ : STD_LOGIC;
  signal count1_carry_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \NLW_count0__49_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0__49_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_count0_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_count1_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \count0__49_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of count0_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__4\ : label is "SWEEP";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of count1_carry : label is 35;
  attribute OPT_MODIFIED of count1_carry : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__0\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__0\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__1\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__2\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__2\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__3\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__3\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__4\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__4\ : label is "SWEEP";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[10]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \count[11]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \count[12]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \count[13]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \count[14]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \count[15]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \count[16]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \count[17]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \count[18]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \count[19]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \count[20]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \count[21]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \count[22]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \count[23]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \count[24]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \count[9]_i_1\ : label is "soft_lutpair57";
begin
\aux[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \aux[7]_i_3_n_0\,
      I1 => \aux[7]_i_4_n_0\,
      I2 => \aux[7]_i_5_n_0\,
      I3 => count(0),
      I4 => \aux[7]_i_6_n_0\,
      O => E(0)
    );
\aux[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => count(3),
      I1 => count(4),
      I2 => count(1),
      I3 => count(2),
      I4 => count(6),
      I5 => count(5),
      O => \aux[7]_i_3_n_0\
    );
\aux[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => count(21),
      I1 => count(22),
      I2 => count(20),
      I3 => count(19),
      I4 => count(23),
      I5 => count(24),
      O => \aux[7]_i_4_n_0\
    );
\aux[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => count(16),
      I1 => count(15),
      I2 => count(13),
      I3 => count(14),
      I4 => count(18),
      I5 => count(17),
      O => \aux[7]_i_5_n_0\
    );
\aux[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => count(10),
      I1 => count(9),
      I2 => count(7),
      I3 => count(8),
      I4 => count(12),
      I5 => count(11),
      O => \aux[7]_i_6_n_0\
    );
\count0__49_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count0__49_carry_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => B"0010",
      O(3) => \count0__49_carry_n_4\,
      O(2) => \count0__49_carry_n_5\,
      O(1) => \count0__49_carry_n_6\,
      O(0) => \count0__49_carry_n_7\,
      S(3) => \count0_carry__1_n_4\,
      S(2) => \count0_carry__1_n_5\,
      S(1) => \count0__49_carry_i_1_n_0\,
      S(0) => \count0_carry__1_n_7\
    );
\count0__49_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry_n_0\,
      CO(3) => \count0__49_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1101",
      O(3) => \count0__49_carry__0_n_4\,
      O(2) => \count0__49_carry__0_n_5\,
      O(1) => \count0__49_carry__0_n_6\,
      O(0) => \count0__49_carry__0_n_7\,
      S(3) => \count0__49_carry__0_i_1_n_0\,
      S(2) => \count0__49_carry__0_i_2_n_0\,
      S(1) => \count0_carry__2_n_6\,
      S(0) => \count0__49_carry__0_i_3_n_0\
    );
\count0__49_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_4\,
      O => \count0__49_carry__0_i_1_n_0\
    );
\count0__49_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_5\,
      O => \count0__49_carry__0_i_2_n_0\
    );
\count0__49_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_7\,
      O => \count0__49_carry__0_i_3_n_0\
    );
\count0__49_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__0_n_0\,
      CO(3) => \count0__49_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1110",
      O(3) => \count0__49_carry__1_n_4\,
      O(2) => \count0__49_carry__1_n_5\,
      O(1) => \count0__49_carry__1_n_6\,
      O(0) => \count0__49_carry__1_n_7\,
      S(3) => \count0__49_carry__1_i_1_n_0\,
      S(2) => \count0__49_carry__1_i_2_n_0\,
      S(1) => \count0__49_carry__1_i_3_n_0\,
      S(0) => \count0_carry__3_n_7\
    );
\count0__49_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_4\,
      O => \count0__49_carry__1_i_1_n_0\
    );
\count0__49_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_5\,
      O => \count0__49_carry__1_i_2_n_0\
    );
\count0__49_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_6\,
      O => \count0__49_carry__1_i_3_n_0\
    );
\count0__49_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__1_n_0\,
      CO(3) => \count0__49_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1100",
      O(3) => \count0__49_carry__2_n_4\,
      O(2) => \count0__49_carry__2_n_5\,
      O(1) => \count0__49_carry__2_n_6\,
      O(0) => \count0__49_carry__2_n_7\,
      S(3) => \count0__49_carry__2_i_1_n_0\,
      S(2) => \count0__49_carry__2_i_2_n_0\,
      S(1) => \count0_carry__4_n_6\,
      S(0) => \count0_carry__4_n_7\
    );
\count0__49_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__4_n_4\,
      O => \count0__49_carry__2_i_1_n_0\
    );
\count0__49_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__4_n_5\,
      O => \count0__49_carry__2_i_2_n_0\
    );
\count0__49_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__2_n_0\,
      CO(3 downto 2) => \NLW_count0__49_carry__3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count0__49_carry__3_n_2\,
      CO(0) => \NLW_count0__49_carry__3_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3 downto 1) => \NLW_count0__49_carry__3_O_UNCONNECTED\(3 downto 1),
      O(0) => \count0__49_carry__3_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \count0_carry__5_n_2\,
      S(0) => \count0_carry__5_n_7\
    );
\count0__49_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_6\,
      O => \count0__49_carry_i_1_n_0\
    );
count0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count0_carry_n_0,
      CO(2 downto 0) => NLW_count0_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => count1(3 downto 0),
      O(3 downto 0) => p_0_in(3 downto 0),
      S(3) => count0_carry_i_2_n_0,
      S(2) => count0_carry_i_3_n_0,
      S(1) => count0_carry_i_4_n_0,
      S(0) => count(0)
    );
\count0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count0_carry_n_0,
      CO(3) => \count0_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(7 downto 4),
      O(3 downto 0) => p_0_in(7 downto 4),
      S(3) => \count0_carry__0_i_1_n_0\,
      S(2) => \count0_carry__0_i_2_n_0\,
      S(1) => \count0_carry__0_i_3_n_0\,
      S(0) => \count0_carry__0_i_4_n_0\
    );
\count0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(7),
      O => \count0_carry__0_i_1_n_0\
    );
\count0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(6),
      O => \count0_carry__0_i_2_n_0\
    );
\count0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(5),
      O => \count0_carry__0_i_3_n_0\
    );
\count0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(4),
      O => \count0_carry__0_i_4_n_0\
    );
\count0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__0_n_0\,
      CO(3) => \count0_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(11 downto 8),
      O(3) => \count0_carry__1_n_4\,
      O(2) => \count0_carry__1_n_5\,
      O(1) => \count0_carry__1_n_6\,
      O(0) => \count0_carry__1_n_7\,
      S(3) => \count0_carry__1_i_1_n_0\,
      S(2) => \count0_carry__1_i_2_n_0\,
      S(1) => \count0_carry__1_i_3_n_0\,
      S(0) => \count0_carry__1_i_4_n_0\
    );
\count0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(11),
      O => \count0_carry__1_i_1_n_0\
    );
\count0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(10),
      O => \count0_carry__1_i_2_n_0\
    );
\count0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(9),
      O => \count0_carry__1_i_3_n_0\
    );
\count0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(8),
      O => \count0_carry__1_i_4_n_0\
    );
\count0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__1_n_0\,
      CO(3) => \count0_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(15 downto 12),
      O(3) => \count0_carry__2_n_4\,
      O(2) => \count0_carry__2_n_5\,
      O(1) => \count0_carry__2_n_6\,
      O(0) => \count0_carry__2_n_7\,
      S(3) => \count0_carry__2_i_1_n_0\,
      S(2) => \count0_carry__2_i_2_n_0\,
      S(1) => \count0_carry__2_i_3_n_0\,
      S(0) => \count0_carry__2_i_4_n_0\
    );
\count0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(15),
      O => \count0_carry__2_i_1_n_0\
    );
\count0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(14),
      O => \count0_carry__2_i_2_n_0\
    );
\count0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(13),
      O => \count0_carry__2_i_3_n_0\
    );
\count0_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(12),
      O => \count0_carry__2_i_4_n_0\
    );
\count0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__2_n_0\,
      CO(3) => \count0_carry__3_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(19 downto 16),
      O(3) => \count0_carry__3_n_4\,
      O(2) => \count0_carry__3_n_5\,
      O(1) => \count0_carry__3_n_6\,
      O(0) => \count0_carry__3_n_7\,
      S(3) => \count0_carry__3_i_1_n_0\,
      S(2) => \count0_carry__3_i_2_n_0\,
      S(1) => \count0_carry__3_i_3_n_0\,
      S(0) => \count0_carry__3_i_4_n_0\
    );
\count0_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(19),
      O => \count0_carry__3_i_1_n_0\
    );
\count0_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(18),
      O => \count0_carry__3_i_2_n_0\
    );
\count0_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(17),
      O => \count0_carry__3_i_3_n_0\
    );
\count0_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(16),
      O => \count0_carry__3_i_4_n_0\
    );
\count0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__3_n_0\,
      CO(3) => \count0_carry__4_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(23 downto 20),
      O(3) => \count0_carry__4_n_4\,
      O(2) => \count0_carry__4_n_5\,
      O(1) => \count0_carry__4_n_6\,
      O(0) => \count0_carry__4_n_7\,
      S(3) => \count0_carry__4_i_1_n_0\,
      S(2) => \count0_carry__4_i_2_n_0\,
      S(1) => \count0_carry__4_i_3_n_0\,
      S(0) => \count0_carry__4_i_4_n_0\
    );
\count0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(23),
      O => \count0_carry__4_i_1_n_0\
    );
\count0_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(22),
      O => \count0_carry__4_i_2_n_0\
    );
\count0_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(21),
      O => \count0_carry__4_i_3_n_0\
    );
\count0_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(20),
      O => \count0_carry__4_i_4_n_0\
    );
\count0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__4_n_0\,
      CO(3 downto 2) => \NLW_count0_carry__5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count0_carry__5_n_2\,
      CO(0) => \NLW_count0_carry__5_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => count1(24),
      O(3 downto 1) => \NLW_count0_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \count0_carry__5_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \count0_carry__5_i_1_n_0\
    );
\count0_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(24),
      O => \count0_carry__5_i_1_n_0\
    );
count0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count(0),
      O => count1(0)
    );
count0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(3),
      O => count0_carry_i_2_n_0
    );
count0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(2),
      O => count0_carry_i_3_n_0
    );
count0_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(1),
      O => count0_carry_i_4_n_0
    );
count1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count1_carry_n_0,
      CO(2 downto 0) => NLW_count1_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => count(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(4 downto 1),
      S(3 downto 0) => count(4 downto 1)
    );
\count1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count1_carry_n_0,
      CO(3) => \count1_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(8 downto 5),
      S(3 downto 0) => count(8 downto 5)
    );
\count1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__0_n_0\,
      CO(3) => \count1_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(12 downto 9),
      S(3 downto 0) => count(12 downto 9)
    );
\count1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__1_n_0\,
      CO(3) => \count1_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(16 downto 13),
      S(3 downto 0) => count(16 downto 13)
    );
\count1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__2_n_0\,
      CO(3) => \count1_carry__3_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(20 downto 17),
      S(3 downto 0) => count(20 downto 17)
    );
\count1_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__3_n_0\,
      CO(3 downto 0) => \NLW_count1_carry__4_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(24 downto 21),
      S(3 downto 0) => count(24 downto 21)
    );
\count[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_5\,
      O => p_0_in(10)
    );
\count[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_4\,
      O => p_0_in(11)
    );
\count[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_7\,
      O => p_0_in(12)
    );
\count[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_6\,
      O => p_0_in(13)
    );
\count[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_5\,
      O => p_0_in(14)
    );
\count[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_4\,
      O => p_0_in(15)
    );
\count[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_7\,
      O => p_0_in(16)
    );
\count[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_6\,
      O => p_0_in(17)
    );
\count[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_5\,
      O => p_0_in(18)
    );
\count[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_4\,
      O => p_0_in(19)
    );
\count[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_7\,
      O => p_0_in(20)
    );
\count[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_6\,
      O => p_0_in(21)
    );
\count[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_5\,
      O => p_0_in(22)
    );
\count[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_4\,
      O => p_0_in(23)
    );
\count[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__3_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__5_n_7\,
      O => p_0_in(24)
    );
\count[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_7\,
      O => p_0_in(8)
    );
\count[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_6\,
      O => p_0_in(9)
    );
\count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(0),
      Q => count(0)
    );
\count_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(10),
      Q => count(10)
    );
\count_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(11),
      Q => count(11)
    );
\count_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(12),
      Q => count(12)
    );
\count_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(13),
      Q => count(13)
    );
\count_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(14),
      Q => count(14)
    );
\count_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(15),
      Q => count(15)
    );
\count_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(16),
      Q => count(16)
    );
\count_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(17),
      Q => count(17)
    );
\count_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(18),
      Q => count(18)
    );
\count_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(19),
      Q => count(19)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(1),
      Q => count(1)
    );
\count_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(20),
      Q => count(20)
    );
\count_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(21),
      Q => count(21)
    );
\count_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(22),
      Q => count(22)
    );
\count_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(23),
      Q => count(23)
    );
\count_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(24),
      Q => count(24)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(2),
      Q => count(2)
    );
\count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(3),
      Q => count(3)
    );
\count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(4),
      Q => count(4)
    );
\count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(5),
      Q => count(5)
    );
\count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(6),
      Q => count(6)
    );
\count_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(7),
      Q => count(7)
    );
\count_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(8),
      Q => count(8)
    );
\count_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(9),
      Q => count(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TIMER_3 is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    \count_reg[24]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of TIMER_3 : entity is "TIMER";
end TIMER_3;

architecture STRUCTURE of TIMER_3 is
  signal \aux[7]_i_3__0_n_0\ : STD_LOGIC;
  signal \aux[7]_i_4__0_n_0\ : STD_LOGIC;
  signal \aux[7]_i_5__0_n_0\ : STD_LOGIC;
  signal \aux[7]_i_6__0_n_0\ : STD_LOGIC;
  signal count : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \count0__49_carry__0_i_1__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_i_2__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_i_3__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_n_4\ : STD_LOGIC;
  signal \count0__49_carry__0_n_5\ : STD_LOGIC;
  signal \count0__49_carry__0_n_6\ : STD_LOGIC;
  signal \count0__49_carry__0_n_7\ : STD_LOGIC;
  signal \count0__49_carry__1_i_1__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_i_2__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_i_3__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_n_4\ : STD_LOGIC;
  signal \count0__49_carry__1_n_5\ : STD_LOGIC;
  signal \count0__49_carry__1_n_6\ : STD_LOGIC;
  signal \count0__49_carry__1_n_7\ : STD_LOGIC;
  signal \count0__49_carry__2_i_1__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_i_2__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_n_4\ : STD_LOGIC;
  signal \count0__49_carry__2_n_5\ : STD_LOGIC;
  signal \count0__49_carry__2_n_6\ : STD_LOGIC;
  signal \count0__49_carry__2_n_7\ : STD_LOGIC;
  signal \count0__49_carry__3_n_2\ : STD_LOGIC;
  signal \count0__49_carry__3_n_7\ : STD_LOGIC;
  signal \count0__49_carry_i_1__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry_n_0\ : STD_LOGIC;
  signal \count0__49_carry_n_4\ : STD_LOGIC;
  signal \count0__49_carry_n_5\ : STD_LOGIC;
  signal \count0__49_carry_n_6\ : STD_LOGIC;
  signal \count0__49_carry_n_7\ : STD_LOGIC;
  signal \count0_carry__0_i_1__0_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_2__0_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_3__0_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_4__0_n_0\ : STD_LOGIC;
  signal \count0_carry__0_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_1__0_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_2__0_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_3__0_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_4__0_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_4\ : STD_LOGIC;
  signal \count0_carry__1_n_5\ : STD_LOGIC;
  signal \count0_carry__1_n_6\ : STD_LOGIC;
  signal \count0_carry__1_n_7\ : STD_LOGIC;
  signal \count0_carry__2_i_1__0_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_2__0_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_3__0_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_4__0_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_4\ : STD_LOGIC;
  signal \count0_carry__2_n_5\ : STD_LOGIC;
  signal \count0_carry__2_n_6\ : STD_LOGIC;
  signal \count0_carry__2_n_7\ : STD_LOGIC;
  signal \count0_carry__3_i_1__0_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_2__0_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_3__0_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_4__0_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_4\ : STD_LOGIC;
  signal \count0_carry__3_n_5\ : STD_LOGIC;
  signal \count0_carry__3_n_6\ : STD_LOGIC;
  signal \count0_carry__3_n_7\ : STD_LOGIC;
  signal \count0_carry__4_i_1__0_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_2__0_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_3__0_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_4__0_n_0\ : STD_LOGIC;
  signal \count0_carry__4_n_0\ : STD_LOGIC;
  signal \count0_carry__4_n_4\ : STD_LOGIC;
  signal \count0_carry__4_n_5\ : STD_LOGIC;
  signal \count0_carry__4_n_6\ : STD_LOGIC;
  signal \count0_carry__4_n_7\ : STD_LOGIC;
  signal \count0_carry__5_i_1__0_n_0\ : STD_LOGIC;
  signal \count0_carry__5_n_2\ : STD_LOGIC;
  signal \count0_carry__5_n_7\ : STD_LOGIC;
  signal \count0_carry_i_2__0_n_0\ : STD_LOGIC;
  signal \count0_carry_i_3__0_n_0\ : STD_LOGIC;
  signal \count0_carry_i_4__0_n_0\ : STD_LOGIC;
  signal count0_carry_n_0 : STD_LOGIC;
  signal count1 : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \count1_carry__0_n_0\ : STD_LOGIC;
  signal \count1_carry__1_n_0\ : STD_LOGIC;
  signal \count1_carry__2_n_0\ : STD_LOGIC;
  signal \count1_carry__3_n_0\ : STD_LOGIC;
  signal count1_carry_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \NLW_count0__49_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0__49_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_count0_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_count1_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \count0__49_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of count0_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__4\ : label is "SWEEP";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of count1_carry : label is 35;
  attribute OPT_MODIFIED of count1_carry : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__0\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__0\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__1\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__2\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__2\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__3\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__3\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__4\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__4\ : label is "SWEEP";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[10]_i_1__0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \count[11]_i_1__0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \count[12]_i_1__0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \count[13]_i_1__0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \count[14]_i_1__0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \count[15]_i_1__0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \count[16]_i_1__0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \count[17]_i_1__0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \count[18]_i_1__0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \count[19]_i_1__0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \count[20]_i_1__0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \count[21]_i_1__0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \count[22]_i_1__0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \count[23]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \count[24]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \count[9]_i_1__0\ : label is "soft_lutpair38";
begin
\aux[7]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \aux[7]_i_3__0_n_0\,
      I1 => \aux[7]_i_4__0_n_0\,
      I2 => \aux[7]_i_5__0_n_0\,
      I3 => count(0),
      I4 => \aux[7]_i_6__0_n_0\,
      O => E(0)
    );
\aux[7]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => count(3),
      I1 => count(4),
      I2 => count(1),
      I3 => count(2),
      I4 => count(6),
      I5 => count(5),
      O => \aux[7]_i_3__0_n_0\
    );
\aux[7]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => count(21),
      I1 => count(22),
      I2 => count(20),
      I3 => count(19),
      I4 => count(23),
      I5 => count(24),
      O => \aux[7]_i_4__0_n_0\
    );
\aux[7]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => count(16),
      I1 => count(15),
      I2 => count(13),
      I3 => count(14),
      I4 => count(18),
      I5 => count(17),
      O => \aux[7]_i_5__0_n_0\
    );
\aux[7]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => count(10),
      I1 => count(9),
      I2 => count(7),
      I3 => count(8),
      I4 => count(12),
      I5 => count(11),
      O => \aux[7]_i_6__0_n_0\
    );
\count0__49_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count0__49_carry_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => B"0010",
      O(3) => \count0__49_carry_n_4\,
      O(2) => \count0__49_carry_n_5\,
      O(1) => \count0__49_carry_n_6\,
      O(0) => \count0__49_carry_n_7\,
      S(3) => \count0_carry__1_n_4\,
      S(2) => \count0_carry__1_n_5\,
      S(1) => \count0__49_carry_i_1__0_n_0\,
      S(0) => \count0_carry__1_n_7\
    );
\count0__49_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry_n_0\,
      CO(3) => \count0__49_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1101",
      O(3) => \count0__49_carry__0_n_4\,
      O(2) => \count0__49_carry__0_n_5\,
      O(1) => \count0__49_carry__0_n_6\,
      O(0) => \count0__49_carry__0_n_7\,
      S(3) => \count0__49_carry__0_i_1__0_n_0\,
      S(2) => \count0__49_carry__0_i_2__0_n_0\,
      S(1) => \count0_carry__2_n_6\,
      S(0) => \count0__49_carry__0_i_3__0_n_0\
    );
\count0__49_carry__0_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_4\,
      O => \count0__49_carry__0_i_1__0_n_0\
    );
\count0__49_carry__0_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_5\,
      O => \count0__49_carry__0_i_2__0_n_0\
    );
\count0__49_carry__0_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_7\,
      O => \count0__49_carry__0_i_3__0_n_0\
    );
\count0__49_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__0_n_0\,
      CO(3) => \count0__49_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1110",
      O(3) => \count0__49_carry__1_n_4\,
      O(2) => \count0__49_carry__1_n_5\,
      O(1) => \count0__49_carry__1_n_6\,
      O(0) => \count0__49_carry__1_n_7\,
      S(3) => \count0__49_carry__1_i_1__0_n_0\,
      S(2) => \count0__49_carry__1_i_2__0_n_0\,
      S(1) => \count0__49_carry__1_i_3__0_n_0\,
      S(0) => \count0_carry__3_n_7\
    );
\count0__49_carry__1_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_4\,
      O => \count0__49_carry__1_i_1__0_n_0\
    );
\count0__49_carry__1_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_5\,
      O => \count0__49_carry__1_i_2__0_n_0\
    );
\count0__49_carry__1_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_6\,
      O => \count0__49_carry__1_i_3__0_n_0\
    );
\count0__49_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__1_n_0\,
      CO(3) => \count0__49_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1100",
      O(3) => \count0__49_carry__2_n_4\,
      O(2) => \count0__49_carry__2_n_5\,
      O(1) => \count0__49_carry__2_n_6\,
      O(0) => \count0__49_carry__2_n_7\,
      S(3) => \count0__49_carry__2_i_1__0_n_0\,
      S(2) => \count0__49_carry__2_i_2__0_n_0\,
      S(1) => \count0_carry__4_n_6\,
      S(0) => \count0_carry__4_n_7\
    );
\count0__49_carry__2_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__4_n_4\,
      O => \count0__49_carry__2_i_1__0_n_0\
    );
\count0__49_carry__2_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__4_n_5\,
      O => \count0__49_carry__2_i_2__0_n_0\
    );
\count0__49_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__2_n_0\,
      CO(3 downto 2) => \NLW_count0__49_carry__3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count0__49_carry__3_n_2\,
      CO(0) => \NLW_count0__49_carry__3_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3 downto 1) => \NLW_count0__49_carry__3_O_UNCONNECTED\(3 downto 1),
      O(0) => \count0__49_carry__3_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \count0_carry__5_n_2\,
      S(0) => \count0_carry__5_n_7\
    );
\count0__49_carry_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_6\,
      O => \count0__49_carry_i_1__0_n_0\
    );
count0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count0_carry_n_0,
      CO(2 downto 0) => NLW_count0_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => count1(3 downto 0),
      O(3 downto 0) => p_0_in(3 downto 0),
      S(3) => \count0_carry_i_2__0_n_0\,
      S(2) => \count0_carry_i_3__0_n_0\,
      S(1) => \count0_carry_i_4__0_n_0\,
      S(0) => count(0)
    );
\count0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count0_carry_n_0,
      CO(3) => \count0_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(7 downto 4),
      O(3 downto 0) => p_0_in(7 downto 4),
      S(3) => \count0_carry__0_i_1__0_n_0\,
      S(2) => \count0_carry__0_i_2__0_n_0\,
      S(1) => \count0_carry__0_i_3__0_n_0\,
      S(0) => \count0_carry__0_i_4__0_n_0\
    );
\count0_carry__0_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(7),
      O => \count0_carry__0_i_1__0_n_0\
    );
\count0_carry__0_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(6),
      O => \count0_carry__0_i_2__0_n_0\
    );
\count0_carry__0_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(5),
      O => \count0_carry__0_i_3__0_n_0\
    );
\count0_carry__0_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(4),
      O => \count0_carry__0_i_4__0_n_0\
    );
\count0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__0_n_0\,
      CO(3) => \count0_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(11 downto 8),
      O(3) => \count0_carry__1_n_4\,
      O(2) => \count0_carry__1_n_5\,
      O(1) => \count0_carry__1_n_6\,
      O(0) => \count0_carry__1_n_7\,
      S(3) => \count0_carry__1_i_1__0_n_0\,
      S(2) => \count0_carry__1_i_2__0_n_0\,
      S(1) => \count0_carry__1_i_3__0_n_0\,
      S(0) => \count0_carry__1_i_4__0_n_0\
    );
\count0_carry__1_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(11),
      O => \count0_carry__1_i_1__0_n_0\
    );
\count0_carry__1_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(10),
      O => \count0_carry__1_i_2__0_n_0\
    );
\count0_carry__1_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(9),
      O => \count0_carry__1_i_3__0_n_0\
    );
\count0_carry__1_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(8),
      O => \count0_carry__1_i_4__0_n_0\
    );
\count0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__1_n_0\,
      CO(3) => \count0_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(15 downto 12),
      O(3) => \count0_carry__2_n_4\,
      O(2) => \count0_carry__2_n_5\,
      O(1) => \count0_carry__2_n_6\,
      O(0) => \count0_carry__2_n_7\,
      S(3) => \count0_carry__2_i_1__0_n_0\,
      S(2) => \count0_carry__2_i_2__0_n_0\,
      S(1) => \count0_carry__2_i_3__0_n_0\,
      S(0) => \count0_carry__2_i_4__0_n_0\
    );
\count0_carry__2_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(15),
      O => \count0_carry__2_i_1__0_n_0\
    );
\count0_carry__2_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(14),
      O => \count0_carry__2_i_2__0_n_0\
    );
\count0_carry__2_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(13),
      O => \count0_carry__2_i_3__0_n_0\
    );
\count0_carry__2_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(12),
      O => \count0_carry__2_i_4__0_n_0\
    );
\count0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__2_n_0\,
      CO(3) => \count0_carry__3_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(19 downto 16),
      O(3) => \count0_carry__3_n_4\,
      O(2) => \count0_carry__3_n_5\,
      O(1) => \count0_carry__3_n_6\,
      O(0) => \count0_carry__3_n_7\,
      S(3) => \count0_carry__3_i_1__0_n_0\,
      S(2) => \count0_carry__3_i_2__0_n_0\,
      S(1) => \count0_carry__3_i_3__0_n_0\,
      S(0) => \count0_carry__3_i_4__0_n_0\
    );
\count0_carry__3_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(19),
      O => \count0_carry__3_i_1__0_n_0\
    );
\count0_carry__3_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(18),
      O => \count0_carry__3_i_2__0_n_0\
    );
\count0_carry__3_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(17),
      O => \count0_carry__3_i_3__0_n_0\
    );
\count0_carry__3_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(16),
      O => \count0_carry__3_i_4__0_n_0\
    );
\count0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__3_n_0\,
      CO(3) => \count0_carry__4_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(23 downto 20),
      O(3) => \count0_carry__4_n_4\,
      O(2) => \count0_carry__4_n_5\,
      O(1) => \count0_carry__4_n_6\,
      O(0) => \count0_carry__4_n_7\,
      S(3) => \count0_carry__4_i_1__0_n_0\,
      S(2) => \count0_carry__4_i_2__0_n_0\,
      S(1) => \count0_carry__4_i_3__0_n_0\,
      S(0) => \count0_carry__4_i_4__0_n_0\
    );
\count0_carry__4_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(23),
      O => \count0_carry__4_i_1__0_n_0\
    );
\count0_carry__4_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(22),
      O => \count0_carry__4_i_2__0_n_0\
    );
\count0_carry__4_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(21),
      O => \count0_carry__4_i_3__0_n_0\
    );
\count0_carry__4_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(20),
      O => \count0_carry__4_i_4__0_n_0\
    );
\count0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__4_n_0\,
      CO(3 downto 2) => \NLW_count0_carry__5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count0_carry__5_n_2\,
      CO(0) => \NLW_count0_carry__5_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => count1(24),
      O(3 downto 1) => \NLW_count0_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \count0_carry__5_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \count0_carry__5_i_1__0_n_0\
    );
\count0_carry__5_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(24),
      O => \count0_carry__5_i_1__0_n_0\
    );
\count0_carry_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count(0),
      O => count1(0)
    );
\count0_carry_i_2__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(3),
      O => \count0_carry_i_2__0_n_0\
    );
\count0_carry_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(2),
      O => \count0_carry_i_3__0_n_0\
    );
\count0_carry_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(1),
      O => \count0_carry_i_4__0_n_0\
    );
count1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count1_carry_n_0,
      CO(2 downto 0) => NLW_count1_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => count(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(4 downto 1),
      S(3 downto 0) => count(4 downto 1)
    );
\count1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count1_carry_n_0,
      CO(3) => \count1_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(8 downto 5),
      S(3 downto 0) => count(8 downto 5)
    );
\count1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__0_n_0\,
      CO(3) => \count1_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(12 downto 9),
      S(3 downto 0) => count(12 downto 9)
    );
\count1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__1_n_0\,
      CO(3) => \count1_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(16 downto 13),
      S(3 downto 0) => count(16 downto 13)
    );
\count1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__2_n_0\,
      CO(3) => \count1_carry__3_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(20 downto 17),
      S(3 downto 0) => count(20 downto 17)
    );
\count1_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__3_n_0\,
      CO(3 downto 0) => \NLW_count1_carry__4_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(24 downto 21),
      S(3 downto 0) => count(24 downto 21)
    );
\count[10]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_5\,
      O => p_0_in(10)
    );
\count[11]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_4\,
      O => p_0_in(11)
    );
\count[12]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_7\,
      O => p_0_in(12)
    );
\count[13]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_6\,
      O => p_0_in(13)
    );
\count[14]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_5\,
      O => p_0_in(14)
    );
\count[15]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_4\,
      O => p_0_in(15)
    );
\count[16]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_7\,
      O => p_0_in(16)
    );
\count[17]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_6\,
      O => p_0_in(17)
    );
\count[18]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_5\,
      O => p_0_in(18)
    );
\count[19]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_4\,
      O => p_0_in(19)
    );
\count[20]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_7\,
      O => p_0_in(20)
    );
\count[21]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_6\,
      O => p_0_in(21)
    );
\count[22]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_5\,
      O => p_0_in(22)
    );
\count[23]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_4\,
      O => p_0_in(23)
    );
\count[24]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__3_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__5_n_7\,
      O => p_0_in(24)
    );
\count[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_7\,
      O => p_0_in(8)
    );
\count[9]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_6\,
      O => p_0_in(9)
    );
\count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(0),
      Q => count(0)
    );
\count_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(10),
      Q => count(10)
    );
\count_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(11),
      Q => count(11)
    );
\count_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(12),
      Q => count(12)
    );
\count_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(13),
      Q => count(13)
    );
\count_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(14),
      Q => count(14)
    );
\count_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(15),
      Q => count(15)
    );
\count_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(16),
      Q => count(16)
    );
\count_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(17),
      Q => count(17)
    );
\count_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(18),
      Q => count(18)
    );
\count_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(19),
      Q => count(19)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(1),
      Q => count(1)
    );
\count_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(20),
      Q => count(20)
    );
\count_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(21),
      Q => count(21)
    );
\count_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(22),
      Q => count(22)
    );
\count_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(23),
      Q => count(23)
    );
\count_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(24),
      Q => count(24)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(2),
      Q => count(2)
    );
\count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(3),
      Q => count(3)
    );
\count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(4),
      Q => count(4)
    );
\count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(5),
      Q => count(5)
    );
\count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(6),
      Q => count(6)
    );
\count_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(7),
      Q => count(7)
    );
\count_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(8),
      Q => count(8)
    );
\count_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(9),
      Q => count(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TIMER_5 is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    \count_reg[24]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of TIMER_5 : entity is "TIMER";
end TIMER_5;

architecture STRUCTURE of TIMER_5 is
  signal \aux[7]_i_3__1_n_0\ : STD_LOGIC;
  signal \aux[7]_i_4__1_n_0\ : STD_LOGIC;
  signal \aux[7]_i_5__1_n_0\ : STD_LOGIC;
  signal \aux[7]_i_6__1_n_0\ : STD_LOGIC;
  signal count : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \count0__49_carry__0_i_1__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_i_2__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_i_3__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_n_0\ : STD_LOGIC;
  signal \count0__49_carry__0_n_4\ : STD_LOGIC;
  signal \count0__49_carry__0_n_5\ : STD_LOGIC;
  signal \count0__49_carry__0_n_6\ : STD_LOGIC;
  signal \count0__49_carry__0_n_7\ : STD_LOGIC;
  signal \count0__49_carry__1_i_1__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_i_2__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_i_3__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__1_n_4\ : STD_LOGIC;
  signal \count0__49_carry__1_n_5\ : STD_LOGIC;
  signal \count0__49_carry__1_n_6\ : STD_LOGIC;
  signal \count0__49_carry__1_n_7\ : STD_LOGIC;
  signal \count0__49_carry__2_i_1__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_i_2__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_n_0\ : STD_LOGIC;
  signal \count0__49_carry__2_n_4\ : STD_LOGIC;
  signal \count0__49_carry__2_n_5\ : STD_LOGIC;
  signal \count0__49_carry__2_n_6\ : STD_LOGIC;
  signal \count0__49_carry__2_n_7\ : STD_LOGIC;
  signal \count0__49_carry__3_n_2\ : STD_LOGIC;
  signal \count0__49_carry__3_n_7\ : STD_LOGIC;
  signal \count0__49_carry_i_1__1_n_0\ : STD_LOGIC;
  signal \count0__49_carry_n_0\ : STD_LOGIC;
  signal \count0__49_carry_n_4\ : STD_LOGIC;
  signal \count0__49_carry_n_5\ : STD_LOGIC;
  signal \count0__49_carry_n_6\ : STD_LOGIC;
  signal \count0__49_carry_n_7\ : STD_LOGIC;
  signal \count0_carry__0_i_1__1_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_2__1_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_3__1_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_4__1_n_0\ : STD_LOGIC;
  signal \count0_carry__0_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_1__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_2__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_3__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_4__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_4\ : STD_LOGIC;
  signal \count0_carry__1_n_5\ : STD_LOGIC;
  signal \count0_carry__1_n_6\ : STD_LOGIC;
  signal \count0_carry__1_n_7\ : STD_LOGIC;
  signal \count0_carry__2_i_1__1_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_2__1_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_3__1_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_4__1_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_4\ : STD_LOGIC;
  signal \count0_carry__2_n_5\ : STD_LOGIC;
  signal \count0_carry__2_n_6\ : STD_LOGIC;
  signal \count0_carry__2_n_7\ : STD_LOGIC;
  signal \count0_carry__3_i_1__1_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_2__1_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_3__1_n_0\ : STD_LOGIC;
  signal \count0_carry__3_i_4__1_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_4\ : STD_LOGIC;
  signal \count0_carry__3_n_5\ : STD_LOGIC;
  signal \count0_carry__3_n_6\ : STD_LOGIC;
  signal \count0_carry__3_n_7\ : STD_LOGIC;
  signal \count0_carry__4_i_1__1_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_2__1_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_3__1_n_0\ : STD_LOGIC;
  signal \count0_carry__4_i_4__1_n_0\ : STD_LOGIC;
  signal \count0_carry__4_n_0\ : STD_LOGIC;
  signal \count0_carry__4_n_4\ : STD_LOGIC;
  signal \count0_carry__4_n_5\ : STD_LOGIC;
  signal \count0_carry__4_n_6\ : STD_LOGIC;
  signal \count0_carry__4_n_7\ : STD_LOGIC;
  signal \count0_carry__5_i_1__1_n_0\ : STD_LOGIC;
  signal \count0_carry__5_n_2\ : STD_LOGIC;
  signal \count0_carry__5_n_7\ : STD_LOGIC;
  signal \count0_carry_i_2__1_n_0\ : STD_LOGIC;
  signal \count0_carry_i_3__1_n_0\ : STD_LOGIC;
  signal \count0_carry_i_4__1_n_0\ : STD_LOGIC;
  signal count0_carry_n_0 : STD_LOGIC;
  signal count1 : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \count1_carry__0_n_0\ : STD_LOGIC;
  signal \count1_carry__1_n_0\ : STD_LOGIC;
  signal \count1_carry__2_n_0\ : STD_LOGIC;
  signal \count1_carry__3_n_0\ : STD_LOGIC;
  signal count1_carry_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \NLW_count0__49_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__49_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0__49_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_count0_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_count1_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \count0__49_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__49_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of count0_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__4\ : label is "SWEEP";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of count1_carry : label is 35;
  attribute OPT_MODIFIED of count1_carry : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__0\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__0\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__1\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__2\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__2\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__3\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__3\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__4\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__4\ : label is "SWEEP";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[10]_i_1__1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \count[11]_i_1__1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \count[12]_i_1__1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \count[13]_i_1__1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \count[14]_i_1__1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \count[15]_i_1__1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \count[16]_i_1__1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \count[17]_i_1__1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \count[18]_i_1__1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \count[19]_i_1__1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \count[20]_i_1__1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \count[21]_i_1__1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \count[22]_i_1__1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \count[23]_i_1__1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \count[24]_i_1__1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \count[9]_i_1__1\ : label is "soft_lutpair30";
begin
\aux[7]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \aux[7]_i_3__1_n_0\,
      I1 => \aux[7]_i_4__1_n_0\,
      I2 => \aux[7]_i_5__1_n_0\,
      I3 => count(0),
      I4 => \aux[7]_i_6__1_n_0\,
      O => E(0)
    );
\aux[7]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => count(3),
      I1 => count(4),
      I2 => count(1),
      I3 => count(2),
      I4 => count(6),
      I5 => count(5),
      O => \aux[7]_i_3__1_n_0\
    );
\aux[7]_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => count(21),
      I1 => count(22),
      I2 => count(20),
      I3 => count(19),
      I4 => count(23),
      I5 => count(24),
      O => \aux[7]_i_4__1_n_0\
    );
\aux[7]_i_5__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => count(16),
      I1 => count(15),
      I2 => count(13),
      I3 => count(14),
      I4 => count(18),
      I5 => count(17),
      O => \aux[7]_i_5__1_n_0\
    );
\aux[7]_i_6__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => count(10),
      I1 => count(9),
      I2 => count(7),
      I3 => count(8),
      I4 => count(12),
      I5 => count(11),
      O => \aux[7]_i_6__1_n_0\
    );
\count0__49_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count0__49_carry_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => B"0010",
      O(3) => \count0__49_carry_n_4\,
      O(2) => \count0__49_carry_n_5\,
      O(1) => \count0__49_carry_n_6\,
      O(0) => \count0__49_carry_n_7\,
      S(3) => \count0_carry__1_n_4\,
      S(2) => \count0_carry__1_n_5\,
      S(1) => \count0__49_carry_i_1__1_n_0\,
      S(0) => \count0_carry__1_n_7\
    );
\count0__49_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry_n_0\,
      CO(3) => \count0__49_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1101",
      O(3) => \count0__49_carry__0_n_4\,
      O(2) => \count0__49_carry__0_n_5\,
      O(1) => \count0__49_carry__0_n_6\,
      O(0) => \count0__49_carry__0_n_7\,
      S(3) => \count0__49_carry__0_i_1__1_n_0\,
      S(2) => \count0__49_carry__0_i_2__1_n_0\,
      S(1) => \count0_carry__2_n_6\,
      S(0) => \count0__49_carry__0_i_3__1_n_0\
    );
\count0__49_carry__0_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_4\,
      O => \count0__49_carry__0_i_1__1_n_0\
    );
\count0__49_carry__0_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_5\,
      O => \count0__49_carry__0_i_2__1_n_0\
    );
\count0__49_carry__0_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_7\,
      O => \count0__49_carry__0_i_3__1_n_0\
    );
\count0__49_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__0_n_0\,
      CO(3) => \count0__49_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1110",
      O(3) => \count0__49_carry__1_n_4\,
      O(2) => \count0__49_carry__1_n_5\,
      O(1) => \count0__49_carry__1_n_6\,
      O(0) => \count0__49_carry__1_n_7\,
      S(3) => \count0__49_carry__1_i_1__1_n_0\,
      S(2) => \count0__49_carry__1_i_2__1_n_0\,
      S(1) => \count0__49_carry__1_i_3__1_n_0\,
      S(0) => \count0_carry__3_n_7\
    );
\count0__49_carry__1_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_4\,
      O => \count0__49_carry__1_i_1__1_n_0\
    );
\count0__49_carry__1_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_5\,
      O => \count0__49_carry__1_i_2__1_n_0\
    );
\count0__49_carry__1_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__3_n_6\,
      O => \count0__49_carry__1_i_3__1_n_0\
    );
\count0__49_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__1_n_0\,
      CO(3) => \count0__49_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0__49_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1100",
      O(3) => \count0__49_carry__2_n_4\,
      O(2) => \count0__49_carry__2_n_5\,
      O(1) => \count0__49_carry__2_n_6\,
      O(0) => \count0__49_carry__2_n_7\,
      S(3) => \count0__49_carry__2_i_1__1_n_0\,
      S(2) => \count0__49_carry__2_i_2__1_n_0\,
      S(1) => \count0_carry__4_n_6\,
      S(0) => \count0_carry__4_n_7\
    );
\count0__49_carry__2_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__4_n_4\,
      O => \count0__49_carry__2_i_1__1_n_0\
    );
\count0__49_carry__2_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__4_n_5\,
      O => \count0__49_carry__2_i_2__1_n_0\
    );
\count0__49_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__49_carry__2_n_0\,
      CO(3 downto 2) => \NLW_count0__49_carry__3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count0__49_carry__3_n_2\,
      CO(0) => \NLW_count0__49_carry__3_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3 downto 1) => \NLW_count0__49_carry__3_O_UNCONNECTED\(3 downto 1),
      O(0) => \count0__49_carry__3_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \count0_carry__5_n_2\,
      S(0) => \count0_carry__5_n_7\
    );
\count0__49_carry_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_6\,
      O => \count0__49_carry_i_1__1_n_0\
    );
count0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count0_carry_n_0,
      CO(2 downto 0) => NLW_count0_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => count1(3 downto 0),
      O(3 downto 0) => p_0_in(3 downto 0),
      S(3) => \count0_carry_i_2__1_n_0\,
      S(2) => \count0_carry_i_3__1_n_0\,
      S(1) => \count0_carry_i_4__1_n_0\,
      S(0) => count(0)
    );
\count0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count0_carry_n_0,
      CO(3) => \count0_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(7 downto 4),
      O(3 downto 0) => p_0_in(7 downto 4),
      S(3) => \count0_carry__0_i_1__1_n_0\,
      S(2) => \count0_carry__0_i_2__1_n_0\,
      S(1) => \count0_carry__0_i_3__1_n_0\,
      S(0) => \count0_carry__0_i_4__1_n_0\
    );
\count0_carry__0_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(7),
      O => \count0_carry__0_i_1__1_n_0\
    );
\count0_carry__0_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(6),
      O => \count0_carry__0_i_2__1_n_0\
    );
\count0_carry__0_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(5),
      O => \count0_carry__0_i_3__1_n_0\
    );
\count0_carry__0_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(4),
      O => \count0_carry__0_i_4__1_n_0\
    );
\count0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__0_n_0\,
      CO(3) => \count0_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(11 downto 8),
      O(3) => \count0_carry__1_n_4\,
      O(2) => \count0_carry__1_n_5\,
      O(1) => \count0_carry__1_n_6\,
      O(0) => \count0_carry__1_n_7\,
      S(3) => \count0_carry__1_i_1__1_n_0\,
      S(2) => \count0_carry__1_i_2__1_n_0\,
      S(1) => \count0_carry__1_i_3__1_n_0\,
      S(0) => \count0_carry__1_i_4__1_n_0\
    );
\count0_carry__1_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(11),
      O => \count0_carry__1_i_1__1_n_0\
    );
\count0_carry__1_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(10),
      O => \count0_carry__1_i_2__1_n_0\
    );
\count0_carry__1_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(9),
      O => \count0_carry__1_i_3__1_n_0\
    );
\count0_carry__1_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(8),
      O => \count0_carry__1_i_4__1_n_0\
    );
\count0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__1_n_0\,
      CO(3) => \count0_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(15 downto 12),
      O(3) => \count0_carry__2_n_4\,
      O(2) => \count0_carry__2_n_5\,
      O(1) => \count0_carry__2_n_6\,
      O(0) => \count0_carry__2_n_7\,
      S(3) => \count0_carry__2_i_1__1_n_0\,
      S(2) => \count0_carry__2_i_2__1_n_0\,
      S(1) => \count0_carry__2_i_3__1_n_0\,
      S(0) => \count0_carry__2_i_4__1_n_0\
    );
\count0_carry__2_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(15),
      O => \count0_carry__2_i_1__1_n_0\
    );
\count0_carry__2_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(14),
      O => \count0_carry__2_i_2__1_n_0\
    );
\count0_carry__2_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(13),
      O => \count0_carry__2_i_3__1_n_0\
    );
\count0_carry__2_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(12),
      O => \count0_carry__2_i_4__1_n_0\
    );
\count0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__2_n_0\,
      CO(3) => \count0_carry__3_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(19 downto 16),
      O(3) => \count0_carry__3_n_4\,
      O(2) => \count0_carry__3_n_5\,
      O(1) => \count0_carry__3_n_6\,
      O(0) => \count0_carry__3_n_7\,
      S(3) => \count0_carry__3_i_1__1_n_0\,
      S(2) => \count0_carry__3_i_2__1_n_0\,
      S(1) => \count0_carry__3_i_3__1_n_0\,
      S(0) => \count0_carry__3_i_4__1_n_0\
    );
\count0_carry__3_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(19),
      O => \count0_carry__3_i_1__1_n_0\
    );
\count0_carry__3_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(18),
      O => \count0_carry__3_i_2__1_n_0\
    );
\count0_carry__3_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(17),
      O => \count0_carry__3_i_3__1_n_0\
    );
\count0_carry__3_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(16),
      O => \count0_carry__3_i_4__1_n_0\
    );
\count0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__3_n_0\,
      CO(3) => \count0_carry__4_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(23 downto 20),
      O(3) => \count0_carry__4_n_4\,
      O(2) => \count0_carry__4_n_5\,
      O(1) => \count0_carry__4_n_6\,
      O(0) => \count0_carry__4_n_7\,
      S(3) => \count0_carry__4_i_1__1_n_0\,
      S(2) => \count0_carry__4_i_2__1_n_0\,
      S(1) => \count0_carry__4_i_3__1_n_0\,
      S(0) => \count0_carry__4_i_4__1_n_0\
    );
\count0_carry__4_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(23),
      O => \count0_carry__4_i_1__1_n_0\
    );
\count0_carry__4_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(22),
      O => \count0_carry__4_i_2__1_n_0\
    );
\count0_carry__4_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(21),
      O => \count0_carry__4_i_3__1_n_0\
    );
\count0_carry__4_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(20),
      O => \count0_carry__4_i_4__1_n_0\
    );
\count0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__4_n_0\,
      CO(3 downto 2) => \NLW_count0_carry__5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count0_carry__5_n_2\,
      CO(0) => \NLW_count0_carry__5_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => count1(24),
      O(3 downto 1) => \NLW_count0_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \count0_carry__5_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \count0_carry__5_i_1__1_n_0\
    );
\count0_carry__5_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(24),
      O => \count0_carry__5_i_1__1_n_0\
    );
\count0_carry_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count(0),
      O => count1(0)
    );
\count0_carry_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(3),
      O => \count0_carry_i_2__1_n_0\
    );
\count0_carry_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(2),
      O => \count0_carry_i_3__1_n_0\
    );
\count0_carry_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(1),
      O => \count0_carry_i_4__1_n_0\
    );
count1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count1_carry_n_0,
      CO(2 downto 0) => NLW_count1_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => count(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(4 downto 1),
      S(3 downto 0) => count(4 downto 1)
    );
\count1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count1_carry_n_0,
      CO(3) => \count1_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(8 downto 5),
      S(3 downto 0) => count(8 downto 5)
    );
\count1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__0_n_0\,
      CO(3) => \count1_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(12 downto 9),
      S(3 downto 0) => count(12 downto 9)
    );
\count1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__1_n_0\,
      CO(3) => \count1_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(16 downto 13),
      S(3 downto 0) => count(16 downto 13)
    );
\count1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__2_n_0\,
      CO(3) => \count1_carry__3_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(20 downto 17),
      S(3 downto 0) => count(20 downto 17)
    );
\count1_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__3_n_0\,
      CO(3 downto 0) => \NLW_count1_carry__4_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(24 downto 21),
      S(3 downto 0) => count(24 downto 21)
    );
\count[10]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_5\,
      O => p_0_in(10)
    );
\count[11]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_4\,
      O => p_0_in(11)
    );
\count[12]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_7\,
      O => p_0_in(12)
    );
\count[13]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_6\,
      O => p_0_in(13)
    );
\count[14]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_5\,
      O => p_0_in(14)
    );
\count[15]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__0_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__2_n_4\,
      O => p_0_in(15)
    );
\count[16]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_7\,
      O => p_0_in(16)
    );
\count[17]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_6\,
      O => p_0_in(17)
    );
\count[18]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_5\,
      O => p_0_in(18)
    );
\count[19]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__1_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__3_n_4\,
      O => p_0_in(19)
    );
\count[20]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_7\,
      O => p_0_in(20)
    );
\count[21]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_6\,
      O => p_0_in(21)
    );
\count[22]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_5\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_5\,
      O => p_0_in(22)
    );
\count[23]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__2_n_4\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__4_n_4\,
      O => p_0_in(23)
    );
\count[24]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry__3_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__5_n_7\,
      O => p_0_in(24)
    );
\count[8]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_7\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_7\,
      O => p_0_in(8)
    );
\count[9]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__49_carry_n_6\,
      I1 => \count0__49_carry__3_n_2\,
      I2 => \count0_carry__1_n_6\,
      O => p_0_in(9)
    );
\count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(0),
      Q => count(0)
    );
\count_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(10),
      Q => count(10)
    );
\count_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(11),
      Q => count(11)
    );
\count_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(12),
      Q => count(12)
    );
\count_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(13),
      Q => count(13)
    );
\count_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(14),
      Q => count(14)
    );
\count_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(15),
      Q => count(15)
    );
\count_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(16),
      Q => count(16)
    );
\count_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(17),
      Q => count(17)
    );
\count_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(18),
      Q => count(18)
    );
\count_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(19),
      Q => count(19)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(1),
      Q => count(1)
    );
\count_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(20),
      Q => count(20)
    );
\count_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(21),
      Q => count(21)
    );
\count_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(22),
      Q => count(22)
    );
\count_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(23),
      Q => count(23)
    );
\count_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(24),
      Q => count(24)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(2),
      Q => count(2)
    );
\count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(3),
      Q => count(3)
    );
\count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(4),
      Q => count(4)
    );
\count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(5),
      Q => count(5)
    );
\count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(6),
      Q => count(6)
    );
\count_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(7),
      Q => count(7)
    );
\count_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(8),
      Q => count(8)
    );
\count_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \count_reg[24]_0\,
      D => p_0_in(9),
      Q => count(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \TIMER__parameterized1\ is
  port (
    reset : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \TIMER__parameterized1\ : entity is "TIMER";
end \TIMER__parameterized1\;

architecture STRUCTURE of \TIMER__parameterized1\ is
  signal count : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \count0__33_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \count0__33_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \count0__33_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \count0__33_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \count0__33_carry__0_n_0\ : STD_LOGIC;
  signal \count0__33_carry__0_n_4\ : STD_LOGIC;
  signal \count0__33_carry__0_n_5\ : STD_LOGIC;
  signal \count0__33_carry__0_n_6\ : STD_LOGIC;
  signal \count0__33_carry__0_n_7\ : STD_LOGIC;
  signal \count0__33_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \count0__33_carry__1_n_0\ : STD_LOGIC;
  signal \count0__33_carry__1_n_4\ : STD_LOGIC;
  signal \count0__33_carry__1_n_5\ : STD_LOGIC;
  signal \count0__33_carry__1_n_6\ : STD_LOGIC;
  signal \count0__33_carry__1_n_7\ : STD_LOGIC;
  signal \count0__33_carry__2_n_1\ : STD_LOGIC;
  signal \count0__33_carry__2_n_6\ : STD_LOGIC;
  signal \count0__33_carry__2_n_7\ : STD_LOGIC;
  signal \count0__33_carry_i_1_n_0\ : STD_LOGIC;
  signal \count0__33_carry_i_2_n_0\ : STD_LOGIC;
  signal \count0__33_carry_n_0\ : STD_LOGIC;
  signal \count0__33_carry_n_4\ : STD_LOGIC;
  signal \count0__33_carry_n_5\ : STD_LOGIC;
  signal \count0__33_carry_n_6\ : STD_LOGIC;
  signal \count0__33_carry_n_7\ : STD_LOGIC;
  signal \count0_carry__0_i_1__3_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_2__3_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_3__3_n_0\ : STD_LOGIC;
  signal \count0_carry__0_i_4__3_n_0\ : STD_LOGIC;
  signal \count0_carry__0_n_0\ : STD_LOGIC;
  signal \count0_carry__0_n_4\ : STD_LOGIC;
  signal \count0_carry__0_n_5\ : STD_LOGIC;
  signal \count0_carry__0_n_6\ : STD_LOGIC;
  signal \count0_carry__0_n_7\ : STD_LOGIC;
  signal \count0_carry__1_i_1__3_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_2__3_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_3__3_n_0\ : STD_LOGIC;
  signal \count0_carry__1_i_4__3_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_0\ : STD_LOGIC;
  signal \count0_carry__1_n_4\ : STD_LOGIC;
  signal \count0_carry__1_n_5\ : STD_LOGIC;
  signal \count0_carry__1_n_6\ : STD_LOGIC;
  signal \count0_carry__1_n_7\ : STD_LOGIC;
  signal \count0_carry__2_i_1__3_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_2__3_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_3__3_n_0\ : STD_LOGIC;
  signal \count0_carry__2_i_4__3_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_0\ : STD_LOGIC;
  signal \count0_carry__2_n_4\ : STD_LOGIC;
  signal \count0_carry__2_n_5\ : STD_LOGIC;
  signal \count0_carry__2_n_6\ : STD_LOGIC;
  signal \count0_carry__2_n_7\ : STD_LOGIC;
  signal \count0_carry__3_i_1__3_n_0\ : STD_LOGIC;
  signal \count0_carry__3_n_2\ : STD_LOGIC;
  signal \count0_carry__3_n_7\ : STD_LOGIC;
  signal \count0_carry_i_2__3_n_0\ : STD_LOGIC;
  signal \count0_carry_i_3__3_n_0\ : STD_LOGIC;
  signal \count0_carry_i_4__3_n_0\ : STD_LOGIC;
  signal count0_carry_n_0 : STD_LOGIC;
  signal count0_carry_n_4 : STD_LOGIC;
  signal count1 : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \count1_carry__0_n_0\ : STD_LOGIC;
  signal \count1_carry__1_n_0\ : STD_LOGIC;
  signal count1_carry_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \^reset\ : STD_LOGIC;
  signal \sel_i[7]_i_2_n_0\ : STD_LOGIC;
  signal \sel_i[7]_i_3_n_0\ : STD_LOGIC;
  signal \sel_i[7]_i_4_n_0\ : STD_LOGIC;
  signal \NLW_count0__33_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__33_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__33_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0__33_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0__33_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_count0_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count0_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count0_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_count1_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \count0__33_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__33_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__33_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0__33_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of count0_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \count0_carry__2\ : label is "SWEEP";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of count1_carry : label is 35;
  attribute OPT_MODIFIED of count1_carry : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__0\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__0\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__1\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count1_carry__2\ : label is 35;
  attribute OPT_MODIFIED of \count1_carry__2\ : label is "SWEEP";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[10]_i_1__3\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \count[11]_i_1__3\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \count[12]_i_1__3\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \count[13]_i_1__3\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \count[14]_i_1__3\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \count[15]_i_1__3\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \count[16]_i_1__3\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \count[3]_i_1__3\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \count[4]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \count[5]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \count[6]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \count[7]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \count[8]_i_1__3\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \count[9]_i_1__3\ : label is "soft_lutpair46";
begin
  reset <= \^reset\;
\FSM_sequential_current_state[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_IBUF,
      O => \^reset\
    );
\count0__33_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count0__33_carry_n_0\,
      CO(2 downto 0) => \NLW_count0__33_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => B"0110",
      O(3) => \count0__33_carry_n_4\,
      O(2) => \count0__33_carry_n_5\,
      O(1) => \count0__33_carry_n_6\,
      O(0) => \count0__33_carry_n_7\,
      S(3) => \count0_carry__0_n_5\,
      S(2) => \count0__33_carry_i_1_n_0\,
      S(1) => \count0__33_carry_i_2_n_0\,
      S(0) => count0_carry_n_4
    );
\count0__33_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__33_carry_n_0\,
      CO(3) => \count0__33_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0__33_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3) => \count0__33_carry__0_n_4\,
      O(2) => \count0__33_carry__0_n_5\,
      O(1) => \count0__33_carry__0_n_6\,
      O(0) => \count0__33_carry__0_n_7\,
      S(3) => \count0__33_carry__0_i_1_n_0\,
      S(2) => \count0__33_carry__0_i_2_n_0\,
      S(1) => \count0__33_carry__0_i_3_n_0\,
      S(0) => \count0__33_carry__0_i_4_n_0\
    );
\count0__33_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_5\,
      O => \count0__33_carry__0_i_1_n_0\
    );
\count0__33_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_6\,
      O => \count0__33_carry__0_i_2_n_0\
    );
\count0__33_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__1_n_7\,
      O => \count0__33_carry__0_i_3_n_0\
    );
\count0__33_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__0_n_4\,
      O => \count0__33_carry__0_i_4_n_0\
    );
\count0__33_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__33_carry__0_n_0\,
      CO(3) => \count0__33_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0__33_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3) => \count0__33_carry__1_n_4\,
      O(2) => \count0__33_carry__1_n_5\,
      O(1) => \count0__33_carry__1_n_6\,
      O(0) => \count0__33_carry__1_n_7\,
      S(3) => \count0_carry__2_n_5\,
      S(2) => \count0_carry__2_n_6\,
      S(1) => \count0__33_carry__1_i_1_n_0\,
      S(0) => \count0_carry__1_n_4\
    );
\count0__33_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__2_n_7\,
      O => \count0__33_carry__1_i_1_n_0\
    );
\count0__33_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0__33_carry__1_n_0\,
      CO(3) => \NLW_count0__33_carry__2_CO_UNCONNECTED\(3),
      CO(2) => \count0__33_carry__2_n_1\,
      CO(1 downto 0) => \NLW_count0__33_carry__2_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0100",
      O(3 downto 2) => \NLW_count0__33_carry__2_O_UNCONNECTED\(3 downto 2),
      O(1) => \count0__33_carry__2_n_6\,
      O(0) => \count0__33_carry__2_n_7\,
      S(3) => '0',
      S(2) => \count0_carry__3_n_2\,
      S(1) => \count0_carry__3_n_7\,
      S(0) => \count0_carry__2_n_4\
    );
\count0__33_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__0_n_6\,
      O => \count0__33_carry_i_1_n_0\
    );
\count0__33_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count0_carry__0_n_7\,
      O => \count0__33_carry_i_2_n_0\
    );
count0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count0_carry_n_0,
      CO(2 downto 0) => NLW_count0_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => count1(3 downto 0),
      O(3) => count0_carry_n_4,
      O(2 downto 0) => p_0_in(2 downto 0),
      S(3) => \count0_carry_i_2__3_n_0\,
      S(2) => \count0_carry_i_3__3_n_0\,
      S(1) => \count0_carry_i_4__3_n_0\,
      S(0) => count(0)
    );
\count0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count0_carry_n_0,
      CO(3) => \count0_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(7 downto 4),
      O(3) => \count0_carry__0_n_4\,
      O(2) => \count0_carry__0_n_5\,
      O(1) => \count0_carry__0_n_6\,
      O(0) => \count0_carry__0_n_7\,
      S(3) => \count0_carry__0_i_1__3_n_0\,
      S(2) => \count0_carry__0_i_2__3_n_0\,
      S(1) => \count0_carry__0_i_3__3_n_0\,
      S(0) => \count0_carry__0_i_4__3_n_0\
    );
\count0_carry__0_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(7),
      O => \count0_carry__0_i_1__3_n_0\
    );
\count0_carry__0_i_2__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(6),
      O => \count0_carry__0_i_2__3_n_0\
    );
\count0_carry__0_i_3__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(5),
      O => \count0_carry__0_i_3__3_n_0\
    );
\count0_carry__0_i_4__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(4),
      O => \count0_carry__0_i_4__3_n_0\
    );
\count0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__0_n_0\,
      CO(3) => \count0_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(11 downto 8),
      O(3) => \count0_carry__1_n_4\,
      O(2) => \count0_carry__1_n_5\,
      O(1) => \count0_carry__1_n_6\,
      O(0) => \count0_carry__1_n_7\,
      S(3) => \count0_carry__1_i_1__3_n_0\,
      S(2) => \count0_carry__1_i_2__3_n_0\,
      S(1) => \count0_carry__1_i_3__3_n_0\,
      S(0) => \count0_carry__1_i_4__3_n_0\
    );
\count0_carry__1_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(11),
      O => \count0_carry__1_i_1__3_n_0\
    );
\count0_carry__1_i_2__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(10),
      O => \count0_carry__1_i_2__3_n_0\
    );
\count0_carry__1_i_3__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(9),
      O => \count0_carry__1_i_3__3_n_0\
    );
\count0_carry__1_i_4__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(8),
      O => \count0_carry__1_i_4__3_n_0\
    );
\count0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__1_n_0\,
      CO(3) => \count0_carry__2_n_0\,
      CO(2 downto 0) => \NLW_count0_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => count1(15 downto 12),
      O(3) => \count0_carry__2_n_4\,
      O(2) => \count0_carry__2_n_5\,
      O(1) => \count0_carry__2_n_6\,
      O(0) => \count0_carry__2_n_7\,
      S(3) => \count0_carry__2_i_1__3_n_0\,
      S(2) => \count0_carry__2_i_2__3_n_0\,
      S(1) => \count0_carry__2_i_3__3_n_0\,
      S(0) => \count0_carry__2_i_4__3_n_0\
    );
\count0_carry__2_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(15),
      O => \count0_carry__2_i_1__3_n_0\
    );
\count0_carry__2_i_2__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(14),
      O => \count0_carry__2_i_2__3_n_0\
    );
\count0_carry__2_i_3__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(13),
      O => \count0_carry__2_i_3__3_n_0\
    );
\count0_carry__2_i_4__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(12),
      O => \count0_carry__2_i_4__3_n_0\
    );
\count0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count0_carry__2_n_0\,
      CO(3 downto 2) => \NLW_count0_carry__3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count0_carry__3_n_2\,
      CO(0) => \NLW_count0_carry__3_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => count1(16),
      O(3 downto 1) => \NLW_count0_carry__3_O_UNCONNECTED\(3 downto 1),
      O(0) => \count0_carry__3_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \count0_carry__3_i_1__3_n_0\
    );
\count0_carry__3_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(16),
      O => \count0_carry__3_i_1__3_n_0\
    );
\count0_carry_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count(0),
      O => count1(0)
    );
\count0_carry_i_2__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(3),
      O => \count0_carry_i_2__3_n_0\
    );
\count0_carry_i_3__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(2),
      O => \count0_carry_i_3__3_n_0\
    );
\count0_carry_i_4__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count1(1),
      O => \count0_carry_i_4__3_n_0\
    );
count1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count1_carry_n_0,
      CO(2 downto 0) => NLW_count1_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => count(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(4 downto 1),
      S(3 downto 0) => count(4 downto 1)
    );
\count1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count1_carry_n_0,
      CO(3) => \count1_carry__0_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(8 downto 5),
      S(3 downto 0) => count(8 downto 5)
    );
\count1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__0_n_0\,
      CO(3) => \count1_carry__1_n_0\,
      CO(2 downto 0) => \NLW_count1_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(12 downto 9),
      S(3 downto 0) => count(12 downto 9)
    );
\count1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count1_carry__1_n_0\,
      CO(3 downto 0) => \NLW_count1_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count1(16 downto 13),
      S(3 downto 0) => count(16 downto 13)
    );
\count[10]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__0_n_4\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__1_n_5\,
      O => p_0_in(10)
    );
\count[11]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__1_n_7\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__1_n_4\,
      O => p_0_in(11)
    );
\count[12]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__1_n_6\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__2_n_7\,
      O => p_0_in(12)
    );
\count[13]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__1_n_5\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__2_n_6\,
      O => p_0_in(13)
    );
\count[14]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__1_n_4\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__2_n_5\,
      O => p_0_in(14)
    );
\count[15]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__2_n_7\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__2_n_4\,
      O => p_0_in(15)
    );
\count[16]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__2_n_6\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__3_n_7\,
      O => p_0_in(16)
    );
\count[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry_n_7\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => count0_carry_n_4,
      O => p_0_in(3)
    );
\count[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry_n_6\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__0_n_7\,
      O => p_0_in(4)
    );
\count[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry_n_5\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__0_n_6\,
      O => p_0_in(5)
    );
\count[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry_n_4\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__0_n_5\,
      O => p_0_in(6)
    );
\count[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__0_n_7\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__0_n_4\,
      O => p_0_in(7)
    );
\count[8]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__0_n_6\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__1_n_7\,
      O => p_0_in(8)
    );
\count[9]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \count0__33_carry__0_n_5\,
      I1 => \count0__33_carry__2_n_1\,
      I2 => \count0_carry__1_n_6\,
      O => p_0_in(9)
    );
\count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(0),
      Q => count(0)
    );
\count_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(10),
      Q => count(10)
    );
\count_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(11),
      Q => count(11)
    );
\count_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(12),
      Q => count(12)
    );
\count_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(13),
      Q => count(13)
    );
\count_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(14),
      Q => count(14)
    );
\count_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(15),
      Q => count(15)
    );
\count_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(16),
      Q => count(16)
    );
\count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(1),
      Q => count(1)
    );
\count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(2),
      Q => count(2)
    );
\count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(3),
      Q => count(3)
    );
\count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(4),
      Q => count(4)
    );
\count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(5),
      Q => count(5)
    );
\count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(6),
      Q => count(6)
    );
\count_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(7),
      Q => count(7)
    );
\count_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(8),
      Q => count(8)
    );
\count_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => \^reset\,
      D => p_0_in(9),
      Q => count(9)
    );
\sel_i[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \sel_i[7]_i_2_n_0\,
      I1 => \sel_i[7]_i_3_n_0\,
      I2 => \sel_i[7]_i_4_n_0\,
      O => E(0)
    );
\sel_i[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => count(7),
      I1 => count(8),
      I2 => count(6),
      I3 => count(5),
      I4 => count(10),
      I5 => count(9),
      O => \sel_i[7]_i_2_n_0\
    );
\sel_i[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => count(0),
      I1 => count(1),
      I2 => count(2),
      I3 => count(4),
      I4 => count(3),
      O => \sel_i[7]_i_3_n_0\
    );
\sel_i[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => count(13),
      I1 => count(14),
      I2 => count(11),
      I3 => count(12),
      I4 => count(16),
      I5 => count(15),
      O => \sel_i[7]_i_4_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity comparador_completo is
  port (
    coinciden : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]\ : in STD_LOGIC;
    done : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end comparador_completo;

architecture STRUCTURE of comparador_completo is
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of inter_comparacion_reg : label is "LDC";
begin
inter_comparacion_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => AR(0),
      D => \FSM_sequential_current_state_reg[0]\,
      G => done,
      GE => '1',
      Q => coinciden
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity debounce is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    \flipflops_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC
  );
end debounce;

architecture STRUCTURE of debounce is
  signal \^d\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal count0 : STD_LOGIC;
  signal \count[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \count[0]_i_4_n_0\ : STD_LOGIC;
  signal \count[0]_i_5_n_0\ : STD_LOGIC;
  signal \count[0]_i_6_n_0\ : STD_LOGIC;
  signal \count[0]_i_7_n_0\ : STD_LOGIC;
  signal count_reg : STD_LOGIC_VECTOR ( 19 downto 6 );
  signal \count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \count_reg_n_0_[0]\ : STD_LOGIC;
  signal \count_reg_n_0_[1]\ : STD_LOGIC;
  signal \count_reg_n_0_[2]\ : STD_LOGIC;
  signal \count_reg_n_0_[3]\ : STD_LOGIC;
  signal \count_reg_n_0_[4]\ : STD_LOGIC;
  signal \count_reg_n_0_[5]\ : STD_LOGIC;
  signal \flipflops_reg_n_0_[0]\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal result_i_1_n_0 : STD_LOGIC;
  signal \NLW_count_reg[0]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count_reg[4]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_count_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \count_reg[0]_i_3\ : label is 11;
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \count_reg[0]_i_3\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count_reg[12]_i_1\ : label is 11;
  attribute OPT_MODIFIED of \count_reg[12]_i_1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count_reg[16]_i_1\ : label is 11;
  attribute OPT_MODIFIED of \count_reg[16]_i_1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count_reg[4]_i_1\ : label is 11;
  attribute OPT_MODIFIED of \count_reg[4]_i_1\ : label is "SWEEP";
  attribute ADDER_THRESHOLD of \count_reg[8]_i_1\ : label is 11;
  attribute OPT_MODIFIED of \count_reg[8]_i_1\ : label is "SWEEP";
begin
  D(0) <= \^d\(0);
\count[0]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \flipflops_reg_n_0_[0]\,
      I2 => p_0_in,
      O => \count[0]_i_1__3_n_0\
    );
\count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \count[0]_i_4_n_0\,
      O => count0
    );
\count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => count_reg(15),
      I1 => \count[0]_i_6_n_0\,
      I2 => count_reg(18),
      I3 => count_reg(19),
      I4 => count_reg(16),
      I5 => count_reg(17),
      O => \count[0]_i_4_n_0\
    );
\count[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count_reg_n_0_[0]\,
      O => \count[0]_i_5_n_0\
    );
\count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00015555FFFFFFFF"
    )
        port map (
      I0 => \count[0]_i_7_n_0\,
      I1 => count_reg(6),
      I2 => count_reg(7),
      I3 => count_reg(8),
      I4 => count_reg(9),
      I5 => count_reg(14),
      O => \count[0]_i_6_n_0\
    );
\count[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_reg(11),
      I1 => count_reg(10),
      I2 => count_reg(13),
      I3 => count_reg(12),
      O => \count[0]_i_7_n_0\
    );
\count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[0]_i_3_n_7\,
      Q => \count_reg_n_0_[0]\,
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_reg[0]_i_3_n_0\,
      CO(2 downto 0) => \NLW_count_reg[0]_i_3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_reg[0]_i_3_n_4\,
      O(2) => \count_reg[0]_i_3_n_5\,
      O(1) => \count_reg[0]_i_3_n_6\,
      O(0) => \count_reg[0]_i_3_n_7\,
      S(3) => \count_reg_n_0_[3]\,
      S(2) => \count_reg_n_0_[2]\,
      S(1) => \count_reg_n_0_[1]\,
      S(0) => \count[0]_i_5_n_0\
    );
\count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[8]_i_1_n_5\,
      Q => count_reg(10),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[8]_i_1_n_4\,
      Q => count_reg(11),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[12]_i_1_n_7\,
      Q => count_reg(12),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_reg[8]_i_1_n_0\,
      CO(3) => \count_reg[12]_i_1_n_0\,
      CO(2 downto 0) => \NLW_count_reg[12]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_reg[12]_i_1_n_4\,
      O(2) => \count_reg[12]_i_1_n_5\,
      O(1) => \count_reg[12]_i_1_n_6\,
      O(0) => \count_reg[12]_i_1_n_7\,
      S(3 downto 0) => count_reg(15 downto 12)
    );
\count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[12]_i_1_n_6\,
      Q => count_reg(13),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[12]_i_1_n_5\,
      Q => count_reg(14),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[12]_i_1_n_4\,
      Q => count_reg(15),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[16]_i_1_n_7\,
      Q => count_reg(16),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_reg[16]_i_1_n_4\,
      O(2) => \count_reg[16]_i_1_n_5\,
      O(1) => \count_reg[16]_i_1_n_6\,
      O(0) => \count_reg[16]_i_1_n_7\,
      S(3 downto 0) => count_reg(19 downto 16)
    );
\count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[16]_i_1_n_6\,
      Q => count_reg(17),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[16]_i_1_n_5\,
      Q => count_reg(18),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[16]_i_1_n_4\,
      Q => count_reg(19),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[0]_i_3_n_6\,
      Q => \count_reg_n_0_[1]\,
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[0]_i_3_n_5\,
      Q => \count_reg_n_0_[2]\,
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[0]_i_3_n_4\,
      Q => \count_reg_n_0_[3]\,
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[4]_i_1_n_7\,
      Q => \count_reg_n_0_[4]\,
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_reg[0]_i_3_n_0\,
      CO(3) => \count_reg[4]_i_1_n_0\,
      CO(2 downto 0) => \NLW_count_reg[4]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_reg[4]_i_1_n_4\,
      O(2) => \count_reg[4]_i_1_n_5\,
      O(1) => \count_reg[4]_i_1_n_6\,
      O(0) => \count_reg[4]_i_1_n_7\,
      S(3 downto 2) => count_reg(7 downto 6),
      S(1) => \count_reg_n_0_[5]\,
      S(0) => \count_reg_n_0_[4]\
    );
\count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[4]_i_1_n_6\,
      Q => \count_reg_n_0_[5]\,
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[4]_i_1_n_5\,
      Q => count_reg(6),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[4]_i_1_n_4\,
      Q => count_reg(7),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[8]_i_1_n_7\,
      Q => count_reg(8),
      R => \count[0]_i_1__3_n_0\
    );
\count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_reg[4]_i_1_n_0\,
      CO(3) => \count_reg[8]_i_1_n_0\,
      CO(2 downto 0) => \NLW_count_reg[8]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_reg[8]_i_1_n_4\,
      O(2) => \count_reg[8]_i_1_n_5\,
      O(1) => \count_reg[8]_i_1_n_6\,
      O(0) => \count_reg[8]_i_1_n_7\,
      S(3 downto 0) => count_reg(11 downto 8)
    );
\count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => count0,
      D => \count_reg[8]_i_1_n_6\,
      Q => count_reg(9),
      R => \count[0]_i_1__3_n_0\
    );
\flipflops_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \flipflops_reg[0]_0\(0),
      Q => \flipflops_reg_n_0_[0]\
    );
\flipflops_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \flipflops_reg_n_0_[0]\,
      Q => p_0_in
    );
result_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE08"
    )
        port map (
      I0 => \flipflops_reg_n_0_[0]\,
      I1 => p_0_in,
      I2 => \count[0]_i_4_n_0\,
      I3 => \^d\(0),
      O => result_i_1_n_0
    );
result_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => result_i_1_n_0,
      Q => \^d\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    current_state_reg : out STD_LOGIC_VECTOR ( 1 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_current_state_reg[0]_1\ : in STD_LOGIC;
    i : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_current_state_reg[0]_2\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    coinciden : in STD_LOGIC;
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end fsm;

architecture STRUCTURE of fsm is
  signal \FSM_sequential_current_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \^current_state_reg\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[3]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_1\ : label is "soft_lutpair1";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s1:01,iSTATE:10,s0:00,iSTATE0:11";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s1:01,iSTATE:10,s0:00,iSTATE0:11";
  attribute SOFT_HLUTNM of \count[19]_i_1__2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \i[0]_i_1\ : label is "soft_lutpair0";
begin
  current_state_reg(1 downto 0) <= \^current_state_reg\(1 downto 0);
\FSM_onehot_current_state[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^current_state_reg\(1),
      I1 => Q(0),
      I2 => \^current_state_reg\(0),
      O => D(0)
    );
\FSM_onehot_current_state[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => Q(0),
      I1 => \^current_state_reg\(0),
      I2 => \^current_state_reg\(1),
      O => D(1)
    );
\FSM_sequential_current_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0DFF0D0"
    )
        port map (
      I0 => coinciden,
      I1 => \FSM_sequential_current_state_reg[0]_2\,
      I2 => \^current_state_reg\(0),
      I3 => \^current_state_reg\(1),
      I4 => \FSM_sequential_current_state_reg[0]_1\,
      O => \FSM_sequential_current_state[0]_i_1_n_0\
    );
\FSM_sequential_current_state[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^current_state_reg\(1),
      I1 => \^current_state_reg\(0),
      I2 => \FSM_sequential_current_state_reg[0]_2\,
      O => \FSM_sequential_current_state[1]_i_1_n_0\
    );
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \FSM_sequential_current_state[0]_i_1_n_0\,
      Q => \^current_state_reg\(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \FSM_sequential_current_state[1]_i_1_n_0\,
      Q => \^current_state_reg\(1)
    );
\count[19]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^current_state_reg\(0),
      I1 => \^current_state_reg\(1),
      O => E(0)
    );
\i[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70008"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[0]_1\,
      I1 => \^current_state_reg\(0),
      I2 => \^current_state_reg\(1),
      I3 => i(1),
      I4 => i(0),
      O => \FSM_sequential_current_state_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm_visualizer is
  port (
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_current_state_reg[1]_0\ : out STD_LOGIC;
    ctrl_bus_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \FSM_onehot_current_state_reg[3]_0\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[3]_1\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[3]_2\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[1]_1\ : out STD_LOGIC;
    current_state_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \ctrl_bus[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \ctrl_bus_OBUF[7]_inst_i_1_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \ctrl_bus[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \ctrl_bus_OBUF[7]_inst_i_1_1\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    \FSM_onehot_current_state_reg[0]_0\ : in STD_LOGIC
  );
end fsm_visualizer;

architecture STRUCTURE of fsm_visualizer is
  signal \FSM_onehot_current_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_state_reg[3]_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_state_reg[3]_1\ : STD_LOGIC;
  signal \^fsm_onehot_current_state_reg[3]_2\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \ctrl_bus_OBUF[0]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[1]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[2]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[3]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[4]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[5]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[6]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[7]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ctrl_bus_OBUF[7]_inst_i_3_n_0\ : STD_LOGIC;
  signal fsm2Output : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]\ : label is "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[1]\ : label is "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[2]\ : label is "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]\ : label is "s0:0001,s1:0010,iSTATE:1000,iSTATE0:0100";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ctrl_bus_OBUF[5]_inst_i_3\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \ctrl_bus_OBUF[6]_inst_i_3\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \ctrl_bus_OBUF[7]_inst_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \ctrl_bus_OBUF[7]_inst_i_4\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \display_bus_OBUF[2]_inst_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of en_point_i_1 : label is "soft_lutpair22";
begin
  \FSM_onehot_current_state_reg[3]_0\ <= \^fsm_onehot_current_state_reg[3]_0\;
  \FSM_onehot_current_state_reg[3]_1\ <= \^fsm_onehot_current_state_reg[3]_1\;
  \FSM_onehot_current_state_reg[3]_2\ <= \^fsm_onehot_current_state_reg[3]_2\;
  Q(0) <= \^q\(0);
\FSM_onehot_current_state[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ACA0"
    )
        port map (
      I0 => \^q\(0),
      I1 => current_state_reg(0),
      I2 => current_state_reg(1),
      I3 => fsm2Output(3),
      O => \FSM_onehot_current_state[3]_i_1_n_0\
    );
\FSM_onehot_current_state_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      D => '0',
      PRE => \FSM_onehot_current_state_reg[0]_0\,
      Q => fsm2Output(3)
    );
\FSM_onehot_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      CLR => \FSM_onehot_current_state_reg[0]_0\,
      D => fsm2Output(3),
      Q => \^q\(0)
    );
\FSM_onehot_current_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      CLR => \FSM_onehot_current_state_reg[0]_0\,
      D => D(0),
      Q => fsm2Output(1)
    );
\FSM_onehot_current_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      CLR => \FSM_onehot_current_state_reg[0]_0\,
      D => D(1),
      Q => fsm2Output(0)
    );
\ctrl_bus_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF8F8F8"
    )
        port map (
      I0 => \ctrl_bus[7]\(0),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[0]_inst_i_2_n_0\,
      I3 => \ctrl_bus_OBUF[7]_inst_i_1_0\(0),
      I4 => \^fsm_onehot_current_state_reg[3]_0\,
      O => ctrl_bus_OBUF(0)
    );
\ctrl_bus_OBUF[0]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000C0A00"
    )
        port map (
      I0 => \ctrl_bus[7]_0\(0),
      I1 => \ctrl_bus_OBUF[7]_inst_i_1_1\(0),
      I2 => fsm2Output(0),
      I3 => fsm2Output(3),
      I4 => fsm2Output(1),
      I5 => \^q\(0),
      O => \ctrl_bus_OBUF[0]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8F8FFF8"
    )
        port map (
      I0 => \ctrl_bus[7]\(1),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[1]_inst_i_2_n_0\,
      I3 => \ctrl_bus[7]_0\(1),
      I4 => \^fsm_onehot_current_state_reg[3]_1\,
      O => ctrl_bus_OBUF(1)
    );
\ctrl_bus_OBUF[1]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000A000C0000"
    )
        port map (
      I0 => \ctrl_bus_OBUF[7]_inst_i_1_0\(1),
      I1 => \ctrl_bus_OBUF[7]_inst_i_1_1\(1),
      I2 => fsm2Output(0),
      I3 => fsm2Output(3),
      I4 => fsm2Output(1),
      I5 => \^q\(0),
      O => \ctrl_bus_OBUF[1]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8F8FFF8"
    )
        port map (
      I0 => \ctrl_bus[7]\(2),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[2]_inst_i_2_n_0\,
      I3 => \ctrl_bus[7]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_1\,
      O => ctrl_bus_OBUF(2)
    );
\ctrl_bus_OBUF[2]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000032000000020"
    )
        port map (
      I0 => \ctrl_bus_OBUF[7]_inst_i_1_1\(2),
      I1 => fsm2Output(0),
      I2 => fsm2Output(1),
      I3 => \^q\(0),
      I4 => fsm2Output(3),
      I5 => \ctrl_bus_OBUF[7]_inst_i_1_0\(2),
      O => \ctrl_bus_OBUF[2]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8F8FFF8"
    )
        port map (
      I0 => \ctrl_bus[7]\(3),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[3]_inst_i_2_n_0\,
      I3 => \ctrl_bus[7]_0\(3),
      I4 => \^fsm_onehot_current_state_reg[3]_1\,
      O => ctrl_bus_OBUF(3)
    );
\ctrl_bus_OBUF[3]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000A000C0000"
    )
        port map (
      I0 => \ctrl_bus_OBUF[7]_inst_i_1_0\(3),
      I1 => \ctrl_bus_OBUF[7]_inst_i_1_1\(3),
      I2 => fsm2Output(0),
      I3 => fsm2Output(3),
      I4 => fsm2Output(1),
      I5 => \^q\(0),
      O => \ctrl_bus_OBUF[3]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF8F8F8"
    )
        port map (
      I0 => \ctrl_bus[7]\(4),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[4]_inst_i_2_n_0\,
      I3 => \ctrl_bus_OBUF[7]_inst_i_1_0\(4),
      I4 => \^fsm_onehot_current_state_reg[3]_0\,
      O => ctrl_bus_OBUF(4)
    );
\ctrl_bus_OBUF[4]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000C0A00"
    )
        port map (
      I0 => \ctrl_bus[7]_0\(4),
      I1 => \ctrl_bus_OBUF[7]_inst_i_1_1\(4),
      I2 => fsm2Output(0),
      I3 => fsm2Output(3),
      I4 => fsm2Output(1),
      I5 => \^q\(0),
      O => \ctrl_bus_OBUF[4]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF8F8F8"
    )
        port map (
      I0 => \ctrl_bus[7]\(5),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[5]_inst_i_2_n_0\,
      I3 => \^fsm_onehot_current_state_reg[3]_2\,
      I4 => \ctrl_bus_OBUF[7]_inst_i_1_1\(5),
      O => ctrl_bus_OBUF(5)
    );
\ctrl_bus_OBUF[5]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002030000020000"
    )
        port map (
      I0 => \ctrl_bus[7]_0\(5),
      I1 => fsm2Output(0),
      I2 => fsm2Output(1),
      I3 => \^q\(0),
      I4 => fsm2Output(3),
      I5 => \ctrl_bus_OBUF[7]_inst_i_1_0\(5),
      O => \ctrl_bus_OBUF[5]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[5]_inst_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => fsm2Output(0),
      I1 => fsm2Output(3),
      I2 => fsm2Output(1),
      I3 => \^q\(0),
      O => \^fsm_onehot_current_state_reg[3]_2\
    );
\ctrl_bus_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF8F8F8"
    )
        port map (
      I0 => \ctrl_bus[7]\(6),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[6]_inst_i_2_n_0\,
      I3 => \ctrl_bus_OBUF[7]_inst_i_1_0\(6),
      I4 => \^fsm_onehot_current_state_reg[3]_0\,
      O => ctrl_bus_OBUF(6)
    );
\ctrl_bus_OBUF[6]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000C0A00"
    )
        port map (
      I0 => \ctrl_bus[7]_0\(6),
      I1 => \ctrl_bus_OBUF[7]_inst_i_1_1\(6),
      I2 => fsm2Output(0),
      I3 => fsm2Output(3),
      I4 => fsm2Output(1),
      I5 => \^q\(0),
      O => \ctrl_bus_OBUF[6]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[6]_inst_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => fsm2Output(0),
      I1 => fsm2Output(1),
      I2 => \^q\(0),
      I3 => fsm2Output(3),
      O => \^fsm_onehot_current_state_reg[3]_0\
    );
\ctrl_bus_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8F8FFF8"
    )
        port map (
      I0 => \ctrl_bus[7]\(7),
      I1 => \ctrl_bus_OBUF[7]_inst_i_2_n_0\,
      I2 => \ctrl_bus_OBUF[7]_inst_i_3_n_0\,
      I3 => \ctrl_bus[7]_0\(7),
      I4 => \^fsm_onehot_current_state_reg[3]_1\,
      O => ctrl_bus_OBUF(7)
    );
\ctrl_bus_OBUF[7]_inst_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEB"
    )
        port map (
      I0 => fsm2Output(0),
      I1 => fsm2Output(3),
      I2 => fsm2Output(1),
      I3 => \^q\(0),
      O => \ctrl_bus_OBUF[7]_inst_i_2_n_0\
    );
\ctrl_bus_OBUF[7]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000A000C0000"
    )
        port map (
      I0 => \ctrl_bus_OBUF[7]_inst_i_1_0\(7),
      I1 => \ctrl_bus_OBUF[7]_inst_i_1_1\(7),
      I2 => fsm2Output(0),
      I3 => fsm2Output(3),
      I4 => fsm2Output(1),
      I5 => \^q\(0),
      O => \ctrl_bus_OBUF[7]_inst_i_3_n_0\
    );
\ctrl_bus_OBUF[7]_inst_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => fsm2Output(0),
      I1 => fsm2Output(1),
      I2 => \^q\(0),
      I3 => fsm2Output(3),
      O => \^fsm_onehot_current_state_reg[3]_1\
    );
\display_bus_OBUF[2]_inst_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFED"
    )
        port map (
      I0 => \^q\(0),
      I1 => fsm2Output(1),
      I2 => fsm2Output(3),
      I3 => fsm2Output(0),
      O => \FSM_onehot_current_state_reg[1]_1\
    );
en_point_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \FSM_onehot_current_state_reg[1]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity pass_storage is
  port (
    done : out STD_LOGIC;
    \i_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \pass_completa_reg[3][1]_0\ : out STD_LOGIC;
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    \pass_completa_reg[0][0]_0\ : in STD_LOGIC;
    current_state_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    done_reg_0 : in STD_LOGIC;
    \i_reg[0]_0\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end pass_storage;

architecture STRUCTURE of pass_storage is
  signal \^done\ : STD_LOGIC;
  signal done_i_1_n_0 : STD_LOGIC;
  signal i : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \i[1]_i_1_n_0\ : STD_LOGIC;
  signal \i[2]_i_1_n_0\ : STD_LOGIC;
  signal \^i_reg[2]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal inter_comparacion_reg_i_2_n_0 : STD_LOGIC;
  signal inter_comparacion_reg_i_3_n_0 : STD_LOGIC;
  signal \pass_completa_reg[0]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \pass_completa_reg[0]0\ : STD_LOGIC;
  signal \pass_completa_reg[1]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \pass_completa_reg[1]0\ : STD_LOGIC;
  signal \pass_completa_reg[2]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \pass_completa_reg[2]0\ : STD_LOGIC;
  signal \pass_completa_reg[3]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \pass_completa_reg[3]0\ : STD_LOGIC;
begin
  done <= \^done\;
  \i_reg[2]_0\(1 downto 0) <= \^i_reg[2]_0\(1 downto 0);
done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAFF8000"
    )
        port map (
      I0 => \^i_reg[2]_0\(1),
      I1 => \^i_reg[2]_0\(0),
      I2 => i(1),
      I3 => done_reg_0,
      I4 => \^done\,
      O => done_i_1_n_0
    );
done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => done_i_1_n_0,
      Q => \^done\
    );
\i[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF700000008"
    )
        port map (
      I0 => \pass_completa_reg[0][0]_0\,
      I1 => current_state_reg(0),
      I2 => current_state_reg(1),
      I3 => \^i_reg[2]_0\(1),
      I4 => \^i_reg[2]_0\(0),
      I5 => i(1),
      O => \i[1]_i_1_n_0\
    );
\i[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF00FF00FF08"
    )
        port map (
      I0 => \pass_completa_reg[0][0]_0\,
      I1 => current_state_reg(0),
      I2 => current_state_reg(1),
      I3 => \^i_reg[2]_0\(1),
      I4 => i(1),
      I5 => \^i_reg[2]_0\(0),
      O => \i[2]_i_1_n_0\
    );
\i_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \i_reg[0]_0\,
      PRE => AR(0),
      Q => \^i_reg[2]_0\(0)
    );
\i_reg[1]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \i[1]_i_1_n_0\,
      PRE => AR(0),
      Q => i(1)
    );
\i_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \i[2]_i_1_n_0\,
      Q => \^i_reg[2]_0\(1)
    );
inter_comparacion_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => inter_comparacion_reg_i_2_n_0,
      I1 => \pass_completa_reg[3]\(1),
      I2 => \pass_completa_reg[3]\(0),
      I3 => \pass_completa_reg[3]\(3),
      I4 => \pass_completa_reg[3]\(2),
      I5 => inter_comparacion_reg_i_3_n_0,
      O => \pass_completa_reg[3][1]_0\
    );
inter_comparacion_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \pass_completa_reg[0]\(1),
      I1 => \pass_completa_reg[0]\(0),
      I2 => \pass_completa_reg[1]\(2),
      I3 => \pass_completa_reg[1]\(3),
      I4 => \pass_completa_reg[0]\(3),
      I5 => \pass_completa_reg[0]\(2),
      O => inter_comparacion_reg_i_2_n_0
    );
inter_comparacion_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => \pass_completa_reg[2]\(3),
      I1 => \pass_completa_reg[2]\(2),
      I2 => \pass_completa_reg[2]\(0),
      I3 => \pass_completa_reg[2]\(1),
      I4 => \pass_completa_reg[1]\(0),
      I5 => \pass_completa_reg[1]\(1),
      O => inter_comparacion_reg_i_3_n_0
    );
\pass_completa[0][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => \pass_completa_reg[0][0]_0\,
      I1 => current_state_reg(0),
      I2 => current_state_reg(1),
      I3 => \^i_reg[2]_0\(0),
      I4 => i(1),
      I5 => \^i_reg[2]_0\(1),
      O => \pass_completa_reg[0]0\
    );
\pass_completa[1][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000080000"
    )
        port map (
      I0 => \pass_completa_reg[0][0]_0\,
      I1 => current_state_reg(0),
      I2 => current_state_reg(1),
      I3 => \^i_reg[2]_0\(1),
      I4 => \^i_reg[2]_0\(0),
      I5 => i(1),
      O => \pass_completa_reg[1]0\
    );
\pass_completa[2][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000080000"
    )
        port map (
      I0 => \pass_completa_reg[0][0]_0\,
      I1 => current_state_reg(0),
      I2 => current_state_reg(1),
      I3 => \^i_reg[2]_0\(1),
      I4 => i(1),
      I5 => \^i_reg[2]_0\(0),
      O => \pass_completa_reg[2]0\
    );
\pass_completa[3][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000000000000"
    )
        port map (
      I0 => \pass_completa_reg[0][0]_0\,
      I1 => current_state_reg(0),
      I2 => current_state_reg(1),
      I3 => \^i_reg[2]_0\(1),
      I4 => i(1),
      I5 => \^i_reg[2]_0\(0),
      O => \pass_completa_reg[3]0\
    );
\pass_completa_reg[0][0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[0]0\,
      CLR => AR(0),
      D => D(0),
      Q => \pass_completa_reg[0]\(0)
    );
\pass_completa_reg[0][1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[0]0\,
      CLR => AR(0),
      D => D(1),
      Q => \pass_completa_reg[0]\(1)
    );
\pass_completa_reg[0][2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[0]0\,
      CLR => AR(0),
      D => D(2),
      Q => \pass_completa_reg[0]\(2)
    );
\pass_completa_reg[0][3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[0]0\,
      CLR => AR(0),
      D => D(3),
      Q => \pass_completa_reg[0]\(3)
    );
\pass_completa_reg[1][0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[1]0\,
      CLR => AR(0),
      D => D(0),
      Q => \pass_completa_reg[1]\(0)
    );
\pass_completa_reg[1][1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[1]0\,
      CLR => AR(0),
      D => D(1),
      Q => \pass_completa_reg[1]\(1)
    );
\pass_completa_reg[1][2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[1]0\,
      CLR => AR(0),
      D => D(2),
      Q => \pass_completa_reg[1]\(2)
    );
\pass_completa_reg[1][3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[1]0\,
      CLR => AR(0),
      D => D(3),
      Q => \pass_completa_reg[1]\(3)
    );
\pass_completa_reg[2][0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[2]0\,
      CLR => AR(0),
      D => D(0),
      Q => \pass_completa_reg[2]\(0)
    );
\pass_completa_reg[2][1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[2]0\,
      CLR => AR(0),
      D => D(1),
      Q => \pass_completa_reg[2]\(1)
    );
\pass_completa_reg[2][2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[2]0\,
      CLR => AR(0),
      D => D(2),
      Q => \pass_completa_reg[2]\(2)
    );
\pass_completa_reg[2][3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[2]0\,
      CLR => AR(0),
      D => D(3),
      Q => \pass_completa_reg[2]\(3)
    );
\pass_completa_reg[3][0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[3]0\,
      CLR => AR(0),
      D => D(0),
      Q => \pass_completa_reg[3]\(0)
    );
\pass_completa_reg[3][1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[3]0\,
      CLR => AR(0),
      D => D(1),
      Q => \pass_completa_reg[3]\(1)
    );
\pass_completa_reg[3][2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[3]0\,
      CLR => AR(0),
      D => D(2),
      Q => \pass_completa_reg[3]\(2)
    );
\pass_completa_reg[3][3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => \pass_completa_reg[3]0\,
      CLR => AR(0),
      D => D(3),
      Q => \pass_completa_reg[3]\(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity selected is
  port (
    \sel2vis[3]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sel2vis[2]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sel2vis[1]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sel2vis[0]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \count_reg[2]\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[0]_0\ : out STD_LOGIC;
    \count_reg[1]\ : out STD_LOGIC;
    \count_reg[0]\ : out STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_3_0\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_3_1\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[0]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus_OBUF[2]_inst_i_1\ : in STD_LOGIC;
    \display_bus_OBUF[1]_inst_i_1\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1_0\ : in STD_LOGIC;
    \display_bus_OBUF[1]_inst_i_1_0\ : in STD_LOGIC;
    \display_bus_OBUF[1]_inst_i_1_1\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \FSM_onehot_current_state_reg[3]_0\ : in STD_LOGIC
  );
end selected;

architecture STRUCTURE of selected is
  signal \^fsm_onehot_current_state_reg[0]_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[3]\ : STD_LOGIC;
  signal current_state : STD_LOGIC;
  signal \display_bus_OBUF[1]_inst_i_5_n_0\ : STD_LOGIC;
  signal \display_bus_OBUF[2]_inst_i_5_n_0\ : STD_LOGIC;
  signal \number_reg[0][2]_i_1_n_0\ : STD_LOGIC;
  signal \number_reg[1][3]_i_1_n_0\ : STD_LOGIC;
  signal \number_reg[2][1]_i_1_n_0\ : STD_LOGIC;
  signal \number_reg[3][2]_i_1_n_0\ : STD_LOGIC;
  signal \^sel2vis[0]_3\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^sel2vis[1]_2\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^sel2vis[2]_1\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^sel2vis[3]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]\ : label is "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[1]\ : label is "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[2]\ : label is "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]\ : label is "iSTATE:10000,s3:01000,s2:00100,s1:00010,s0:00001,";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \display_bus_OBUF[1]_inst_i_5\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \display_bus_OBUF[2]_inst_i_5\ : label is "soft_lutpair39";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \number_reg[0][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \number_reg[0][2]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \number_reg[1][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \number_reg[1][3]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \number_reg[2][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \number_reg[2][1]_i_1\ : label is "soft_lutpair41";
  attribute XILINX_LEGACY_PRIM of \number_reg[3][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \number_reg[3][2]_i_1\ : label is "soft_lutpair41";
begin
  \FSM_onehot_current_state_reg[0]_0\ <= \^fsm_onehot_current_state_reg[0]_0\;
  \sel2vis[0]_3\(0) <= \^sel2vis[0]_3\(0);
  \sel2vis[1]_2\(0) <= \^sel2vis[1]_2\(0);
  \sel2vis[2]_1\(0) <= \^sel2vis[2]_1\(0);
  \sel2vis[3]_0\(0) <= \^sel2vis[3]_0\(0);
\FSM_onehot_current_state[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888880"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg[0]_1\,
      I1 => \FSM_onehot_current_state_reg[0]_2\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_current_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_current_state_reg_n_0_[2]\,
      I5 => \FSM_onehot_current_state_reg_n_0_[3]\,
      O => current_state
    );
\FSM_onehot_current_state_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => current_state,
      D => '0',
      PRE => \FSM_onehot_current_state_reg[3]_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[0]\
    );
\FSM_onehot_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => current_state,
      CLR => \FSM_onehot_current_state_reg[3]_0\,
      D => \FSM_onehot_current_state_reg_n_0_[0]\,
      Q => \FSM_onehot_current_state_reg_n_0_[1]\
    );
\FSM_onehot_current_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => current_state,
      CLR => \FSM_onehot_current_state_reg[3]_0\,
      D => \FSM_onehot_current_state_reg_n_0_[1]\,
      Q => \FSM_onehot_current_state_reg_n_0_[2]\
    );
\FSM_onehot_current_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => current_state,
      CLR => \FSM_onehot_current_state_reg[3]_0\,
      D => \FSM_onehot_current_state_reg_n_0_[2]\,
      Q => \FSM_onehot_current_state_reg_n_0_[3]\
    );
\display_bus_OBUF[1]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CC5A335A"
    )
        port map (
      I0 => \display_bus_OBUF[1]_inst_i_1_1\,
      I1 => \display_bus_OBUF[1]_inst_i_5_n_0\,
      I2 => \display_bus_OBUF[2]_inst_i_1_0\,
      I3 => \display_bus_OBUF[1]_inst_i_1\,
      I4 => \^fsm_onehot_current_state_reg[0]_0\,
      O => \count_reg[0]\
    );
\display_bus_OBUF[1]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3F305F5F3F305050"
    )
        port map (
      I0 => \display_bus_OBUF[2]_inst_i_1_0\,
      I1 => \^fsm_onehot_current_state_reg[0]_0\,
      I2 => \display_bus_OBUF[1]_inst_i_1_0\,
      I3 => \display_bus_OBUF[2]_inst_i_5_n_0\,
      I4 => \display_bus_OBUF[1]_inst_i_1\,
      I5 => \display_bus_OBUF[2]_inst_i_1\,
      O => \count_reg[1]\
    );
\display_bus_OBUF[1]_inst_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00CA"
    )
        port map (
      I0 => \^sel2vis[3]_0\(0),
      I1 => \^sel2vis[1]_2\(0),
      I2 => \display_bus_OBUF[2]_inst_i_3_0\,
      I3 => \display_bus_OBUF[2]_inst_i_3_1\,
      O => \display_bus_OBUF[1]_inst_i_5_n_0\
    );
\display_bus_OBUF[2]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF3F553FFF"
    )
        port map (
      I0 => \display_bus_OBUF[2]_inst_i_1\,
      I1 => \display_bus_OBUF[2]_inst_i_5_n_0\,
      I2 => \^fsm_onehot_current_state_reg[0]_0\,
      I3 => \display_bus_OBUF[1]_inst_i_1\,
      I4 => \display_bus_OBUF[2]_inst_i_1_0\,
      I5 => \display_bus_OBUF[1]_inst_i_1_0\,
      O => \count_reg[2]\
    );
\display_bus_OBUF[2]_inst_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3FF5"
    )
        port map (
      I0 => \^sel2vis[3]_0\(0),
      I1 => \^sel2vis[0]_3\(0),
      I2 => \display_bus_OBUF[2]_inst_i_3_0\,
      I3 => \display_bus_OBUF[2]_inst_i_3_1\,
      O => \display_bus_OBUF[2]_inst_i_5_n_0\
    );
\display_bus_OBUF[2]_inst_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B800"
    )
        port map (
      I0 => \^sel2vis[0]_3\(0),
      I1 => \display_bus_OBUF[2]_inst_i_3_0\,
      I2 => \^sel2vis[2]_1\(0),
      I3 => \display_bus_OBUF[2]_inst_i_3_1\,
      O => \^fsm_onehot_current_state_reg[0]_0\
    );
\number_reg[0][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_current_state_reg_n_0_[0]\,
      G => \number_reg[0][2]_i_1_n_0\,
      GE => '1',
      Q => \^sel2vis[0]_3\(0)
    );
\number_reg[0][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAAB"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[0]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[3]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[2]\,
      I3 => \FSM_onehot_current_state_reg_n_0_[1]\,
      O => \number_reg[0][2]_i_1_n_0\
    );
\number_reg[1][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_current_state_reg_n_0_[0]\,
      G => \number_reg[1][3]_i_1_n_0\,
      GE => '1',
      Q => \^sel2vis[1]_2\(0)
    );
\number_reg[1][3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[3]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[0]\,
      O => \number_reg[1][3]_i_1_n_0\
    );
\number_reg[2][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_current_state_reg_n_0_[0]\,
      G => \number_reg[2][1]_i_1_n_0\,
      GE => '1',
      Q => \^sel2vis[2]_1\(0)
    );
\number_reg[2][1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[0]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[2]\,
      O => \number_reg[2][1]_i_1_n_0\
    );
\number_reg[3][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_current_state_reg_n_0_[0]\,
      G => \number_reg[3][2]_i_1_n_0\,
      GE => '1',
      Q => \^sel2vis[3]_0\(0)
    );
\number_reg[3][2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[0]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      O => \number_reg[3][2]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Flash_Visualizer is
  port (
    \aux_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC;
    \count_reg[24]\ : in STD_LOGIC
  );
end Flash_Visualizer;

architecture STRUCTURE of Flash_Visualizer is
  signal Tim2OnOff : STD_LOGIC;
begin
OnOff_unit: entity work.OnOff_4
     port map (
      CLK => CLK,
      E(0) => Tim2OnOff,
      \aux_reg[7]_0\(7 downto 0) => \aux_reg[7]\(7 downto 0)
    );
timer_unit: entity work.TIMER_5
     port map (
      CLK => CLK,
      E(0) => Tim2OnOff,
      \count_reg[24]_0\ => \count_reg[24]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Flash_Visualizer_0 is
  port (
    \aux_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC;
    \count_reg[24]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Flash_Visualizer_0 : entity is "Flash_Visualizer";
end Flash_Visualizer_0;

architecture STRUCTURE of Flash_Visualizer_0 is
  signal Tim2OnOff : STD_LOGIC;
begin
OnOff_unit: entity work.OnOff_2
     port map (
      CLK => CLK,
      E(0) => Tim2OnOff,
      \aux_reg[7]_0\(7 downto 0) => \aux_reg[7]\(7 downto 0)
    );
timer_unit: entity work.TIMER_3
     port map (
      CLK => CLK,
      E(0) => Tim2OnOff,
      \count_reg[24]_0\ => \count_reg[24]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Flash_Visualizer_1 is
  port (
    \aux_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC;
    \count_reg[24]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Flash_Visualizer_1 : entity is "Flash_Visualizer";
end Flash_Visualizer_1;

architecture STRUCTURE of Flash_Visualizer_1 is
  signal Tim2OnOff : STD_LOGIC;
begin
OnOff_unit: entity work.OnOff
     port map (
      CLK => CLK,
      E(0) => Tim2OnOff,
      \aux_reg[7]_0\(7 downto 0) => \aux_reg[7]\(7 downto 0)
    );
timer_unit: entity work.TIMER
     port map (
      CLK => CLK,
      E(0) => Tim2OnOff,
      \count_reg[24]_0\ => \count_reg[24]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity NormalVisualizer is
  port (
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    display_bus_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \sel_i_reg[4]\ : out STD_LOGIC;
    \sel_i_reg[4]_0\ : out STD_LOGIC;
    \sel_i_reg[4]_1\ : out STD_LOGIC;
    \sel_i_reg[7]\ : out STD_LOGIC;
    reset : out STD_LOGIC;
    \display_bus[6]\ : in STD_LOGIC;
    \display_bus[2]\ : in STD_LOGIC;
    \display_bus[1]\ : in STD_LOGIC;
    \display_bus[1]_0\ : in STD_LOGIC;
    \display_bus[2]_0\ : in STD_LOGIC;
    \display_bus[3]\ : in STD_LOGIC;
    \display_bus[6]_0\ : in STD_LOGIC;
    \sel2vis[1]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus_OBUF[6]_inst_i_5\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \sel2vis[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sel2vis[0]_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus_OBUF[0]_inst_i_1\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1_0\ : in STD_LOGIC;
    \sel2vis[2]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end NormalVisualizer;

architecture STRUCTURE of NormalVisualizer is
  signal STROBE : STD_LOGIC;
  signal \^reset\ : STD_LOGIC;
begin
  reset <= \^reset\;
scanner_unit: entity work.SCANNER
     port map (
      CLK => CLK,
      E(0) => STROBE,
      Q(7 downto 0) => Q(7 downto 0),
      \display_bus[1]\ => \display_bus[1]\,
      \display_bus[1]_0\ => \display_bus[1]_0\,
      \display_bus[2]\ => \display_bus[2]\,
      \display_bus[2]_0\ => \display_bus[2]_0\,
      \display_bus[3]\ => \display_bus[3]\,
      \display_bus[6]\ => \display_bus[6]\,
      \display_bus[6]_0\ => \display_bus[6]_0\,
      display_bus_OBUF(6 downto 0) => display_bus_OBUF(6 downto 0),
      \display_bus_OBUF[0]_inst_i_1_0\ => \display_bus_OBUF[0]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1_0\ => \display_bus_OBUF[2]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1_1\ => \display_bus_OBUF[2]_inst_i_1_0\,
      \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_3\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0),
      \sel2vis[0]_3\(0) => \sel2vis[0]_3\(0),
      \sel2vis[1]_2\(0) => \sel2vis[1]_2\(0),
      \sel2vis[2]_1\(0) => \sel2vis[2]_1\(0),
      \sel2vis[3]_0\(0) => \sel2vis[3]_0\(0),
      \sel_i_reg[0]_0\ => \^reset\,
      \sel_i_reg[4]_0\ => \sel_i_reg[4]\,
      \sel_i_reg[4]_1\ => \sel_i_reg[4]_0\,
      \sel_i_reg[4]_2\ => \sel_i_reg[4]_1\,
      \sel_i_reg[7]_0\ => \sel_i_reg[7]\
    );
timer_unit: entity work.\TIMER__parameterized1\
     port map (
      CLK => CLK,
      E(0) => STROBE,
      reset => \^reset\,
      reset_IBUF => reset_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity button_treatment is
  port (
    \sreg_reg[3]\ : out STD_LOGIC;
    \sreg_reg[2]\ : out STD_LOGIC;
    CLK : in STD_LOGIC;
    button_IBUF : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC
  );
end button_treatment;

architecture STRUCTURE of button_treatment is
  signal p_1_in : STD_LOGIC_VECTOR ( 0 to 0 );
  signal result : STD_LOGIC;
begin
inst_debouncer: entity work.debounce
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      D(0) => result,
      \flipflops_reg[0]_0\(0) => p_1_in(0),
      reset_IBUF => reset_IBUF
    );
inst_edgedtctr: entity work.EDGEDTCTR
     port map (
      CLK => CLK,
      D(0) => result,
      E(0) => E(0),
      \sreg_reg[2]_0\ => \sreg_reg[2]\,
      \sreg_reg[3]_0\ => \sreg_reg[3]\
    );
inst_sincro: entity work.SYNCHRNZR
     port map (
      CLK => CLK,
      button_IBUF => button_IBUF,
      \sreg_reg[0]_0\(0) => p_1_in(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top_timer is
  port (
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[3]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[3]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[3]_1\ : out STD_LOGIC;
    \count_reg[3]_2\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[2]\ : out STD_LOGIC;
    \count_reg[0]\ : out STD_LOGIC;
    \count_reg[1]\ : out STD_LOGIC;
    done : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus_OBUF[2]_inst_i_3\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_3_0\ : in STD_LOGIC;
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end top_timer;

architecture STRUCTURE of top_timer is
  signal CENTESIMAS : STD_LOGIC;
  signal DECIMAS : STD_LOGIC;
  signal SEC : STD_LOGIC;
  signal \^count_reg[3]\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^count_reg[3]_0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^count_reg[3]_2\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal t_cent_n_0 : STD_LOGIC;
  signal t_dec_n_0 : STD_LOGIC;
  signal t_sec_n_0 : STD_LOGIC;
  signal \tiempo[1]_5\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \tiempo[2]_6\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \tiempo[3]_7\ : STD_LOGIC_VECTOR ( 2 to 2 );
begin
  \count_reg[3]\(2 downto 0) <= \^count_reg[3]\(2 downto 0);
  \count_reg[3]_0\(2 downto 0) <= \^count_reg[3]_0\(2 downto 0);
  \count_reg[3]_2\(2 downto 0) <= \^count_reg[3]_2\(2 downto 0);
t_cent: entity work.BCDCNTR
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      E(0) => CENTESIMAS,
      Q(2 downto 0) => Q(2 downto 0),
      \count_reg[0]_0\ => \count_reg[0]\,
      \count_reg[0]_1\(0) => E(0),
      \count_reg[1]_0\ => \count_reg[1]\,
      \count_reg[2]_0\ => \count_reg[2]\,
      \count_reg[3]_0\ => t_cent_n_0,
      \display_bus_OBUF[2]_inst_i_3\(2) => \tiempo[1]_5\(2),
      \display_bus_OBUF[2]_inst_i_3\(1 downto 0) => \^count_reg[3]_0\(1 downto 0),
      \display_bus_OBUF[2]_inst_i_3_0\(2) => \tiempo[2]_6\(2),
      \display_bus_OBUF[2]_inst_i_3_0\(1 downto 0) => \^count_reg[3]\(1 downto 0),
      \display_bus_OBUF[2]_inst_i_3_1\ => \display_bus_OBUF[2]_inst_i_3\,
      \display_bus_OBUF[2]_inst_i_3_2\(2) => \tiempo[3]_7\(2),
      \display_bus_OBUF[2]_inst_i_3_2\(1 downto 0) => \^count_reg[3]_2\(1 downto 0),
      \display_bus_OBUF[2]_inst_i_3_3\ => \display_bus_OBUF[2]_inst_i_3_0\
    );
t_dec: entity work.BCDCNTR_6
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      E(0) => SEC,
      Q(3) => \^count_reg[3]_0\(2),
      Q(2) => \tiempo[1]_5\(2),
      Q(1 downto 0) => \^count_reg[3]_0\(1 downto 0),
      \count_reg[1]_0\(0) => DECIMAS,
      \count_reg[3]_0\ => t_dec_n_0,
      \count_reg[3]_1\(0) => CENTESIMAS,
      \count_reg[3]_2\ => t_sec_n_0
    );
t_dsec: entity work.BCDCNTR_7
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      E(0) => SEC,
      \FSM_sequential_current_state_reg[0]\ => t_cent_n_0,
      \FSM_sequential_current_state_reg[0]_0\ => t_dec_n_0,
      \FSM_sequential_current_state_reg[0]_1\ => t_sec_n_0,
      Q(3) => \^count_reg[3]_2\(2),
      Q(2) => \tiempo[3]_7\(2),
      Q(1 downto 0) => \^count_reg[3]_2\(1 downto 0),
      \count_reg[3]_0\ => \count_reg[3]_1\,
      done => done
    );
t_sec: entity work.BCDCNTR_8
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      E(0) => DECIMAS,
      Q(3) => \^count_reg[3]\(2),
      Q(2) => \tiempo[2]_6\(2),
      Q(1 downto 0) => \^count_reg[3]\(1 downto 0),
      \count_reg[3]_0\ => t_sec_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TimePassVisualizer is
  port (
    en_point : out STD_LOGIC;
    reset : out STD_LOGIC;
    \sel_i_reg[7]\ : out STD_LOGIC;
    \sel_i_reg[4]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    display_bus_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    en_point_reg_0 : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \FSM_onehot_current_state_reg[0]\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus[6]\ : in STD_LOGIC;
    \display_bus[2]\ : in STD_LOGIC;
    \display_bus[3]\ : in STD_LOGIC;
    \display_bus[6]_0\ : in STD_LOGIC;
    \display_bus_OBUF[6]_inst_i_5\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[2]_inst_i_1\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1_0\ : in STD_LOGIC;
    \display_bus_OBUF[1]_inst_i_1\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end TimePassVisualizer;

architecture STRUCTURE of TimePassVisualizer is
  signal FSM_unit_n_4 : STD_LOGIC;
  signal FSM_unit_n_5 : STD_LOGIC;
  signal FSM_unit_n_6 : STD_LOGIC;
  signal FSM_unit_n_7 : STD_LOGIC;
  signal Vis_unit_n_15 : STD_LOGIC;
  signal Vis_unit_n_17 : STD_LOGIC;
  signal \^reset\ : STD_LOGIC;
  signal \sel2vis[0]_3\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \sel2vis[1]_2\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \sel2vis[2]_1\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \sel2vis[3]_0\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \^sel_i_reg[4]\ : STD_LOGIC;
  signal \^sel_i_reg[7]\ : STD_LOGIC;
begin
  reset <= \^reset\;
  \sel_i_reg[4]\ <= \^sel_i_reg[4]\;
  \sel_i_reg[7]\ <= \^sel_i_reg[7]\;
FSM_unit: entity work.selected
     port map (
      CLK => CLK,
      \FSM_onehot_current_state_reg[0]_0\ => FSM_unit_n_5,
      \FSM_onehot_current_state_reg[0]_1\ => \FSM_onehot_current_state_reg[0]\,
      \FSM_onehot_current_state_reg[0]_2\(0) => \FSM_onehot_current_state_reg[0]_0\(0),
      \FSM_onehot_current_state_reg[3]_0\ => \^reset\,
      \count_reg[0]\ => FSM_unit_n_7,
      \count_reg[1]\ => FSM_unit_n_6,
      \count_reg[2]\ => FSM_unit_n_4,
      \display_bus_OBUF[1]_inst_i_1\ => Vis_unit_n_17,
      \display_bus_OBUF[1]_inst_i_1_0\ => Vis_unit_n_15,
      \display_bus_OBUF[1]_inst_i_1_1\ => \display_bus_OBUF[1]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1\ => \display_bus_OBUF[2]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1_0\ => \display_bus_OBUF[2]_inst_i_1_0\,
      \display_bus_OBUF[2]_inst_i_3_0\ => \^sel_i_reg[7]\,
      \display_bus_OBUF[2]_inst_i_3_1\ => \^sel_i_reg[4]\,
      \sel2vis[0]_3\(0) => \sel2vis[0]_3\(2),
      \sel2vis[1]_2\(0) => \sel2vis[1]_2\(3),
      \sel2vis[2]_1\(0) => \sel2vis[2]_1\(1),
      \sel2vis[3]_0\(0) => \sel2vis[3]_0\(2)
    );
Vis_unit: entity work.NormalVisualizer
     port map (
      CLK => CLK,
      Q(7 downto 0) => Q(7 downto 0),
      \display_bus[1]\ => FSM_unit_n_7,
      \display_bus[1]_0\ => FSM_unit_n_6,
      \display_bus[2]\ => \display_bus[2]\,
      \display_bus[2]_0\ => FSM_unit_n_4,
      \display_bus[3]\ => \display_bus[3]\,
      \display_bus[6]\ => \display_bus[6]\,
      \display_bus[6]_0\ => \display_bus[6]_0\,
      display_bus_OBUF(6 downto 0) => display_bus_OBUF(6 downto 0),
      \display_bus_OBUF[0]_inst_i_1\ => \display_bus_OBUF[2]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1\ => \display_bus_OBUF[2]_inst_i_1_0\,
      \display_bus_OBUF[2]_inst_i_1_0\ => FSM_unit_n_5,
      \display_bus_OBUF[6]_inst_i_5\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0),
      reset => \^reset\,
      reset_IBUF => reset_IBUF,
      \sel2vis[0]_3\(0) => \sel2vis[0]_3\(2),
      \sel2vis[1]_2\(0) => \sel2vis[1]_2\(3),
      \sel2vis[2]_1\(0) => \sel2vis[2]_1\(1),
      \sel2vis[3]_0\(0) => \sel2vis[3]_0\(2),
      \sel_i_reg[4]\ => Vis_unit_n_15,
      \sel_i_reg[4]_0\ => \^sel_i_reg[4]\,
      \sel_i_reg[4]_1\ => Vis_unit_n_17,
      \sel_i_reg[7]\ => \^sel_i_reg[7]\
    );
en_point_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => en_point_reg_0,
      PRE => \^reset\,
      Q => en_point
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top_countdown_time is
  port (
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[3]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[3]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[3]_1\ : out STD_LOGIC;
    \count_reg[3]_2\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \count_reg[2]\ : out STD_LOGIC;
    \count_reg[0]\ : out STD_LOGIC;
    \count_reg[1]\ : out STD_LOGIC;
    done : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_3\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_3_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end top_countdown_time;

architecture STRUCTURE of top_countdown_time is
  signal S_STROBE : STD_LOGIC;
begin
counter: entity work.top_timer
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      E(0) => S_STROBE,
      Q(2 downto 0) => Q(2 downto 0),
      \count_reg[0]\ => \count_reg[0]\,
      \count_reg[1]\ => \count_reg[1]\,
      \count_reg[2]\ => \count_reg[2]\,
      \count_reg[3]\(2 downto 0) => \count_reg[3]\(2 downto 0),
      \count_reg[3]_0\(2 downto 0) => \count_reg[3]_0\(2 downto 0),
      \count_reg[3]_1\ => \count_reg[3]_1\,
      \count_reg[3]_2\(2 downto 0) => \count_reg[3]_2\(2 downto 0),
      \display_bus_OBUF[2]_inst_i_3\ => \display_bus_OBUF[2]_inst_i_3\,
      \display_bus_OBUF[2]_inst_i_3_0\ => \display_bus_OBUF[2]_inst_i_3_0\,
      done => done
    );
strobe_gen: entity work.STROBE
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      E(0) => S_STROBE,
      \count_reg[0]_0\(0) => E(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity OutputTreatmentFSMVisualizer is
  port (
    en_point : out STD_LOGIC;
    reset : out STD_LOGIC;
    \sel_i_reg[7]\ : out STD_LOGIC;
    \sel_i_reg[4]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    display_bus_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \aux_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \aux_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \aux_reg[7]_1\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    en_point_reg : in STD_LOGIC;
    CLK : in STD_LOGIC;
    \FSM_onehot_current_state_reg[0]\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \display_bus[6]\ : in STD_LOGIC;
    \display_bus[2]\ : in STD_LOGIC;
    \display_bus[3]\ : in STD_LOGIC;
    \display_bus[6]_0\ : in STD_LOGIC;
    \display_bus_OBUF[6]_inst_i_5\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[2]_inst_i_1\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1_0\ : in STD_LOGIC;
    \display_bus_OBUF[1]_inst_i_1\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end OutputTreatmentFSMVisualizer;

architecture STRUCTURE of OutputTreatmentFSMVisualizer is
  signal \^reset\ : STD_LOGIC;
begin
  reset <= \^reset\;
BadEnd_unit: entity work.Flash_Visualizer
     port map (
      CLK => CLK,
      \aux_reg[7]\(7 downto 0) => \aux_reg[7]_1\(7 downto 0),
      \count_reg[24]\ => \^reset\
    );
GoodEnd_unit: entity work.Flash_Visualizer_0
     port map (
      CLK => CLK,
      \aux_reg[7]\(7 downto 0) => \aux_reg[7]_0\(7 downto 0),
      \count_reg[24]\ => \^reset\
    );
TimPassVis_unit: entity work.TimePassVisualizer
     port map (
      CLK => CLK,
      \FSM_onehot_current_state_reg[0]\ => \FSM_onehot_current_state_reg[0]\,
      \FSM_onehot_current_state_reg[0]_0\(0) => \FSM_onehot_current_state_reg[0]_0\(0),
      Q(7 downto 0) => Q(7 downto 0),
      \display_bus[2]\ => \display_bus[2]\,
      \display_bus[3]\ => \display_bus[3]\,
      \display_bus[6]\ => \display_bus[6]\,
      \display_bus[6]_0\ => \display_bus[6]_0\,
      display_bus_OBUF(6 downto 0) => display_bus_OBUF(6 downto 0),
      \display_bus_OBUF[1]_inst_i_1\ => \display_bus_OBUF[1]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1\ => \display_bus_OBUF[2]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1_0\ => \display_bus_OBUF[2]_inst_i_1_0\,
      \display_bus_OBUF[6]_inst_i_5\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0),
      en_point => en_point,
      en_point_reg_0 => en_point_reg,
      reset => \^reset\,
      reset_IBUF => reset_IBUF,
      \sel_i_reg[4]\ => \sel_i_reg[4]\,
      \sel_i_reg[7]\ => \sel_i_reg[7]\
    );
flasher_unit: entity work.Flash_Visualizer_1
     port map (
      CLK => CLK,
      \aux_reg[7]\(7 downto 0) => \aux_reg[7]\(7 downto 0),
      \count_reg[24]\ => \^reset\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Visualizer is
  port (
    en_point : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sel_i_reg[7]\ : out STD_LOGIC;
    \sel_i_reg[4]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    display_bus_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ctrl_bus_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC;
    current_state_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_onehot_current_state_reg[0]\ : in STD_LOGIC;
    \display_bus_OBUF[6]_inst_i_5\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[6]_inst_i_5_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_bus_OBUF[2]_inst_i_1\ : in STD_LOGIC;
    \display_bus_OBUF[2]_inst_i_1_0\ : in STD_LOGIC;
    \display_bus_OBUF[1]_inst_i_1\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end Visualizer;

architecture STRUCTURE of Visualizer is
  signal \^ar\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal FSM_unit_n_1 : STD_LOGIC;
  signal FSM_unit_n_10 : STD_LOGIC;
  signal FSM_unit_n_11 : STD_LOGIC;
  signal FSM_unit_n_12 : STD_LOGIC;
  signal FSM_unit_n_13 : STD_LOGIC;
  signal Out_unit_n_27 : STD_LOGIC;
  signal Out_unit_n_28 : STD_LOGIC;
  signal Out_unit_n_29 : STD_LOGIC;
  signal Out_unit_n_30 : STD_LOGIC;
  signal Out_unit_n_31 : STD_LOGIC;
  signal Out_unit_n_32 : STD_LOGIC;
  signal Out_unit_n_33 : STD_LOGIC;
  signal Out_unit_n_34 : STD_LOGIC;
  signal Out_unit_n_35 : STD_LOGIC;
  signal Out_unit_n_36 : STD_LOGIC;
  signal Out_unit_n_37 : STD_LOGIC;
  signal Out_unit_n_38 : STD_LOGIC;
  signal Out_unit_n_39 : STD_LOGIC;
  signal Out_unit_n_40 : STD_LOGIC;
  signal Out_unit_n_41 : STD_LOGIC;
  signal Out_unit_n_42 : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal aux : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal bus_ctrl : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
  AR(0) <= \^ar\(0);
  Q(0) <= \^q\(0);
FSM_unit: entity work.fsm_visualizer
     port map (
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      \FSM_onehot_current_state_reg[0]_0\ => \^ar\(0),
      \FSM_onehot_current_state_reg[1]_0\ => FSM_unit_n_1,
      \FSM_onehot_current_state_reg[1]_1\ => FSM_unit_n_13,
      \FSM_onehot_current_state_reg[3]_0\ => FSM_unit_n_10,
      \FSM_onehot_current_state_reg[3]_1\ => FSM_unit_n_11,
      \FSM_onehot_current_state_reg[3]_2\ => FSM_unit_n_12,
      Q(0) => \^q\(0),
      \ctrl_bus[7]\(7) => Out_unit_n_35,
      \ctrl_bus[7]\(6) => Out_unit_n_36,
      \ctrl_bus[7]\(5) => Out_unit_n_37,
      \ctrl_bus[7]\(4) => Out_unit_n_38,
      \ctrl_bus[7]\(3) => Out_unit_n_39,
      \ctrl_bus[7]\(2) => Out_unit_n_40,
      \ctrl_bus[7]\(1) => Out_unit_n_41,
      \ctrl_bus[7]\(0) => Out_unit_n_42,
      \ctrl_bus[7]_0\(7 downto 0) => aux(7 downto 0),
      ctrl_bus_OBUF(7 downto 0) => ctrl_bus_OBUF(7 downto 0),
      \ctrl_bus_OBUF[7]_inst_i_1_0\(7 downto 0) => bus_ctrl(7 downto 0),
      \ctrl_bus_OBUF[7]_inst_i_1_1\(7) => Out_unit_n_27,
      \ctrl_bus_OBUF[7]_inst_i_1_1\(6) => Out_unit_n_28,
      \ctrl_bus_OBUF[7]_inst_i_1_1\(5) => Out_unit_n_29,
      \ctrl_bus_OBUF[7]_inst_i_1_1\(4) => Out_unit_n_30,
      \ctrl_bus_OBUF[7]_inst_i_1_1\(3) => Out_unit_n_31,
      \ctrl_bus_OBUF[7]_inst_i_1_1\(2) => Out_unit_n_32,
      \ctrl_bus_OBUF[7]_inst_i_1_1\(1) => Out_unit_n_33,
      \ctrl_bus_OBUF[7]_inst_i_1_1\(0) => Out_unit_n_34,
      current_state_reg(1 downto 0) => current_state_reg(1 downto 0)
    );
Out_unit: entity work.OutputTreatmentFSMVisualizer
     port map (
      CLK => CLK,
      \FSM_onehot_current_state_reg[0]\ => \FSM_onehot_current_state_reg[0]\,
      \FSM_onehot_current_state_reg[0]_0\(0) => \^q\(0),
      Q(7 downto 0) => bus_ctrl(7 downto 0),
      \aux_reg[7]\(7 downto 0) => aux(7 downto 0),
      \aux_reg[7]_0\(7) => Out_unit_n_27,
      \aux_reg[7]_0\(6) => Out_unit_n_28,
      \aux_reg[7]_0\(5) => Out_unit_n_29,
      \aux_reg[7]_0\(4) => Out_unit_n_30,
      \aux_reg[7]_0\(3) => Out_unit_n_31,
      \aux_reg[7]_0\(2) => Out_unit_n_32,
      \aux_reg[7]_0\(1) => Out_unit_n_33,
      \aux_reg[7]_0\(0) => Out_unit_n_34,
      \aux_reg[7]_1\(7) => Out_unit_n_35,
      \aux_reg[7]_1\(6) => Out_unit_n_36,
      \aux_reg[7]_1\(5) => Out_unit_n_37,
      \aux_reg[7]_1\(4) => Out_unit_n_38,
      \aux_reg[7]_1\(3) => Out_unit_n_39,
      \aux_reg[7]_1\(2) => Out_unit_n_40,
      \aux_reg[7]_1\(1) => Out_unit_n_41,
      \aux_reg[7]_1\(0) => Out_unit_n_42,
      \display_bus[2]\ => FSM_unit_n_13,
      \display_bus[3]\ => FSM_unit_n_12,
      \display_bus[6]\ => FSM_unit_n_10,
      \display_bus[6]_0\ => FSM_unit_n_11,
      display_bus_OBUF(6 downto 0) => display_bus_OBUF(6 downto 0),
      \display_bus_OBUF[1]_inst_i_1\ => \display_bus_OBUF[1]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1\ => \display_bus_OBUF[2]_inst_i_1\,
      \display_bus_OBUF[2]_inst_i_1_0\ => \display_bus_OBUF[2]_inst_i_1_0\,
      \display_bus_OBUF[6]_inst_i_5\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_0\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_1\(2 downto 0),
      \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0) => \display_bus_OBUF[6]_inst_i_5_2\(2 downto 0),
      en_point => en_point,
      en_point_reg => FSM_unit_n_1,
      reset => \^ar\(0),
      reset_IBUF => reset_IBUF,
      \sel_i_reg[4]\ => \sel_i_reg[4]\,
      \sel_i_reg[7]\ => \sel_i_reg[7]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    en_point : out STD_LOGIC;
    display_bus_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ctrl_bus_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC;
    button_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_IBUF : in STD_LOGIC
  );
end top;

architecture STRUCTURE of top is
  signal coinciden : STD_LOGIC;
  signal current_state_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal done : STD_LOGIC;
  signal fsm1_n_0 : STD_LOGIC;
  signal fsm1_n_4 : STD_LOGIC;
  signal fsm1_n_5 : STD_LOGIC;
  signal fsm2Output : STD_LOGIC_VECTOR ( 2 to 2 );
  signal i : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reg_clave_n_3 : STD_LOGIC;
  signal start_cuenta : STD_LOGIC;
  signal \tiempo[0]_4\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tiempo[1]_5\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tiempo[2]_6\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tiempo[3]_7\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal timer_n_13 : STD_LOGIC;
  signal timer_n_14 : STD_LOGIC;
  signal timer_n_15 : STD_LOGIC;
  signal timer_n_9 : STD_LOGIC;
  signal trat_boton_n_0 : STD_LOGIC;
  signal trat_boton_n_1 : STD_LOGIC;
  signal visualizador_n_1 : STD_LOGIC;
  signal visualizador_n_2 : STD_LOGIC;
  signal visualizador_n_3 : STD_LOGIC;
begin
comparator: entity work.comparador_completo
     port map (
      AR(0) => visualizador_n_1,
      \FSM_sequential_current_state_reg[0]\ => reg_clave_n_3,
      coinciden => coinciden,
      done => done
    );
fsm1: entity work.fsm
     port map (
      AR(0) => visualizador_n_1,
      CLK => CLK,
      D(1) => fsm1_n_4,
      D(0) => fsm1_n_5,
      E(0) => start_cuenta,
      \FSM_sequential_current_state_reg[0]_0\ => fsm1_n_0,
      \FSM_sequential_current_state_reg[0]_1\ => trat_boton_n_1,
      \FSM_sequential_current_state_reg[0]_2\ => timer_n_9,
      Q(0) => fsm2Output(2),
      coinciden => coinciden,
      current_state_reg(1 downto 0) => current_state_reg(1 downto 0),
      i(1) => i(2),
      i(0) => i(0)
    );
reg_clave: entity work.pass_storage
     port map (
      AR(0) => visualizador_n_1,
      CLK => CLK,
      D(3 downto 0) => D(3 downto 0),
      current_state_reg(1 downto 0) => current_state_reg(1 downto 0),
      done => done,
      done_reg_0 => trat_boton_n_0,
      \i_reg[0]_0\ => fsm1_n_0,
      \i_reg[2]_0\(1) => i(2),
      \i_reg[2]_0\(0) => i(0),
      \pass_completa_reg[0][0]_0\ => trat_boton_n_1,
      \pass_completa_reg[3][1]_0\ => reg_clave_n_3
    );
timer: entity work.top_countdown_time
     port map (
      AR(0) => visualizador_n_1,
      CLK => CLK,
      E(0) => start_cuenta,
      Q(2) => \tiempo[0]_4\(3),
      Q(1 downto 0) => \tiempo[0]_4\(1 downto 0),
      \count_reg[0]\ => timer_n_14,
      \count_reg[1]\ => timer_n_15,
      \count_reg[2]\ => timer_n_13,
      \count_reg[3]\(2) => \tiempo[2]_6\(3),
      \count_reg[3]\(1 downto 0) => \tiempo[2]_6\(1 downto 0),
      \count_reg[3]_0\(2) => \tiempo[1]_5\(3),
      \count_reg[3]_0\(1 downto 0) => \tiempo[1]_5\(1 downto 0),
      \count_reg[3]_1\ => timer_n_9,
      \count_reg[3]_2\(2) => \tiempo[3]_7\(3),
      \count_reg[3]_2\(1 downto 0) => \tiempo[3]_7\(1 downto 0),
      \display_bus_OBUF[2]_inst_i_3\ => visualizador_n_3,
      \display_bus_OBUF[2]_inst_i_3_0\ => visualizador_n_2,
      done => done
    );
trat_boton: entity work.button_treatment
     port map (
      AR(0) => visualizador_n_1,
      CLK => CLK,
      E(0) => start_cuenta,
      button_IBUF => button_IBUF,
      reset_IBUF => reset_IBUF,
      \sreg_reg[2]\ => trat_boton_n_1,
      \sreg_reg[3]\ => trat_boton_n_0
    );
visualizador: entity work.Visualizer
     port map (
      AR(0) => visualizador_n_1,
      CLK => CLK,
      D(1) => fsm1_n_4,
      D(0) => fsm1_n_5,
      \FSM_onehot_current_state_reg[0]\ => trat_boton_n_1,
      Q(0) => fsm2Output(2),
      ctrl_bus_OBUF(7 downto 0) => ctrl_bus_OBUF(7 downto 0),
      current_state_reg(1 downto 0) => current_state_reg(1 downto 0),
      display_bus_OBUF(6 downto 0) => display_bus_OBUF(6 downto 0),
      \display_bus_OBUF[1]_inst_i_1\ => timer_n_14,
      \display_bus_OBUF[2]_inst_i_1\ => timer_n_13,
      \display_bus_OBUF[2]_inst_i_1_0\ => timer_n_15,
      \display_bus_OBUF[6]_inst_i_5\(2) => \tiempo[0]_4\(3),
      \display_bus_OBUF[6]_inst_i_5\(1 downto 0) => \tiempo[0]_4\(1 downto 0),
      \display_bus_OBUF[6]_inst_i_5_0\(2) => \tiempo[1]_5\(3),
      \display_bus_OBUF[6]_inst_i_5_0\(1 downto 0) => \tiempo[1]_5\(1 downto 0),
      \display_bus_OBUF[6]_inst_i_5_1\(2) => \tiempo[2]_6\(3),
      \display_bus_OBUF[6]_inst_i_5_1\(1 downto 0) => \tiempo[2]_6\(1 downto 0),
      \display_bus_OBUF[6]_inst_i_5_2\(2) => \tiempo[3]_7\(3),
      \display_bus_OBUF[6]_inst_i_5_2\(1 downto 0) => \tiempo[3]_7\(1 downto 0),
      en_point => en_point,
      reset_IBUF => reset_IBUF,
      \sel_i_reg[4]\ => visualizador_n_3,
      \sel_i_reg[7]\ => visualizador_n_2
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TOP_Implementation is
  port (
    reset : in STD_LOGIC;
    clk : in STD_LOGIC;
    button : in STD_LOGIC;
    switches : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ctrl_bus : out STD_LOGIC_VECTOR ( 7 downto 0 );
    display_bus : out STD_LOGIC_VECTOR ( 6 downto 0 );
    en_point : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of TOP_Implementation : entity is true;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of TOP_Implementation : entity is "c1da32de";
end TOP_Implementation;

architecture STRUCTURE of TOP_Implementation is
  signal button_IBUF : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal ctrl_bus_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal display_bus_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal en_point_OBUF : STD_LOGIC;
  signal reset_IBUF : STD_LOGIC;
  signal switches_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
button_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button,
      O => button_IBUF
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
\ctrl_bus_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(0),
      O => ctrl_bus(0)
    );
\ctrl_bus_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(1),
      O => ctrl_bus(1)
    );
\ctrl_bus_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(2),
      O => ctrl_bus(2)
    );
\ctrl_bus_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(3),
      O => ctrl_bus(3)
    );
\ctrl_bus_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(4),
      O => ctrl_bus(4)
    );
\ctrl_bus_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(5),
      O => ctrl_bus(5)
    );
\ctrl_bus_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(6),
      O => ctrl_bus(6)
    );
\ctrl_bus_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ctrl_bus_OBUF(7),
      O => ctrl_bus(7)
    );
\display_bus_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_bus_OBUF(0),
      O => display_bus(0)
    );
\display_bus_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_bus_OBUF(1),
      O => display_bus(1)
    );
\display_bus_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_bus_OBUF(2),
      O => display_bus(2)
    );
\display_bus_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_bus_OBUF(3),
      O => display_bus(3)
    );
\display_bus_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_bus_OBUF(4),
      O => display_bus(4)
    );
\display_bus_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_bus_OBUF(5),
      O => display_bus(5)
    );
\display_bus_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_bus_OBUF(6),
      O => display_bus(6)
    );
en_point_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => en_point_OBUF,
      O => en_point
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
\switches_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => switches(0),
      O => switches_IBUF(0)
    );
\switches_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => switches(1),
      O => switches_IBUF(1)
    );
\switches_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => switches(2),
      O => switches_IBUF(2)
    );
\switches_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => switches(3),
      O => switches_IBUF(3)
    );
unit: entity work.top
     port map (
      CLK => clk_IBUF_BUFG,
      D(3 downto 0) => switches_IBUF(3 downto 0),
      button_IBUF => button_IBUF,
      ctrl_bus_OBUF(7 downto 0) => ctrl_bus_OBUF(7 downto 0),
      display_bus_OBUF(6 downto 0) => display_bus_OBUF(6 downto 0),
      en_point => en_point_OBUF,
      reset_IBUF => reset_IBUF
    );
end STRUCTURE;
