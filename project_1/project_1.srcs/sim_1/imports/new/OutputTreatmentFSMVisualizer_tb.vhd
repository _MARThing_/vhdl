library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.common.all;

entity OutputTreatmentFSMVisualizer_tb is
end;

architecture bench of OutputTreatmentFSMVisualizer_tb is

  component OutputTreatmentFSMVisualizer
      generic( 
          n_numbers: positive:=4;
          clk_freq : long := 100e6
      );
      port (
          clk   : in std_logic;
          button: in std_logic;
          reset : in std_logic;   
          SEL  : in std_logic_vector (3 downto 0) ;
          number: in bcd_vector (n_numbers-1 downto 0);
          ctrl_bus: out std_logic_vector (2*n_numbers-1 downto 0);
          displays_bus: out std_logic_vector(6 downto 0);
          en_point: out std_logic
      );
  end component;
  
  constant n_numbers: positive:=4;
  signal clk: std_logic;
  signal button: std_logic;
  signal reset: std_logic;
  signal SEL: std_logic_vector (3 downto 0);
  signal number: bcd_vector (n_numbers-1 downto 0);
  signal ctrl_bus: std_logic_vector (2*n_numbers-1 downto 0);
  signal displays_bus: std_logic_vector(6 downto 0);
  signal en_point: std_logic ;
   
   constant clk_freq: long:=100;
  constant clock_period: time := 1sec/clk_freq;
  signal stop_the_clock: boolean;
begin

  -- Insert values for generic parameters !!
  uut: OutputTreatmentFSMVisualizer generic map ( n_numbers    => n_numbers,
                                                  clk_freq     =>  clk_freq)
                                       port map ( clk          => clk,
                                                  button       => button,
                                                  reset        => reset,
                                                  SEL          => SEL,
                                                  number       => number,
                                                  ctrl_bus     => ctrl_bus,
                                                  displays_bus => displays_bus,
                                                  en_point     => en_point );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset <='0','1' after clock_period*1.05;
    button<='0';
    for i in number'range loop
        number(i)<=to_unsigned(i,4);
    end loop;
    SEL<= "1000", "0100" after 75*clock_period;
    wait for 100*clock_period;
    button<='1','0' after  1.05*clock_period;
    wait for 50*clock_period;
    button<='1','0' after  1.05*clock_period;
    wait for 50*clock_period;
    button<='1','0' after  1.05*clock_period;
    wait for 50*clock_period;
    button<='1','0' after  1.05*clock_period;
    wait for 50*clock_period;
    SEL<="0010";
    wait for 50*clock_period;
    -- Put test bench stimulus code here

    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      CLK <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;