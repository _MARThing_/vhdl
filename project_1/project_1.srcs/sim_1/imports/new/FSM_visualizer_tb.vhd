library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.common.all; 

entity fsm_visualizer_tb is
end;

architecture bench of fsm_visualizer_tb is

  component fsm_visualizer
      port (
          reset : in std_logic;
          clk: in std_logic;
          topin: in int0to9 ;
          SEL  : out std_logic_vector (3 downto 0)
              );
  end component;

  signal reset: std_logic;
  signal clk: std_logic;
  signal topin: int0to9;
  signal SEL: std_logic_vector (3 downto 0) ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: fsm_visualizer port map ( reset => reset,
                                 clk   => clk,
                                 topin => topin,
                                 SEL   => SEL );

  stimulus: process
  begin
  
    -- Put initialisation code here

    reset <= '0';
    topin<= 0;
    wait for clock_period*1.5;
    reset <= '1';
    wait for clock_period*1.5;
    wait for clock_period*5;
    topin<=2;
    wait for clock_period*2;
    topin<=1;
    wait for clock_period*1.3;
    topin<=3;
    wait for clock_period*2.1;
    reset<='0','1' after clock_period*2.05;
    wait for clock_period*2.3;
    topin<=1,2 after 3*clock_period;
    wait for clock_period*2.5;
    topin<= 2,1 after 2.5*clock_period;
    wait for 3*clock_period;
    -- Put test bench stimulus code here

    stop_the_clock <= true;
    assert false report "SIMULATION FINISHED" severity failure;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      CLK <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;