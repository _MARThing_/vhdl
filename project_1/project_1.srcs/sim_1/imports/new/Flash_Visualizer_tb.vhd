library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;

entity Flash_Visualizer_tb is
end;

architecture bench of Flash_Visualizer_tb is

  component Flash_Visualizer
      generic(n_digits: positive :=4;
        clk_freq: long:= 100e6 );
      port (
          numberBCD: in bcd;
          clk: in std_logic;
          reset: in std_logic;
          out7seg : out std_logic_vector(6 downto 0);
          outCtrl: out std_logic_vector(2*n_digits -1  downto 0)
      );
  end component;
  
  constant n_digits: positive :=4;
  signal numberBCD: bcd;
  signal clk: std_logic;
  signal reset: std_logic;
  signal out7seg: std_logic_vector(6 downto 0);
  signal outCtrl: std_logic_vector(2*n_digits -1 downto 0) ;
  -- Clock period definitions
  constant test_clock_freq   : long := 100;
  constant clk_period : time := 1 sec / test_clock_freq;
begin

  -- Insert values for generic parameters !!
  uut: Flash_Visualizer generic map ( n_digits  => n_digits,
                                      clk_freq  =>  test_clock_freq )
                           port map ( numberBCD => numberBCD,
                                      clk       => clk,
                                      reset     => reset,
                                      out7seg   => out7seg,
                                      outCtrl   => outCtrl );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset<='0', '1' after clk_period;
    numberBCD<="1001";
    wait for (100)*clk_period;

    -- Put test bench stimulus code here
    assert false report "FINISHED SIMULATION" severity failure;
    wait;
  end process;
    
 
    -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;

end;
  