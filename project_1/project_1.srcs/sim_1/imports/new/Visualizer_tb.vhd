library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;
entity Visualizer_tb is
end;

architecture bench of Visualizer_tb is

  component Visualizer
      generic(n_digits:positive := 4;
              clk_freq : long := 100e6);
      port (
          clk   : in std_logic;
          button: in std_logic;
          reset : in std_logic;  
          topin: in integer ;
          number: in bcd_vector (n_digits-1 downto 0);
          ctrl_bus: out std_logic_vector (2*n_digits-1 downto 0);
          displays_bus: out std_logic_vector(6 downto 0);
          en_point: out std_logic
      );
  end component;

   
  constant n_digits: positive:=4;
  constant clk_freq: long := 100;
  signal clk: std_logic;
  signal button: std_logic;
  signal reset: std_logic;
  signal topin: integer;
  signal number: bcd_vector (n_digits-1 downto 0);
  signal ctrl_bus: std_logic_vector (2*n_digits-1 downto 0);
  signal displays_bus: std_logic_vector(6 downto 0);
  signal en_point: std_logic ;
    
      -- Clock period definitions
  constant clk_period : time := 1 sec / clk_freq;
begin

  -- Insert values for generic parameters !!
  uut: Visualizer generic map ( n_digits     => n_digits,
                                clk_freq     => clk_freq )
                     port map ( clk          => clk,
                                button       => button,
                                reset        => reset,
                                topin        => topin,
                                number       => number,
                                ctrl_bus     => ctrl_bus,
                                displays_bus => displays_bus,
                                en_point     => en_point );

  stimulus: process
  begin
  
    -- Put initialisation code here
    for i in number'range loop
        number(i)<= to_unsigned(i+1,4);
    end loop;
    topin<=0;
    button<='0';
    reset<='0','1' after 2*clk_period;
    wait for 1 sec;
    topin<=1;
    wait for 0.5 sec;
    button<='1', '0' after clk_period;
    wait for 1 sec;
    button<='1', '0' after clk_period;
    wait for 1 sec;
    button<='1', '0' after clk_period;
    wait for 1 sec;
     button<='1', '0' after clk_period;
    wait for 1 sec;
      topin<=2;
      wait for 1 sec;
    -- Put test bench stimulus code here
    assert false report "SIMULATION FINISHED" severity failure;
    wait;
  end process;
  
    -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;


end;