-- Testbench created online at:
--   https://www.doulos.com/knowhow/perl/vhdl-testbench-creation-using-perl/
-- Copyright Doulos Ltd

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;
entity top_tb is
end;
 
architecture bench of top_tb is

  component top
  generic(
  	n_clave:positive:=4;
      clk_freq:long:=100e6);
  port(
  	reset: in std_logic;
  	clk: in std_logic;
  	button: in std_logic;
  	switches: in std_logic_vector(3 downto 0);
      ctrl_bus:out std_logic_vector (2*n_clave-1 downto 0);
  	display_bus: out std_logic_vector(6 downto 0);
      en_point:out std_logic
      );
  end component;

  signal reset: std_logic;
  signal clk: std_logic;
  signal button: std_logic;
  signal switches: std_logic_vector(3 downto 0);
  signal ctrl_bus: std_logic_vector (2*4-1 downto 0);
  signal display_bus: std_logic_vector(6 downto 0);
  signal en_point: std_logic ;
  -- Clock period definitions
  constant clk_freq: long := 100e6;
  constant clk_period : time := 1 sec / clk_freq;
begin

  -- Insert values for generic parameters !!
  uut: top generic map ( n_clave     => 4,
                         clk_freq    => clk_freq )
              port map ( reset       => reset,
                         clk         => clk,
                         button      => button,
                         switches    => switches,
                         ctrl_bus    => ctrl_bus,
                         display_bus => display_bus,
                         en_point    => en_point );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset<='0', '1' after 1.5*clk_period;
    button<='0';
    for i in switches'range loop
        switches(i)<='1';
    end loop;
    wait for 1 ms;
    button <= '1', '0' after 15 ms;
    wait for 30 ms;
    
     button <= '1', '0' after 15 ms;
    wait for 30 ms;
     button <= '1', '0' after 15 ms;
    wait for 30 ms;
     button <= '1', '0' after 15 ms;
    wait for 30 ms;
     button <= '1', '0' after 15 ms;
    wait for 30 ms;
     button <= '1', '0' after 15 ms;
    wait for 30 ms;
    
     button <= '1', '0' after 15 ms;
    wait for 30 ms;
    -- Put test bench stimulus code here

  ---  wait for 0.5 sec;
    -- Put test bench stimulus code here
    assert false report "SIMULATION FINISHED" severity failure;
    wait;
  end process;
  
    -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;
end;
  