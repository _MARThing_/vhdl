library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;
entity Visualizer_test_tb is
end;

architecture bench of Visualizer_test_tb is

  component Visualizer_test
  port (
          clk   : in std_logic;
          button: in std_logic;
          reset : in std_logic;
          ctrl_bus: out std_logic_vector (2*4-1 downto 0);
          displays_bus: out std_logic_vector(6 downto 0);
          en_point: out std_logic;
          LED: out std_logic
      );
  end component;

  signal clk: std_logic;
  signal button: std_logic;
  signal reset: std_logic;
  signal ctrl_bus: std_logic_vector (2*4-1 downto 0);
  signal displays_bus: std_logic_vector(6 downto 0);
  signal en_point: std_logic;
  signal LED: std_logic ;
    
        -- Clock period definitions
  constant clk_freq: long := 100e6;
  constant clk_period : time := 1 sec / clk_freq;
begin

  uut: Visualizer_test port map ( clk          => clk,
                                  button       => button,
                                  reset        => reset,
                                  ctrl_bus     => ctrl_bus,
                                  displays_bus => displays_bus,
                                  en_point     => en_point,
                                  LED          => LED );

  stimulus: process
  begin
  
    -- Put initialisation code here
    button<= '0';
    reset<='0','1' after 5 ms;
    wait for 30 ms;
    button<='1','0' after 3.5 ms;
    wait for 10 ms;
    button<='1','0' after 5 ms;
    wait for 11 ms;
    button<='1','0' after 10 ms;
    wait for 13 ms;
    button<='1','0' after 10 ms;
    wait for 15 ms;
    button<='1','0' after 25 ms;
    wait for 40 ms;
    --button<='1','0' after 2 ms;
   -- wait for 2.05 ms;
   -- button<='1', '0' after 0.5 sec;
   -- wait for 0.50005 sec;
    --button<='1','0' after 2 ms;
    --wait for 2.05 ms;
    --button<='1','0' after 250 ns;
   -- wait for 400 ns;
   -- button<='1','0' after 100 ns;
   -- wait for 150 ns;
  --  button<='1','0' after 10 ns;
  --  wait for 13 ns;
  --  button<='1','0' after 5 ns;
  --  wait for 11 ns;
    
    
    wait for 0.5 sec;
    -- Put test bench stimulus code here
    assert false report "SIMULATION FINISHED" severity failure;
    wait;
  end process;
  
    -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;
end;