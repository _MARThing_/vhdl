library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.common.all;

entity debounce_tb is
end;

architecture bench of debounce_tb is

  component button_treatment
    generic(
      clk_freq    : INTEGER := 100e6;
      stable_time : INTEGER := 10);
	port(
	     reset: in std_logic;
	     clk:in std_logic;
	     pushbutton:in std_logic;
	     boton_tratado:out std_logic
	);
  end component;

  signal clk: STD_LOGIC;
  signal reset_n: STD_LOGIC;
  signal button: STD_LOGIC;
  signal result: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: button_treatment generic map ( clk_freq    => 100e6,
                                        stable_time => 5 )
                   port map ( clk         => clk,
                              reset     => reset_n,
                              pushbutton      => button,
                              boton_tratado      => result );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset_n<='0','1' after 10*clock_period;
    button<='0';
    wait for clock_period*50.03;
    button<='1';
    wait for clock_period*8;
    
    button<='0';
    wait for clock_period;
    button<='1';
    wait for 5*clock_period;
    
    button<='0';
    wait for clock_period*10;
    button<='1';
    wait for 9*clock_period;
    
    button<='0';
    wait for 5*clock_period;
    button<='1';
    wait for 13.2*clock_period;
    
    button<='0';
    wait for 1*clock_period;
    button<='1';
    wait for 38*clock_period;
    
    button<='0';
    wait for 0.8*clock_period;
    button<='1';
    wait for 7 ms;
    
    button<='0';
    wait for 20*clock_period;
    button<='1';
    wait for 50*clock_period;
    button<='0';
    wait for 30*clock_period;
    button<='1';
    wait for 20*clock_period;
    button<='0';
    wait for 40*clock_period;
    button<='1';
    wait for 5*clock_period;
    button<='0';
    wait for 7 ms;
    -- Put test bench stimulus code here

    stop_the_clock <= true;
    assert false report "SIMULATION FINISHED" severity failure;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;