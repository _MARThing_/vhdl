----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.12.2021 11:51:53
-- Design Name: 
-- Module Name: Visualizer_test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
use work.common.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Visualizer_test is
port (
        clk   : in std_logic;
        button: in std_logic;
        reset : in std_logic;  
       -- topin: in int0to9 ;-- input from FSM top, to change the state
        --numberhalf: in bcd_vector (2-1 downto 0);
        ctrl_bus: out std_logic_vector (2*4-1 downto 0);
        displays_bus: out std_logic_vector(6 downto 0);
        en_point: out std_logic;
        LED: out std_logic
    );
end Visualizer_test;

architecture Behavioral of Visualizer_test is
    component Visualizer is
    generic(n_digits:positive := 4;
            clk_freq : long := 100e6);
    port (
        clk   : in std_logic;
        button: in std_logic;
        reset : in std_logic;  
        topin: in int0to9 ;-- input from FSM top, to change the state
        number: in bcd_vector (n_digits-1 downto 0);
        ctrl_bus: out std_logic_vector (2*n_digits-1 downto 0);
        displays_bus: out std_logic_vector(6 downto 0);
        en_point: out std_logic
    );
    end component Visualizer;
    
    
    component top_countdown_time IS
    GENERIC (N: INTEGER := 4);
        PORT(
            RESET_N: IN std_logic;
            CLK    : IN std_logic;
            CIN    : IN STD_LOGIC;
            COUT   : OUT STD_LOGIC;
            BCD    : OUT bcd_vector (N-1 DOWNTO 0));
    END component top_countdown_time;

    component button_treatment is
	 generic(
      clk_freq    : INTEGER := 100e6;
      stable_time : INTEGER := 10);
	port(
	     reset: in std_logic;
	     clk:in std_logic;
	     pushbutton:in std_logic;
	     boton_tratado:out std_logic
	);
    end component button_treatment;

signal numberS: bcd_vector (4-1 downto 0);
signal Button_t: std_logic;

begin
uut:Visualizer 
    generic map(n_digits=>4,
            clk_freq => 100e6)
    port map(
        clk  =>clk,
        button => Button_t,
        reset =>reset,
        topin => 1,
        number => numberS,
        ctrl_bus => ctrl_bus,
        displays_bus => displays_bus,
        en_point => en_point
    );
    
  uut_t: top_countdown_time 
    GENERIC MAP( N => 4)
        PORT MAP(
            RESET_N => reset,
            CLK    => clk,
            CIN    => '1',
            COUT   => LED,
            BCD   =>numberS
           );

   ayud_ut: button_treatment
   generic map(clk_freq=>100e6,stable_time=>2)
	port map(
	     reset=>reset,
	     clk=> clk,
	     pushbutton=>button,
	     boton_tratado=>Button_t
	);
end Behavioral;
