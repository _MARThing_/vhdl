----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.12.2021 21:10:17
-- Design Name: 
-- Module Name: TOP_Implementation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

use work.common.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TOP_Implementation is
port(
	reset: in std_logic;
	clk: in std_logic;
	button: in std_logic;
	switches: in std_logic_vector(3 downto 0);--se usan 4 switches
    ctrl_bus:out std_logic_vector (7 downto 0);
	display_bus: out std_logic_vector(6 downto 0);
    en_point:out std_logic
    );
end TOP_Implementation;

architecture Behavioral of TOP_Implementation is

component top is
generic(
	n_clave:positive:=4;
    clk_freq:long:=100e6);
port(
	reset: in std_logic;
	clk: in std_logic;
	button: in std_logic;
	switches: in std_logic_vector(3 downto 0);--se usan 4 switches
    ctrl_bus:out std_logic_vector (2*n_clave-1 downto 0);
	display_bus: out std_logic_vector(6 downto 0);
    en_point:out std_logic
    );
end component;

begin

unit: top 
generic map(
	n_clave=>4,
    clk_freq =>100e6)
port map(
	reset => reset,
	clk => clk,
	button => button,
	switches => switches,
    ctrl_bus => ctrl_bus,
	display_bus => display_bus,
    en_point => en_point
    );


end Behavioral;
