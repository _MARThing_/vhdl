----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.12.2021 11:52:36
-- Design Name: 
-- Module Name: top_countdown_time - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.common.all;


ENTITY top_countdown_time IS
GENERIC (N: INTEGER := 4);
    PORT(
        RESET_N: IN std_logic;
        CLK    : IN std_logic;
        CIN    : IN STD_LOGIC;
        COUT   : OUT STD_LOGIC;
        BCD    : OUT bcd_vector (N-1 DOWNTO 0));
END top_countdown_time;

ARCHITECTURE structural OF top_countdown_time IS 
-----------------------------------------------------
COMPONENT top_timer IS 
	
    PORT (
      RESET_N : IN  std_logic;
      CLK     : IN  std_logic;
      CIN     : IN  std_logic;                   -- CLOCK ENABLE
      COUT    : OUT std_logic;                   -- FLAG DE FIN DE LA CUENTA ATR�S
      BCD_V   : OUT bcd_vector (N-1 DOWNTO 0));  -- VECTOR DE SALIDA EN BCD
    END COMPONENT;
------------------------------------------------------
COMPONENT STROBE IS
      generic (
        MODULO  : long := 1e6
      );
      port ( 
        RST_N  : in   std_logic;
        CLK    : in   std_logic;
        CE     : in   std_logic;
        STROBE : out  std_logic
      );
    END COMPONENT;
--------------------------
SIGNAL S_STROBE: STD_LOGIC;
BEGIN
    strobe_gen: STROBE PORT MAP(
        RST_N => RESET_N,
        CLK => CLK,
        CE => CIN,
        STROBE => S_STROBE);
        
    counter: TOP_TIMER PORT MAP(
        RESET_N => RESET_N,
        CLK => CLK,
        CIN => S_STROBE,
        COUT => COUT,
        BCD_V => BCD);
        
END STRUCTURAL;
        


