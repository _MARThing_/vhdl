----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.12.2021 12:12:24
-- Design Name: 
-- Module Name: top_countdown - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.common.all;


ENTITY top_timer IS 
	GENERIC (N: INTEGER := 4);
    PORT (
      RESET_N : IN  std_logic;
      CLK     : IN  std_logic;
      CIN     : IN  std_logic;                   -- CLOCK ENABLE
      COUT    : OUT std_logic;                   -- FLAG DE FIN DE LA CUENTA ATR�S
      BCD_V   : OUT bcd_vector (N-1 DOWNTO 0));  -- VECTOR DE SALIDA EN BCD
    END top_timer;
    
ARCHITECTURE structural OF top_timer IS
----------------------------------------------
    COMPONENT BCDCNTR IS
        PORT (
          RESET_N   : in  std_logic;
          CLK     : in  std_logic;
          CIN     : in  std_logic;
          COUT    : out std_logic;
          BCD_OUT : out bcd
      );
    END COMPONENT;
--------------------------------------------------
-- DEFINIMOS LAS SE�ALES DE LOS CLOCK ENABLES DE LOS DISTINTOS BLOQUES DEL BCD
SIGNAL CENTESIMAS: STD_LOGIC;   
SIGNAL DECIMAS: STD_LOGIC;
SIGNAL SEC: STD_LOGIC;
SIGNAL DSEC: STD_LOGIC;
SIGNAL FIN_CUENTA: STD_LOGIC;
SIGNAL BCD_SIGNAL_VECTOR: bcd_vector(3 DOWNTO 0);  
-- CONSTANTE PARA LA COMPROBACI�N DE FIN DE CUENTA
CONSTANT FIN_CARRERA: bcd_vector(3 DOWNTO 0) := ("0000", "0000", "0000", "0000");

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

BEGIN
    t_cent: BCDCNTR PORT MAP(
        RESET_N => RESET_N,
        CLK => CLK,
        CIN => CIN,
        COUT => CENTESIMAS,
        BCD_OUT => BCD_SIGNAL_VECTOR(0)
    );
    
    t_dec: BCDCNTR PORT MAP(
        RESET_N => RESET_N,
        CLK => CLK,
        CIN => CENTESIMAS,
        COUT => DECIMAS,
        BCD_OUT => BCD_SIGNAL_VECTOR(1)
    );
    
    t_sec: BCDCNTR PORT MAP(
        RESET_N => RESET_N,
        CLK => CLK,
        CIN => DECIMAS,
        COUT => SEC,
        BCD_OUT => BCD_SIGNAL_VECTOR(2)
    );
    
    t_dsec: BCDCNTR PORT MAP(
        RESET_N => RESET_N,
        CLK => CLK,
        CIN => SEC,
        COUT => DSEC,
        BCD_OUT => BCD_SIGNAL_VECTOR(3)
    );
    
    FIN_CUENTA <= '1' WHEN BCD_SIGNAL_VECTOR = FIN_CARRERA ELSE '0';
    COUT <= FIN_CUENTA;
    BCD_V <= BCD_SIGNAL_VECTOR;
    
END structural;
