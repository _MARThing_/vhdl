----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.12.2021 12:02:03
-- Design Name: 
-- Module Name: STROBE - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.common.all;

entity STROBE is
  generic (
    MODULO  : long := 1e6
  );
  port ( 
    RST_N  : in   std_logic;
    CLK    : in   std_logic;
    CE     : in   std_logic;
    STROBE : out  std_logic
  );
end STROBE;

architecture BEHAVIORAL of STROBE is
begin
  process (RST_N, CLK)
    subtype count_t is natural range 0 to MODULO - 1; 
    variable count : count_t;
  begin
    if RST_N = '0' then
      count := 0;
    elsif rising_edge(CLK) then 
        if CE = '1' then
            count := (count + 1) mod MODULO;
        end if;
    end if;

    if count = MODULO - 1 then
      STROBE <= '1';
    else
      STROBE <= '0';
    end if;
  end process;
end BEHAVIORAL;
