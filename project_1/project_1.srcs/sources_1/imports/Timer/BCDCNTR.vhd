----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.12.2021 11:55:03
-- Design Name: 
-- Module Name: BCDCNTR - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.common.all;

entity BCDCNTR is
  port (
    RESET_N : in  std_logic;
    CLK     : in  std_logic;
    CIN     : in  std_logic;
    COUT    : out std_logic;
    BCD_OUT : out bcd
  );
end BCDCNTR;

architecture BEHAVIORAL of BCDCNTR is
  subtype count_t is integer range 0 to 9;
  signal count : count_t;
begin
  process (RESET_N, CLK)
  begin
    if RESET_N = '0' then
      count <= 9;
    elsif rising_edge(CLK) then
      if CIN = '1' then
        count <= (count - 1) mod 10;
      end if;
    end if;
  end process;

  BCD_OUT <= bcd(to_unsigned(count, BCD_OUT'length));
  COUT <= '1' when CIN = '1' and count = 0 else
          '0';
end BEHAVIORAL;
