-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;

--use work.common.all;

entity fsm is
port(
	reset: in std_logic;
	clk: in std_logic;
	boton_tratado: in std_logic;
    done:in std_logic;--es 1 si se han recibido todos los numeros
    finished_cuenta:in std_logic;--es 1 si la cuenta ha terminado
    pass_match:in std_logic;
	start_cuenta:out std_logic;--le pasa al timer para que empiece a contar
    to_visualizer:out integer--0 nada,1 en juego,2 �xito,3 fallo
    );
end entity;

architecture arch_fsm of fsm is
	type STATES is (S0,S1,S2,S3);
	signal current_state:STATES:=S0;
	signal next_state:STATES;
begin

 state_register:process(reset,clk)
 begin
 	if reset='0' then 
    	current_state<=S0;
	elsif rising_edge(clk) then current_state<=next_state;
	end if;
 end process;

 nextstate_decod:process(boton_tratado,current_state,done,finished_cuenta,pass_match)
 begin
    next_state <= current_state;
	case current_state is
	when S0=>
	 if boton_tratado='1' then
	  next_state<=S1;
	 end if;
	when S1=>
	 if ((done='1' or finished_cuenta='1')and pass_match='1') then
	  next_state<=S2;--si la clave es correcta,exito(S2)
     elsif ((done='1' or finished_cuenta='1')and pass_match='0') then
	  next_state<=S3;--si la clave es incorrecta, explota/falla(S3)
     end if;
    when others=>
    	
	end case;
    
 end process;

-- Tratamiento de la salida para el temporizador
 time_decod:process(current_state)
 begin
	if(current_state=S1) then
	  start_cuenta<='1';
	else 
	  start_cuenta<='0';
    end if;
 end process;
 
 -- Tratamiento del estado del visualizador
 visualizer_decod:process(current_state)
 begin
 	if(current_state<=S0) then
    	to_visualizer<=0;
    elsif(current_state<=S1) then
    	to_visualizer<=1;
    elsif(current_state<=S2) then
    	to_visualizer<=2;
    elsif(current_state<=S3) then
    	to_visualizer<=3;
    end if;
 end process;
end architecture;