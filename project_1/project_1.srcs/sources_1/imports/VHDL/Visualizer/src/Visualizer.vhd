library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;

entity Visualizer is
    generic(n_digits:positive := 4;
            clk_freq : long := 100e6);
    port (
        clk   : in std_logic;
        button: in std_logic;
        reset : in std_logic;  
        topin: in int0to9 ;-- input from FSM top, to change the state
        number: in bcd_vector (n_digits-1 downto 0);
        ctrl_bus: out std_logic_vector (2*n_digits-1 downto 0);
        displays_bus: out std_logic_vector(6 downto 0);
        en_point: out std_logic
    );
end entity Visualizer;

architecture Structural of Visualizer is

    component OutputTreatmentFSMVisualizer is
        generic( 
            n_numbers: positive:=4;
            clk_freq : long := 100e6
        );
        port (
            clk   : in std_logic;
            button: in std_logic;
            --CE    : in std_logic;
            reset : in std_logic;   
            SEL  : in std_logic_vector (3 downto 0) ;
            number: in bcd_vector (n_numbers-1 downto 0);
            ctrl_bus: out std_logic_vector (2*n_numbers-1 downto 0);
            displays_bus: out std_logic_vector(6 downto 0);
            en_point: out std_logic
        );
    end component OutputTreatmentFSMVisualizer;

    component fsm_visualizer is
        port (
            reset : in std_logic;
            clk: in std_logic;
            topin: in int0to9 ;-- input from FSM top, to change the state
            SEL  : out std_logic_vector (3 downto 0)
                );
    end component fsm_visualizer;
    
    signal fsm2Output: std_logic_vector (3 downto 0);
begin
    
    Out_unit: OutputTreatmentFSMVisualizer
        generic map( 
            n_numbers => n_digits,
            clk_freq => clk_freq
        )
        port map(
            clk   => clk,
            button => button,
            --CE    : in std_logic;
            reset => reset,   
            SEL  =>fsm2Output,
            number => number,
            ctrl_bus => ctrl_bus,
            displays_bus => displays_bus,
            en_point => en_point
        );
    
        FSM_unit: fsm_visualizer
            port map(
                clk   => clk,
                reset => reset, 
                topin => topin,
                SEL  => fsm2Output
                    );
    
end architecture Structural;