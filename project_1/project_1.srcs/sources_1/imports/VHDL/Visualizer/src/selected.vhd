library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;

entity selected is
    generic(n_digits: positive :=4 );
    port (
        clk   : in std_logic;
        CE    : in std_logic;
        reset : in std_logic;    
        button: in std_logic;
        data  : in bcd_vector(n_digits-1 downto 0);
        number: out bcd_vector(2*n_digits-1 downto 0)
    );
end entity selected;

architecture Dataflow of selected is

    type STATES is (S0,S1,S2,S3,S4);
    signal current_state: STATES;
    signal next_state: STATES;
    
begin
    
    
    state_register: process (reset, clk)
    begin
      if reset = '0' then
        current_state <= S0;
      elsif rising_edge(clk)then 
        if CE='1' then
        current_state <= next_state;
        end if; 
      end if;
    end process;

    nextstate_decod: process (button, current_state)
    begin
      next_state <= current_state;
      if button='1' then
      case current_state is
        when S3 =>
          next_state<=S4;
        when S2 =>  
             next_state <= S3;
        when S1 =>     
            next_state <= S2;
        when S0 =>
                next_state <= S1;
        when others=> 
            -- Do nothing
      end case;
      end if;
    end process;
    
    
    output_decod: process (current_state,data)
        variable n_zeros: integer; -- Just something to show
        constant Offv: bcd:= "0000"; -- Just something to show
        variable change: bcd_vector (n_digits-1 downto 0);
        variable aux2: bcd_vector(2*n_digits-1 downto 0);
       
    begin
          change(3):="0101";
          change(2):="1001";
          change(1):= "0010";
          change(0):="0110";
          n_zeros:=3;
      case current_state is
        when S1 =>
          n_zeros:= n_zeros-1;
        when S2 => 
           n_zeros:= n_zeros-2;
        when S3 => 
            n_zeros:= n_zeros-3;
        when S4 => 
           n_zeros:= n_zeros-4;
        when others =>
            -- Do nothing
      end case;
      
     for i in change'range loop
        if i> n_zeros then
            change(i):=Offv;
        end if;
     end loop;
      
     for i in aux2'range loop
           IF i<= n_digits-1 then
                aux2(i):=change(i);
            ELSIF i> n_digits-1 then
                aux2(i):=data(i-n_digits);
            end IF ;
        end loop;
        
    number<=aux2;
   
    end process;
    
end architecture Dataflow;
