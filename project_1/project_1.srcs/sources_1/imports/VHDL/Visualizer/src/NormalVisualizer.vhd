library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;

entity NormalVisualizer is
    generic(n_displays:positive := 8;
            clk_freq : long := 100e6 );
    port (
       reset      : in std_logic;
       CLK        : in std_logic;
       number     : in bcd_vector (n_displays-1 downto 0);
       bus_display: out std_logic_vector (6 downto 0) ;
       bus_ctrl   : out std_logic_vector (n_displays-1 downto 0)
    );
end entity NormalVisualizer;

architecture Structural of NormalVisualizer is
    -- Timer 
    component TIMER is
        generic (
            MODULO  : long := 416667 -- Long subtype, is a natural on a range of 0 to 10e10
        );
        port ( 
            RESET_N : in   std_logic;
            CLK     : in   std_logic;
            STROBE  : out  std_logic
        );
    end component TIMER;

    -- A special Mux for only BCD numbers
    component MUXERBCD is
        generic (
          DIGITS : positive := 4
        );
        port (
          SEL     : in  std_logic_vector(DIGITS - 1 downto 0);
          BCD_IN  : in  bcd_vector(DIGITS - 1 downto 0);
          BCD_OUT : out bcd
        );
      end component MUXERBCD;

    -- From a BCD to 7 segments Displays
    component BIN2SEG is
        port (
          BCDVAL   : in  bcd;
          SEGMENTS : out std_logic_vector(6 downto 0)
        );
    end component BIN2SEG;

    -- SCANNER that changes which number to put on the displays
    component SCANNER is
        generic (
          DIGITS : positive := 4
        );
        port (
          RESET : in  std_logic;
          CLK     : in  std_logic;
          CE      : in  std_logic;
          SEL     : out std_logic_vector(DIGITS - 1 downto 0)
        );
    end component SCANNER;

    signal mux2bin: bcd;
    signal scan2mux: std_logic_vector (n_displays-1 downto 0);
    signal tim2scan: std_logic;
    
begin 

    muxBCD_unit: MUXERBCD 
        generic map(DIGITS => n_displays)
        port map(
          SEL     => scan2mux,
          BCD_IN  =>  number,
          BCD_OUT =>  mux2bin
        );

    bin2seg_unit: BIN2SEG 
        port map(
          BCDVAL   =>  mux2bin,
          SEGMENTS => bus_display
        );
        
    
    timer_unit: TIMER 
        generic map(
            MODULO  => (clk_freq/(100*n_displays)) -- 100 Hz frecuency 
        )
        port map( 
            RESET_N => reset,
            CLK     => clk,
            STROBE  => tim2scan
        );
    
    scanner_unit : SCANNER 
        generic map(
          DIGITS => n_displays
        )
        port  map(
          RESET => reset,
          CLK   =>  clk ,
          CE    =>  tim2scan,
          SEL   => scan2mux
        );
        bus_ctrl<=scan2mux;
end architecture Structural;