library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;

entity TimePassVisualizer is
    generic(n_digits: positive :=4;
            clk_freq : long := 100e6 );
    port (
        clk   : in std_logic;
        CE    : in std_logic;
        reset : in std_logic;    
        button: in std_logic;
        data  : in bcd_vector(n_digits-1 downto 0);
        bus_display: out std_logic_vector (6 downto 0) ;
        bus_ctrl   : out std_logic_vector (2*n_digits-1 downto 0);
        en_point   : out std_logic
    );
end entity TimePassVisualizer;

architecture Structural of TimePassVisualizer is
    
    component NormalVisualizer is
        generic(n_displays:positive := 8;
                clk_freq : long := 100e6 );
        port (
           reset      : in std_logic;
           CLK        : in std_logic;
           number     : in bcd_vector (n_displays-1 downto 0);
           bus_display: out std_logic_vector (6 downto 0) ;
           bus_ctrl   : out std_logic_vector (n_displays-1 downto 0)
        );
    end component NormalVisualizer;
    
    component selected is
        generic(n_digits: positive :=4 );
        port (
            clk   : in std_logic;
            CE    : in std_logic;
            reset : in std_logic;    
            button: in std_logic;
            data  : in bcd_vector(n_digits-1 downto 0);
            number: out bcd_vector(2*n_digits-1 downto 0)
        );
    end component selected;
    
    signal sel2vis: bcd_vector (2*n_digits-1 downto 0);
begin
    
    FSM_unit: selected 
        generic map(n_digits => n_digits)
        port map(
            clk   => clk,
            CE    => CE,
            reset => reset, 
            button => button,
            data  => data,
            number => sel2vis
        );

    enP_process: process (reset,clk)
    begin
        if reset='0' then
            en_point<='1';
        elsif rising_edge(clk) then
            if CE='1' then
                en_point <='0';
            else 
                en_point <='1';
            end if;
        end if;
    end process enP_process;
 
    Vis_unit: NormalVisualizer
        generic map(n_displays=> 2* n_digits,
                    clk_freq=> clk_freq)
        port map(
            clk        => clk,
            reset => reset, 
            number     => sel2vis,
           bus_display => bus_display,
           bus_ctrl    => bus_ctrl
        );
end architecture Structural;