library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity OnOff is
generic( nouts: positive := 8);
port(
	CE: in std_logic;
	CLK: in std_logic;
    Buff: out std_logic_vector (nouts-1 downto 0)
);
end entity OnOff;

architecture Dataflow of OnOff is
	signal aux: std_logic_vector (nouts-1 downto 0):= (others=>'0');
begin
	aux<= (not aux) when (rising_edge(CLK) and CE='1') else aux;
    Buff<=aux;
end architecture Dataflow;