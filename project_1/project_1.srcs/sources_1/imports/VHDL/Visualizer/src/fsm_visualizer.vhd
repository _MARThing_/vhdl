library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

USE WORK.COMMON.ALL;

entity fsm_visualizer is
    port (
        reset : in std_logic;
        clk: in std_logic;
        topin: in int0to9 ;-- input from FSM top, to change the state
        SEL  : out std_logic_vector (3 downto 0)
            );
end entity fsm_visualizer;

architecture Behavioral of fsm_visualizer is
    type STATES is (S0, S1, S2, S3);
    signal current_state: STATES;
    signal next_state: STATES;

   
  begin
    state_register: process (RESET, CLK)
    begin
      if reset = '0' then
        current_state <= S0;
      elsif CLK'event and CLK = '1' then
        current_state <= next_state;     
      end if;
    end process;
    
    nextstate_decod: process (topin, current_state)
    begin
      next_state <= current_state;
      case current_state is
        when S0 =>
          if topin = 1 then      
            next_state <= S1;
          end if;
        when S1 =>
          if topin = 2 then      
            next_state <= S2;
          elsif topin = 3 then      
            next_state <= S3;
          end if;
        when others =>
         -- Do nothing
      end case;
    end process;
  
    output_decod: process (current_state)
    begin
      case current_state is
        when S0 =>
          SEL <= "1000";
        when S1 =>
          SEL <= "0100";
        when S2 => 
          SEL <= "0010";
        when S3 => 
          SEL <= "0001";
        when others => 
          --SEL <= SEL;
      end case;
    end process;
end architecture Behavioral;