-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;

use work.common.all;

entity top is
generic(
	n_clave:positive:=4;
    clk_freq:long:=100e6);
port(
	reset: in std_logic;
	clk: in std_logic;
	button: in std_logic;
	switches: in std_logic_vector(3 downto 0);--se usan 4 switches
    ctrl_bus:out std_logic_vector (2*n_clave-1 downto 0);
	display_bus: out std_logic_vector(6 downto 0);
    en_point:out std_logic
    );
end entity;

architecture structural of top is

	component button_treatment is
	generic(
		clk_freq    : INTEGER := 100e6;
		stable_time : INTEGER := 10);
	port(
		reset: in std_logic;
		clk:in std_logic;
	    pushbutton:in std_logic;
	    boton_tratado:out std_logic
	);
	end component;

	component fsm is
	port(
		reset: in std_logic;
		clk: in std_logic;
		boton_tratado: in std_logic;
    	done:in std_logic;--es 1 si se han recibido todos los numeros
    	finished_cuenta:in std_logic;--es 1 si la cuenta ha terminado
    	pass_match:in std_logic;
		start_cuenta:out std_logic;--le pasa al timer para que empiece a contar
    	to_visualizer:out integer--0 nada,1 en juego,2 exito,3 fallo
    	);
	end component;

	component pass_storage is
	generic (N: INTEGER := 4); --numero de digitos de clave
    
    port(
        CE:IN STD_LOGIC;
    	sync_button: IN STD_LOGIC;	--entrada del boton sincronizado
        switch: IN STD_LOGIC_VECTOR (3 DOWNTO 0);  --switches de binario para la placa
        clk: IN STD_LOGIC;
        reset: IN STD_LOGIC;
        pass_completa: OUT bcd_vector (N-1 DOWNTO 0);  --salida de la contraseña completa
        done: OUT STD_LOGIC  --CHIP ENABLE PARA COMPARADOR
	);
	end component;
	
	component comparador_completo is
	generic(n_clave:positive);
  	port(
  		    sec_introducida,sec_original:in bcd_vector (n_clave-1 downto 0);
        	CE_comparador,reset:in std_logic;
        	comparacion:out std_logic
        );
	end component;
    
	component top_countdown_time is
	generic (N: INTEGER := 4);
    port(
        RESET_N: IN std_logic;
        CLK    : IN std_logic;
        CIN    : IN STD_LOGIC;
        COUT   : OUT STD_LOGIC;
        BCD    : OUT bcd_vector (N-1 DOWNTO 0));
	end component;
	
    component Visualizer is
    generic(n_digits:positive := 4;
            clk_freq : long := 100e6);
    port(
        clk   : in std_logic;
        button: in std_logic;
        reset : in std_logic;  
        topin: in int0to9 ;-- input from FSM top, to change the state
        number: in bcd_vector (n_digits-1 downto 0);
        ctrl_bus: out std_logic_vector (2*n_digits-1 downto 0);
        displays_bus: out std_logic_vector(6 downto 0);
        en_point: out std_logic
    );
	end component Visualizer;
    
    constant PASSWORD: bcd_vector:=("0101","1001","0010","0110");
	signal boton_tratado:std_logic;
	signal done_rvnumber:std_logic:='0';--es 1 si se han recibido todos lo numeros
	signal coinciden:std_logic;--es 1 si la clave introducida es correcta
	signal start_game:std_logic:='0';--si es 1, empieza la cuenta atras
	signal time_out:std_logic;--:='0';--es 1 si ya ha terminado la cuenta atras
	signal player_pass:bcd_vector(n_clave-1 downto 0);--secuencia de numeros introducida por el jugador
    signal to_visualizer:integer;--salida de fsm, entrada para visualizador
    signal tiempo:bcd_vector(n_clave-1 downto 0);--salida del timer, entrada para visualizador

begin
	trat_boton:button_treatment  
				generic map(clk_freq  => clk_freq,
							stable_time => 10) -- 10 ms
				port map(reset=>reset,
						 clk=>clk,
					     pushbutton=>button,
					     boton_tratado=>boton_tratado
				);

	fsm1:fsm port map(	reset=>reset,
						clk=>clk,
						boton_tratado=>boton_tratado,
    					done=>done_rvnumber,
    					finished_cuenta=>time_out,
    					pass_match=>coinciden,
						start_cuenta=>start_game,
    					to_visualizer=>to_visualizer
			 );

	reg_clave:pass_storage generic map(N=>n_clave)
				port map(CE=>start_game,
                         sync_button=>boton_tratado,
        				 switch=>switches,
        				 clk=>clk,
        				 reset=>reset,
        				 pass_completa=>player_pass,
        				 done=>done_rvnumber
				);
 

	comparator:comparador_completo generic map(n_clave=>n_clave)
				       port map(sec_introducida=>player_pass,
								sec_original=>PASSWORD,
         						CE_comparador=>done_rvnumber,
								reset=>reset,
         						comparacion=>coinciden
						);

	timer: top_countdown_time generic map(N=>n_clave)
    						  port map(RESET_N=>reset,
        							   CLK=>clk,
        							   CIN=>start_game,
        							   COUT=>time_out,
        							   BCD=>tiempo
        					  );

	visualizador: Visualizer generic map(n_digits=>n_clave,
            							clk_freq=>clk_freq)
    						port map(
    						          clk=>clk,
       		 						 button=>boton_tratado,
        							 reset=>reset, 
        							 topin=>to_visualizer,
        							 number=>tiempo,
        							 ctrl_bus=>ctrl_bus,
        							 displays_bus=>display_bus,
        							 en_point=>en_point
                            );

end architecture;