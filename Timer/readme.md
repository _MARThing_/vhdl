## ENTIDAD COUNTDOWN_TIMER
### CUENTA ATRÁS DE 100 SEGUNDOS EN BCD
- Contamos con un contador BCD modificado, en este caso para contar hacia abajo y comunicarse adecuadamente con posibles expansiones
- Se utiliza esta propiedad en la entidad top, para poder generar cómodamente decenas de segundos, segundos, décimas de segundos y centésimas de segundo
- El bloque top en cuanto llega a 0 envía un flag de aviso
