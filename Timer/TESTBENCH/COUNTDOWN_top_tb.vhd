----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.12.2021 12:32:30
-- Design Name: 
-- Module Name: COUNTDOWN_top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;

entity top_timer_tb is
end;

architecture bench of top_timer_tb is

  component top_timer 
  	GENERIC (N: INTEGER := 4);
      PORT (
        RESET_N : IN  std_logic;
        CLK     : IN  std_logic;
        CIN     : IN  std_logic;
        COUT    : OUT std_logic;
        BCD_V   : OUT bcd_vector (N-1 DOWNTO 0));
      end component;

  signal RESET_N: std_logic;
  signal CLK: std_logic;
  signal CIN: std_logic;
  signal COUT: std_logic;
  signal BCD_V: bcd_vector (3 DOWNTO 0);
  constant clock_period: time := 0.1 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: top_timer generic map ( N       => 4 )
                    port map ( RESET_N => RESET_N,
                               CLK     => CLK,
                               CIN     => CIN,
                               COUT    => COUT,
                               BCD_V   => BCD_V );

  stimulus: process
  begin
  
    -- Put initialisation code here
    RESET_N <= '0';
    wait for 5 ns;
    RESET_N <= '1';
    wait for 5 ns;
    CIN <= '1';
    wait for 4900ns;

    -- Put test bench stimulus code here
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      CLK <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
