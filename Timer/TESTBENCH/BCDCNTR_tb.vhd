----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.12.2021 11:57:13
-- Design Name: 
-- Module Name: BCDCNTR_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
-- Code your testbench here
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;
entity BCDCNTR_tb is
end;

architecture bench of BCDCNTR_tb is

  component BCDCNTR
    port (
      RESET_N : in  std_logic;
      CLK     : in  std_logic;
      CIN     : in  std_logic;
      COUT    : out std_logic;
      BCD_OUT : out bcd
    );
  end component;

  signal RESET_N: std_logic;
  signal CLK: std_logic;
  signal CIN: std_logic;
  signal COUT: std_logic;
  signal BCD_OUT: bcd ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: BCDCNTR port map ( RESET_N => RESET_N,
                          CLK     => CLK,
                          CIN     => CIN,
                          COUT    => COUT,
                          BCD_OUT => BCD_OUT );

  stimulus: process
  begin
  
    -- Put initialisation code here

    RESET_N <= '0';
    wait for 5 ns;
    RESET_N <= '1';
    wait for 5 ns;
    CIN <= '1';
    wait for 200ns;

    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      CLK <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
