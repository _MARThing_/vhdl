library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
entity button_treatment is
    generic(
      clk_freq    : INTEGER := 100e6;
      stable_time : INTEGER := 10);
	port(
	     reset: in std_logic;
	     clk:in std_logic;
	     pushbutton:in std_logic;
	     boton_tratado:out std_logic
	);
end entity;

architecture arch_button_treat of button_treatment is
	component EDGEDTCTR is
    	port (
        	CLK : in std_logic;
        	SYNC_IN : in std_logic;
        	EDGE : out std_logic
    	);
	end component EDGEDTCTR;
	
	component debounce
    GENERIC(
      clk_freq    : INTEGER := 50_000_000;
      stable_time : INTEGER := 10);
    PORT(
      clk     : IN  STD_LOGIC;
      reset_n : IN  STD_LOGIC;
      button  : IN  STD_LOGIC;
      result  : OUT STD_LOGIC);
  end component;
	
	component SYNCHRNZR is
    	port (
        	CLK : in std_logic;
        	ASYNC_IN : in std_logic;
        	SYNC_OUT : out std_logic
    	);
	end component SYNCHRNZR;

	signal sync_button:std_logic;
	signal debounce_button: std_logic;
begin
	inst_sincro: SYNCHRNZR port map(CLK=>clk, ASYNC_IN=>pushbutton, SYNC_OUT=>sync_button);
	inst_edgedtctr: EDGEDTCTR port map(CLK=>clk,SYNC_IN=>debounce_button,EDGE=>boton_tratado);
	inst_debouncer: debounce generic map( clk_freq=>clk_freq, stable_time=>stable_time) -- Default 10 ms and 100 MHz
	                           port map(clk=>clk,reset_n=>reset,button=>sync_button,result=>debounce_button);
end architecture;