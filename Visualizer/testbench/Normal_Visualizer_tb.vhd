library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;
entity NormalVisualizer_tb is
end;

architecture bench of NormalVisualizer_tb is

  component NormalVisualizer
      generic(n_displays:positive := 8;
            clk_freq : long := 100e6 );
      port (
       reset      : in std_logic;
       CLK        : in std_logic;
       number     : in bcd_vector (n_displays-1 downto 0);
       bus_display: out std_logic_vector (6 downto 0) ;
       bus_ctrl   : out std_logic_vector (n_displays-1 downto 0)
    );
  end component;
    constant n_displays: positive :=8;
  signal reset: std_logic;
  signal CLK: std_logic;
  signal number: bcd_vector (n_displays-1 downto 0);
  signal bus_display: std_logic_vector (6 downto 0);
  signal bus_ctrl: std_logic_vector (n_displays-1 downto 0);
  -- Clock period definitions
  constant clock_freq   : long := 100;
  constant clk_period : time := 1 sec /clock_freq;
begin

  -- Insert values for generic parameters !!
  uut: NormalVisualizer generic map ( n_displays  =>n_displays,
                                      clk_freq => clock_freq   )
                           port map ( reset       => reset,
                                      CLK         => CLK,
                                      number      => number,
                                      bus_display => bus_display,
                                      bus_ctrl    => bus_ctrl );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset<='0','1' after clk_period;
    for i in number'range loop
        number(i)<="0110";
    end loop;
    number(0) <= "1000";
    number(1)<= "0010";
    number(2)<= "0100";
    wait for 100*clk_period;
    -- Put test bench stimulus code here
    assert false report "SIMULATION FINISHED" severity failure;  
    wait;
  end process;
     clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;

end;