library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;
entity selected_tb is
end;

architecture bench of selected_tb is

  component selected
      generic(n_digits: positive :=4 );
      port (
          clk   : in std_logic;
          CE    : in std_logic;
          reset : in std_logic;    
          button: in std_logic;
          data  : in bcd_vector(n_digits-1 downto 0);
          number: out bcd_vector(2*n_digits-1 downto 0)
      );
  end component;
   constant n_digits: positive:=4;
  signal clk: std_logic;
  signal CE: std_logic;
  signal reset: std_logic;
  signal button: std_logic;
  signal data: bcd_vector(n_digits-1 downto 0);
  signal number: bcd_vector(2*n_digits-1 downto 0) ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: selected generic map ( n_digits =>  n_digits)
                   port map ( clk      => clk,
                              CE       => CE,
                              reset    => reset,
                              button   => button,
                              data     => data,
                              number   => number );

  stimulus: process
  begin
  
    -- Put initialisation code here
    for i in data'range loop
        data(i)<= to_unsigned(i,4);
    end loop;
    reset <= '0';
    CE <=  '0';
    button<='0';   
    wait for clock_period;
    reset <= '1';
    wait for clock_period*1.05;
    CE<='1' after 4*clock_period;
    wait for 2*clock_period;
    button<='1','0' after clock_period;
    wait for 2*clock_period;
    button<='1','0' after clock_period;
    wait for 2*clock_period;
    button<='1','0' after clock_period;
    wait for 2*clock_period;
    button<='1','0' after clock_period;
    wait for 2*clock_period;
    reset<='0', '1' after clock_period*1.5;
    wait for 3*clock_period;
    -- Put test bench stimulus code here

    stop_the_clock <= true;
    assert false report "SIMULATION FINISHED" severity failure;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;