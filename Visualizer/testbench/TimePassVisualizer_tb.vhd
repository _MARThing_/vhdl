library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;
entity TimePassVisualiazer_tb is
end;

architecture bench of TimePassVisualiazer_tb is

  component TimePassVisualizer
      generic(n_digits: positive :=4;
            clk_freq : long := 100e6 );
      port (
          clk   : in std_logic;
          CE    : in std_logic;
          reset : in std_logic;    
          button: in std_logic;
          data  : in bcd_vector(n_digits-1 downto 0);
          bus_display: out std_logic_vector (6 downto 0) ;
          bus_ctrl   : out std_logic_vector (2*n_digits-1 downto 0);
          en_point   : out std_logic
      );
  end component;
    constant n_digits: positive :=4;
  signal clk: std_logic;
  signal CE: std_logic;
  signal reset: std_logic;
  signal button: std_logic;
  signal data: bcd_vector(n_digits-1 downto 0);
  signal bus_display: std_logic_vector (6 downto 0);
  signal bus_ctrl: std_logic_vector (2*n_digits-1 downto 0);
  signal en_point: std_logic ;

      -- Clock period definitions
  constant test_clock_freq   : long := 1e3;
  constant clk_period : time := 1 sec / test_clock_freq;
begin

  -- Insert values for generic parameters !!
  uut: TimePassVisualizer generic map ( n_digits    => n_digits,
                                        clk_freq=>test_clock_freq )
                              port map ( clk         => clk,
                                         CE          => CE,
                                         reset       => reset,
                                         button      => button,
                                         data        => data,
                                         bus_display => bus_display,
                                         bus_ctrl    => bus_ctrl,
                                         en_point    => en_point );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset<='0','1' after clk_period;
    CE<='0','1' after 2*clk_period;
    button<='0';
    for i in data'range loop
        data(i)<= to_unsigned(i,4);
    end loop;
    wait for 10*clk_period;
    button<='1','0' after 1.05*clk_period;
    wait for 10*clk_period;
    button<='1','0' after 1.05*clk_period;
    wait for 10*clk_period;
    button<='1','0' after 1.05*clk_period;
    wait for 10*clk_period;
    button<='1','0' after 1.05*clk_period;
    wait for 10*clk_period;
    -- Put test bench stimulus code here
    assert false report "SIMULATION FINISHED" severity failure;
    wait;
  end process;

 -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;

end;