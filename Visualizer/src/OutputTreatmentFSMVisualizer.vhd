----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:25:47 11/22/2019 
-- Design Name: 
-- Module Name:    OutputTreatmentFSMVisualizer - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies:  
--      TimePassVisualizer
--      Flash_Visualizer
--      common.vhd
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--  It contains the module of the FSM of the Visualizer and the module of the Output Treatment of the FSM_visualizer
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;

entity OutputTreatmentFSMVisualizer is
    generic( 
        n_numbers: positive:=4;
        clk_freq : long := 100e6
    );
    port (
        clk   : in std_logic;
        button: in std_logic;
        --CE    : in std_logic;
        reset : in std_logic;   
        SEL  : in std_logic_vector (3 downto 0) ;
        number: in bcd_vector (n_numbers-1 downto 0);
        ctrl_bus: out std_logic_vector (2*n_numbers-1 downto 0);
        displays_bus: out std_logic_vector(6 downto 0);
        en_point: out std_logic
    );
end entity OutputTreatmentFSMVisualizer;

architecture Structural of OutputTreatmentFSMVisualizer is
    component TimePassVisualizer is
        generic(n_digits: positive :=4;
                clk_freq : long := 100e6);
        port (
            clk   : in std_logic;
            CE    : in std_logic;
            reset : in std_logic;    
            button: in std_logic;
            data  : in bcd_vector(n_digits-1 downto 0);
            bus_display: out std_logic_vector (6 downto 0) ;
            bus_ctrl   : out std_logic_vector (2*n_digits-1 downto 0);
            en_point   : out std_logic
        );
      end component TimePassVisualizer;
  
      component Flash_Visualizer is
        generic(
          n_digits: positive :=4;
          clk_freq : long := 100e6
        );
        port (
            numberBCD: in bcd;
            clk: in std_logic;
            reset: in std_logic;
            out7seg : out std_logic_vector(6 downto 0);
            outCtrl: out std_logic_vector(2*n_digits -1  downto 0)
        );
      end component Flash_Visualizer;

      constant n_digits: positive := n_numbers;
      signal outFlasherSeg: std_logic_vector(6 downto 0);
      signal outFlasherCtrl: std_logic_vector(2*n_digits -1  downto 0);
      signal outTimPassSeg: std_logic_vector(6 downto 0);
      signal outTimPassCtrl: std_logic_vector(2*n_digits -1  downto 0);
      signal outGoodSeg: std_logic_vector(6 downto 0);
      signal outGoodCtrl: std_logic_vector(2*n_digits -1  downto 0);
      signal outBadSeg: std_logic_vector(6 downto 0);
      signal outBadCtrl: std_logic_vector(2*n_digits -1  downto 0);
begin

    -- MUXER of 7 SEGMENTS
    with SEL select
    displays_bus <= outFlasherSeg when "1000",
                    outTimPassSeg when "0100",
                    outGoodSeg    when "0010",
                    outBadSeg     when others;

    
    -- MUXER of Control of displays
    with SEL select
    ctrl_bus  <=    outFlasherCtrl when "1000",
                    outTimPassCtrl when "0100",
                    outGoodCtrl    when "0010",
                    outBadCtrl     when others;

    -- REST OF UNITS
    flasher_unit: Flash_Visualizer 
        generic map(
          n_digits=> n_numbers,
          clk_freq => clk_freq
        )
        port map(
            numberBCD=>"0000",
            clk=> clk,
            reset => reset,
            out7seg => outFlasherSeg,
            outCtrl => outFlasherCtrl
        );
    
    TimPassVis_unit: TimePassVisualizer 
        generic map(n_digits=> n_numbers,
                    clk_freq => clk_freq)
        port map(
            clk=> clk,
            reset => reset,
            CE    =>  SEL(2),
            button => button,
            data =>  number,
            bus_display => outTimPassSeg,
            bus_ctrl  => outTimPassCtrl,
            en_point  => en_point
        );
        
    GoodEnd_unit: Flash_Visualizer 
    generic map(
      n_digits=> n_numbers,
      clk_freq => clk_freq
    )
    port map(
        numberBCD=>X"F",
        clk=> clk,
        reset => reset,
        out7seg => outGoodSeg,
        outCtrl => outGoodCtrl
    );

    BadEnd_unit: Flash_Visualizer 
    generic map(
      n_digits=> n_numbers,
      clk_freq => clk_freq
    )
    port map(
        numberBCD=>X"E",
        clk=> clk,
        reset => reset,
        out7seg => outBadSeg,
        outCtrl => outBadCtrl
    );



end architecture Structural;
