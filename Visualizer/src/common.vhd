--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package common is

  subtype bcd is unsigned(3 downto 0);
  type bcd_vector is array(integer range <>) of bcd;
  subtype int0to9 is integer range 0 to 9;
  type int0to9_vector is array(integer range <>) of int0to9;
  subtype long is integer range 0 to 10e7;
  type long_vector is array(integer range <>) of long;
  type std_logic_matrix is array (integer range <>) of std_logic_vector;
end common;

package body common is
end common;
