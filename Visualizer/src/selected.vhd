library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;

entity selected is
    generic(n_digits: positive :=4 );
    port (
        clk   : in std_logic;
        CE    : in std_logic;
        reset : in std_logic;    
        button: in std_logic;
        data  : in bcd_vector(n_digits-1 downto 0);
        number: out bcd_vector(2*n_digits-1 downto 0)
    );
end entity selected;

architecture Dataflow of selected is

    type STATES is (S0,S1,S2,S3,S4);
    signal current_state: STATES;
    signal next_state: STATES;
    signal aux1: bcd_vector(n_digits-1 downto 0);
    signal aux2: bcd_vector(2*n_digits-1 downto 0);
begin
    
    
    state_register: process (reset, clk)
    begin
      if reset = '0' then
        current_state <= S0;
      elsif (rising_edge(clk) and CE='1') then
        current_state <= next_state; 
      end if;
    end process;

    nextstate_decod: process (button, current_state)
    begin
      next_state <= current_state;
      case current_state is
        when S0 =>
          if button = '1' then      
            next_state <= S1;
          end if;
        when S1 =>
          if button = '1' then      
            next_state <= S2;
          end if;
        when S2 => 
          if button = '1' then      
            next_state <= S3;
          end if;
        when S3 =>
            if button='1' then
                next_state <= S4;
            end if;
        when others=> 
            if button='1' then
                next_state <= S0;
            end if;
      end case;
    end process;
    a:FOR i in aux2'range generate
        b:IF i<= n_digits-1 generate
            aux2(i)<=aux1(i);
        end generate b;
        c:IF i> n_digits-1 generate
            aux2(i)<=data(i-n_digits);
        end generate c;
    end generate a;
    number<=aux2;
    output_decod: process (current_state)
        constant Onv: bcd:= "1001"; -- Just something to show
        constant Offv: bcd:= "0000"; -- Just something to show
    begin
      case current_state is
        when S0 =>
          for i in 0 to n_digits-1 loop
            aux1(i)<=Onv;
          end loop; 
        when S1 =>
          aux1(n_digits-1)<= Offv;
        when S2 => 
          aux1(n_digits-2)<= Offv;
        when S3 => 
           aux1(n_digits-3)<= Offv;
        when others => 
           aux1(n_digits-4)<= Offv;
      end case;
    end process;
end architecture Dataflow;
