--------------------------------------------------
--              Flash Visualizer                --
--  It flash the number passed on a group of    --
--  7 segments displays, turning them ON and    --
--  OFF every 0.2 seconds (5 Hz)                --
--------------------------------------------------
--  Author: Martin Augusto Reigadas Teran       --
--  The entities TIMER and BIG2SEG was given    --
--  and it can be found in the moodle of the    --
--  subject "Sistemas ElectrÃ³nicos Digitales"   --
--------------------------------------------------


library  IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;

entity Flash_Visualizer is
    generic(
        n_digits: positive :=4;
        clk_freq: long:= 100e6 
    );
    port (
        numberBCD: in bcd;
        clk: in std_logic;
        reset: in std_logic;
        out7seg : out std_logic_vector(6 downto 0);
        outCtrl: out std_logic_vector(2*n_digits -1  downto 0)
    );
end entity Flash_Visualizer;


architecture Structural of Flash_Visualizer is
    
    -- Timer 
    component TIMER is
        generic (
            MODULO  : long := 416667 -- Long subtype, is a natural on a range of 0 to 10e10
        );
        port ( 
            RESET_N : in   std_logic;
            CLK     : in   std_logic;
            STROBE  : out  std_logic
        );
    end component TIMER;
    
    -- From a BCD to 7 segments Displays
    component BIN2SEG is
        port (
          BCDVAL   : in  bcd;
          SEGMENTS : out std_logic_vector(6 downto 0)
        );
    end component BIN2SEG;

    -- Toogle the outputs with every rising edge of the CE
    component OnOff is
        generic( nouts: positive := 8);
        port(
            CE: in std_logic;
            CLK: in std_logic;
            Buff: out std_logic_vector (nouts-1 downto 0)
        );
    end component OnOff;

    
    signal Tim2OnOff: std_logic; -- Signal between the timer and the OnOff

begin
    
    timer_unit: TIMER 
        generic map(
            MODULO  => (clk_freq/(5))
        )
        port map( 
            RESET_N => reset,
            CLK     => clk,
            STROBE  => Tim2OnOff
        );
    
    Big2Seg_unit : BIN2SEG
        port map(
          BCDVAL   => numberBCD,
          SEGMENTS => out7seg
        );
    
    OnOff_unit : OnOff 
        generic map( nouts => n_digits*2)
        port map(
            CE  => Tim2OnOff,
            CLK => clk,
            Buff => outCtrl
        );
    
end architecture Structural;