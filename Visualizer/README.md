# Documentation of entity Visualizer

## This folder contains all the code of the Visualizer entity and most of the testbenchs for the more important entities.

The visualizer is a FSM (Finit State Machine) that shifts between 4 states:

- The first State S0 flash a cero with a 5 Hz frequency to indicate that the program has started

- The second state S1 it's another FSM, that shows the number that is passed and every time the button is pressed it turns to cero one of the four nines that are shown in the rest of the displays.

- The Third state S2 flashes a F with a 5 Hz frequency to show that the Password inserted was correct.

- The forth state S3 flashes a E with a 5 Hz frequency to show that the password inserted was incorrect.



The visualizer divides in two main modules:

- fsm_visualizer: module that permutes between the differents states following the Finit State Machine
    
    
- OutputTreatmentFSMVisualizer: module that makes sure the output is the right one between all of the states.


### FSM_VISUALIZER

This module permutes between each of the four states of the main FSM.

It starts in the S0 state that sends the value of "1000" to the OutputTreatmentFSMVisualizer (OTFV), so it selects the corresponding output. When the main FSM sends the value of 1 in the variable topin to indicate to change to the state S1.

In the S1 state it sends "0100" to activate the Second module of the OTFV. If the topin changes to 2 the FSM changes to the state S2 and if it receives a 3 it changes to S3.

In the S2 state it sends the "0010" to the OTFV, so it shows the "F" on the 7-seg displays, to indicate a successful password.

In the S3 state it sends "0001" to the OTFV so it shows the "E" on the 7-seg displays to indicate a unsuccessful password or the user took too much time.

### OutputTreatmentFSMVisualizer

This module treats the output of the visualizer depending on the state that the FSM of the visualizer is at the moment.

It is composed by two for instances and a concurrence code to select the output. Three of those instances are part of a module called Flash_Visualizer and the third one is called TimPassVisualizer.

#### Flash_Visualizer

This module takes the one input and shows it in all the diplays selected with a frequency of 5 Hz.

#### TimPassVisualizer

This module shows the numbers pass by the user and 4 numbers to indicate how many times the button has been pressed.

This module it's a structure of 2 modules:

- The Select module is a FSM that permutes between 5 states, each one with an amount of zeros and other numbers, every time the button is pressed it goes to the next state, from S0 to S1, S1 to S2 and so on.

This module also combines the data recieve with this number.

The first state S0 doesn't have any zero, passing the entire 4 digit number, the second state S1 pass only one zero, the one on the left.

The third state S2 pass two zeros the ones on the left.

The fourth state S3 shows three zeros and the fifth state only pass zeros.

- The NormalVisualizer takes the number passed and shows it in the 7-segment displays.
