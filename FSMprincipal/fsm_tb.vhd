library ieee;
use ieee.std_logic_1164.all;
--use work.common.all;
entity tb_fsm is
end tb_fsm;

architecture tb of tb_fsm is

    component fsm
        port (reset           : in std_logic;
              clk             : in std_logic;
              boton_tratado   : in std_logic;
              --switches        : in std_logic_vector (3 downto 0);
              done            : in std_logic;
              finished_cuenta : in std_logic;
              pass_match:in std_logic;
              start_cuenta    : out std_logic;
              to_visualizer   : out integer);
    end component;

    signal reset           : std_logic;
    signal clk             : std_logic;
    signal boton_tratado   : std_logic;
   -- signal switches        : std_logic_vector (3 downto 0);
    signal done            : std_logic;
    signal finished_cuenta : std_logic;
    signal pass_match      : std_logic;
    signal start_cuenta    : std_logic;
    signal to_visualizer   : integer;

    constant TbPeriod : time := 1000 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : fsm
    port map (reset           => reset,
              clk             => clk,
              boton_tratado   => boton_tratado,
             -- switches        => switches,
              done            => done,
              finished_cuenta => finished_cuenta,
              pass_match      => pass_match,
              start_cuenta    => start_cuenta,
              to_visualizer   => to_visualizer);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        boton_tratado <= '0';
       -- switches <= (others => '0');
       -- done <= '0';
       -- finished_cuenta <= '0';
		--pass_match<='0';
        
        -- Reset generation
        -- EDIT: Check that reset is really your reset signal
        reset <= '0';
        wait for 100 ns;
        reset <= '1';
        wait for 100 ns;
        boton_tratado <= '1';
        wait for 1000 ns;
        boton_tratado <= '0';
        wait for 2000 ns;
        done<='1';
        wait for 500 ns;
        pass_match<='1';
        wait for 4000 ns;
        reset<='0';
        done<='0';
        pass_match<='0';
        wait for 2000 ns;
        reset <= '1';
        wait for 500 ns;
		boton_tratado <= '1';
        wait for 1000 ns;
        boton_tratado <= '0';
        wait for 6000 ns;
        finished_cuenta<='1';
        pass_match<='0';
        wait for 2000 ns;
        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;