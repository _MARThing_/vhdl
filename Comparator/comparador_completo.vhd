

-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity comparador_completo is
  generic(n_clave:positive:=4);
  port(
  	sec_introducida,sec_original:in bcd_vector (n_clave-1 downto 0);
        CE_comparador,reset:in std_logic;
        comparacion:out std_logic
        );
end entity;

architecture arch_comparador of comparador_completo is


begin

process(CE_comparador,reset)
variable comparaciones:std_logic_vector(n_clave-1 downto 0);
variable inter_comparacion:std_logic:='0';
begin
   if (reset='0') then
     inter_comparacion:='0';
     elsif (CE_comparador='1') then
      for i in 0 to (n_clave-1) loop
        if sec_introducida(i)=sec_original(i) then
            comparaciones(i) := '1';
        else 
            comparaciones(i) := '0';
        end if;
      end loop;
      inter_comparacion:='1';
      for i in 0 to (n_clave-1) loop
      	inter_comparacion:=(inter_comparacion and comparaciones(i));
      end loop;
     end if;
  comparacion<=inter_comparacion;
end process;
end architecture;
