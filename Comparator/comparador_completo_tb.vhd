library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity tb_comparador_completo is
end tb_comparador_completo;

architecture tb of tb_comparador_completo is

    component comparador_completo
    	generic(n_clave:positive:=4);
        port (sec_introducida : in int0to9_vector (n_clave-1 downto 0);
              sec_original    : in int0to9_vector (n_clave-1 downto 0);
              CE_comparador   : in std_logic;
              reset   : in std_logic;
              comparacion     : out std_logic);
    end component;
	constant n_clave: positive:=4;
    signal sec_introducida :  int0to9_vector (n_clave-1 downto 0);
    signal sec_original    : int0to9_vector (n_clave-1 downto 0);
    signal CE_comparador   : std_logic;
    signal comparacion     : std_logic;
	signal reset           : std_logic;
begin

    dut : comparador_completo
    generic map(n_clave=>4)
    port map (sec_introducida => sec_introducida,
              sec_original    => sec_original,
              CE_comparador   => CE_comparador,
              comparacion     => comparacion,
              reset           =>       reset);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        for i in sec_introducida'range loop
          sec_introducida(i) <= (i mod 9);
          sec_original(i) <= (i mod 9) ;     
        end loop;
        CE_comparador <= '0';
        reset<='0','1' after  5 ns;
        --sec_introducida(2)<= 7;
		wait for 10 ns;
        -- EDIT Add stimuli here
		CE_comparador <='1';
        wait for 10 ns;
        CE_comparador <= '0';
 		wait for 1 ns;
        sec_introducida(2)<= 2;
        wait for 1 ns;
        CE_comparador <='1';
        reset <='0' after 5 ns;
        wait for 7 ns;
   		CE_comparador <= '0' after 0.5 ns;
        reset<='1';
 		wait for 1 ns;
        sec_introducida(2)<= 2;
        wait for 1 ns;
        CE_comparador <= '1';
 		wait for 1 ns;
        sec_introducida(2)<= 2;
        wait for 1 ns;
        CE_comparador <='0';
        wait for 10 ns;
        CE_comparador <= '1';
 		wait for 1 ns;
        wait;
    end process;
    
   end tb;