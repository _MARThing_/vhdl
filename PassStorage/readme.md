
## ENTIDAD PASS_STORAGE

### ALMACENAMIENTO DE LA CONTRASEÑA CON SWITCHES Y BOTÓN
- El objetivo es ir introduciendo la contraseña de los displays
- La dificultad es transportar esta contraseña a binario e introducirla con los switches de la placa
- Iremos introduciendo la contraseña en un vector de uno en uno pulsando el botón cada vez
- Este vector cuando se halla completado (4 numeros) se pasará al comparador

