----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.12.2021 11:51:48
-- Design Name: 
-- Module Name: pass_storage_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.common.all;

entity pass_storage_tb is
end;

architecture bench of pass_storage_tb is

  component pass_storage
  	GENERIC (N: INTEGER := 4);
      PORT(
      CE: IN STD_LOGIC;
      	sync_button: IN STD_LOGIC;
          switch: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
          clk: IN STD_LOGIC;
          reset: IN STD_LOGIC;
          pass_completa: OUT bcd_vector (N-1 DOWNTO 0);
          done: OUT STD_LOGIC
          );
  end component;
  constant N: INTEGER := 4;
  signal CE: STD_LOGIC;
  signal sync_button: STD_LOGIC;
  signal switch: STD_LOGIC_VECTOR (3 DOWNTO 0);
  signal clk: STD_LOGIC;
  signal reset: STD_LOGIC;
  signal pass_completa: bcd_vector (N-1 DOWNTO 0);
  signal done: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: pass_storage generic map ( N             => 4 )
                       port map ( CE			=> CE,
                       			  sync_button   => sync_button,
                                  switch        => switch,
                                  clk           => clk,
                                  reset         => reset,
                                  pass_completa => pass_completa,
                                  done          => done );

  stimulus: process
  begin
  
    -- Put initialisation code here

    reset <= '0';
    CE<='0';
    wait for 5 ns;
    reset <= '1';
    wait for 5 ns;
    sync_button <= '1';
    wait for 3 ns;
    sync_button <= '0';
    wait for 4 ns;
    switch <= "0101";
    wait for 9 ns;
    sync_button <= '1';
    wait for 6 ns;
    sync_button <= '0';
    wait for 4 ns;
    CE<='1';
    switch <= "0011";
    wait for 6 ns;
    sync_button <= '1';
    wait for 7 ns;
    reset <= '0';
    wait for 5 ns;
    reset <= '1';
    switch <= "1010";
    wait for 8 ns;
    sync_button <= '0';
    wait for 5 ns;
    switch <= "0010";
    sync_button <= '1';
    wait for 10 ns;
    sync_button <= '0';
    wait for 8 ns;
    switch <= "0110";
    wait for 3 ns;
    sync_button <= '1';
    switch <= "0011";
    wait for 8 ns;
    sync_button <= '0';
    wait for 8 ns;
    sync_button <= '1';
    wait for 100 ns;
    
    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;