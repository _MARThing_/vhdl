----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.12.2021 13:13:06
-- Design Name: 
-- Module Name: PASS_STORAGE - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use IEEE.NUMERIC_STD.all;
use work.common.all;

ENTITY pass_storage is
	GENERIC (N: INTEGER := 4); --NUMERO DE SWITCHES PARA CONTRASEÑA, 4 EN TOTAL
    
    PORT(
    	CE: IN STD_LOGIC;--cuando es 1, empieza a registar las contraseñas
    	sync_button: IN STD_LOGIC;	--entrada del botón sincronizado
        switch: IN STD_LOGIC_VECTOR (3 DOWNTO 0);  --switches de binario para la placa
        clk: IN STD_LOGIC;
        reset: IN STD_LOGIC;
         
        pass_completa: OUT bcd_vector (N-1 DOWNTO 0);  --salida de la contraseña completa
        done: OUT STD_LOGIC  --CHIP ENABLE PARA COMPARADOR
        );
END pass_storage;

ARCHITECTURE behavioral OF pass_storage IS
BEGIN
	PROCESS(reset, clk)
   		 VARIABLE i: INTEGER RANGE N-1 DOWNTO -1 := N-1; --VARIABLE PARA RECORRER EL VECTOR
    BEGIN
    
      IF reset = '0' THEN
        FOR j IN pass_completa'range LOOP
          pass_completa(j) <= (OTHERS => '0'); --RESETEAMOS EL CODIGO
        END LOOP;
          done <= '0';
          i:=N-1;

      ELSIF RISING_EDGE(clk) AND sync_button = '1' AND CE='1' THEN -- CONDICIÓN, FLANCO DE RELOJ, BOTÓN Y CE ACTIVO
            IF i >= 0 THEN 
                pass_completa(i) <= unsigned(switch);
                i := i-1;
                done <= '0';
            ELSIF i = -1 THEN --CUANDO FLANCO, BOTÓN E i = -1 ESTÁ HECHO
                	done <= '1';
            END IF;
       END IF;
     END PROCESS;
END behavioral;